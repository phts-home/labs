// ServerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Server.h"
#include "ServerDlg.h"
#include "Winsock2.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CServerDlg dialog




CServerDlg::CServerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CServerDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CServerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_edit);
}

BEGIN_MESSAGE_MAP(CServerDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDCANCEL, &CServerDlg::OnBnClickedCancel)
	ON_BN_CLICKED(IDC_BUTTON1, &CServerDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// CServerDlg message handlers

BOOL CServerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

void CServerDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CServerDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CServerDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CServerDlg::OnBnClickedCancel()
{
	WSACleanup();
	// TODO: Add your control notification handler code here
	OnCancel();
}
/*
void CServerDlg::appendDebug(const char* text) {
	CString str;
	m_edit.GetWindowText(str);
	str.Append(text);
	m_edit.SetWindowText(_T(str));
}*/

void CServerDlg::OnBnClickedButton1() {
	//CString str;

	WSADATA WsaData;
	int err = WSAStartup (0x0101, &WsaData);
	if (err == SOCKET_ERROR) {
		m_edit.SetWindowText(_T("WSAStartup failed\n"));
   		printf("WSAStartup() failed: %ld\n", GetLastError());
		return;
	}
	m_edit.SetWindowText(_T("WSAStartup done\n"));
	//appendDebug("WSAStartup done\n");
	
	//m_edit.SetWindowText(_T("WSAStartup done\n"));

	SOCKET s = -1;
	if ( (s = socket(AF_INET,SOCK_STREAM,0)) == -1) {
		m_edit.SetWindowText(_T("socket failed\n"));
		printf("socket failed: %ld\n", GetLastError());
		return;
	}
	m_edit.SetWindowText(_T("socket done\n"));
	/*m_edit.GetWindowText(str);
	str.Append(_T("socket done\n"));
	m_edit.SetWindowText(_T(str));*/
	
	SOCKADDR_IN sin;
	sin.sin_family = AF_INET;
	sin.sin_port = htons(12200);
	sin.sin_addr.s_addr = INADDR_ANY;
	
	if (bind(s, (sockaddr*)&sin, sizeof(sin)) == -1) {
		m_edit.SetWindowText(_T("bind failed\n"));
		printf("bind failed: %ld\n", GetLastError());
		return;
	}
	m_edit.SetWindowText(_T("bind done\n"));

	if (listen(s,5) == -1) {
		m_edit.SetWindowText(_T("listen failed\n"));
		printf("listen failed: %ld\n", GetLastError());
		return;
	}
	m_edit.SetWindowText(_T("listen done\n"));
	
	SOCKET s1 = accept(s, NULL, NULL);
	if (s1 == -1) {
		m_edit.SetWindowText(_T("accept failed\n"));
		printf("accept failed: %ld\n", GetLastError());
		return;
	}
	m_edit.SetWindowText(_T("accept done\n"));
	

	char RecvBuffer[4000];
	CString str1;
	int bytesRecv = 0;
	while(bytesRecv != SOCKET_ERROR) {
		bytesRecv = recv(s1, RecvBuffer, 4000, 0);
		if (bytesRecv  > 0 ){
			str1 = (CString(RecvBuffer));
		}
		str1 = str1.Mid(0,bytesRecv);
		m_edit.SetWindowText(str1);
		if ( bytesRecv < 4000 || bytesRecv == WSAECONNRESET ) {
			break;
		}
		
		//send(s1, MsgText, sizeof(MsgText), MSG_DONTROUTE);

	}
	double res = eq(str1);
	CString strres;
	strres.Format(_T("%lf"),res);

	m_edit.SetWindowText(strres);

	send(s1, (char*)strres.GetBuffer(), strres.GetLength(), 0);
	
	
	
}

double CServerDlg::eq(CString str) {
	if (str.Compare(_T("")) == 0) {
		return 0;
	}
	char c;
	double res = 0.0;
	double tmp = 0.0;
	int prevPos = 0;
	int numlen = 0;
	int sign = 0;
	CString tok;
	for (int i = 0; i < str.GetLength(); i++) {
		c = (char)str[i];
		char *s = new char[15];
		switch (c) {
			case '+':
				tok = str.Mid(prevPos, numlen);
				strcpy(s,(char*)tok.GetBuffer());
				tmp = atof(s);
				switch (sign) {
					case 0:
						res = tmp;
						break;
					case 1:
						res += tmp;
						break;
					case 2:
						res -= tmp;
						break;
					case 3:
						res *= tmp;
						break;
					case 4:
						res /= tmp;
						break;
				}
				prevPos = i+1;
				numlen = 0;
				sign = 1;
				break;
			case '-':
				tok = str.Mid(prevPos, numlen);
				strcpy(s,(char*)tok.GetBuffer());
				tmp = atof(s);
				switch (sign) {
					case 0:
						res = tmp;
						break;
					case 1:
						res += tmp;
						break;
					case 2:
						res -= tmp;
						break;
					case 3:
						res *= tmp;
						break;
					case 4:
						res /= tmp;
						break;
				}
				prevPos = i+1;
				numlen = 0;
				sign = 2;
				break;
			case '*':
				tok = str.Mid(prevPos, numlen);
				strcpy(s,(char*)tok.GetBuffer());
				tmp = atof(s);
				switch (sign) {
					case 0:
						res = tmp;
						break;
					case 1:
						res += tmp;
						break;
					case 2:
						res -= tmp;
						break;
					case 3:
						res *= tmp;
						break;
					case 4:
						res /= tmp;
						break;
				}
				prevPos = i+1;
				numlen = 0;
				sign = 3;
				break;
			case '/':
				tok = str.Mid(prevPos, numlen);
				strcpy(s,(char*)tok.GetBuffer());
				tmp = atof(s);
				switch (sign) {
					case 0:
						res = tmp;
						break;
					case 1:
						res += tmp;
						break;
					case 2:
						res -= tmp;
						break;
					case 3:
						res *= tmp;
						break;
					case 4:
						res /= tmp;
						break;
				}
				prevPos = i+1;
				numlen = 0;
				sign = 4;
				break;
			default:
				numlen++;
				break;
		}
		if (i == str.GetLength()-1) {
			tok = str.Mid(prevPos, numlen);
			tmp = atof((char*)tok.GetBuffer());
			switch (sign) {
				case 0:
					res = tmp;
					break;
				case 1:
					res += tmp;
					break;
				case 2:
					res -= tmp;
					break;
				case 3:
					res *= tmp;
					break;
				case 4:
					res /= tmp;
					break;
			}
			prevPos = i+1;
			numlen = 0;
		}
		delete s;
	}
	return res;
}

