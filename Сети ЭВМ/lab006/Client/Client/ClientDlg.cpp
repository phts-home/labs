// ClientDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Client.h"
#include "ClientDlg.h"
#include "Winsock2.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CClientDlg dialog




CClientDlg::CClientDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CClientDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CClientDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_edit);
	DDX_Control(pDX, IDC_EDIT2, m_eq);
}

BEGIN_MESSAGE_MAP(CClientDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, &CClientDlg::OnBnClickedButton1)
END_MESSAGE_MAP()


// CClientDlg message handlers

BOOL CClientDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CClientDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CClientDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CClientDlg::OnBnClickedButton1()
{
	WSADATA WsaData;
	int err = WSAStartup (0x0101, &WsaData);
	if (err == SOCKET_ERROR) {
		m_edit.SetWindowText(_T("WSAStartup() failed\n"));
   		printf ("WSAStartup() failed: %ld\n", GetLastError());
		return;
	}
	m_edit.SetWindowText(_T("WSAStartup() done\n"));

	SOCKET s = -1;
	if ( (s = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP)) == -1) {
		m_edit.SetWindowText(_T("socket failed\n"));
		printf ("socket failed: %ld\n", GetLastError());
		return;
	}
	m_edit.SetWindowText(_T("socket done\n"));
	
	SOCKADDR_IN anAddr;
	anAddr.sin_family = AF_INET;
	anAddr.sin_port = htons(12200);
	anAddr.sin_addr.S_un.S_addr = inet_addr("127.0.0.1");

	if (connect(s, (SOCKADDR*)&anAddr, sizeof(anAddr)) == -1) {
		m_edit.SetWindowText(_T("connect failed\n"));
		printf ("connect failed: %ld\n", GetLastError());
		return;
	}
	m_edit.SetWindowText(_T("connect done\n"));

	
	//char RecvBuffer[1];
	/*while( recv(s, RecvBuffer, sizeof(RecvBuffer), 0) != SOCKET_ERROR) {
		m_edit.SetWindowText(_T("sent"));
		send(s, MsgText, sizeof(MsgText), MSG_DONTROUTE);
		printf ("sent: %c\n", RecvBuffer[1]);
	}*/
	/*char MsgText[6];
	strcpy_s(MsgText, "12345");*/

	//char buf[126] = "2+33+1";

	CString strres;
	m_eq.GetWindowText(strres);

	int r = send(s, (char*)strres.GetBuffer(), strres.GetLength(), 0);
//	int r = send(s, buf, strlen(buf), 0);

	m_edit.SetWindowText(_T("sent\n"));
	
	char buf[126] = "";
	int rc = 0;
	while (1) {
		rc = recv(s, buf, sizeof(buf), 0);
		if (rc == -1) {
			WSACleanup();
			return;
		}
		if (rc > 0) {
			m_edit.SetWindowText((CString)buf);
			break;
		}
	}
	
	
	
	WSACleanup();

	//m_edit.SetWindowText(_T("WSACleanup()\n"));

}
