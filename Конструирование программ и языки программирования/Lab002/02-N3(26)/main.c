#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{
    unsigned char ca, cb;
    int res, a, b;
    printf("Enter 1st number (unsigned char) [0..9]  ");
    scanf("%c",&ca);
    fflush(stdin);
    printf("Enter 2nd number (unsigned char) [0..9]  ");
    scanf("%c",&cb);
    res = (ca-48) + (cb-48) < 10;
    printf("Result  %d\n",res);
    return 0;
}
