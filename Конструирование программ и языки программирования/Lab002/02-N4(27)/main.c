#include <stdio.h>


int main(){
	double b, c, d, min, mid, max, res;
  printf("Enter b (double)  ");
  scanf("%lf",&b);
  printf("Enter c (double)  ");
  scanf("%lf",&c);
  printf("Enter d (double)  ");
  scanf("%lf",&d);

  max = (10>d)?((c>10)?c:10):((c>d)?c:d);
	min = (15<d)?((c<15)?c:15):((c>d)?d:c);
	mid = (b>c)?((d>b)?b:(c>d)?c:d):((d>c)?c:(b>d)?b:d);

	res = max * min / mid;

  printf("max(10, %.2lf, %.2lf) = %.2lf\n",c,d,max);
  printf("min(15, %.2lf, %.2lf) = %.2lf\n",c,d,min);
  printf("mid(%.2lf, %.2lf, %.2lf) = %.2lf\n",b,c,d,mid);
	printf("max * min / mid = %.2lf\n",res);
	return 0;
}
