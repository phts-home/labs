#include <stdio.h>


int main(int argc, char *argv[])
{
    unsigned int A, res1, res2, res3, res4, res5, res6;
    printf("Enter the unsigned integer number  ");
    scanf("%u",&A);

    res1 = A * A;       //2
    res2 = res1 * res1; //4
    res3 = res2 * res2; //8
    res4 = res3 * res3; //16
    res5 = res4 * res2; //20
    res6 = res5 * res3; //28

    printf("%u (dec), %x (hex)\n",res6,res6);
    return 0;
}

