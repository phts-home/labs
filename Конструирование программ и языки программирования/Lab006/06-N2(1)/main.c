#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
	//Init {size}
	unsigned size;
	printf("> Set Database Size (unsigned)\n> ");
	scanf("%d",&size);
	printf("\n");
	if(size<1){
		printf("> Error. Minimum Database Size Is 1\n");
		return 0;
	}

	//Init {ar}
	char line[256], word[256], st[50];
	char *c = NULL;
	int k, kt, w;
	typedef struct {unsigned sender; unsigned receiver; char text[50]; char date[11];} tMess;
	tMess ar[size];
	int count = 0;
/*
	int count = 6;
	ar[0].sender = 123;
	ar[0].receiver = 222;
	strcpy(ar[0].text,"text1");
	strcpy(ar[0].date,"33.44.2007");
	ar[1].sender = 333;
	ar[1].receiver = 444;
	strcpy(ar[1].text,"text2");
	strcpy(ar[1].date,"30.44.2007");
	ar[2].sender = 555;
	ar[2].receiver = 666;
	strcpy(ar[2].text,"text3");
	strcpy(ar[2].date,"33.40.2007");
	ar[3].sender = 700;
	ar[3].receiver = 720;
	strcpy(ar[3].text,"text4");
	strcpy(ar[3].date,"33.44.2006");
	ar[4].sender = 800;
	ar[4].receiver = 820;
	strcpy(ar[4].text,"text5");
	strcpy(ar[4].date,"33.42.2007");
	ar[5].sender = 900;
	ar[5].receiver = 920;
	strcpy(ar[5].text,"text6");
	strcpy(ar[5].date,"33.40.2000");
*/
	//Main Cycle
	tMess tmpItem;
	int ch, canExit = 0, ind;
	int iVal, mode;
	char cVal[50];
	int d1, d2, m1, m2, y1, y2;
	char tmpStr[4], tmpWord[11];
	int foundCount;
	while(canExit == 0){
		printf("\n");
		printf("------ Menu ------\n");
		printf("1 - Add New Record\n");
		printf("2 - Delete Record\n");
		printf("3 - Edit Record\n");
		printf("4 - Sort Records\n");
		printf("5 - Search\n");
		printf("6 - Print\n");
		printf("0 - Exit\n");
		printf("------------------\n");
		printf("> Your Choice   ");
		scanf("%d",&ch);
		printf("\n");
		switch(ch){
			case 1:{												/*** ADD NEW RECORD ***/

				printf("> Add New Record\n");
				if(count == size){printf("> Error. Can't Add New Record\n");break;}
				printf("> Format: AAA BBB \"Message\" DD.MM.YYYY\n");
				fflush(stdin);
				gets(line);
				fflush(stdin);
				count++;
				k = 0;
				while(line[k]!='"') k++;
				w = 0;
				k++;
				kt = k;
				while(line[kt]!='"'){
					kt++;
					w++;
				}
				if(w>50){
					printf("\n> Error. Maximum Size Of Text Is 50 Symbols\n");
					break;
				}
				strcpy(st,"");
				for(int r=0;r<w;r++) st[r] = line[k+r];
				st[w] = 0;
				strcpy(ar[count-1].text,st);

				k = kt+1;
				while(line[k]==' ') k++;

				strcpy(st,"");
				for(int r=0;r<10;r++) st[r] = line[k+r];
				st[10] = 0;
				strcpy(ar[count-1].date,st);

				c = strtok(line," ");
				strcpy(word,c);
				ar[count-1].sender = atoi(word);

				c = strtok(NULL," ");
				strcpy(word,c);
				ar[count-1].receiver = atoi(word);
				break;
			}

			case 2:{												/*** DELETE RECORD ***/

				printf("> Delete Record\n");
				printf("> Enter Index Of Record\n");
				printf("> ");
				scanf("%d",&ind);
				if(ind>count-1){printf("> Error. No Records With That Index\n");break;}
				for(int i=ind;i<count-1;i++) ar[i] = ar[i+1];
				count--;
				break;
			}

			case 3:{												/*** EDIT RECORD ***/

				printf("> Edit Record\n");
				printf("> Enter Index\n> ");
				scanf("%d",&ind);
				if(ind>count-1){printf("> Error. No Records With That Index\n");break;}
				printf("> Old Record: %d %d \"%s\" %s\n",ar[ind].sender,ar[ind].receiver,ar[ind].text,ar[ind].date);
				printf("> Format: AAA BBB \"Message\" DD.MM.YYYY\n");
				fflush(stdin);
				gets(line);
				fflush(stdin);
				k = 0;
				while(line[k]!='"') k++;
				w = 0;
				k++;
				kt = k;
				while(line[kt]!='"'){
					kt++;
					w++;
				}
				if(w>50){
					printf("\n> Error. Maximum Size Of Text Is 50 Symbols\n");
					break;
				}

				strcpy(st,"");
				for(int r=0;r<w;r++) st[r] = line[k+r];
				st[w] = 0;
				strcpy(ar[ind].text,st);

				k = kt+1;
				while(line[k]==' ') k++;

				strcpy(st,"");
				for(int r=0;r<10;r++) st[r] = line[k+r];
				st[10] = 0;
				strcpy(ar[ind].date,st);

				c = strtok(line," ");
				strcpy(word,c);
				ar[ind].sender = atoi(word);

				c = strtok(NULL," ");
				strcpy(word,c);
				ar[ind].receiver = atoi(word);
				break;
			}

			case 4:{											/*** SORT RECORDS ***/

				printf("> Sort Records\n");
				printf("> Select Row [1-Sender; 2-Receiver; 3-Text; 4-Date]\n> ");
				scanf("%d",&mode);
				if((mode > 4)||(mode < 1)){printf("> Error\n");break;}
				printf("> Set Direction [1-Direct | 0-Indirect]\n> ");
				scanf("%d",&iVal);
				printf("> Sorting...\n");

				if(mode == 1){
					for(int i=0;i<count-1;i++)
						for(int j=i+1;j<count;j++){
							if(iVal){
								if(ar[i].sender > ar[j].sender){
									tmpItem = ar[i];
									ar[i] = ar[j];
									ar[j] = tmpItem;
								}
							}else{
								if(ar[i].sender < ar[j].sender){
								tmpItem = ar[i];
								ar[i] = ar[j];
								ar[j] = tmpItem;
								}
							}
						}
					printf("> Done\n");
					break;
				}

				if(mode == 2){
					for(int i=0;i<count-1;i++)
						for(int j=i+1;j<count;j++){
							if(iVal){
								if(ar[i].receiver > ar[j].receiver){
									tmpItem = ar[i];
									ar[i] = ar[j];
									ar[j] = tmpItem;
								}
							}else{
								if(ar[i].receiver < ar[j].receiver){
									tmpItem = ar[i];
									ar[i] = ar[j];
									ar[j] = tmpItem;
								}
							}
						}
					printf("> Done\n");
					break;
				}

				if(mode == 3){
					for(int i=0;i<count-1;i++)
						for(int j=i+1;j<count;j++){
							if(iVal){
								if(strcmp(ar[i].text,ar[j].text)>0){
									tmpItem = ar[i];
									ar[i] = ar[j];
									ar[j] = tmpItem;
								}
							}else{
								if(strcmp(ar[i].text,ar[j].text)<0){
									tmpItem = ar[i];
									ar[i] = ar[j];
									ar[j] = tmpItem;
								}
							}
						}
					printf("> Done\n");
					break;
				}

				if(mode == 4){
					for(int i=0;i<count-1;i++)
						for(int j=i+1;j<count;j++){
							strcpy(tmpWord,ar[i].date);
							c = strtok(tmpWord,".");
							strcpy(tmpStr,c);
							d1 = atoi(tmpStr);
							c = strtok(NULL,".");
							strcpy(tmpStr,c);
							m1 = atoi(tmpStr);
							c = strtok(NULL,".");
							strcpy(tmpStr,c);
							y1 = atoi(tmpStr);

							strcpy(tmpWord,ar[j].date);
							c = strtok(tmpWord,".");
							strcpy(tmpStr,c);
							d2 = atoi(tmpStr);
							c = strtok(NULL,".");
							strcpy(tmpStr,c);
							m2 = atoi(tmpStr);
							c = strtok(NULL,".");
							strcpy(tmpStr,c);
							y2 = atoi(tmpStr);
							if(iVal == 1){
								if((y1>y2)||((y1==y2)&&(m1>m2))||((y1==y2)&&(m1==m2)&&(d1>d2))){
									tmpItem = ar[i];
									ar[i] = ar[j];
									ar[j] = tmpItem;
								}
							}else
								if((y1<y2)||((y1==y2)&&(m1<m2))||((y1==y2)&&(m1==m2)&&(d1<d2))){
									tmpItem = ar[i];
									ar[i] = ar[j];
									ar[j] = tmpItem;
								}
						}
					printf("> Done\n");
					break;
				}
			}

			case 5:{												/*** SEARCH ***/

				printf("> Search\n");
				printf("> Select Row [1-Sender; 2-Receiver; 3-Text; 4-Date]\n> ");
				scanf("%d",&ind);
				if((ind > 4)||(ind < 1)){printf("> Error\n");break;}
				printf("> Enter Value\n> ");
				if(ind < 3)scanf("%d",&iVal); else scanf("%s",&cVal);
				printf("> Search...\n");
				foundCount = 0;
				for(int i=0;i<count;i++){
					if((ind == 1)&&(ar[i].sender == iVal)){
						foundCount++;
						printf("%d %d \"%s\" %s\n",ar[i].sender,ar[i].receiver,ar[i].text,ar[i].date);
					}
					if((ind == 2)&&(ar[i].receiver == iVal)){
						foundCount++;
						printf("%d %d \"%s\" %s\n",ar[i].sender,ar[i].receiver,ar[i].text,ar[i].date);
					}
					if((ind == 3)&&(strcmp(ar[i].text,cVal)==0)){
						foundCount++;
						printf("%d %d \"%s\" %s\n",ar[i].sender,ar[i].receiver,ar[i].text,ar[i].date);
					}
					if((ind == 4)&&(strcmp(ar[i].date,cVal)==0)){
						foundCount++;
						printf("%d %d \"%s\" %s\n",ar[i].sender,ar[i].receiver,ar[i].text,ar[i].date);
					}
				}
				printf("> %d Records Found\n",foundCount);
				break;
			}

			case 6:{												/*** PRINT RECORDS***/

				printf("> Printing Records...\n");
				printf("> Total: %d | Database Size: %d\n",count,size);
				for(int i=0;i<count;i++)
					printf("%d %d \"%s\" %s\n",ar[i].sender,ar[i].receiver,ar[i].text,ar[i].date);
				printf("> Done\n");
				break;
			}

			case 0:{												/*** EXIT ***/

				canExit = 1;
				break;
			}

		}
	}

	printf("\n> Exit\n\n");
	printf("> Copyright (c) 2007,  Phil Tsarik\n\n");
	return 0;
}
