#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{
	//Init {size}
	unsigned size;
	printf("> Enter Size Of Array (unsigned)\n> ");
	scanf("%d",&size);
	printf("\n");
	if(size<1){
		printf("> Error\n");
		return 0;
	}

	//Init {ar}
	char line[256], word[256], st[50];
	char *c = NULL;
	int k, kt, w;
	typedef struct {char lastName[16]; char firstName[16]; char patronymicName[16]; unsigned cource; double average;} tStudent;
	tStudent ar[size];
	printf("> Enter Elements Of Array (tStudent)\n");
	printf("> Format: LAST_NAME FIRST_NAME PATRONYMIC_NAME COURCE AVERAG_BALL\n");
	for(int i=0;i<size;i++){
		fflush(stdin);
		gets(line);
		fflush(stdin);
		k = 0;
		while(line[k]!='"') k++;
		w = 0;
		k++;
		kt = k;
		while(line[kt]!='"'){
			kt++;
			w++;
		}

		if(w>50){
			printf("\n> Error\n");
			return 0;
		}

		strcpy(st,"");
		for(int r=0;r<w;r++) st[r] = line[k+r];
		st[w] = 0;
		strcpy(ar[i].text,st);
		
		k = kt+1;
		while(line[k]==' ') k++;

		strcpy(st,"");
		for(int r=0;r<10;r++) st[r] = line[k+r];
		st[10] = 0;
		strcpy(ar[i].date,st);

		c = strtok(line," ");
		strcpy(word,c);
		ar[i].sender = atoi(word);

		c = strtok(NULL," ");
		strcpy(word,c);
		ar[i].receiver = atoi(word);
	}
	printf("\n");

	//Sort lines by Sender
	tMess tmpItem;
	int direct;
	printf("> Set Direction Of Sort By Sender [1-Direct | 0-Indirect]\n> ");
	scanf("%d",&direct);
	if((direct!=0)&&(direct!=1)){
		printf("\n> Error\n");
		return 0;
	}
	for(int i=0;i<size-1;i++){
		for(int j=i+1;j<size;j++){
			if(direct){
				if(ar[i].sender > ar[j].sender){
					tmpItem = ar[i];
					ar[i] = ar[j];
					ar[j] = tmpItem;
				}
			}else{
				if(ar[i].sender < ar[j].sender){
					tmpItem = ar[i];
					ar[i] = ar[j];
					ar[j] = tmpItem;
				}
			}
		}
	}
	printf("\n");

	//Sort lines by Date
	int d1, d2, m1, m2, y1, y2;
	char tmpStr[4], tmpWord[10];
	printf("> Set Direction Of Sort By Date [1-Direct | 0-Indirect]\n> ");
	scanf("%d",&direct);
	if((direct!=0)&&(direct!=1)){
		printf("\n> Error\n");
		return 0;
	}
	for(int i=0;i<size-1;i++)
		for(int j=i+1;j<size;j++)
			if(ar[i].sender == ar[j].sender){
				strcpy(tmpWord,ar[i].date);
				c = strtok(tmpWord,".");
				strcpy(tmpStr,c);
				d1 = atoi(tmpStr);
				c = strtok(NULL,".");
				strcpy(tmpStr,c);
				m1 = atoi(tmpStr);
				c = strtok(NULL,".");
				strcpy(tmpStr,c);
				y1 = atoi(tmpStr);

				strcpy(tmpWord,ar[j].date);
				c = strtok(tmpWord,".");
				strcpy(tmpStr,c);
				d2 = atoi(tmpStr);
				c = strtok(NULL,".");
				strcpy(tmpStr,c);
				m2 = atoi(tmpStr);
				c = strtok(NULL,".");
				strcpy(tmpStr,c);
				y2 = atoi(tmpStr);
				if(direct){
					if((y1>y2)||((y1==y2)&&(m1>m2))||((y1==y2)&&(m1==m2)&&(d1>d2))){
						tmpItem = ar[i];
						ar[i] = ar[j];
						ar[j] = tmpItem;
					}
				}else{
					if((y1<y2)||((y1==y2)&&(m1<m2))||((y1==y2)&&(m1==m2)&&(d1<d2))){
						tmpItem = ar[i];
						ar[i] = ar[j];
						ar[j] = tmpItem;
					}
				}
			}
		
	
	printf("\n");

	//Print {ar}
	printf("> Printing Elements...\n");
	for(int i=0;i<size;i++)
		printf("%d %d \"%s\" %.10s\n",ar[i].sender,ar[i].receiver,ar[i].text,ar[i].date);
	printf("\n");

	return 0;
}
