/*
******************************************************************************
*
* File                            interface.c
*
* Description                     ���� ���������� ������ interface.h
*
* Creation date                   30.11.2007
*
* Author                          ������ �����
*
* Other                           ������������ ������ #15
*                                 ������� #1
*                                 ������� #21
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include "interface.h"


/*
******************************************************************************
*                                 LOCAL PROTOTYPES
******************************************************************************
*/
static  int   IF_ReadElem(TList *, int);
static  int   IF_PrintElem(TData, int);
static  int   IF_InitInsElem(TList *, TData);


/*
******************************************************************************
*                                 GLOBAL FUNCTIONS
******************************************************************************
*/

//
// ��������� ����
//
int drwMenu(TList *src, char *cpt)
{
	int ch, canExit = 0;
	while (!canExit) {
		printf("--- Menu for %s ---\n",cpt);
		printf("1 - Add elements\n");
		printf("2 - Insert element\n");
		printf("3 - Delete element\n");
		printf("4 - Sort\n");
		printf("5 - Print\n");
		printf("0 - Exit\n");
		printf("---------------------\n");
		printf("> Your Choice   ");
		scanf("%d",&ch);
		printf("\n");
		switch(ch){
			case 1:{
				execInputList(src, cpt);
				break;
			}
			case 2:{
				execInsertElem(src, cpt);
				break;
			}
			case 3:{
				execDeleteElem(src, cpt);
				break;
			}
			case 4:{
				execSortList(src, cpt);
				break;
			}
			case 5:{
				execPrintList(src, cpt);
				break;
			}
			case 0:{
				canExit = 1;
				break;
			}
		}
	}
	return 0;
}

//
// ���������� �����
//
int execInputList(TList *src, char *cpt)
{
	int err = 0;

	// �������� ���������
	printf("> INPUT ELEMENTS IN LIST %s\n",cpt);

	// ��������� ��������
	printf("> Enter elements of list\n");
	printf("> Format: LAST_NAME FIRST_NAME PATRONYMIC [RECORD_BOOK_NO[int,length=6]], DATE[format:DD.MM.YYYY]\n");
	printf("> Enter an empty string to exit to menu\n");
	while (err != 2) {
		do {
			err = IF_ReadElem(src,1);
		} while (err == 1);
	}

	// �����
	printf("> Done\n\n");
	return 0;
}

//
// ����� ������ �� �����
//
int execPrintList(TList *src, char *cpt)
{
	// �������� ���������
	printf("> PRINT LIST %s\n",cpt);
	printf("> Printing elements...\n");
	// ���� ������ ���� -> �����
	if (!src->head) {
		printf("> No elements\n");
		printf("> Done\n\n");
		return 0;
	}
	// ��������� �� ����� ������
	int index = 0;
	moveFirst(src);
	while (src->cur) {
		index++;
		IF_PrintElem(src->cur->data,index);
		moveNext(src);
	}
	moveLast(src);

	// �����
	printf("> Total: %d\n",src->size);
	printf("> Done\n\n");
	return 0;
}

//
// ������������� ������� �������� ��������
//
int execDeleteElem(TList *src, char *cpt)
{
	// �������� ���������
	printf("> DELETE ELEMENT FROM LIST %s\n",cpt);

	// �������� ������
	execPrintList(src, cpt);

	// �������� ������ ��������
	printf("> Set index of element to delete\n> ");
	int n = 0;
	scanf("%d",&n);

	printf("> Processing...\n");

	// �������
	deleteElem(src,n);

	// �����
	printf("> Done\n\n");
	return 0;
}

//
// ������� �������� � ������
//
int execInsertElem(TList *src, char *cpt)
{
	int err = 0;

	// �������� ���������
	printf("> INSERT ELEMENT TO LIST %d\n",cpt);

	// ��������� �������
	printf("> Format: LAST_NAME FIRST_NAME PATRONYMIC [RECORD_BOOK_NO[int,length=6]], DATE[format:DD.MM.YYYY]\n");
	do {
		err = IF_ReadElem(src,2);
	} while (err == 1);

	// �����
	printf("> Done\n\n");
	return 0;
}

//
// ������������� ���������� ���������
//
int execSortList(TList *src, char *cpt)
{
	//
	// row                         ���� ����������
	//    1                           LAST_NAME
	//    2                           FIRST_NAME
	//    3                           PATRONYMIC
	//    4                           RECORD_BOOK_NO
	//    5                           DATE
	// dir                         ����������� ����������
	//    1                           �� �����������
	//    0                           �� ��������
	//
	int dir = -1, row = -1;

	// �������� ���������
	printf("> SORT LIST %s\n",cpt);

	// �������� ������ ����
	while ( (row < 1) || (row > 5) ) {
		printf("> Set row [1-LAST_NAME | 2-FIRST_NAME | 3-PATRONYMIC | 4-RECORD_BOOK_NO | 5-DATE]\n> ");
		scanf("%d",&row);
	}
	src->row = row;

	// �������� �����������
	while ( (dir != 1) && (dir != 0) ) {
		printf("> Set direction [1-Direct | 0-Indirect]\n> ");
		scanf("%d",&dir);
	}
	src->dir = dir;

	printf("> Sorting...\n");

	// ���������
	sortList(src);

	// �����
	printf("> Done\n\n");
	return 0;
}


/*
******************************************************************************
*                                 LOCAL FUNCTIONS
******************************************************************************
*/

//
// ���������� ������ ��������
//
//    ������� ���������
//       src                      ������
//       mode                     �����
//          1                        ���������� �������� � ����� ������
//          2                        �������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        ������ ��� ������� ������
//       2                        ���� ���������
//
static  int IF_ReadElem(TList *src, int mode)
{
	//
	// line[257]                   ��������������� ������ � ���������� �����
	// word[257]                   ������� � line
	// tmpTok[5]                   ��������� ����������, ��� �������� ���� �� ������������
	// tmpWord[11]                 ��������� ����������, ����������� ���� date
	// �                           ��������� ���������� ��� ����������� �����
	// k                           ������� �������
	// kt                          ���������� ������������� ��������� k
	// ival                        �������� �������� ������ ����
	// curData                     ��������� � ������� �� ������� ������ line
	//
	char line[257], word[257], tmpTok[5], tmpWord[11];
	int k, kt, ival;
	char *c = NULL;
	TData curData;

	fflush(stdin);
	gets(line);
	fflush(stdin);

	if (strcmp(line,"") == 0) return 2;

	// ����� RECORD_BOOK_NO
	k = 0;
	while ( (line[k] != '[') && (line[k] != 0) ) k++;
	if (k == strlen(line)) {
		printf("\n> Error. RECORD_BOOK_NO is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	kt = k+1;
	k++;
	while( (line[k] != ']') && (line[k] != 0) ) k++;
	if (kt == strlen(line)) {
		printf("\n> Error. RECORD_BOOK_NO is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if(strlen(word) != 6){
		printf("\n> Error. Length of RECORD_BOOK_NO is 6 figures\n",word);
		printf("> Try again\n\n");
		return 1;
	}
	if(atol(word) == 0){
		printf("\n> Error. RECORD_BOOK_NO must be of integer type\n");
		printf("> Try again\n\n");
		return 1;
	}
	curData.num = atoi(word);

	// ����� DATE
	k++;
	while ( ((line[k] == ' ') || (line[k] == ',')) && (line[k] != 0)) k++;
	if(k == strlen(line)){
		printf("\n> Error. DATE is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	kt = k;
	k++;
	while ( (line[k] != ' ') && (line[k] != 0) ) k++;
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if (strcmp(word,"") == 0) {
		printf("\n> Error. DATE is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	if (!((word[0] != '.')&&(word[1] != '.')&&(word[2] == '.')&&
			(word[3] != '.')&&(word[4] != '.')&&(word[5] == '.')&&
			(word[6] != '.')&&(word[7] != '.')&&(word[8] != '.')&&
			(word[9] != '.'))) {
		printf("\n> Error. DATE format must be DD.MM.YYYY\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(tmpWord,word);
	c = strtok(tmpWord,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if ((ival <= 0)||(ival > 31)) {
		printf("\n> Error. \"%s\" is wrong value of a DAY\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if ((ival <= 0)||(ival > 13)) {
		printf("\n> Error. \"%s\" is wrong value of a MOUNTH\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if ((ival <= 0)||(ival > 9999)) {
		printf("\n> Error. \"%s\" is wrong value of a YEAR\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(curData.date,word);

	// ����� LAST_NAME
	c = strtok(line," ");
	strcpy(word,c);
	if (strlen(word) > 15) {
		printf("\n> Error. Maximum length of LAST_NAME is 15 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(curData.lname,word);

	// ����� FIRST_NAME
	c = strtok(NULL," ");
	strcpy(word,c);
	if (strlen(word) > 15) {
		printf("\n> Error. Maximum length of FIRST_NAME is 15 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(curData.fname,word);

	// ����� PATRONYMIC
	c = strtok(NULL," ");
	strcpy(word,c);
	if (strlen(word) > 15) {
		printf("\n> Error. Maximum length of PATRONYMIC is 15 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(curData.pname,word);

	// ���������� � ����� | ������� ��������
	switch(mode){
		case 1:
			addElem(src,curData);
			break;
		case 2:
			IF_InitInsElem(src,curData);
			break;
	}

	return 0;
}

//
// ������ ������ ��������
//
static  int IF_PrintElem(TData data, int index)
{
	printf("%.2d | %s %s %s [%d], %s\n",index,data.lname,data.fname,data.pname,data.num,data.date);
	return 0;
}

//
// ������������� ������� ��������
//
static  int IF_InitInsElem(TList *src, TData data)
{
	// �������� ������
	execPrintList(src, "");

	// �������� ������ ��������
	printf("> Set index\n> ");
	int n = 0;
	scanf("%d",&n);

	printf("> Processing...\n");

	// ��������� ����� �������
	insertElem(src,data,n);

	// �����
	printf("> Done\n\n");
	return 0;
}
