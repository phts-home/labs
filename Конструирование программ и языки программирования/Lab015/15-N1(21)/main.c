/*
******************************************************************************
*
* File                            main.c
*
* Description                     ���������� ������� main
*
* Creation date                   30.11.2007
*
* Author                          ������ �����
*
* Other                           ������������ ������ #15
*                                 ������� #1
*                                 ������� #21
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include "interface.h"
#include "list.h"


/*
******************************************************************************
*                                 MAIN FUNCTION
******************************************************************************
*/

int main(int argc, char *argv[])
{
	// ������������ ������

	TData d;
	// �������� ������� ������
	TList *list1 = initList();

	// ���������� ��������
	strcpy(d.fname,"john");
	strcpy(d.lname,"john2");
	strcpy(d.pname,"john3");
	strcpy(d.date,"13.12.2007");
	d.num = 123456;
	addElem(list1,d);

	// ���������� ��������
	strcpy(d.fname,"peter");
	strcpy(d.lname,"peter2");
	strcpy(d.pname,"peter3");
	strcpy(d.date,"10.10.2000");
	d.num = 112233;
	addElem(list1,d);

	// ���������� ��������
	strcpy(d.fname,"adam");
	strcpy(d.lname,"adam2");
	strcpy(d.pname,"adam3");
	strcpy(d.date,"09.10.2009");
	d.num = 235678;
	addElem(list1,d);

	// �������� ������� ������
	TList *list2 = initList();
	addElem(list2,list1->head->data);

	// ������ �������
	execPrintList(list1, "list1");
	execPrintList(list2, "list2");

	// ���������� ������� ������
	list1->row = 1;
	list1->dir = 0;
	sortList(list1);
	execPrintList(list1, "list1");

	// ���������� �� ������ ������ ������� �������� ������� ������
	moveFirst(list1);
	moveNext(list1);
	addElem(list2,list1->cur->data);
	execPrintList(list2, "list2");

	// ������� �������
	clearList(list1);
	clearList(list2);
	execPrintList(list1, "list1");
	execPrintList(list2, "list2");

	// �������� �������
	destroyList(list1);
	destroyList(list2);

	// �����
	fflush(stdin);
	printf("\n> Press any key to quit\n");
	_getch();
	return 0;
}
