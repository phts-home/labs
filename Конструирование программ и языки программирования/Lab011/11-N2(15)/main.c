/*
******************************************************************************
*       ������������ ������ #11. ������� #2. ������� #15
*
*       ��������: ������� ��. 06��-1 ��� ����� ������
******************************************************************************
*/

/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - < nothing >
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/*
******************************************************************************
*                                 DATA TYPES
******************************************************************************
*/
typedef struct {
   int num;                       // ����� ���������
   char date[11];                 // ���� � ������� ��.��.����
   double amount;                 // ����� ����� �� ���������
   char name[21];                 // ��� ���������
} TData;
typedef struct _TListItem{
   TData data;                    // ������ ��������
   struct _TListItem *prev;       // ���������� ������� � ������
   struct _TListItem *next;       // ��������� ������� � ������
} TListItem;
typedef struct {
   TListItem *head;               // ������ ������� � ������
   TListItem *cur;                // ������� ������� � ������
} TList;


/*
******************************************************************************
*                                 PROTOTYPES
******************************************************************************
*/
int drwMenu(TList *);
int initList (TList *);
int destroyList(TList *);

int inputList(TList *);
int inputItem(TList *);
int printList(TList *);
int printItem(TData);

int addItem(TList *, TData);
int insertItem(TList *, TData);
int delItem(TList *);

int initBubbleSort(TList *);
int bubbleSort(TList *, int);
int exchangeElem(TList *);


/*
******************************************************************************
*                                 FUNCTIONS
******************************************************************************
*/

int main(int argc, char *argv[])
{
	TList *list1;
	initList(list1);
	drwMenu(list1);
	destroyList(list1);
	return 0;
}

//
// ��������� ����
//
int drwMenu(TList *src)
{
	int ch, canExit = 0;
	while(!canExit){
		printf("\n");
		printf("------ Menu ------\n");
		printf("1 - Add items\n");
		printf("2 - Print\n");
		printf("3 - Sort\n");
		printf("0 - Exit\n");
		printf("------------------\n");
		printf("> Your Choice   ");
		scanf("%d",&ch);
		printf("\n");
		switch(ch){
			case 1:{
				inputList(src);
				break;
			}
			case 2:{
				printList(src);
				break;
			}
			case 3:{
				initBubbleSort(src);
				break;
			}
			case 0:{
				canExit = 1;
				break;
			}
		}
	}
	return 0;
}

//
// ������������� ������
//
int initList(TList *src)
{
	src->cur = NULL;
	src->head = NULL;
	return 0;
}

//
// �������� ������
//
int destroyList(TList *src)
{
	while(src->head){
		src->cur = src->head;
		src->head = src->head->next;
		free(src->cur);
	}
	src->cur = NULL;
	src->head = NULL;
	return 0;
}

//
// ���������� ������
//
int inputList(TList *src)
{
	//
	// er                          ������ ������
	//    0                           ��� ��
	//    1                           ������
	//
	int er = 0;

	printf("> Enter items of array\n");
	printf("> Format: NO[int]: \"DATE[format:DD.MM.YYYY]\", AMOUNT[double], NAME[string(maxlen 20)]\n");
	printf("> Enter an empty string to exit to menu\n");
	while(er != 2){
		do{
			er = inputItem(src);
		}while(er == 1);
	}
	printf("> Done\n");
	return 0;
}

//
// ���������� ������ ��������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        ������ ��� ������� ������
//       2                        ���� ���������
//
int inputItem(TList *src)
{
	//
	// line[257]                   ��������������� ������ � ���������� ������
	// word[257]                   ������� � line
	// tmpTok[5]                   ��������� ����������, ��� �������� ���� �� ������������
	// tmpWord[11]                 ��������� ����������, ����������� ���� date
	// �                           ��������� ���������� ��� ����������� �����
	// k                           ������� �������
	// kt                          ���������� ������������� ��������� k
	// ival                        �������� �������� ������ ����
	// dval                        �������� �������� ������������� ����
	// curData                     ��������� � ������� �� ������� ������ line
	//
	char line[257], word[257], tmpTok[5], tmpWord[11];
	int k, kt, ival, dval;
	char *c = NULL;
	TData curData;

	fflush(stdin);
	gets(line);
	fflush(stdin);

	if(strcmp(line,"") == 0) return 2;

	// ����� NO
	k = 0;
	while( (line[k] == ' ') && (line[k] != 0) ) k++;
	if(k == strlen(line)){
		printf("\n> Error. NO is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	kt = k;
	k++;
	while( (line[k] != ' ') && (line[k] != ':') && (line[k] != 0) ) k++;
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if( (atoi(word) == 0) && (strcmp(word,"0") != 0) ){
		printf("\n> Error. \"%s\" is not a number\n",word);
		printf("> Try again\n\n");
		return 1;
	}
	curData.num = atoi(word);

	// ����� DATE
	k++;
	while( (line[k] != '"') && (line[k] != 0) ) k++;
	if(k == strlen(line)){
		printf("\n> Error. DATE is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	kt = k+1;
	k++;
	while( (line[k] != '"') && (line[k] != 0) ) k++;
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if(strcmp(word,"") == 0){
		printf("\n> Error. DATE is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	if(strlen(word) != 10){
		printf("\n> Error. DATE format must be DD.MM.YYYY\n");
		printf("> Try again\n\n");
		return 1;
	}
	if(!((word[0] != '.')&&(word[1] != '.')&&(word[2] == '.')&&
			(word[3] != '.')&&(word[4] != '.')&&(word[5] == '.')&&
			(word[6] != '.')&&(word[7] != '.')&&(word[8] != '.')&&
			(word[9] != '.'))){
		printf("\n> Error. DATE format must be DD.MM.YYYY\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(tmpWord,word);
	c = strtok(tmpWord,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if((ival <= 0)||(ival > 31)){
		printf("\n> Error. \"%s\" is wrong value of a DAY\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if((ival <= 0)||(ival > 13)){
		printf("\n> Error. \"%s\" is wrong value of a MOUNTH\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if((ival <= 0)||(ival > 9999)){
		printf("\n> Error. \"%s\" is wrong value of a YEAR\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(curData.date,word);

	// ����� AMOUNT
	k++;
	while( ((line[k] == ' ') || (line[k] == ',')) && (line[k] != 0) ) k++;
	if(k == strlen(line)){
		printf("\n> Error. AMOUNT is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	kt = k;
	k++;
	while( ((line[k] != ' ') && (line[k] != ',')) && (line[k] != 0) ) k++;
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if( (atof(word) == 0) && (strcmp(word,"0") != 0) ){
		printf("\n> Error. \"%s\" is not a number\n",word);
		printf("> Try again\n\n");
		return 1;
	}
	dval = atof(word);
	if((dval < 0)){
		printf("\n> Error. AMOUNT must be nonnegative\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	curData.amount = dval;

	// ����� NAME
	k++;
	while( ((line[k] == ' ') || (line[k] == ',')) && (line[k] != 0) ) k++;
	if(k == strlen(line)){
		printf("\n> Error. NAME is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	kt = k;
	k++;
	while( (line[k] != ' ') && (line[k] != 0) ) k++;
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if(strlen(word) > 20){
		printf("\n> Error. Max length of NAME is 20\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(curData.name,word);

	addItem(src,curData);

	return 0;
}

//
// ����� ������ �� �����
//
int printList(TList *src)
{
	printf("> Printing items...\n");
	if(!src->head){
		printf("> No items\n");
		printf("> Done\n");
		return 0;
	}
	src->cur = src->head;
	while(src->cur){
		printItem(src->cur->data);
		src->cur = src->cur->next;
	}
	src->cur = src->head;
	while(src->cur->next){
		src->cur = src->cur->next;
	}
	printf("> Done\n");
	return 0;
}

//
// ������ ������ ��������
//
int printItem(TData data)
{
	printf("%d: \"%s\", %.3lf, %s\n",data.num,data.date,data.amount,data.name);
	return 0;
}

//
// ���������� �������� � ����� ������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        �� ������� �������� ������
//
int addItem(TList *src, TData data)
{
	TListItem *tmp = (TListItem *)malloc(sizeof(TListItem));
	if(!tmp) return 1;
	if(!src->head){
		src->head = tmp;
		tmp->prev = NULL;
		tmp->next = NULL;
	}else{
		if(!src->cur) src->cur = src->head;
		while(src->cur->next) src->cur = src->cur->next;
		src->cur->next = tmp;
		tmp->prev = src->cur;
		tmp->next = NULL;
	}
	tmp->data = data;
	src->cur = tmp;
	return 0;
}

//
// ������� �������� ����� ������� ���������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        �� ������� �������� ������
//
int insertItem(TList *src, TData data)
{
	if(!src->cur) return addItem(src,data);
	TListItem *tmp=(TListItem *)malloc(sizeof(TListItem));
	if(!tmp) return 1;
	tmp->next = src->cur;
	tmp->prev = src->cur->prev;
	src->cur->prev = tmp;
	if(tmp->prev)
		tmp->prev->next = tmp;
	else 
		src->head = tmp;
	tmp->data = data;
	src->cur = tmp;
	return 0;
}

//
// �������� �������� ��������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        �� ������� �������� ������
//
int delItem(TList *src)
{
	if(src->cur == NULL) return 1;
	TListItem *tmp = src->cur->prev;
	if(!tmp){
		src->head = src->head->next;
		src->head->prev = NULL;
	}else{
		tmp->next = src->cur->next;
		if(src->cur->next) src->cur->next->prev = tmp;
	}
	free(src->cur);
	src->cur = tmp;
	return 0;
}

//
// ������������� ���������� ���������
//
int initBubbleSort(TList *src)
{
	int dir = -1;
	printf("> Sort records\n");
	while( (dir != 1) && (dir != 0) ){
		printf("> Set direction [1-Direct | 0-Indirect]\n> ");
		scanf("%d",&dir);
	}
	printf("> Sorting...\n");

	if(bubbleSort(src,dir) == 0) printf("> Done\n");

	return 0;
}

//
// ���������� ���������
//
int bubbleSort(TList *src, int dir)
{
	if(!src->head) return 1;
	int canExit = 0;
	while(!canExit){
		canExit = 1;
		src->cur = src->head;
		while(src->cur->next){
			if( ((strcmp(src->cur->data.name,src->cur->next->data.name) >= 0)&&(dir))
				|| ((strcmp(src->cur->data.name,src->cur->next->data.name) <= 0)&&(!dir)) ){
				exchangeElem(src);
				canExit = 0;
			}else src->cur = src->cur->next;
		}
	}
	return 0;
}

//
// ������������ ���� ���������
// 
int exchangeElem(TList *src)
{
	TListItem *tmp = src->cur->next;
	src->cur->next = tmp->next;
	tmp->prev = src->cur->prev;
	if(src->cur->next != NULL) src->cur->next->prev = src->cur;
	if(tmp->prev != NULL) tmp->prev->next = tmp;
	tmp->next = src->cur;
	src->cur->prev = tmp;
	if(src->cur == src->head) src->head = tmp;
	return 0;
}
