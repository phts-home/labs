/*
******************************************************************************
*       ������������ ������ #11. ������� #3. ������� #1
*
*       ��������: ������� ��. 06��-1 ��� ����� ������
******************************************************************************
*/

/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - < nothing >
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/*
******************************************************************************
*                                 DATA TYPES
******************************************************************************
*/
typedef struct {
   char lname[16];                // �������
   char fname[16];                // ���
   char pname[16];                // ��������
   long num;                      // ����� �������
   char date[11];                 // ����
} TData;
typedef struct _TListItem{
   TData data;                    // ������ ��������
   struct _TListItem *prev;       // ���������� ������� � ������
   struct _TListItem *next;       // ��������� ������� � ������
} TListItem;
typedef struct {
   int row;                       // �������, �� �������� ���������� ������
   int dir;                       // ����������� ����������
   TListItem *head;               // ������ ������� � ������
   TListItem *cur;                // ������� ������� � ������
} TList;


/*
******************************************************************************
*                                 PROTOTYPES
******************************************************************************
*/
int drwMenu(TList *);
TList *initList (void);
int destroyList(TList *);

int inputList(TList *);
int inputItem(TList *);
int printList(TList *);
int printItem(TData, int);
int initDelItem(TList *);

int addItem(TList *, TData);
int insertItem(TList *, TData);
int delItem(TList *);

int initBubbleSort(TList *);
int bubbleSort(TList *, int, int);
int exchangeElem(TList *);


/*
******************************************************************************
*                                 FUNCTIONS
******************************************************************************
*/

int main(int argc, char *argv[])
{
	TList *list1 = initList();
	drwMenu(list1);
	destroyList(list1);
	return 0;
}

//
// ��������� ����
//
int drwMenu(TList *src)
{
	int ch, canExit = 0;
	while(!canExit){
		printf("\n");
		printf("------ Menu ------\n");
		printf("1 - Add items\n");
		printf("2 - Delete item\n");
		printf("3 - Sort\n");
		printf("4 - Print\n");
		printf("0 - Exit\n");
		printf("------------------\n");
		printf("> Your Choice   ");
		scanf("%d",&ch);
		printf("\n");
		switch(ch){
			case 1:{
				inputList(src);
				break;
			}
			case 2:{
				initDelItem(src);
				break;
			}
			case 3:{
				initBubbleSort(src);
				break;
			}
			case 4:{
				printList(src);
				break;
			}
			case 0:{
				canExit = 1;
				break;
			}
		}
	}
	return 0;
}

//
// ������������� ������
//
TList *initList(void)
{
	TList *list = (TList *)malloc(sizeof(TList));
	list->row = 0;
	list->dir = 1;
	list->cur = NULL;
	list->head = NULL;
	return list;
}

//
// �������� ������
//
int destroyList(TList *src)
{
	while(src->head){
		src->cur = src->head;
		src->head = src->head->next;
		free(src->cur);
	}
	src->cur = NULL;
	src->head = NULL;
	return 0;
}

//
// ���������� ������
//
int inputList(TList *src)
{
	int er = 0;

	printf("> Enter items of array\n");
	printf("> Format: LASTNAME[string(maxlen 15)] FIRSTNAME[string(maxlen 15)] PATRONYMIC[string(maxlen 15)] [REC_BOOK_NO[int,len 6]], DATE[format:DD.MM.YYYY]\n");
	printf("> Enter an empty string to exit to menu\n");
	// ���� �� �������� ����
	while(er != 2){
		// ���� ������ � �����
		do{
			er = inputItem(src);
		}while(er == 1);
	}
	// ���������� ������
	bubbleSort(src,src->row,src->dir);
	// �����
	printf("> Done\n");
	return 0;
}

//
// ���������� ������ ��������
//
//    ������������ ��������:
//       0                        ��� ��
//       1                        ������ ��� ������� ������
//       2                        ���� ���������
//
int inputItem(TList *src)
{
	//
	// line[257]                   ��������������� ������ � ���������� ������
	// word[257]                   ������� � line
	// tmpTok[5]                   ��������� ����������, ��� �������� ���� �� ������������
	// tmpWord[11]                 ��������� ����������, ����������� ���� date
	// �                           ��������� ���������� ��� ����������� �����
	// k                           ������� �������
	// kt                          ���������� ������������� ��������� k
	// ival                        �������� �������� ������ ����
	// cData                       ��������� � ������� �� ������� ������ line
	//
	char line[257], word[257], tmpTok[5], tmpWord[11];
	int k, kt, ival;
	char *c = NULL;
	TData cData;

	// ��������� ������
	fflush(stdin);
	gets(line);
	fflush(stdin);

	// ���� ������ ����� - �����
	if(strcmp(line,"") == 0) return 2;

	// ����� REC_BOOK_NO
	k = 0;
	while((line[k] != '[')&&(line[k] != 0)) k++;
	if(k == strlen(line)){
		printf("\n> Error. REC_BOOK_NO is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	k++;
	kt = k;
	while((line[k] != ']')&&(line[k] != 0)){
		k++;
	}
	if(k == strlen(line)){
		printf("\n> Error. REC_BOOK_NO is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	if(k-kt != 6){
		printf("\n> Error. Length of REC_BOOK_NO is 6 figures\n");
		printf("%d\n",k-kt-1);
		printf("> Try again\n\n");
		return 1;
	}
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if( (atoi(word) == 0) && (strcmp(word,"0") != 0) ){
		printf("\n> Error. \"%s\" is not a number\n",word);
		printf("> Try again\n\n");
		return 1;
	}
	cData.num = atol(word);

	// ����� DATE
	k++;
	while( (line[k] == ' ') || (line[k] == ',') ) k++;
	if(k == strlen(line)){
		printf("\n> Error. DATE is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	kt = k;
	k++;
	while( (line[k] != ' ') && (line[k] != 0) ) k++;
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if(strcmp(word,"") == 0){
		printf("\n> Error. DATE is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	if(!((word[0] != '.')&&(word[1] != '.')&&(word[2] == '.')&&
			(word[3] != '.')&&(word[4] != '.')&&(word[5] == '.')&&
			(word[6] != '.')&&(word[7] != '.')&&(word[8] != '.')&&
			(word[9] != '.'))){
		printf("\n> Error. DATE format must be DD.MM.YYYY\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(tmpWord,word);
	c = strtok(tmpWord,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if((ival <= 0)||(ival > 31)){
		printf("\n> Error. \"%s\" is wrong value of a DAY\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if((ival <= 0)||(ival > 13)){
		printf("\n> Error. \"%s\" is wrong value of a MOUNTH\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if((ival <= 0)||(ival > 9999)){
		printf("\n> Error. \"%s\" is wrong value of a YEAR\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(cData.date,word);

	// ����� LASTNAME
	c = strtok(line," ");
	strcpy(word,c);
	if(strlen(word) > 15){
		printf("\n> Error. Maximum length of LASTNAME is 15 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(cData.lname,word);

	// ����� FIRSTNAME
	c = strtok(NULL," ");
	strcpy(word,c);
	if(strlen(word) > 15){
		printf("\n> Error. Maximum length of FIRSTNAME is 15 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(cData.fname,word);

	// ����� PATRONYMIC
	c = strtok(NULL," ");
	strcpy(word,c);
	if(strlen(word) > 15){
		printf("\n> Error. Maximum length of PATRONYMIC is 15 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(cData.pname,word);

	// ��������� ������� � ������
	addItem(src,cData);

	return 0;
}

//
// ����� ������ �� �����
//
int printList(TList *src)
{
	printf("> Printing items...\n");
	// ���� ������ ���� - �����
	if(!src->head){
		printf("> No items\n");
		printf("> Done\n");
		return 0;
	}
	// ��������� �� ����� ������
	int index = 0;
	src->cur = src->head;
	while(src->cur){
		index++;
		printItem(src->cur->data,index);
		src->cur = src->cur->next;
	}
	// ���� ������ ����������
	if( src->head ){
		// ��������������� ������� ������
		src->cur = src->head;
		while(src->cur->next){
			src->cur = src->cur->next;
		}
	}
	// �����
	printf("> Done\n");
	return 0;
}

//
// ������ ������ ��������
//
int printItem(TData data, int index)
{
	printf("%.2d | %s %s %s [%d], %s\n",index,data.lname,data.fname,data.pname,data.num,data.date);
	return 0;
}

//
// ������������� ������� �������� ��������
//
int initDelItem(TList *src)
{
	printList(src);
	printf("> Set item to delete\n> ");
	int n = 0;
	scanf("%d",&n);

	printf("> Processing...\n");
	int index = 1;
	src->cur = src->head;
	while( (src->cur) && (index != n) ){
		index++;
		src->cur = src->cur->next;
	}
	if( index == n ){
		delItem(src);
	}

	// ���� ������ ����������
	if( src->head ){
		// ��������������� ������� ������
		src->cur = src->head;
		while(src->cur->next){
			src->cur = src->cur->next;
		}
	}
	printf("> Done\n");
	return 0;
}

//
// ���������� �������� � ����� ������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        �� ������� �������� ������
//
int addItem(TList *src, TData data)
{
	TListItem *tmp = (TListItem *)malloc(sizeof(TListItem));
	if(!tmp) return 1;
	if(!src->head){
		src->head = tmp;
		tmp->prev = NULL;
		tmp->next = NULL;
	}else{
		if(!src->cur) src->cur = src->head;
		while(src->cur->next) src->cur = src->cur->next;
		src->cur->next = tmp;
		tmp->prev = src->cur;
		tmp->next = NULL;
	}
	tmp->data = data;
	src->cur = tmp;
	return 0;
}

//
// ������� �������� ����� ������� ���������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        �� ������� �������� ������
//
int insertItem(TList *src, TData data)
{
	if(!src->cur) return addItem(src,data);
	TListItem *tmp=(TListItem *)malloc(sizeof(TListItem));
	if(!tmp) return 1;
	tmp->next = src->cur;
	tmp->prev = src->cur->prev;
	src->cur->prev = tmp;
	if(tmp->prev)
		tmp->prev->next = tmp;
	else 
		src->head = tmp;
	tmp->data = data;
	src->cur = tmp;
	return 0;
}

//
// �������� �������� ��������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        ������� ������� ������
//
int delItem(TList *src)
{
	if(src->cur == NULL) return 1;
	if(src->cur == src->head){
		src->cur = NULL;
		src->head = NULL;
		return 0;
	}
	TListItem *tmp = src->cur->prev;
	if(!tmp){
		src->head = src->head->next;
		src->head->prev = NULL;
	}else{
		tmp->next = src->cur->next;
		if(src->cur->next) src->cur->next->prev = tmp;
	}
	free(src->cur);
	src->cur = tmp;
	return 0;
}

//
// ������������� ���������� ���������
//
int initBubbleSort(TList *src)
{
	//
	// dir                         ����������� ����������
	//    1                           �� �����������
	//    0                           �� ��������
	// row                         ����� ����, �� ���. ����������� ������
	//    0                           <���������� ����������>
	//    1                           LASTNAME
	//    2                           FIRSTNAME
	//    3                           PATRONYMIC
	//    4                           REC_BOOK_NO
	//    5                           DATE
	//
	int dir = -1, row = -1;
	printf("> Sort records\n");

	while( (row < 0) || (row > 5) ){
		printf("> Select Row [0-<OFF> | 1-LASTNAME | 2-FIRSTNAME | 3-PATRONYMIC | 4-REC_BOOK_NO | 5-DATE]\n> ");
		scanf("%d",&row);
	}
	src->row = row;
	if(row == 0) return 0;

	while( (dir != 1) && (dir != 0) ){
		printf("> Set direction [1-Direct | 0-Indirect]\n> ");
		scanf("%d",&dir);
	}
	printf("> Sorting...\n");

	src->dir = dir;
	if(bubbleSort(src,row,dir) == 0) printf("> Done\n");

	return 0;
}

//
// ���������� ���������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        ������ ����
//
int bubbleSort(TList *src, int row, int dir)
{
	//
	// d1, d2, m1, m2, y1, y2      �������� ���, ������, ���� � ����� date[11] 
	//                                ���� ��������� ������� �����.
	// tmpTok[5]                   ��������� ����������, �������� d1, d2, m1, 
	//                                m2, y1, y2 ��� ����������� � �����������
	// tmpWord[11]                 ��������� ����������, ����������� ���� date
	//

	// ���� ������ ���� - �����
	if( !src->head ) return 1;
	if(row == 0) return 0;

	int d1, d2, m1, m2, y1, y2;
	char tmpTok[5], tmpWord[11];
	char *c = NULL;
	int canExit = 0;

	while( !canExit ){
		canExit = 1;
		src->cur = src->head;
		while(src->cur->next){
			switch(row){
				case 1:
					if( ( (strcmp(src->cur->data.lname,src->cur->next->data.lname) > 0)&&(dir == 1) )
						|| ( (strcmp(src->cur->data.lname,src->cur->next->data.lname) < 0)&&(dir == 0) ) ){
						exchangeElem(src);
						canExit = 0;
					}else{
						src->cur = src->cur->next;
					}
					break;
				case 2:
					if( ( (strcmp(src->cur->data.fname,src->cur->next->data.fname) > 0)&&(dir == 1) )
						|| ( (strcmp(src->cur->data.fname,src->cur->next->data.fname) < 0)&&(dir == 0) ) ){
						exchangeElem(src);
						canExit = 0;
					}else{
						src->cur = src->cur->next;
					}
					break;
				case 3:
					if( ( (strcmp(src->cur->data.pname,src->cur->next->data.pname) > 0)&&(dir == 1) )
						|| ( (strcmp(src->cur->data.pname,src->cur->next->data.pname) < 0)&&(dir == 0) ) ){
						exchangeElem(src);
						canExit = 0;
					}else{
						src->cur = src->cur->next;
					}
					break;
				case 4:
					if( ( (src->cur->data.num > src->cur->next->data.num)&&(dir == 1) )
						|| ( (src->cur->data.num < src->cur->next->data.num)&&(dir == 0) ) ){
						exchangeElem(src);
						canExit = 0;
					}else{
						src->cur = src->cur->next;
					}
					break;
				case 5:
					// ��������� ����_���_������ 1-�� �������� �� ����������_���_�����
					// ����������� ���� � ������������� ����������
					strcpy(tmpWord,src->cur->data.date);
					// ����� <���>
					c = strtok(tmpWord,".");
					strcpy(tmpTok,c);
					d1 = atoi(tmpTok);
					// ����� <������>
					c = strtok(NULL,".");
					strcpy(tmpTok,c);
					m1 = atoi(tmpTok);
					// ����� <����>
					c = strtok(NULL,".");
					strcpy(tmpTok,c);
					y1 = atoi(tmpTok);
					// ��������� ����_���_������ 1-�� �������� �� ����������_���_�����
					// ����������� ���� � ������������� ����������
					strcpy(tmpWord,src->cur->next->data.date);
					// ����� <���>
					c = strtok(tmpWord,".");
					strcpy(tmpTok,c);
					d2 = atoi(tmpTok);
					// ����� <������>
					c = strtok(NULL,".");
					strcpy(tmpTok,c);
					m2 = atoi(tmpTok);
					// ����� <����>
					c = strtok(NULL,".");
					strcpy(tmpTok,c);
					y2 = atoi(tmpTok);
					if( ( 
							( (y1>y2)||((y1==y2)&&(m1>m2))||((y1==y2)&&(m1==m2)&&(d1>d2)) )&&(dir == 1) )
							|| ( 
							( (y1<y2)||((y1==y2)&&(m1<m2))||((y1==y2)&&(m1==m2)&&(d1<d2)) )&&(dir == 0) ) ){
						exchangeElem(src);
						canExit = 0;
					}else{
						src->cur = src->cur->next;
					}
					break;
			}
		}
	}
	return 0;
}

//
// ������������ ���� ��������� � ������
//
int exchangeElem(TList *src)
{
	TListItem *tmp = src->cur->next;
	src->cur->next = tmp->next;
	tmp->prev = src->cur->prev;
	if( src->cur->next != NULL ){
		src->cur->next->prev = src->cur;
	}
	if( tmp->prev != NULL ){
		tmp->prev->next = tmp;
	}
	tmp->next = src->cur;
	src->cur->prev = tmp;
	if( src->cur == src->head ){
		src->head = tmp;
	}
	return 0;
}
