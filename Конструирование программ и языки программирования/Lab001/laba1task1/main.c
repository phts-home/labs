#include <stdio.h>


int main(int argc, char *argv[])
{
    int n;
    printf("Puts size of array: "); scanf("%d",&n);
    int a[n];
    printf("Puts array: ");
    for(int i=0;i<n;i++) scanf("%d",&a[i]);

    int max = a[0], posMax = 0, min = a[0], posMin = 0;
	for(int i=1;i<n;i++){
        if(max<a[i]){
            max = a[i];
            posMax = i;
        }
        if(min>a[i]){
            min = a[i];
            posMin = i;
        }
    }

    int temp;
    if(posMax>posMin){
        for(int i=posMin+1;i<posMax-1;i++){
            for(int j=i;j<=posMax-1;j++){
                if(a[i]>a[j]){
                    temp = a[j];
                    a[j] = a[i];
                    a[i] = temp;
                }
            }

        }
    }
    else{
        for(int i=posMax+1;i<posMin-1;i++){
            for(int j=i;j<=posMin-1;j++){
                if(a[i]<a[j]){
                    temp = a[j];
                    a[j] = a[i];
                    a[i] = temp;
                }
            }
        }
    }

    printf("Result: ");
    for(int i=0;i<n;i++){
        printf("%d ",a[i]);
    }
    printf("\n");

    return 0;
}
