#include <stdio.h>


int main(int argc, char *argv[])
{
    int n;
    printf("Puts size of array: "); scanf("%d",&n);
    int a[n];
    printf("Puts array: ");
    for(int i=0;i<n;i++) scanf("%d",&a[i]);

    int max = a[0], posMax = 0, min = a[0], posMin = 0;
	for(int i=1;i<n;i++){
        if(max<a[i]){
            max = a[i];
            pos = i;
        }
        if(min>a[i]){
            min = a[i];
            posMin = i;
        }
    }

    int temp = max;
    a[posMax] = a[posMin];
    a[posMin] = temp;

    printf("Result:\n"
    for(int i=0;i<n;i++){
        printf("%d ",a[i]);
    }

    printf("Value of maximum: %d,\nhis position: %d\n",max,pos);
    return 0;
}

