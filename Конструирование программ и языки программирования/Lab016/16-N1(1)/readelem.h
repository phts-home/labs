/*
******************************************************************************
*
* File                            readelem.h
*
* Description                     ���������� ������� ����������� ����� ������
*
* Creation date                   30.11.2007
*
* Author                          ������ �����
*
* Other                           ������������ ������ #16
*                                 ������� #1
*                                 ������� #1
*
******************************************************************************
*/


/*
******************************************************************************
*                                 MODULE BEGIN
******************************************************************************
*/
#ifndef READELEM_H
#define READELEM_H


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "interface.h"


/*
******************************************************************************
*                                 GLOBAL PROTOTYPES
******************************************************************************
*/
int   readElem(TList *, int);


/*
******************************************************************************
*                                 MODULE END
******************************************************************************
*/ 
#endif
