/*
******************************************************************************
*
* File                            list.h
*
* Description                     ���������� ������������ ��������� ������
*
* Creation date                   30.11.2007
*
* Author                          ������ �����
*
* Other                           ������������ ������ #16
*                                 ������� #1
*                                 ������� #1
*
******************************************************************************
*/


/*
******************************************************************************
*                                 MODULE BEGIN
******************************************************************************
*/
#ifndef LIST_H
#define LIST_H


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/*
******************************************************************************
*                                 GLOBAL DATA TYPES
******************************************************************************
*/
typedef struct {
   char lname[16];                // �������
   char fname[16];                // ���
   char pname[16];                // ��������
   long num;                      // ����� �������
   char date[11];                 // ����
} TData;
typedef struct _TListItem{
   TData data;                    // ������ ��������
   struct _TListItem *prev;       // ���������� ������� � ������
   struct _TListItem *next;       // ��������� ������� � ������
} TListItem;
typedef struct {
   int size;                      // ���������� ���������
   int row;                       // ����, �� �������� ����������� ������
   int dir;                       // ����������� ����������
   TListItem *head;               // ������ ������� � ������
   TListItem *cur;                // ������� ������� � ������
} TList;


/*
******************************************************************************
*                                 GLOBAL PROTOTYPES
******************************************************************************
*/
TList *initList (void);
int   destroyList(TList *);
int   clearList(TList *);

int   addElem(TList *, TData);
int   insertElem(TList *, TData, int);
int   deleteElem(TList *src, int);
int   sortList(TList *);

int   moveNext(TList *);
int   movePrev(TList *);
int   moveFirst(TList *);
int   moveLast(TList *);
int   getSize(TList *);
TData getData(TList *);

int   saveList(TList *, FILE *);
int   loadList(TList *, FILE *);


/*
******************************************************************************
*                                 MODULE END
******************************************************************************
*/ 
#endif
