/*
******************************************************************************
*
* File                            main.c
*
* Description                     ���������� ������� main
*
* Creation date                   30.11.2007
*
* Author                          ������ �����
*
* Other                           ������������ ������ #16
*                                 ������� #1
*                                 ������� #1
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include "interface.h"
#include "list.h"


/*
******************************************************************************
*                                 MAIN FUNCTION
******************************************************************************
*/

int main(int argc, char *argv[])
{
	TList *list1 = initList();

	// DEBUG
	//loadList(list1, 
	/*
	TData d;

	strcpy(d.fname,"john");
	strcpy(d.lname,"john2");
	strcpy(d.pname,"john3");
	strcpy(d.date,"13.12.2007");
	d.num = 123456;
	addElem(list1,d);

	strcpy(d.fname,"peter");
	strcpy(d.lname,"peter2");
	strcpy(d.pname,"peter3");
	strcpy(d.date,"10.10.2000");
	d.num = 112233;
	addElem(list1,d);

	strcpy(d.fname,"adam");
	strcpy(d.lname,"adam2");
	strcpy(d.pname,"adam3");
	strcpy(d.date,"09.10.2009");
	d.num = 235678;
	addElem(list1,d);

	deleteElem(list1, 1);
	//*/

	drwMenu(list1, "");

	destroyList(list1);

	// �����
	fflush(stdin);
	printf("\n> Press any key to quit\n");
	_getch();
	return 0;
}
