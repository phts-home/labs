/*
******************************************************************************
*
* File                            readelem.c
*
* Description                     ���� ���������� ������ readelem.h
*
* Creation date                   30.11.2007
* 
* Author                          ������ �����
*
* Other                           ������������ ������ #16
*                                 ������� #1
*                                 ������� #1
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include "readelem.h"


/*
******************************************************************************
*                                 LOCAL FUNCTIONS
******************************************************************************
*/

//
// ������������� ������� ��������
//
static  int RE_InitInsElem(TList *src, TData data);


/*
******************************************************************************
*                                 GLOBAL FUNCTIONS
******************************************************************************
*/

//
// ���������� ������ ��������
//
//    ������� ���������
//       src                      ������
//       mode                     �����
//          1                        ���������� �������� � ����� ������
//          2                        �������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        ������ ��� ������� ������
//       2                        ���� ���������
//
int readElem(TList *src, int mode)
{
	//
	// line[257]                   ��������������� ������ � ���������� �����
	// word[257]                   ������� � line
	// tmpTok[5]                   ��������� ����������, ��� �������� ���� �� ������������
	// tmpWord[11]                 ��������� ����������, ����������� ���� date
	// �                           ��������� ���������� ��� ����������� �����
	// k                           ������� �������
	// kt                          ���������� ������������� ��������� k
	// ival                        �������� �������� ������ ����
	// curData                     ��������� � ������� �� ������� ������ line
	//
	char line[257], word[257], tmpTok[5], tmpWord[11];
	int k, kt, ival;
	char *c = NULL;
	TData curData;

	fflush(stdin);
	gets(line);
	fflush(stdin);

	if (strcmp(line,"") == 0) return 2;

	// ����� RECORD_BOOK_NO
	k = 0;
	while( (line[k] != '[') && (line[k] != 0) ) k++;
	if (k == strlen(line)) {
		printf("\n> Error. RECORD_BOOK_NO is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	kt = k+1;
	k++;
	while ( (line[k] != ']') && (line[k] != 0) ) k++;
	if (kt == strlen(line)) {
		printf("\n> Error. RECORD_BOOK_NO is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if (strlen(word) != 6) {
		printf("\n> Error. Length of RECORD_BOOK_NO is 6 figures\n",word);
		printf("> Try again\n\n");
		return 1;
	}
	if (atol(word) == 0){
		printf("\n> Error. RECORD_BOOK_NO must be of integer type\n");
		printf("> Try again\n\n");
		return 1;
	}
	curData.num = atoi(word);

	// ����� DATE
	k++;
	while ( ((line[k] == ' ') || (line[k] == ',')) && (line[k] != 0)) k++;
	if (k == strlen(line)) {
		printf("\n> Error. DATE is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	kt = k;
	k++;
	while ( (line[k] != ' ') && (line[k] != 0) ) k++;
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if (strcmp(word,"") == 0) {
		printf("\n> Error. DATE is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	if (!((word[0] != '.')&&(word[1] != '.')&&(word[2] == '.')&&
			(word[3] != '.')&&(word[4] != '.')&&(word[5] == '.')&&
			(word[6] != '.')&&(word[7] != '.')&&(word[8] != '.')&&
			(word[9] != '.'))) {
		printf("\n> Error. DATE format must be DD.MM.YYYY\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(tmpWord,word);
	c = strtok(tmpWord,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if ((ival <= 0)||(ival > 31)) {
		printf("\n> Error. \"%s\" is wrong value of a DAY\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if ((ival <= 0)||(ival > 13)) {
		printf("\n> Error. \"%s\" is wrong value of a MOUNTH\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if ((ival <= 0)||(ival > 9999)) {
		printf("\n> Error. \"%s\" is wrong value of a YEAR\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(curData.date,word);

	// ����� LAST_NAME
	c = strtok(line," ");
	strcpy(word,c);
	if (strlen(word) > 15) {
		printf("\n> Error. Maximum length of LAST_NAME is 15 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(curData.lname,word);

	// ����� FIRST_NAME
	c = strtok(NULL," ");
	strcpy(word,c);
	if (strlen(word) > 15) {
		printf("\n> Error. Maximum length of FIRST_NAME is 15 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(curData.fname,word);

	// ����� PATRONYMIC
	c = strtok(NULL," ");
	strcpy(word,c);
	if (strlen(word) > 15) {
		printf("\n> Error. Maximum length of PATRONYMIC is 15 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(curData.pname,word);

	// ���������� � ����� | ������� ��������
	switch(mode){
		case 1:
			addElem(src,curData);
			break;
		case 2:
			RE_InitInsElem(src,curData);
			break;
	}

	return 0;
}


/*
******************************************************************************
*                                 LOCAL FUNCTIONS
******************************************************************************
*/

//
// ������������� ������� ��������
//
static  int RE_InitInsElem(TList *src, TData data)
{
	// �������� ������
	execPrintList(src, "");

	// �������� ������ ��������
	printf("> Set index\n> ");
	int n = 0;
	scanf("%d",&n);

	printf("> Processing...\n");

	// ��������� ����� �������
	insertElem(src,data,n);

	// �����
	printf("> Done\n\n");
	return 0;
}
