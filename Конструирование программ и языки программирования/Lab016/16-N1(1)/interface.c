/*
******************************************************************************
*
* File                            interface.c
*
* Description                     ���� ���������� ������ interface.h
*
* Creation date                   30.11.2007
*
* Author                          ������ �����
*
* Other                           ������������ ������ #16
*                                 ������� #1
*                                 ������� #1
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include "interface.h"


/*
******************************************************************************
*                                 LOCAL PROTOTYPES
******************************************************************************
*/
static  int   IF_PrintElem(TData, int);
static  int   IF_InitInsElem(TList *, TData);


/*
******************************************************************************
*                                 GLOBAL FUNCTIONS
******************************************************************************
*/

//
// ��������� ����
//
int drwMenu(TList *src, char *cpt)
{
	int ch, canExit = 0;
	while (!canExit) {
		printf("--- Menu for %s ---\n",cpt);
		printf("1 - Add elements\n");
		printf("2 - Insert element\n");
		printf("3 - Delete element\n");
		printf("4 - Sort\n");
		printf("5 - Print\n");
		printf("6 - Load from file\n");
		printf("7 - Save to file\n");
		printf("0 - Exit\n");
		printf("---------------------\n");
		printf("> Your Choice   ");
		scanf("%d",&ch);
		printf("\n");
		switch(ch){
			case 1:{
				execInputList(src, cpt);
				break;
			}
			case 2:{
				execInsertElem(src, cpt);
				break;
			}
			case 3:{
				execDeleteElem(src, cpt);
				break;
			}
			case 4:{
				execSortList(src, cpt);
				break;
			}
			case 5:{
				execPrintList(src, cpt);
				break;
			}
			case 6:{
				execLoadList(src, cpt);
				break;
			}
			case 7:{
				execSaveList(src, cpt);
				break;
			}
			case 0:{
				canExit = 1;
				break;
			}
		}
	}
	return 0;
}

//
// ���������� �����
//
int execInputList(TList *src, char *cpt)
{
	int err = 0;

	// �������� ���������
	printf("> INPUT ELEMENTS IN LIST %s\n",cpt);

	// ��������� ��������
	printf("> Enter elements of list\n");
	printf("> Format: LAST_NAME FIRST_NAME PATRONYMIC [RECORD_BOOK_NO[int,length=6]], DATE[format:DD.MM.YYYY]\n");
	printf("> Enter an empty string to exit to menu\n");
	while (err != 2) {
		do {
			err = readElem(src,1);
		} while (err == 1);
	}

	// �����
	printf("> Done\n\n");
	return 0;
}

//
// ����� ������ �� �����
//
int execPrintList(TList *src, char *cpt)
{
	// �������� ���������
	printf("> PRINT LIST %s\n",cpt);
	printf("> Printing elements...\n");
	// ���� ������ ���� -> �����
	if (!src->head) {
		printf("> No elements\n");
		printf("> Done\n\n");
		return 0;
	}
	// ��������� �� ����� ������
	int index = 0;
	moveFirst(src);
	while (src->cur) {
		index++;
		IF_PrintElem(src->cur->data,index);
		moveNext(src);
	}
	moveLast(src);

	// �����
	printf("> Total: %d\n",src->size);
	printf("> Done\n\n");
	return 0;
}

//
// ������������� ������� �������� ��������
//
int execDeleteElem(TList *src, char *cpt)
{
	// �������� ���������
	printf("> DELETE ELEMENT FROM LIST %s\n",cpt);

	// �������� ������
	execPrintList(src, cpt);

	// �������� ������ ��������
	printf("> Set index of element to delete\n> ");
	int n = 0;
	scanf("%d",&n);

	printf("> Processing...\n");

	// �������
	deleteElem(src,n);

	// �����
	printf("> Done\n\n");
	return 0;
}

//
// ������� �������� � ������
//
int execInsertElem(TList *src, char *cpt)
{
	int err = 0;

	// �������� ���������
	printf("> INSERT ELEMENT TO LIST %d\n",cpt);

	// ��������� �������
	printf("> Format: LAST_NAME FIRST_NAME PATRONYMIC [RECORD_BOOK_NO[int,length=6]], DATE[format:DD.MM.YYYY]\n");
	do {
		err = readElem(src,2);
	} while (err == 1);

	// �����
	printf("> Done\n\n");
	return 0;
}

//
// ������������� ���������� ���������
//
int execSortList(TList *src, char *cpt)
{
	//
	// row                         ���� ����������
	//    1                           LAST_NAME
	//    2                           FIRST_NAME
	//    3                           PATRONYMIC
	//    4                           RECORD_BOOK_NO
	//    5                           DATE
	// dir                         ����������� ����������
	//    1                           �� �����������
	//    0                           �� ��������
	//
	int dir = -1, row = -1;

	// �������� ���������
	printf("> SORT LIST %s\n",cpt);

	// �������� ������ ����
	while ( (row < 1) || (row > 5) ) {
		printf("> Set row [1-LAST_NAME | 2-FIRST_NAME | 3-PATRONYMIC | 4-RECORD_BOOK_NO | 5-DATE]\n> ");
		scanf("%d",&row);
	}
	src->row = row;

	// �������� �����������
	while ( (dir != 1) && (dir != 0) ) {
		printf("> Set direction [1-Direct | 0-Indirect]\n> ");
		scanf("%d",&dir);
	}
	src->dir = dir;

	printf("> Sorting...\n");

	// ���������
	sortList(src);

	// �����
	printf("> Done\n\n");
	return 0;
}

//
// ������ ������� ���������� ������ �� �����
//
int execSaveList(TList *src, char *cpt)
{
	// �������� ���������
	printf("> SAVE LIST %s\n",cpt);

	char path[257];
	FILE *f = NULL;

	// �������� ��� �����
	while (!f) {
		printf("> Enter filename\n> ");
		scanf("%s", path);
		f = fopen(path, "wb");
	}

	// ���������
	saveList(src, f);

	// �����
	printf("> Done\n\n");
	return 0;
}

//
// ������ ������� ���������� ������ � ����
//
int execLoadList(TList *src, char *cpt)
{
	// �������� ���������
	printf("> LOAD LIST %s\n",cpt);

	char path[257];

	printf("> Enter filename\n> ");
	scanf("%s", &path);
	FILE *f = fopen(path, "rb");

	// �������� ��� �����
	while (!f) {
		printf("> Unable to open the file. Try again\n> ");
		//printf("> Enter filename\n> ");
		scanf("%s", &path);
		f = fopen(path, "rb");
		//if (!f) {
			//printf("> Unable to open the file. Try again\n");
		//}
	}

	// ���������
	loadList(src, f);

	// �����
	printf("> Done\n\n");
	return 0;
}


/*
******************************************************************************
*                                 LOCAL FUNCTIONS
******************************************************************************
*/

//
// ������ ������ ��������
//
static  int IF_PrintElem(TData data, int index)
{
	printf("%.2d | %s %s %s [%d], %s\n",index,data.lname,data.fname,data.pname,data.num,data.date);
	return 0;
}
