/*
******************************************************************************
*
* File                            list.c
*
* Description                     ���� ���������� ������ list.h
*
* Creation date                   30.11.2007
* 
* Author                          ������ �����
*
* Other                           ������������ ������ #16
*                                 ������� #1
*                                 ������� #1
*
******************************************************************************
*/


/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - <nothing>
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include "list.h"


/*
******************************************************************************
*                                 LOCAL PROTOTYPES
******************************************************************************
*/
static  int   Lst_ExchElem(TList *);


/*
******************************************************************************
*                                 GLOBAL FUNCTIONS
******************************************************************************
*/

//
// ������������� ������
//
TList *initList(void)
{
	TList *list = (TList *)malloc(sizeof(TList));
	list->cur = NULL;
	list->head = NULL;
	list->size = 0;
	list->row = 1;
	list->dir = 1;
	return list;
}

//
// �������� ������
//
int destroyList(TList *src)
{
	while (src->head) {
		src->cur = src->head;
		src->head = src->head->next;
		free(src->cur);
	}
	src->cur = NULL;
	src->head = NULL;
	free(src);
	return 0;
}

//
// ������� ������
//
int clearList(TList *src)
{
	while (src->head) {
		src->cur = src->head;
		src->head = src->head->next;
		free(src->cur);
	}
	src->cur = NULL;
	src->head = NULL;
	src->size = 0;
	return 0;
}

//
// ���������� �������� � ����� ������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        �� ������� �������� ������
//
int addElem(TList *src, TData data)
{
	TListItem *tmp = (TListItem *)malloc(sizeof(TListItem));
	if (!tmp) return 1;
	if (!src->head) {
		src->head = tmp;
		tmp->prev = NULL;
		tmp->next = NULL;
	} else {
		moveLast(src);
		src->cur->next = tmp;
		tmp->prev = src->cur;
		tmp->next = NULL;
	}
	tmp->data = data;
	src->cur = tmp;
	src->size++;
	return 0;
}

//
// ������� ��������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        �� ������� �������� ������
//
int insertElem(TList *src, TData data, int n)
{
	// ������������� ������� ������� �� ��� �������
	int index = 1;
	moveFirst(src);
	while ( (src->cur) && (index != n) ) {
		index++;
		moveNext(src);
	}

	// ���� �������� � ����� �������� ��� -> ��������� ������� � ����� ������
	if (!src->cur) return addElem(src,data);

	TListItem *tmp=(TListItem *)malloc(sizeof(TListItem));

	if (!tmp) return 1;

	// ��������� ������� ����� �������
	tmp->next = src->cur;
	tmp->prev = src->cur->prev;
	src->cur->prev = tmp;
	if (tmp->prev) {
		tmp->prev->next = tmp;
	} else {
		src->head = tmp;
	}
	tmp->data = data;
	src->cur = tmp;
	src->size++;

	// ������������ � ����� ������
	moveLast(src);

	return 0;
}

//
// �������� ��������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        ��� �������� � ����� ��������
//
int deleteElem(TList *src, int n)
{
	// ������������� ������� ������� �� ��� �������
	int index = 1;
	moveFirst(src);
	while ( (src->cur) && (index != n) ) {
		index++;
		moveNext(src);
	}

	// ���� �������� ��� � ����� �������� ��� -> �����
	if (!src->cur) return 1;

	TListItem *tmp = src->cur->prev;

	// ���� ������� ������� - ������ ������
	if (!tmp) {
		src->head = src->head->next;
	} else {
		tmp->next = src->cur->next;
		if(src->cur->next) src->cur->next->prev = tmp;
	}
	// ������� ������� �������
	free(src->cur);
	src->cur = tmp;
	src->size--;

	// ������������ � ����� ������
	moveLast(src);

	return 0;
}

//
// ���������� ���������
//
int sortList(TList *src)
{
	//
	// d1, d2, m1, m2, y1, y2      �������� ���, ������, ���� � ����� date[11] 
	//                                ���� ��������� ������� �����.
	// tmpTok[5]                   ��������� ����������, �������� d1, d2, m1, 
	//                                m2, y1, y2 ��� ����������� � �����������
	// tmpWord[11]                 ��������� ����������, ����������� ���� date
	//

	// ���� ������ ���� -> �����
	if (!src->head) return 1;

	int d1, d2, m1, m2, y1, y2;
	char tmpTok[5], tmpWord[11];
	char *c = NULL;
	int canExit = 0;

	while (!canExit) {
		canExit = 1;
		moveFirst(src);
		while(src->cur->next){
			switch(src->row){
				case 1:
					// ���������� �� LAST_NAME
					if (src->dir == 1) {
						if (strcmp(src->cur->data.lname,src->cur->next->data.lname) >= 0) {
							Lst_ExchElem(src);
							canExit = 0;
						} else {
							src->cur = src->cur->next;
						}
					} else {
						if (strcmp(src->cur->data.lname,src->cur->next->data.lname) <= 0) {
							Lst_ExchElem(src);
							canExit = 0;
						} else {
							src->cur = src->cur->next;
						}
					}
					break;
				case 2:
					// ���������� �� FIRST_NAME
					if (src->dir == 1) {
						if (strcmp(src->cur->data.fname,src->cur->next->data.fname) >= 0) {
							Lst_ExchElem(src);
							canExit = 0;
						} else {
							src->cur = src->cur->next;
						}
					} else {
						if (strcmp(src->cur->data.fname,src->cur->next->data.fname) <= 0) {
							Lst_ExchElem(src);
							canExit = 0;
						} else {
							src->cur = src->cur->next;
						}
					}
					break;
				case 3:
					// ���������� �� PATRONYMIC
					if (src->dir == 1) {
						if (strcmp(src->cur->data.pname,src->cur->next->data.pname) >= 0) {
							Lst_ExchElem(src);
							canExit = 0;
						} else {
							src->cur = src->cur->next;
						}
					} else {
						if (strcmp(src->cur->data.pname,src->cur->next->data.pname) <= 0) {
							Lst_ExchElem(src);
							canExit = 0;
						} else {
							src->cur = src->cur->next;
						}
					}
					break;
				case 4:
					// ���������� �� RECORD_BOOK_NO
					if (src->dir == 1) {
						if (src->cur->data.num >= src->cur->next->data.num) {
							Lst_ExchElem(src);
							canExit = 0;
						} else {
							src->cur = src->cur->next;
						}
					} else {
						if (src->cur->data.num <= src->cur->next->data.num) {
							Lst_ExchElem(src);
							canExit = 0;
						} else {
							src->cur = src->cur->next;
						}
					}
					break;
				case 5:
					// ���������� �� DATE
					// ��������� ����_���_������ 1-�� �������� �� ����������_���_�����
					// ����������� ���� � ������������� ����������
					strcpy(tmpWord,src->cur->data.date);
					// ����� <���>
					c = strtok(tmpWord,".");
					strcpy(tmpTok,c);
					d1 = atoi(tmpTok);
					// ����� <������>
					c = strtok(NULL,".");
					strcpy(tmpTok,c);
					m1 = atoi(tmpTok);
					// ����� <����>
					c = strtok(NULL,".");
					strcpy(tmpTok,c);
					y1 = atoi(tmpTok);
					// ��������� ����_���_������ 1-�� �������� �� ����������_���_�����
					// ����������� ���� � ������������� ����������
					strcpy(tmpWord,src->cur->next->data.date);
					// ����� <���>
					c = strtok(tmpWord,".");
					strcpy(tmpTok,c);
					d2 = atoi(tmpTok);
					// ����� <������>
					c = strtok(NULL,".");
					strcpy(tmpTok,c);
					m2 = atoi(tmpTok);
					// ����� <����>
					c = strtok(NULL,".");
					strcpy(tmpTok,c);
					y2 = atoi(tmpTok);
					if ( ( 
							( (y1>y2)||((y1==y2)&&(m1>m2))||((y1==y2)&&(m1==m2)&&(d1>d2)) )&&(src->dir == 1) )
							|| ( 
							( (y1<y2)||((y1==y2)&&(m1<m2))||((y1==y2)&&(m1==m2)&&(d1<d2)) )&&(src->dir == 0) ) ) {
						Lst_ExchElem(src);
						canExit = 0;
					} else {
						src->cur = src->cur->next;
					}
					break;
			}
		}
	}
	return 0;
}

//
// ����������� �� ��������� �������
//
int moveNext(TList *src)
{
	if (src->head) {
		if (src->cur) {
			src->cur = src->cur->next;
		}
	}
	return 0;
}

//
// ����������� �� ���������� �������
//
int movePrev(TList *src)
{
	if (src->head) {
		if (src->cur->prev) {
			src->cur = src->cur->prev;
		}
	}
	return 0;
}

//
// ����������� �� ������ �������
//
int moveFirst(TList *src)
{
	if (src->head) {
		src->cur = src->head;
	}
	return 0;
}

//
// ����������� �� ��������� �������
//
int moveLast(TList *src)
{
	if (src->head) {
		moveFirst(src);
		while (src->cur->next) {
			moveNext(src);
		}
	}
	return 0;
}

//
// ������ ������
//
int getSize(TList *src)
{
	if (!src) return -1;
	return src->size;
}

//
// ������ �������� ��������
//
TData getData(TList *src)
{
	return src->cur->data;
}

//
// ���������� ������ � ����
//
int saveList(TList *src, FILE *buf)
{
	// ��������� �� ����� ������
	moveFirst(src);
	while (src->cur) {
		fwrite(&src->cur->data, sizeof(TData), 1, buf);
		moveNext(src);
	}

	// ������������ � ����� ������
	moveLast(src);

	return 0;
}

//
// �������� ������ �� �����
//
int loadList(TList *src, FILE *buf)
{
	TData curData;

	// ������� ������
	clearList(src);

	// ���� �� ����� �����
	while (1) {
		fread(&curData, sizeof(TData), 1, buf);
		if (feof(buf) != 0) {
			break;
		}
		addElem(src, curData);
	}

	return 0;
}


/*
******************************************************************************
*                                 LOCAL FUNCTIONS
******************************************************************************
*/

//
// ������������ ���� ��������� � ������
//
static  int Lst_ExchElem(TList *src)
{
	TListItem *tmp = src->cur->next;
	src->cur->next = tmp->next;
	tmp->prev = src->cur->prev;
	if (src->cur->next != NULL) {
		src->cur->next->prev = src->cur;
	}
	if (tmp->prev != NULL) {
		tmp->prev->next = tmp;
	}
	tmp->next = src->cur;
	src->cur->prev = tmp;
	if (src->cur == src->head) {
		src->head = tmp;
	}
	return 0;
}
