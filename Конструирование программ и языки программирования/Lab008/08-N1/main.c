#include <stdio.h>
#include <stdarg.h>


int maxmin(double *, double *, int, ...);


int main(int argc, char *argv[])
{
	double max,min;

	maxmin(&max,&min,5,3.0,5.0,2.0,1.0,2.0);
	printf("> max == %lf\n> min == %lf\n\n",max,min);
	return 0;
}

int maxmin(double *resmax, double *resmin, int amount, ...)
{
	va_list val;
	double cur;
	va_start(val,amount);
	cur = va_arg(val,double);
	(*resmin) = cur;
	(*resmax) = cur;

	for(int i=1;i<amount;i++){
		cur = va_arg(val,double);
		if(cur>(*resmax))(*resmax) = cur;
		if(cur<(*resmin))(*resmin) = cur;
	}

	va_end(val);
	return 0;
}

