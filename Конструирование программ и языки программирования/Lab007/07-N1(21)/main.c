#include <stdio.h>
#include <string.h>


char *wordN(char *, int);


int main(int argc, char *argv[])
{
	fflush(stdin);
	char line[256];
	printf("> Enter String (char)\n> ");
	gets(line);

	unsigned k1, k2, k3;
	printf("\n> Enter {k1} (unsigned)\n> ");
	scanf("%d",&k1);
	printf("> Enter {k2} (unsigned)\n> ");
	scanf("%d",&k2);
	printf("> Enter {k3} (unsigned)\n> ");
	scanf("%d",&k3);

	char word[256];
	printf("\n> Printing..\n");
	strcpy(word,wordN(line, k1));
	printf("%s\n",word);
	strcpy(word,wordN(line, k2));
	printf("%s\n",word);
	strcpy(word,wordN(line, k3));
	printf("%s\n",word);

	printf("\n");
	return 0;
}

char *wordN(char *source, int num)
{
	char res[256] = "";
	char tmpStr[256];
	char *c = NULL;
	strcpy(tmpStr,source);

	c = strtok(tmpStr," ,.;:!?");
	if(c != NULL)
		strcpy(res,c);
	for(int i=1;i<num;i++){
		c = strtok(NULL," ,.;:!?");
		if(c != NULL)
			strcpy(res,c);
		else
			return "";
	}

	return res;
}

