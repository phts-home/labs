#include <stdio.h>
#include <string.h>
#include <stdlib.h>


typedef struct {char name[16]; char unit[6]; double price; unsigned amount;} tRec;


void inputArray(tRec*, unsigned);
void outputArray(tRec*, unsigned);
int sort(tRec*, unsigned);


int main(int argc, char *argv[])
{
	//Init {size}
	unsigned size;
	printf("> Enter Size Of Array (unsigned)\n> ");
	scanf("%d",&size);
	printf("\n");
	if(size<1){
		printf("> Error\n");
		return 0;
	}

	//Init {ar}
	tRec *ar = NULL;
	ar = (tRec *)calloc(size, sizeof(tRec));


	inputArray(ar, size);

	sort(ar, size);

	outputArray(ar, size);

	return 0;
}

void inputArray(tRec *source, unsigned size)
{
	printf("> Enter Elements Of Array (tMess)\n");
	printf("> Format: NAME(string 15) UNIT(string 5) PRICE(double) AMOUNT(unsigned)\n");
	char line[257], tmpWord[257];
	char *c = NULL;
	for(int i=0;i<size;i++){
		fflush(stdin);
		gets(line);
		fflush(stdin);
		
		c = strtok(line," ");
		strcpy(source[i].name,c);

		c = strtok(NULL," ");
		strcpy(source[i].unit,c);
		
		c = strtok(NULL," ");
		strcpy(tmpWord,c);
		source[i].price = atof(tmpWord);

		c = strtok(NULL," ");
		strcpy(tmpWord,c);
		source[i].amount = atoi(tmpWord);
	}
	printf("\n");
}

void outputArray(tRec *source, unsigned size)
{
	printf("> Printing Elements...\n");
	for(int i=0;i<size;i++)
		printf("%s %s %.2lf %u\n", source[i].name, source[i].unit, source[i].price, source[i].amount);
	printf("\n");
}

int sort(tRec *source, unsigned size)
{
	//Sort array by Name
	tRec tmpItem;
	int direct;
	printf("> Set Direction Of Sort By Name [1-Direct | 0-Indirect]\n> ");
	scanf("%d",&direct);
	if((direct!=0)&&(direct!=1)){
		printf("\n> Error\n");
		return 0;
	}
	for(int i=0;i<size-1;i++)
		for(int j=i+1;j<size;j++)
			if(direct){
				if(strcmp(source[i].name,source[j].name) > 0){
					tmpItem = source[i];
					source[i] = source[j];
					source[j] = tmpItem;
				}
			}else{
				if(strcmp(source[i].name,source[j].name) < 0){
					tmpItem = source[i];
					source[i] = source[j];
					source[j] = tmpItem;
				}
			}
		
	
	printf("\n");

	//Sort array by Price
	printf("> Set Direction Of Sort By Price [1-Direct | 0-Indirect]\n> ");
	scanf("%d",&direct);
	if((direct!=0)&&(direct!=1)){
		printf("\n> Error\n");
		return 0;
	}
	for(int i=0;i<size-1;i++)
		for(int j=i+1;j<size;j++)
			if(strcmp(source[i].name,source[j].name) == 0){
				if(direct){
					if(source[i].price > source[j].price){
						tmpItem = source[i];
						source[i] = source[j];
						source[j] = tmpItem;
					}
				}else{
					if(source[i].price < source[j].price){
						tmpItem = source[i];
						source[i] = source[j];
						source[j] = tmpItem;
					}
				}
			}
	
	printf("\n");
	return 0;
}
