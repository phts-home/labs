#include <stdio.h>
#include <stdlib.h>


int pasteLine(double **, unsigned *, unsigned, unsigned, double *);
double **initArray(unsigned, unsigned);
int inputArray(double **, unsigned, unsigned);
int outputArray(double **, unsigned, unsigned);


int main(int argc, char *argv[])
{
	unsigned sizeM, sizeN;
	printf("> Enter Size Of Array (unsigned, unsigned)\n> ");
	scanf("%d %d",&sizeM,&sizeN);
	printf("\n");

	double **ar;
	ar = initArray(sizeM,sizeN);
	inputArray(ar,sizeM,sizeN);
	outputArray(ar,sizeM,sizeN);

	unsigned strNum;
	printf("> Enter Number Of Strings (unsigned)\n");
	scanf("%d",&strNum);
	printf("\n");

	double line[sizeN];
	unsigned pos;
	int res;
	for(int i=0;i<strNum;i++){
		printf("> Enter Values #%d (double)\n",i+1);
		for(int j=0;j<sizeN;j++)scanf("%lf",&line[j]);

		printf("\n");
		printf("> Enter Position (unsigned)\n");
		scanf("%d",&pos);

		res = pasteLine(ar,&sizeM,sizeN,pos,line);
		if(res == 1)printf("> Error\n");

		outputArray(ar,sizeM,sizeN);

		printf("\n");
	}

	outputArray(ar,sizeM,sizeN);
	return 0;
}

int pasteLine(double **source, unsigned *height, unsigned width, unsigned num, double *values)
{
	if(num>(*height)) return 1;
	(*height)++;

	source[(*height)] = (double *)calloc(width, sizeof(double));
	for(int i=(*height)-1;i>num;i--) source[i] = source[i-1];

	source[num] = (double *)calloc(width, sizeof(double));
	for(int i=0;i<width;i++) source[num][i] = values[i];


/*
	source = realloc(source,(*height)*sizeof(double));
	source[(*height)-1] = NULL;
//	source[(*height)-1] = (double *)calloc(width, sizeof(double));

//	source = realloc(source,(*height)*sizeof(double));
//	source = (double **)calloc((*height), sizeof(double *));
//	source[(*height)] = (double *)calloc(width, sizeof(double));

//	source = realloc(source,(*height)*sizeof(double));
//	source[(*height)-1] = (double *)calloc(width, sizeof(double));

//	source[(*height)-1] = NULL;
	for(int i=(*height)-1;i>num;i--){
//		for(int j=0;j<width;j++){
//			printf("> !!!!!\n");
//			printf("%.2lf\n",source[i-1][j]);
			outputArray(source,(*height),width);
			source[i] = source[i-1];
//			printf("> &&&&&\n");
	}
	source[num] = (double *)calloc(width, sizeof(double));
	for(int i=0;i<width;i++) source[num][i] = values[i];
	*/
	return 0;
}

double **initArray(unsigned height, unsigned width)
{
	double **source;
	source = (double **)calloc(height, sizeof(double *));
	for(int i=0;i<height;i++) source[i] = (double *)calloc(width, sizeof(double));
	return source;
}

int inputArray(double **source, unsigned height, unsigned width)
{
	printf("> Enter Elements Of Array (double)\n");
	for(int i=0;i<height;i++)
		for(int j=0;j<width;j++)
			scanf("%lf",&source[i][j]);
	printf("\n");
	return 0;
}

int outputArray(double **source, unsigned height, unsigned width)
{
	printf("> Printing...\n");
	for(int i=0;i<height;i++){
		for(int j=0;j<width;j++) 
			printf("%.2lf\t",source[i][j]);
		printf("\n");
	}
	printf("\n");
	return 0;
}
