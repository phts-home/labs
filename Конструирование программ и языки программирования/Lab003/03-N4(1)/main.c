#include <stdio.h>
#include <conio.h>


int main(int argc, char *argv[])
{
	unsigned char key;
	int item;
	item = 0;
   while (1){
		_textbackground(1);
		_textcolor(12);
		_gotoxy(32,5);
		_cprintf("  Menu  ");
		_textcolor(7);
		switch (item){
			case 0:{
				_textbackground(4);
				_gotoxy(30,7);
				_cprintf("New project");
				_textbackground(0);
				_gotoxy(30,8);
				_cprintf("Open project...");
				_gotoxy(30,9);
				_cprintf("Save");
				_gotoxy(30,10);
				_cprintf("Save As...");
				_gotoxy(30,11);
				_cprintf("Exit\n");
				break;
			}
			case 1:{
				_textbackground(0);
				_gotoxy(30,7);
				_cprintf("New project");
				_textbackground(4);
				_gotoxy(30,8);
				_cprintf("Open project...");
				_textbackground(0);
				_gotoxy(30,9);
				_cprintf("Save");
				_gotoxy(30,10);
				_cprintf("Save As...");
				_gotoxy(30,11);
				_cprintf("Exit\n");
				break;
			}
			case 2:{
				_textbackground(0);
				_gotoxy(30,7);
				_cprintf("New project");
				_gotoxy(30,8);
				_cprintf("Open project...");
				_textbackground(4);
				_gotoxy(30,9);
				_cprintf("Save");
				_textbackground(0);
				_gotoxy(30,10);
				_cprintf("Save As...");
				_gotoxy(30,11);
				_cprintf("Exit\n");

				break;
			}
			case 3:{
				_textbackground(0);
				_gotoxy(30,7);
				_cprintf("New project");
				_gotoxy(30,8);
				_cprintf("Open project...");
				_gotoxy(30,9);
				_cprintf("Save");
				_textbackground(4);
				_gotoxy(30,10);
				_cprintf("Save As...");
				_textbackground(0);
				_gotoxy(30,11);
				_cprintf("Exit\n");
				break;
			}
			case 4:{
				_textbackground(0);
				_gotoxy(30,7);
				_cprintf("New project");
				_gotoxy(30,8);
				_cprintf("Open project...");
				_gotoxy(30,9);
				_cprintf("Save");
				_gotoxy(30,10);
				_cprintf("Save As...");
				_textbackground(4);
				_gotoxy(30,11);
				_cprintf("Exit\n");
				break;
			}
		}
		_textbackground(0);
		key = _getch();
		switch (key){
			case 13:{
				switch (item){
					case 0:{_gotoxy(1,15);_cprintf("Item  'New Project'  selected\n");break;}
					case 1:{_gotoxy(1,15);_cprintf("Item  'Open Project'  selected\n");break;}
					case 2:{_gotoxy(1,15);_cprintf("Item  'Save'  selected\n");break;}
					case 3:{_gotoxy(1,15);_cprintf("Item  'Save As'  selected\n");break;}
					case 4:{_gotoxy(1,15);_cprintf("Item  'Exit'  selected\n");return 0;break;}
				}
				break;
			}
			case 72:{//up
				item -= 1;
				if(item<0){item = 4;};
				_clrscr();
				break;
			}
			case 80:{//down
				item ++;
				if(item>4){item = 0;};
				_clrscr();
				break;
			}
		}
	}
   return 0;
}
