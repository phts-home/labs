#include <stdio.h>
#include <math.h>


int main(int argc, char *argv[])
{
		long int n, k;
		int res,c;
		printf("n (long int) = ");
    scanf("%ld",&n);
		printf("k (long int) = ");
    scanf("%ld",&k);

		long int tmp = pow(10,n);
		tmp -= 1;

		res = 0;
		for(long int i = 0;i<=tmp;i++){
				long int num1 = i;
				int s = 0;
				while(num1>0){
						c = num1 % 10;
						s += c;
						num1 = num1 / 10;
				}
				if(s==k) res ++;
		}

		printf("Result = %d\n",res);
    return 0;
}

