/*
******************************************************************************
*       ������������ ������ #14. ������� #3. ������� #10
*
*       ��������: ������� ��. 06��-1 ��� ����� ������
******************************************************************************
*/
/*
******************************************************************************
*                                 TODOLIST
******************************************************************************
* - writeFile
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>


/*
******************************************************************************
                                  DATA TYPES
******************************************************************************
*/
typedef struct {
	char lname[16];                // �������
	char fname[16];                // ���
	char pname[16];                // ��������
	long num;                      // ����� �������
	char date[11];                 // ����
} TData;


/*
******************************************************************************
*                                 PROTOTYPES
******************************************************************************
*/
FILE *initFile(void);

int readFile(FILE *);

int printItem(TData, int);


/*
******************************************************************************
*                                 FUNCTIONS
******************************************************************************
*/

int main(int argc, char *argv[])
{
	FILE *file1 = initFile();

	readFile(file1);

	fclose(file1);
	fflush(stdin);
	printf("> Press any key to quit");
	_getch();
	return 0;
}

//
// ������������� �����
//
FILE *initFile(void)
{
	char fname[257];
	printf("> Enter filepath\n> ");
	scanf("%s",fname);
	FILE *file = fopen(fname,"rb");
	while( !file ){
		printf("> Error. Invalid filename. Try again.\n> ");
		scanf("%s",fname);
		file = fopen(fname,"rb");
	}
	printf("\n");
	return file;
}

//
// ������ �����
//
int readFile(FILE *fSource)
{
	TData data;
	int index = 1;

	printf("> Reading file...\n");
	if(feof(fSource) == 0){
		fread(&data,sizeof(TData),1,fSource);
	}
	while( feof(fSource) == 0 ){
		// ��������� ������ �� �����
		printItem(data,index);
		fread(&data,sizeof(TData),1,fSource);
		index++;
	}
	printf("> Done\n\n");
	return 0;
}

//
// ������ ������ ��������
//
int printItem(TData data, int index)
{
	printf("%.2d | %s %s %s [%d], %s\n",index,data.lname,data.fname,data.pname,data.num,data.date);
	return 0;
}
