/*
******************************************************************************
*       ������������ ������ #14. ������� #3. ������� #10
*
*       ��������: ������� ��. 06��-1 ��� ����� ������
******************************************************************************
*/
/*
******************************************************************************
*                                 TODOLIST
******************************************************************************
* - writeFile
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>


/*
******************************************************************************
                                  DATA TYPES
******************************************************************************
*/
typedef struct {
	char lname[16];                // �������
	char fname[16];                // ���
	char pname[16];                // ��������
	long num;                      // ����� �������
	char date[11];                 // ����
} TData;
typedef struct _TListItem{
   TData data;                    // ������ ��������
   struct _TListItem *prev;       // ���������� ������� � ������
   struct _TListItem *next;       // ��������� ������� � ������
} TListItem;
typedef struct {
   int dir;                       // ����������� ����������
   TListItem *head;               // ������ ������� � ������
   TListItem *cur;                // ������� ������� � ������
} TList;


/*
******************************************************************************
                                  VARIABLES
******************************************************************************
*/
TList *list1 = NULL;


/*
******************************************************************************
*                                 PROTOTYPES
******************************************************************************
*/
FILE *initFile(char []);
TList *initList (TList *);
int destroyList(TList *);

char *getPath(int, char *[], char []);
char *getResPath(char []);

int readSourceFile(FILE *);
int readItem(FILE *, TList *,  char [], int);
int writeFile(char [], TList *);

int printList(TList *);
int printItem(TData, int);

int addItem(TList *, TData);

int initBubbleSort(TList *);
int bubbleSort(TList *, int);
int exchangeElem(TList *);


/*
******************************************************************************
*                                 FUNCTIONS
******************************************************************************
*/

int main(int argc, char *argv[])
{
	list1 = initList(list1);

	char respath[257];
	char fn[257];
	strcpy(fn, getPath(argc, argv, respath));
	FILE *file1 = fopen(fn,"rt");
	if( file1 == NULL ){
		if( file1 ) printf("> Error. File is not found\n\n");
		file1 = initFile(respath);
	}

	printf("> Working...\n");

	readSourceFile(file1);
	initBubbleSort(list1);
	writeFile(respath,list1);

	fclose(file1);
	destroyList(list1);

	fflush(stdin);
	printf("> Press any key to quit");
	_getch();
	return 0;
}

//
// ������������� �����
//
FILE *initFile(char nameResFile[])
{
	char fname[257];
	printf("> Enter filepath\n> ");
	scanf("%s",fname);
	FILE *file = fopen(fname,"rt");
	while( !file ){
		printf("> Error. Invalid filename. Try again.\n> ");
		scanf("%s",fname);
		file = fopen(fname,"rt");
	}
	printf("\n");
	strcpy(nameResFile,getResPath(fname));
	return file;
}

//
// ������������� ������
//
TList *initList(TList *list)
{
	list = (TList *)malloc(sizeof(TList));
	list->cur = NULL;
	list->head = NULL;
	return list;
}

//
// �������� ������
//
int destroyList(TList *list)
{
	while(list->head){
		list->cur = list->head;
		list->head = list->head->next;
		free(list->cur);
	}
	list->cur = NULL;
	list->head = NULL;
	free(list);
	return 0;
}

//
// ������ ��������� ������
//
char *getPath(int argc1, char *argv1[], char nameResFile[])
{
	int valDir, valField;
	char fname[257];
	printf("> Analysing command string...\n");
	if(argc1 > 1){
		if( (argv1[1][0] == 'n') && (argv1[1][1] == ':') ){
			for(int i=2; i<strlen(argv1[1]); i++){
				fname[i-2] = argv1[1][i];
			}
			fname[strlen(argv1[1])-2] = 0;
			printf("> Done. Filename is %s\n\n",fname);
			strcpy(nameResFile,getResPath(fname));
			return fname;
		}
	}
	printf("> Command string error...\n\n");
	return NULL;
}

//
// ���������� ����� ����� � ������������
//
char *getResPath(char pathInputFile[])
{
	char path[257];
	char tmp[257];
	strcpy(tmp,pathInputFile);
	strcpy(path,strtok(tmp,"."));
	strcat(path,".bin");
	return path;
}

//
// ������ �����
//
int readSourceFile(FILE *fSource)
{
	char line[257];
	int index = 0;

	printf("> Reading file...\n");
	while( feof(fSource) == 0 ){
		// ��������� ������ �� �����
		fgets(line,300,fSource);
		if( line[strlen(line)-1] == '\n'){
			line[strlen(line)-1] = 0;
		}
		index++;
		readItem(fSource,list1,line,index);
	}
	printf("> Done\n\n");
	return 0;
}

//
// �������������� ������ ��������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        ������ ��� ������� ������
//       2                        ���� ���������
//
int readItem(FILE *fSource, TList *list, char line[], int index)
{
	//
	// word[257]                   ������� � line
	// tmpTok[5]                   ��������� ����������, ��� �������� ���� �� ������������
	// tmpWord[11]                 ��������� ����������, ����������� ���� date
	// �                           ��������� ���������� ��� ����������� �����
	// k                           ������� �������
	// kt                          ���������� ������������� ��������� k
	// ival                        �������� �������� ������ ����
	// cData                       ��������� � ������� �� ������� ������ line
	//
	char word[257], tmpTok[5], tmpWord[11];
	int k, kt, ival;
	char *c = NULL;
	TData cData;

	if(strcmp(line,"") == 0) return 2;

	// ����� ������ �������
	k = 0;
	while( (line[k] != '[') && (line[k] != 0) ) k++;
	if(k == strlen(line)){
		printf("> Error in line #%d. RECORD_BOOK_NO is not found\n",index);
		return 1;
	}
	kt = k+1;
	k++;
	while( (line[k] != ']') && (line[k] != 0) ) k++;
	if(kt == strlen(line)){
		printf("> Error in line #%d. RECORD_BOOK_NO is not found\n",index);
		return 1;
	}
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if(strlen(word) != 6){
		printf("> Error in line #%d. Length of RECORD_BOOK_NO is 6 figures  <%s>\n",index,word);
		return 1;
	}
	if(atol(word) == 0){
		printf("> Error in line #%d. RECORD_BOOK_NO must be of integer type\n",index);
		return 1;
	}
	cData.num = atoi(word);

	// ����� ����
	k++;
	while( ((line[k] == ' ') || (line[k] == ',')) && (line[k] != 0)) k++;
	if(k == strlen(line)){
		printf("> Error in line #%d. DATE is not found\n",index);
		return 1;
	}
	kt = k;
	k++;
	while( (line[k] != ' ') && (line[k] != 0) ) k++;
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if(strcmp(word,"") == 0){
		printf("> Error in line #%d. DATE is not found\n",index);
		return 1;
	}
	if(!((word[0] != '.')&&(word[1] != '.')&&(word[2] == '.')&&
			(word[3] != '.')&&(word[4] != '.')&&(word[5] == '.')&&
			(word[6] != '.')&&(word[7] != '.')&&(word[8] != '.')&&
			(word[9] != '.'))){
		printf("> Error in line #%d. DATE format must be DD.MM.YYYY\n",index);
		return 1;
	}
	strcpy(tmpWord,word);
	c = strtok(tmpWord,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if((ival <= 0)||(ival > 31)){
		printf("> Error in line #%d. \"%s\" is wrong value of a DAY\n",index,tmpTok);
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if((ival <= 0)||(ival > 13)){
		printf("> Error in line #%d. \"%s\" is wrong value of a MOUNTH\n",index,tmpTok);
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if((ival <= 0)||(ival > 9999)){
		printf("> Error in line #%d. \"%s\" is wrong value of a YEAR\n",index,tmpTok);
		return 1;
	}
	strcpy(cData.date,word);

	// ����� �������
	c = strtok(line," ");
	strcpy(word,c);
	if(strlen(word) > 15){
		printf("> Error in line #%d. Maximum length of LAST_NAME is 15 symbols\n",index);
		return 1;
	}
	strcpy(cData.lname,word);

	// ����� �����
	c = strtok(NULL," ");
	strcpy(word,c);
	if(strlen(word) > 15){
		printf("> Error in line #%d. Maximum length of FIRST_NAME is 15 symbols\n",index);
		return 1;
	}
	strcpy(cData.fname,word);

	// ����� ��������
	c = strtok(NULL," ");
	strcpy(word,c);
	if(strlen(word) > 15){
		printf("> Error in line #%d. Maximum length of PATRONYMIC is 15 symbols\n",index);
		return 1;
	}
	strcpy(cData.pname,word);

	// ���������� �������� � ����� ������
	addItem(list,cData);

	return 0;
}

//
// ������ ����� � ������������
//
int writeFile(char path[], TList *list)
{
	printf("> Writing file [%s]\n",path);
	// ���� ������ ���� - �����
	if( !list->head ){
		return 1;
	}
	// ��������� ����
	FILE *fDest = fopen(path,"wb");
	// ��������� �� ����� ������
	list->cur = list->head;
	while( list->cur ){
		fwrite(&list->cur->data,sizeof(TData),1,fDest);
		list->cur = list->cur->next;
	}
	// ��������������� ������� ������
	list->cur = list->head;
	while( list->cur->next ){
		list->cur = list->cur->next;
	}
	// ��������� ����
	fclose(fDest);
	printf("> Done\n\n");
	
	return 0;
}

//
// ����� ������ �� �����
//
int printList(TList *list)
{
	printf("> Printing items...\n");
	// ���� ������ ���� - �����
	if(!list->head){
		printf("> No items\n");
		printf("> Done\n");
		return 0;
	}
	// ��������� �� ����� ������
	int index = 0;
	list->cur = list->head;
	while(list->cur){
		index++;
		printItem(list->cur->data,index);
		list->cur = list->cur->next;
	}
	// ���� ������ ����������
	if( list->head ){
		// ��������������� ������� ������
		list->cur = list->head;
		while(list->cur->next){
			list->cur = list->cur->next;
		}
	}
	// �����
	printf("> Done\n\n");
	return 0;
}

//
// ������ ������ ��������
//
int printItem(TData data, int index)
{
	printf("%.2d | %s %s %s [%d], %s\n",index,data.lname,data.fname,data.pname,data.num,data.date);
	return 0;
}

//
// ���������� �������� � ����� ������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        �� ������� �������� ������
//
int addItem(TList *list, TData data)
{
	TListItem *tmp = (TListItem *)malloc(sizeof(TListItem));
	if(!tmp) return 1;
	if( !list->head ){
		list->head = tmp;
		tmp->prev = NULL;
		tmp->next = NULL;
	}else{
		if( !list->cur ){
			list->cur = list->head;
		}
		while( list->cur->next ){
			list->cur = list->cur->next;
		}
		list->cur->next = tmp;
		tmp->prev = list->cur;
		tmp->next = NULL;
	}
	tmp->data = data;
	list->cur = tmp;
	return 0;
}

//
// ������������� ���������� ���������
//
int initBubbleSort(TList *list)
{
	//
	// dir                         ����������� ����������
	//    1                           �� �����������
	//    0                           �� ��������
	//
	int dir = -1;
	printf("> Sort records\n");
	while( (dir != 1) && (dir != 0) ){
		printf("> Set direction [1-Direct | 0-Indirect]\n> ");
		scanf("%d",&dir);
	}
	printf("> Sorting...\n");
	list->dir = dir;

	if(bubbleSort(list,dir) == 0) printf("> Done\n\n");

	return 0;
}

//
// ���������� ���������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        ������ ����
//
int bubbleSort(TList *list, int dir)
{
	//
	// d1, d2, m1, m2, y1, y2      �������� ���, ������, ���� � ����� date[11] 
	//                                ���� ��������� ������� �����.
	// tmpTok[5]                   ��������� ����������, �������� d1, d2, m1, 
	//                                m2, y1, y2 ��� ����������� � �����������
	// tmpWord[11]                 ��������� ����������, ����������� ���� date
	//

	// ���� ������ ���� - �����
	if( !list->head ) return 1;

	int canExit = 0;

	while( !canExit ){
		canExit = 1;
		list->cur = list->head;
		while(list->cur->next){
			if( ( (strcmp(list->cur->data.lname,list->cur->next->data.lname) > 0)&&(dir == 1) )
				|| ( (strcmp(list->cur->data.lname,list->cur->next->data.lname) < 0)&&(dir == 0) ) ){
				exchangeElem(list);
				canExit = 0;
			}else{
				list->cur = list->cur->next;
			}
		}
	}
	return 0;
}

//
// ������������ ���� ��������� � ������
//
int exchangeElem(TList *list)
{
	TListItem *tmp = list->cur->next;
	list->cur->next = tmp->next;
	tmp->prev = list->cur->prev;
	if( list->cur->next != NULL ){
		list->cur->next->prev = list->cur;
	}
	if( tmp->prev != NULL ){
		tmp->prev->next = tmp;
	}
	tmp->next = list->cur;
	list->cur->prev = tmp;
	if( list->cur == list->head ){
		list->head = tmp;
	}
	return 0;
}
