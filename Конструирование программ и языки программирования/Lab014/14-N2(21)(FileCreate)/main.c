/*
******************************************************************************
*       ������������ ������ #14. ������� #2. ������� #21
*
*       ��������: ������� ��. 06��-1 ��� ����� ������
******************************************************************************
*/
/*
******************************************************************************
*                                 TODOLIST
******************************************************************************
* - ������� getItem
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>


/*
******************************************************************************
                                  DATA TYPES
******************************************************************************
*/
typedef struct {
   double x;                      // ���������� x
   double y;                      // ���������� y
} tPoint;


/*
******************************************************************************
*                                 PROTOTYPES
******************************************************************************
*/
FILE *createFile(void);

int writeSourceFile(FILE *);

int getItem(tPoint *);


/*
******************************************************************************
*                                 FUNCTIONS
******************************************************************************
*/

int main(int argc, char *argv[])
{
	FILE *file1 = createFile();
	writeSourceFile(file1);
	fclose(file1);

	fflush(stdin);
	printf("> Press any key to quit");
	_getch();
	return 0;
}

//
// �������� �����
//
FILE *createFile(void)
{
	char path[257];
	printf("> Enter filepath\n> ");
	scanf("%s",path);
	FILE *file = fopen(path,"wb");
	while( !file ){
		printf("> Error. Invalid filename. Try again.\n> ");
		scanf("%s",path);
		file = fopen(path,"wb");
	}
	printf("\n");
	return file;
}

//
// ������ ����� � ������������
//
int writeSourceFile(FILE *dFile)
{
	int res = 0;
	tPoint p;

	printf("> Enter coordinates\n");
	printf("> Format: X_COORDINATE[double], Y_COORDINATE[double]\n");
	printf("> Enter an empty string to exit to menu\n");
	while(res != 2){
		do{
			res = getItem(&p);
		}while(res == 1);
		if( res == 0 ){
			fwrite(&p, sizeof(tPoint), 1, dFile);
		}
	}

	printf("> Done\n");
	return 0;
}

//
// ���������� ������ ��������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        ������ ��� ������� ������
//       2                        ���� ���������
//
int getItem(tPoint *p)
{
	char line[257], tmpTok[257];
	char *c;
	double dval;

	fflush(stdin);
	gets(line);
	fflush(stdin);

	if(strcmp(line,"") == 0) return 2;

	c = strtok(line," ,");
	strcpy(tmpTok,c);
	dval = atof(tmpTok);
	if( (dval == 0.0) && (strcmp(tmpTok,"0") != 0) && (strcmp(tmpTok,"0.0") != 0) ){
		printf("\n> Error. Invalid X_COORDINATE\n");
		printf("> Try again\n\n");
		return 1;
	}
	p->x = dval;
	//printf("%s >>> %lf  | [getItem](1-st call)\n",tmpTok,dval);		// **DEBUG

	c = strtok(NULL," ,");
	strcpy(tmpTok,c);
	dval = atof(tmpTok);
	if( (dval == 0.0) && (strcmp(tmpTok,"0") != 0) && (strcmp(tmpTok,"0.0") != 0) ){
		printf("\n> Error. Invalid Y_COORDINATE\n");
		printf("> Try again\n\n");
		return 1;
	}
	p->y = dval;
	//printf("%s >>> %lf  | [getItem](2-st call)\n",tmpTok,dval);		// **DEBUG

	return 0;
}
