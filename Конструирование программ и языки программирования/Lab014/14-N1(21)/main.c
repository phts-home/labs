/*
******************************************************************************
*       ������������ ������ #14. ������� #1. ������� #21
*
*       ��������: ������� ��. 06��-1 ��� ����� ������
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/*
******************************************************************************
*                                 PROTOTYPES
******************************************************************************
*/
FILE *initFile(void);
int readFile(FILE *);

int searchOpenBr(FILE *, int *, char *, int *);
int searchCloseBr(FILE *, int *, char *, int, char []);


/*
******************************************************************************
*                                 FUNCTIONS
******************************************************************************
*/

int main(int argc, char *argv[])
{
	FILE *file1 = initFile();
	printf("> Working...\n");
	int sum = readFile(file1);
	fclose(file1);
	printf("> Done\n");
	printf("> Result: %d\n",sum);
	return 0;
}

//
// ������������� �����
//
FILE *initFile(void)
{
	char path[257];
	printf("> Enter filepath\n> ");
	scanf("%s",path);
	FILE *file = fopen(path,"rt");
	while( !file ){
		printf("> Error. Invalid filename. Try again.\n> ");
		scanf("%s",path);
		file = fopen(path,"rt");
	}
	printf("\n");
	return file;
}

//
// ������ �����
//
int readFile(FILE *source)
{
	FILE *fOutput = NULL;
	char curCh;
	int k = 0;
	int curInt;
	char curPart[257];
	int mode;
	int sum = 0;
	int res;
	// �������� �������� ������
	fOutput = fopen("().txt", "wt");
	fclose(fOutput);
	fOutput = fopen("[].txt", "wt");
	fclose(fOutput);
	fOutput = fopen("{}.txt", "wt");
	fclose(fOutput);
	// ���������� ������� ������� ��� ������������� ����������
	if( feof(source) == 0 ){
		fscanf(source,"%c",&curCh);
	}
	while( 1 ){
		// ����� ����������� ������
		res = searchOpenBr(source,&k,&curCh,&mode);
		// ���� ��������� ����� ����� - ����� �� �����
		if(res == 1){
			break;
		}
		// ����� ����������� ������
		res = searchCloseBr(source,&k,&curCh,mode,curPart);
		// ���� �� ������� ������ ������ - �������� ���� � ������
		if( (res == 1) || (res == 2) || (res == 3) || (res == 4) || (res == 5) || (res == 7)){
			continue;
		}
		// ���� ��������� ����� ����� - ����� �� �����
		if(res == 6){
			break;
		}
		// �������������� �����_���_����� ����� �������� � �����
		curInt = atoi(curPart);
		// ������������
		sum += curInt;
		// ������ ����� � ��������������� ����
		switch(mode){
			case 1:
				fOutput = fopen("().txt", "at+");
				break;
			case 2:
				fOutput = fopen("[].txt", "at+");
				break;
			case 3:
				fOutput = fopen("{}.txt", "at+");
				break;
		}
		fprintf(fOutput,"%d\n",curInt);
		fclose(fOutput);
	}
	return sum;
}

//
// ����� ����������� ������
//
//    ���������:
//       source                   ��������� �� �������� ����
//       index                    ������� �������
//       curCh                    ������� ��������� ������
//       mode                     ��� ��������� ������ ( 1 == '(' | 2 == '[' | 3 == '{' )
//
//    ������������ ��������:
//       0                        ������� '(' | '[' | '{'
//       1                        ������ �� ������� � ��������� ����� �����
//
int searchOpenBr(FILE *source, int *index, char *curCh, int *mode)
{
	// ���� �� ������� ����������� ������
	while( ((*curCh) != '(') && ((*curCh) != '[') && ((*curCh) != '{') ){
		// ���� ����� ����� - �����
		if( feof(source) != 0 ){
			return 1;
		}
		// ��������� ��������� �������
		fscanf(source,"%c",&(*curCh));
		// �������� ������� �������
		(*index)++;
	}
	// �������� mode
	switch( *curCh ){
		case '(': (*mode) = 1; break;
		case '[': (*mode) = 2; break;
		case '{': (*mode) = 3; break;
	}
	// ��������� ��������� ������� �� �������
	if( feof(source) == 0 ){
		fscanf(source,"%c",&(*curCh));
	}
	return 0;
}

//
// ����� ����������� ������
//
//    ���������:
//       source                   ��������� �� �������� ����
//       index                    ������� �������
//       curCh                    ������� ��������� ������
//       mode                     ��� ��������� ������ ( 1 == '(' | 2 == '[' | 3 == '{' )
//       curPart[]                ����� ����� ��������
//
//    ������������ ��������:
//       0                        ������� ������ ����������� ������
//       1                        ������: ������� '('
//       2                        ������: ������� '['
//       3                        ������: ������� '{'
//       4                        ������: ������ �� �������� ������
//       5                        ������: ������� �������� ����������� ������
//       6                        ������: ������ �� ������� � ��������� ����� �����
//       7                        ������: ������ ������ ����� ��������
//
int searchCloseBr(FILE *source, int *index, char *curCh, int mode, char curPart[])
{
	int begin = (*index);
	// ���� �� ������� ����������� ������
	while( ((*curCh) != ')') && ((*curCh) != ']') && ((*curCh) != '}') ){
		// ���� ����� ����� - �����
		if( feof(source) != 0 ){
			return 6;
		}
		// ���� ������� '(' - �����
		if( (*curCh) == '(' ){
			return 1;
		}
		// ���� ������� '[' - �����
		if( (*curCh) == '[' ){
			return 2;
		}
		// ���� ������� '{' - �����
		if( (*curCh) == '{' ){
			return 3;
		}
		// ���� ������ ����� �������� �� �������� ����� ��� ����������� ������� - �����
		if( (((*curCh) < '0') || ((*curCh) > '9')) && ((*curCh) != ')') && ((*curCh) != ']') && ((*curCh) != '}') ){
			return 4;
		}
		// ���������� � ������ ������
		curPart[(*index)-begin] = (*curCh);
		// ��������� ����� ������
		fscanf(source,"%c",&(*curCh));
		// �������� ������� �������
		(*index)++;
	}
	// ����������� ����������� ������
	switch(mode){
		case 1: 
			if( (*curCh) != ')' ){
				return 5;
			}
			break;
		case 2: 
			if( (*curCh) != ']' ){
				return 5;
			}
			break;
		case 3: 
			if( (*curCh) != '}' ){
				return 5;
			}
			break;
	}
	// ��������� ������
	curPart[(*index)-begin] = 0;
	// ���� ������ ������ - �����
	if( strcmp(curPart,"") == 0 ){
		return 7;
	}
	return 0;
}
