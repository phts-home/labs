/*
******************************************************************************
*       ������������ ������ #14. ������� #2. ������� #21
*
*       ��������: ������� ��. 06��-1 ��� ����� ������
******************************************************************************
*/
/*
******************************************************************************
*                                 TODOLIST
******************************************************************************
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <conio.h>
#include <math.h>


/*
******************************************************************************
                                  DATA TYPES
******************************************************************************
*/
typedef struct {
   double x;                      // ���������� x
   double y;                      // ���������� y
} tPoint;
typedef struct {
   tPoint point;                  // �����
   double distance;               // ���������� �� ����� �� ������ ���������
   int quarter;                   // ��������
} tData;
typedef struct _tListItem{
   tData data;                    // ������ ��������
   struct _tListItem *prev;       // ���������� ������� � ������
   struct _tListItem *next;       // ��������� ������� � ������
} tListItem;
typedef struct {
   tListItem *head;               // ������ ������� � ������
   tListItem *cur;                // ������� ������� � ������
} tList;


/*
******************************************************************************
                                  VARIABLES
******************************************************************************
*/
tList *list1 = NULL, *list2 = NULL, *list3 = NULL, *list4 = NULL;


/*
******************************************************************************
*                                 PROTOTYPES
******************************************************************************
*/
FILE *initFile(void);
tList *initList (void);
int destroyList(tList *);

int readFile(FILE *);
int writeFile(char [], tList *);

int addItem(tList *, tData);

int bubbleSort(tList *);
int exchangeElem(tList *);


/*
******************************************************************************
*                                 FUNCTIONS
******************************************************************************
*/

int main(int argc, char *argv[])
{
	list1 = initList();
	list2 = initList();
	list3 = initList();
	list4 = initList();

	FILE *file1 = initFile();

	printf("> Working...\n");
	readFile(file1);
	fclose(file1);

	bubbleSort(list1);
	bubbleSort(list2);
	bubbleSort(list3);
	bubbleSort(list4);

	writeFile("1.bin",list1);
	writeFile("2.bin",list2);
	writeFile("3.bin",list3);
	writeFile("4.bin",list4);
	printf("> Done\n");

	destroyList(list1);
	destroyList(list2);
	destroyList(list3);
	destroyList(list4);

	fflush(stdin);
	printf("\n> Press any key to quit");
	_getch();
	return 0;
}

//
// ������������� �����
//
FILE *initFile(void)
{
	char path[257];
	printf("> Enter filepath\n> ");
	scanf("%s",path);
	//strcpy(path,"in.bin");											// **DEBUG
	FILE *file = fopen(path,"rb");
	while( !file ){
		printf("> Error. Invalid filename. Try again.\n> ");
		scanf("%s",path);
		file = fopen(path,"rb");
	}
	printf("\n");
	return file;
}

//
// ������������� ������
//
tList *initList(void)
{
	tList *list = (tList *)malloc(sizeof(tList));
	list->cur = NULL;
	list->head = NULL;
	return list;
}

//
// �������� ������
//
int destroyList(tList *list)
{
	while(list->head){
		list->cur = list->head;
		list->head = list->head->next;
		free(list->cur);
	}
	list->cur = NULL;
	list->head = NULL;
	free(list);
	return 0;
}

//
// ������ �����
//
int readFile(FILE *fSource)
{
	tPoint p;
	tData cData;
	tList *cList = NULL;
	if( feof(fSource) == 0 ){
		fread(&p, sizeof(tPoint), 1, fSource);
	}
	fread(&p, sizeof(tPoint), 1, fSource);
	while( feof(fSource) == 0 ){
		// ���������� ������ �������� ������
		cData.point = p;
		cData.distance = sqrt(p.x*p.x+p.y*p.y);
		if( p.x >= 0 ){
			if( p.y >= 0 ){
				cData.quarter = 1;
				cList = list1;
				//addItem(list1,cData);
			}else{
				cData.quarter = 4;
				cList = list4;
			}
		}else{
			if( p.y >= 0 ){
				cData.quarter = 2;
				cList = list2;
			}else{
				cData.quarter = 3;
				cList = list3;
			}
		}
		// ���������� �������� � ����� ������
		addItem(cList,cData);
		// ������ ���� ������ � ��������� tPoint
		fread(&p, sizeof(tPoint), 1, fSource);
	}
	return 0;
}

//
// ������ ����� � ������������
//
int writeFile(char path[], tList *list)
{
	// ���� ������ ���� - �����
	if( !list->head ){
		return 1;
	}
	// ��������� ����
	FILE *fDest = fopen(path,"wt");
	// ��������� �� ����� ������
	list->cur = list->head;
	while( list->cur ){
		fprintf(fDest,"%lf, %lf\n", list->cur->data.point.x, list->cur->data.point.y);
		list->cur = list->cur->next;
	}
	// ��������������� ������� ������
	list->cur = list->head;
	while( list->cur->next ){
		list->cur = list->cur->next;
	}
	// ��������� ����
	fclose(fDest);
	return 0;
}

//
// ���������� �������� � ����� ������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        �� ������� �������� ������
//
int addItem(tList *list, tData data)
{
	tListItem *tmp = (tListItem *)malloc(sizeof(tListItem));
	if(!tmp) return 1;
	if( !list->head ){
		list->head = tmp;
		tmp->prev = NULL;
		tmp->next = NULL;
	}else{
		if( !list->cur ){
			list->cur = list->head;
		}
		while( list->cur->next ){
			list->cur = list->cur->next;
		}
		list->cur->next = tmp;
		tmp->prev = list->cur;
		tmp->next = NULL;
	}
	tmp->data = data;
	list->cur = tmp;
	return 0;
}

//
// ���������� ���������
//
int bubbleSort(tList *list)
{
	if( !list->head ) return 1;
	int canExit = 0;
	while(!canExit){
		canExit = 1;
		list->cur = list->head;
		while(list->cur->next){
			if( list->cur->data.distance >= list->cur->next->data.distance ){
				exchangeElem(list);
				canExit = 0;
			}else list->cur = list->cur->next;
		}
	}
	return 0;
}

//
// ������������ ���� ��������� � ������
//
int exchangeElem(tList *list)
{
	tListItem *tmp = list->cur->next;
	list->cur->next = tmp->next;
	tmp->prev = list->cur->prev;
	if( list->cur->next != NULL ){
		list->cur->next->prev = list->cur;
	}
	if( tmp->prev != NULL ){
		tmp->prev->next = tmp;
	}
	tmp->next = list->cur;
	list->cur->prev = tmp;
	if( list->cur == list->head ){
		list->head = tmp;
	}
	return 0;
}
