#include <stdio.h>


int main(int argc, char *argv[])
{
		int n;
		printf("Enter size of Array (int) : ");
    scanf("%d",&n);
		if(n<3){
				printf("Error\n");
				return 0;
		}
		//�������� Ar
		double Ar[n];
		printf("Enter elements of Array (double) : ");
		for(int i = 0;i<n;i++){
				scanf("%lf",&Ar[i]);
		}
		//�������� Temp
		double Temp[n];
		for(int i = 0;i<n;i++){
				Temp[i] = Ar[i];
		}
		//���������� Temp
		double TempEl;
		for(int i = 0;i<n-1;i++){
				for(int j = i;j<n;j++){
						if(Temp[i]>Temp[j]){
								TempEl = Temp[i];
								Temp[i] = Temp[j];
								Temp[j] = TempEl;
						}
				}
		}

		//����� max3 � min2
		double max3;
		double min2;
		int c = 1;
		for(int i = 1;i<n;i++){
				if(Temp[i] != Temp[i-1]){
						c++;
				}
				if(c==2){min2 = Temp[i];break;};
		}
		c = 1;
		for(int i = n-1;i>0;i--){
				if(Temp[i] != Temp[i+1]){
						c++;
				}
				if(c==4){max3 = Temp[i];break;};
		}

		//����� � Ar max3 � min2
		int maxi3;
		int mini2;
		for(int i = 0;i<n;i++){
				if(Ar[i] == max3){
						maxi3 = i;
				}
				if(Ar[i] == min2){
						mini2 = i;
				}
		}

		//��������� ����� max3 � min2
		if(mini2<maxi3){
				for(int i = mini2+1;i<=maxi3-1;i++){
						Ar[i] = 0;
				}
		}else
		{
				for(int i = maxi3+1;i<=mini2-1;i++){
						Ar[i] = 0;
				}
		}
		
		printf("Result:\n");
		printf("3rd max == %.2lf, i == %d\n",max3,maxi3);
		printf("2nd min == %.2lf, i == %d\n",min2,mini2);
		printf("max * min  == %.2lf\n",max3*min2);
		printf("Array:");
		for(int i = 0;i<n;i++){
				printf("%.2lf ",Ar[i]);
		}
		printf("\n");
    return 0;
}

