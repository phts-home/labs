#include <stdio.h>


int main(int argc, char *argv[])
{
	//Init {size}
	int size,k;
	printf("> Enter Size Of Array (int)\n> ");
	scanf("%d",&size);
	printf("\n");
	if(size<3){
		printf("> Error\n");
		return 0;
	}

	//Init {ar}
	int ar[size];
	printf("> Enter Array Elements (int)\n");
	for(int i = 0;i<size;i++){
		printf("> ");
		scanf("%d",&ar[i]);
	}
	printf("\n");
	
	//Init {k}
	printf("> Enter k (int)\n> ");
	scanf("%d",&k);
	printf("\n");
	if(k==1){
		printf("> Error\n");
		return 0;
	}

	//Searching Series
	//sw - widht; si - index; sv - value; kk - cur.#
	int sw = 0,si = -1, kk = 0;
	int sw1,si1,sv1,swk,sik,svk;
	for(int i = 0;i<size;i++){
			sw = 1;
			si = i;
			kk++;
			while(ar[i+1] == ar[i]){
				sw++;
				i++;
			}
			if(kk == 1){
				sw1 = sw;
				si1 = si;
				sv1 = ar[i];
			}
			if(kk == k){
				swk = sw;
				sik = si;
				svk = ar[i];
				break;
			}
	}

	//Delete Series
	int newSize = size, newSik = sik;
	while(ar[si1]==ar[si1+1]){
		for(int j=si1;j<size-1;j++){
			ar[j] = ar[j+1];
		}
		newSize--;
		newSik--;
	}
	while(ar[newSik]==ar[newSik+1]){
		for(int j=newSik;j<size-1;j++){
			ar[j] = ar[j+1];
		}
		newSize--;
	}

	//Paste Series
	int curW = 1;
	ar[si1] = svk;
	while(curW != swk){
		for(int j=size-1;j>si1;j--){
			ar[j+1] = ar[j];
		}
		newSize++;
		newSik++;
		curW++;
		ar[si1+1] = svk;
	}
	curW = 1;
	ar[newSik] = sv1;
	while(curW != sw1){
		for(int j=size-1;j>newSik;j--){
			ar[j+1] = ar[j];
		}
		newSize++;
		curW++;
		ar[newSik+1] = sv1;
	}

	//Print results
	printf("> Ser.#1:  Index == %d  Width == %d  Value == %d\n",si1,sw1,sv1);
	printf("> Ser.#%d:  Index == %d  Width == %d  Value == %d\n",k,sik,swk,svk);
	printf("> Result Array:\n");
	printf("> ");
	for(int i=0;i<size;i++){
		printf("%d  ",ar[i]);
	}
	printf("\n\n");
	return 0;
}

