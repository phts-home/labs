#include <stdio.h>


int main(int argc, char *argv[])
{
	//Init {m} {n} {ar}
   int m,n;
	printf("> Enter Rows Count (int)\n> ");
	scanf("%d",&m);
	printf("\n> Enter Columns Count (int)\n> ");
	scanf("%d",&n);
	printf("\n> Enter Array Elements (int)\n");
	int ar[m][n];
	for(int i=0;i<m;i++)
		for(int j=0;j<n;j++){
			printf("> ");
			scanf("%d",&ar[i][j]);
		}
	printf("\n");

	//Print {ar}
	printf("> Source Array:\n");
	for(int i=0;i<m;i++){
		for(int j=0;j<n;j++){
			printf("%d\t",ar[i][j]);
		}
		printf("\n");
	}
	printf("\n");
	
	//Create {tmpAr}
	int k = 0, kmax = m*n;
	int tmpAr[kmax];
	for(int i=0;i<m;i++)
		for(int j=0;j<n;j++){
			tmpAr[k] = ar[i][j];
			k++;
		}

	//Sort {tmpAr}
	int t;
	for(int i=0;i<kmax-1;i++)
		for(int j=i+1;j<kmax;j++){
			if(tmpAr[j] < tmpAr[i]){
				t = tmpAr[i];
				tmpAr[i] = tmpAr[j];
				tmpAr[j] = t;
			}
		}

	//Write {tmpAr} to {ar}
	int r = 0, c = 0;
	while(c < kmax){
		for(int i=r;i<n-r;i++){ar[r][i] = tmpAr[c]; c++;}
		if(c==kmax){break;}
		for(int i=r+1;i<m-r;i++){ar[i][n-r-1] = tmpAr[c]; c++;}
		if(c==kmax){break;}
		for(int i=n-r-2;i>=r;i--){ar[m-r-1][i] = tmpAr[c]; c++;}
		if(c==kmax){break;}
		for(int i=m-r-2;i>r;i--){ar[i][r] = tmpAr[c]; c++;}
		r++;
	}

	//Print {ar}
	printf("> Sorted Array:\n");
	for(int i=0;i<m;i++){
		for(int j=0;j<n;j++){
			printf("%d\t",ar[i][j]);
		}
		printf("\n");
	}
	printf("\n");
   return 0;
}

