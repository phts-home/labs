/****************************************************************************
 *                                                                          *
 * File    : main.c                                                         *
 *                                                                          *
 * Purpose : Console mode (command line) program.                           *
 *                                                                          *
 * History : Date      Reason                                               *
 *           00/00/00  Created                                              *
 *                                                                          *
 ****************************************************************************/

#include <stdio.h>

/****************************************************************************
 *                                                                          *
 * Function: main                                                           *
 *                                                                          *
 * Purpose : Main entry point.                                              *
 *                                                                          *
 * History : Date      Reason                                               *
 *           00/00/00  Created                                              *
 *                                                                          *
 ****************************************************************************/

int main(int argc, char *argv[])
{
	//Init {size1}
	int size1;
	printf("> Enter Rows/Columns Count of Arrays (int)\n> ");
	scanf("%d",&size1);
	if(size1<2){
		printf("> Error\n");
		return 0;
	}

	//Init {ar1}
	printf("\n> Enter Elements of Array#1 (int)\n");
	int ar1[size1][size1];
	for(int i=0;i<size1;i++)
		for(int j=0;j<size1;j++){
			printf("> ");
			scanf("%d",&ar1[i][j]);
		}
	printf("\n");

	//Init {ar2}
	printf("\n> Enter Elements of Array#2 (int)\n");
	int ar2[size1][size1];
	for(int i=0;i<size1;i++)
		for(int j=0;j<size1;j++){
			printf("> ");
			scanf("%d",&ar2[i][j]);
		}
	printf("\n");

	//Print {ar1}
	printf("> Array#1:\n");
	for(int i=0;i<size1;i++){
		for(int j=0;j<size1;j++){
			printf("%d\t",ar1[i][j]);
		}
		printf("\n");
	}
	printf("\n");

	//Print {ar2}
	printf("> Array#2:\n");
	for(int i=0;i<size1;i++){
		for(int j=0;j<size1;j++){
			printf("%d\t",ar2[i][j]);
		}
		printf("\n");
	}
	printf("\n");

	//Create {tmpAr}
	int tmpAr[size1][size1];
	for(int i=0;i<size1;i++)
		for(int j=0;j<size1;j++)
			tmpAr[i][j] = -1;
		
	//Calculate
	int s;
	for(int i=0;i<size1;i++){
		for(int j=0;j<size1;j++){
			s = 0;
			for(int ii=0;ii<size1;ii++){
				s += ar1[i][ii]*ar2[ii][j];
			}
			tmpAr[i][j] = s;
		}
	}

	//Print {tmpAr}
	printf("> Result Array:\n");
	for(int i=0;i<size1;i++){
		for(int j=0;j<size1;j++){
			printf("%d\t",tmpAr[i][j]);
		}
		printf("\n");
	}
	printf("\n");

	//Check {tmpAr}
	int isOne = 1;
	for(int i=0;i<size1;i++){
		for(int j=0;j<size1;j++){
			if(i==j){
				if(tmpAr[i][j]!=1){isOne = 0;}
			}else{
				if(tmpAr[i][j]!=0){isOne = 0;}
			}
		}
	}
	if(isOne == 1){printf("> Yes!\n");}else{printf("> No!\n");};
	return 0;
}

