/*
******************************************************************************
*       ������������ ������ #13. ������� #1.
*
*       ��������: ������� ��. 06��-1 ��� ����� ������
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/*
******************************************************************************
*                                 DATA TYPES
******************************************************************************
*/
typedef struct {
	long snum;                     // �������� ����� (�������������)
	char model[21];                // �����-������
	int year;                      // ��� ������� (��������������)
	int ftime;                     // ����� ������
} tData;
typedef struct _tTreeElem{
	tData data;                    // ������ ��������
	struct _tTreeElem *parent;     // ������
	struct _tTreeElem *left;       // ������� �����
	struct _tTreeElem *right;      // ������� ������
} tTreeElem;
typedef struct {
	tTreeElem *root;               // ������ ������
	tTreeElem *cur;                // ������� �������
} tTree;


/*
******************************************************************************
*                                 PROTOTYPES
******************************************************************************
*/
int drwMenu(tTree *);

int getStr(void);

int initTree(tTree *);
int destroyTree(tTree *);
int delElement(tTreeElem *);

tTreeElem *moveParent(const tTreeElem *);
tTreeElem *moveLeft(const tTreeElem *);
tTreeElem *moveRight(const tTreeElem *);

int addLeft(tTreeElem *, tData);
int addRight(tTreeElem *, tData);


/*
******************************************************************************
*                                 FUNCTIONS
******************************************************************************
*/

int main(int argc, char *argv[])
{
	tTree *tree1;
	initTree(tree1);
	drwMenu(tree1);
	destroyTree(tree1);
	return 0;
}

//
// ��������� ����
//
int drwMenu(tTree *tree)
{
	int ch, canExit = 0;
	while(!canExit){
		printf("\n");
		printf("------ Menu ------\n");
		printf("1 - New equation\n");
		printf("2 - Print\n");
		printf("0 - Exit\n");
		printf("------------------\n");
		printf("> Your Choice   ");
		scanf("%d",&ch);
		printf("\n");
		switch(ch){
			case 1:{
				getStr();
				//inputQueue(queue);
				break;
			}
			case 2:{
				//printQueue(queue);
				break;
			}
			case 0:{
				canExit = 1;
				break;
			}
		}
	}
	return 0;
}

int getStr(void)
{
	char line[257], tmp[257];
	char *c = NULL;
	char tok[257];
	fflush(stdin);
	gets(line);
	strcpy(tmp,line);
	c = strtok(tmp,"+-*/^");
	while( c ){
		strcpy(tok,c);
		printf("%s\n",tok);
		c = strtok(NULL,"+-*/^");
	}
	
	return 0;
}

//
// ������������� ������
//
int initTree(tTree *tree)
{
	tree->root = NULL;
	return 0;
}

//
// �������� ������
//
int destroyTree(tTree *tree)
{
	delElement(tree->root);
	tree->root = NULL;
	return 0;
}

//
// �������� �������� � ���� ��� ��������
//
int delElement(tTreeElem *elem)
{
	if(!elem) return 0;
	if(elem->left) delElement(elem->left);
	if(elem->right) delElement(elem-> right);
	if(elem->parent){
		if(elem->parent->left == elem)
			elem->parent->left = NULL;
		else 
			elem->parent->right = NULL;
	}
	free(elem);
	return 0;
}

//
// ����������� � ������
//
tTreeElem *moveParent(const tTreeElem *elem)
{
	if(!elem) return NULL;
	return elem->parent;
}

//
// ����������� � ������� �����
//
tTreeElem *moveLeft(const tTreeElem *elem)
{
	if(!elem) return NULL;
	return elem->left;
}

//
// ����������� � ������� ������
//
tTreeElem *moveRight(const tTreeElem *elem)
{
	if(!elem) return NULL;
	return elem->right;
}

//
// ���������� ������� �����
//
int addLeft(tTreeElem *elem, tData data)
{
	if(!elem || elem->left) return 1;
	tTreeElem *tmp = (tTreeElem *)malloc(sizeof(tTreeElem));
	if(!tmp) return 1;
	tmp->left = NULL;
	tmp->right = NULL;
	tmp->data = data;
	tmp->parent = elem;
	elem->left = tmp;
	return 0;
}

//
// ���������� ������� �����
//
int addRight(tTreeElem *elem, tData data)
{
	if(!elem || elem->right) return 1;
	tTreeElem *tmp = (tTreeElem *)malloc(sizeof(tTreeElem));
	if(!tmp) return 1;
	tmp->left = NULL;
	tmp->right = NULL;
	tmp->data = data;
	tmp->parent = elem;
	elem->right = tmp;
	return 0;
}

