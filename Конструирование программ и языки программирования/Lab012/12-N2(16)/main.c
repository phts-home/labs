/*
******************************************************************************
*       ������������ ������ #12. ������� #2. ������� #16
*
*       ��������: ������� ��. 06��-1 ��� ����� ������
******************************************************************************
*/

/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - < nothing >
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/*
******************************************************************************
*                                 DATA TYPES
******************************************************************************
*/
typedef struct {
   char lname[16];                // �������
   char fname[16];                // ���
   char pname[16];                // ��������
   long num;                      // ����� �������
   char date[11];                 // ����
} TData;
typedef struct _TStackItem{
   TData data;                    // ������ ��������
   struct _TStackItem *next;      // ��������� ������� �����
} TStackItem;
typedef struct {
   TStackItem *head;              // ������� �����
} TStack;


/*
******************************************************************************
*                                 PROTOTYPES
******************************************************************************
*/
int drwMenu(TStack *);
int initStack(TStack *);
int destroyStack(TStack *);

int inputStack(TStack *);
int inputItem(TStack *);
int printStack(TStack *);
int printItem(TData);

int pushElement(TStack *, TData);
int popElement(TStack *, TData *);

/*
******************************************************************************
*                                 FUNCTIONS
******************************************************************************
*/

int main(int argc, char *argv[])
{
	TStack *stack1;
	initStack(stack1);
	drwMenu(stack1);
	destroyStack(stack1);
	return 0;
}

//
// ��������� ����
//
int drwMenu(TStack *src)
{
	int ch, canExit = 0;
	while(!canExit){
		printf("\n");
		printf("------ Menu ------\n");
		printf("1 - Add items\n");
		printf("2 - Print\n");
		printf("0 - Exit\n");
		printf("------------------\n");
		printf("> Your Choice   ");
		scanf("%d",&ch);
		printf("\n");
		switch(ch){
			case 1:{
				inputStack(src);
				break;
			}
			case 2:{
				printStack(src);
				break;
			}
			case 0:{
				canExit = 1;
				break;
			}
		}
	}
	return 0;
}

//
// ������������� �����
//
int initStack(TStack *src)
{
	src->head = NULL;
	return 0;
}

//
// �������� �����
//
int destroyStack(TStack *src)
{
	while(src->head){
		TStackItem *tmp = src->head;
		src->head = src->head->next;
		free(tmp);
	}
	return 0;
}

//
// ���������� �����
//
int inputStack(TStack *src)
{
	int er = 0;

	printf("> Enter items of array\n");
	printf("> Format: LAST_NAME FIRST_NAME PATRONYMIC [RECORD_BOOK_NO[int,length=6]], DATE[format:DD.MM.YYYY]\n");
	printf("> Enter an empty string to exit to menu\n");
	while(er != 2){
		do{
			er = inputItem(src);
		}while(er == 1);
	}

	printf("> Done\n");
	return 0;
}

//
// ���������� ������ ��������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        ������ ��� ������� ������
//       2                        ���� ���������
//
int inputItem(TStack *src)
{
	//
	// line[257]                   ��������������� ������ � ���������� �����
	// word[257]                   ������� � line
	// tmpTok[5]                   ��������� ����������, ��� �������� ���� �� ������������
	// tmpWord[11]                 ��������� ����������, ����������� ���� date
	// �                           ��������� ���������� ��� ����������� �����
	// k                           ������� �������
	// kt                          ���������� ������������� ��������� k
	// ival                        �������� �������� ������ ����
	// curData                     ��������� � ������� �� ������� ������ line
	//
	char line[257], word[257], tmpTok[5], tmpWord[11];
	int k, kt, ival;
	char *c = NULL;
	TData curData;

	fflush(stdin);
	gets(line);
	fflush(stdin);

	if(strcmp(line,"") == 0) return 2;

	// ����� RECORD_BOOK_NO
	k = 0;
	while( (line[k] != '[') && (line[k] != 0) ) k++;
	if(k == strlen(line)){
		printf("\n> Error. RECORD_BOOK_NO is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	kt = k+1;
	k++;
	while( (line[k] != ']') && (line[k] != 0) ) k++;
	if(k == strlen(line)){
		printf("\n> Error. RECORD_BOOK_NO is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if(strlen(word) != 6){
		printf("\n> Error. Length of RECORD_BOOK_NO is 6 figures\n");
		printf("> Try again\n\n");
		return 1;
	}
	if(atol(word) == 0){
		printf("\n> Error. RECORD_BOOK_NO must be of integer type\n");
		printf("> Try again\n\n");
		return 1;
	}
	curData.num = atoi(word);

	// ����� DATE
	k++;
	while( ((line[k] == ' ') || (line[k] == ',')) && (line[k] != 0)) k++;
	if(k == strlen(line)){
		printf("\n> Error. DATE is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	kt = k;
	k++;
	while( (line[k] != ' ') && (line[k] != 0) ) k++;
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if(strcmp(word,"") == 0){
		printf("\n> Error. DATE is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	if(!((word[0] != '.')&&(word[1] != '.')&&(word[2] == '.')&&
			(word[3] != '.')&&(word[4] != '.')&&(word[5] == '.')&&
			(word[6] != '.')&&(word[7] != '.')&&(word[8] != '.')&&
			(word[9] != '.'))){
		printf("\n> Error. DATE format must be DD.MM.YYYY\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(tmpWord,word);
	c = strtok(tmpWord,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if((ival <= 0)||(ival > 31)){
		printf("\n> Error. \"%s\" is wrong value of a DAY\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if((ival <= 0)||(ival > 13)){
		printf("\n> Error. \"%s\" is wrong value of a MOUNTH\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpTok,c);
	ival = atoi(tmpTok);
	if((ival <= 0)||(ival > 9999)){
		printf("\n> Error. \"%s\" is wrong value of a YEAR\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(curData.date,word);

	// ����� LAST_NAME
	c = strtok(line," ");
	strcpy(word,c);
	if(strlen(word) > 15){
		printf("\n> Error. Maximum length of LAST_NAME is 15 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(curData.lname,word);

	// ����� FIRST_NAME
	c = strtok(NULL," ");
	strcpy(word,c);
	if(strlen(word) > 15){
		printf("\n> Error. Maximum length of FIRST_NAME is 15 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(curData.fname,word);

	// ����� PATRONYMIC
	c = strtok(NULL," ");
	strcpy(word,c);
	if(strlen(word) > 15){
		printf("\n> Error. Maximum length of PATRONYMIC is 15 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(curData.pname,word);

	pushElement(src,curData);

	return 0;
}

//
// ����� ������ �� �����
//
int printStack(TStack *src)
{
	printf("> Printing items...\n");
	if(!src->head){
		printf("> No items\n");
		printf("> Done\n");
		return 0;
	}

	TStackItem *tmp = src->head;
	printItem(tmp->data);

	while(tmp->next){
		tmp = tmp->next;
		printItem(tmp->data);
	}

	printf("> Done\n");
	return 0;
}

//
// ������ ������ ��������
//
int printItem(TData data)
{
	printf("%s %s %s [%d], %s\n",data.lname,data.fname,
		data.pname,data.num,data.date);
	return 0;
}

//
// ��������� �������� � ����
//
//    ������������ ��������
//       0                        ��� ��
//       1                        �� ������� �������� ������
//
int pushElement(TStack *src, TData data)
{
	TStackItem *tmp = (TStackItem*)malloc(sizeof(TStackItem));
	if(!tmp) return 1;
	tmp->next = src->head;
	tmp->data = data;
	src->head = tmp;
	return 0;
}

//
// ������� �������� �� �����
//
//    ������������ ��������
//       0                        ��� ��
//       1                        �� ������� �������� ������
//
int popElement(TStack *src, TData *data)
{
	if(!src->head) return 0;
	TStackItem *tmp = src->head;
	src->head = src->head->next;
	*data = tmp->data;
	free(tmp);
	return 0;
}
