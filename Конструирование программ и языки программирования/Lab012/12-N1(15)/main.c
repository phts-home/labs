/*
******************************************************************************
*       ������������ ������ #12. ������� #1. ������� #15
*
*       ��������: ������� ��. 06��-1 ��� ����� ������
******************************************************************************
*/

/*
******************************************************************************
*                                 TODO LIST
******************************************************************************
* - < nothing >
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/*
******************************************************************************
*                                 DATA TYPES
******************************************************************************
*/
typedef struct {
   long snum;                     // �������� ����� (�������������)
   char model[21];                // �����-������
   int year;                      // ��� ������� (��������������)
   int ftime;                     // ����� ������
} TData;
typedef struct _TQueueItem{
   TData data;                    // ������ ��������
   struct _TQueueItem *next;      // ��������� ������� �������
} TQueueItem;
typedef struct {
   TQueueItem *head;              // ������ ������� �������
   TQueueItem *tail;              // ��������� ������� �������
} TQueue;


/*
******************************************************************************
*                                 PROTOTYPES
******************************************************************************
*/
int drwMenu(TQueue *);
int initQueue(TQueue *);
int destroyQueue(TQueue *);

int putElement(TQueue *, TData);
int getElement(TQueue *, TData *);

int inputQueue(TQueue *);
int inputItem(TQueue *);
int printQueue(TQueue *);
int printItem(TData);


/*
******************************************************************************
*                                 FUNCTIONS
******************************************************************************
*/

int main(int argc, char *argv[])
{
	TQueue *queue1;
	initQueue(queue1);
	drwMenu(queue1);
	destroyQueue(queue1);
	return 0;
}

//
// ��������� ����
//
int drwMenu(TQueue *src)
{
	int ch, canExit = 0;
	while(!canExit){
		printf("\n");
		printf("------ Menu ------\n");
		printf("1 - Add items\n");
		printf("2 - Print\n");
		printf("0 - Exit\n");
		printf("------------------\n");
		printf("> Your Choice   ");
		scanf("%d",&ch);
		printf("\n");
		switch(ch){
			case 1:{
				inputQueue(src);
				break;
			}
			case 2:{
				printQueue(src);
				break;
			}
			case 0:{
				canExit = 1;
				break;
			}
		}
	}
	return 0;
}

//
// ������������� �������
//
int initQueue(TQueue *src)
{
	src->head = NULL;
	src->tail = NULL;
	return 0;
}

//
// �������� �������
//
int destroyQueue(TQueue *src)
{
	while(src->head){
		src->tail = src->head;
		src->head = src->head->next;
		free(src->tail);
	}
	src->head = NULL;
	src->tail = NULL;
	return 0;
}

//
// ���������� �������
//
int inputQueue(TQueue *src)
{
	int er = 0;

	printf("> Enter items of array\n");
	printf("> Format: SERIAL_NUM BRAND - MODEL, YEAR, FLYING_TIME\n");
	printf("> Enter an empty string to exit to menu\n");
	while(er != 2){
		do{
			er = inputItem(src);
		}while(er == 1);
	}

	printf("> Done\n");
	return 0;
}

//
// ���������� ������ ��������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        ������ ��� ������� ������
//       2                        ���� ���������
//
int inputItem(TQueue *src)
{
	//
	// line[257]                   ��������������� ������ � ���������� �����
	// word[257]                   ������� � line
	// tmpTok[5]                   ��������� ����������, ��� �������� ���� �� ������������
	// tmpWord[11]                 ��������� ����������, ����������� ���� date
	// �                           ��������� ���������� ��� ����������� �����
	// k                           ������� �������
	// kt                          ���������� ������������� ��������� k
	// ival                        �������� �������� ������ ����
	// curData                     ��������� � ������� �� ������� ������ line
	//
	char line[257], word[257], tmpTok[5], tmpWord[11];
	int k, kt, ival;
	char *c = NULL;
	TData curData;

	fflush(stdin);
	gets(line);
	fflush(stdin);

	if(strcmp(line,"") == 0) return 2;

	/* ����� SERIAL_NUM */
	k = 0;
	while( (line[k] == ' ') && (line[k] != 0) ) k++;
	if(k == strlen(line)){
		printf("\n> Error. SERIAL_NUM is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	kt = k;
	k++;
	while( (line[k] != ' ') && (line[k] != 0) ) k++;
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if( (atoi(word) == 0) && (strcmp(word,"0") != 0) ){
		printf("\n> Error. \"%s\" is not a number\n",word);
		printf("> Try again\n\n");
		return 1;
	}
	ival = atoi(word);
	if((ival < 0)){
		printf("\n> Error. SERIAL_NUM must be nonnegative\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	curData.snum = ival;

	// ����� BRAND - MODEL
	k++;
	while( (line[k] == ' ') && (line[k] != 0) ) k++;
	if(k == strlen(line)){
		printf("\n> Error. BRAND - MODEL is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	kt = k;
	k++;
	while( (line[k] != '-') &&(line[k] != 0) ) k++;
	while( ((line[k] == ' ') || (line[k] == '-')) &&(line[k] != 0) ) k++;
	while( (line[k] != ',') &&(line[k] != 0) ) k++;
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	strcpy(curData.model,word);

	// ����� YEAR
	k++;
	while( ((line[k] == ' ') || (line[k] == ',')) && (line[k] != 0) ) k++;
	if(k == strlen(line)){
		printf("\n> Error. YEAR is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	kt = k;
	k++;
	while( (line[k] != ' ') && (line[k] != ',') && (line[k] != 0) ) k++;
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if( (atof(word) == 0) && (strcmp(word,"0") != 0) ){
		printf("\n> Error. \"%s\" is not a number\n",word);
		printf("> Try again\n\n");
		return 1;
	}
	ival = atoi(word);
	if((ival < 0)){
		printf("\n> Error. YEAR must be nonnegative\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	curData.year = ival;

	// ����� FLYING_TIME
	k++;
	while( ((line[k] == ' ') || (line[k] == ',')) && (line[k] != 0) ) k++;
	if(k == strlen(line)){
		printf("\n> Error. YEAR is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	kt = k;
	k++;
	while( (line[k] != ' ') && (line[k] != 0) ) k++;
	for(int r=0; r<k-kt; r++) word[r] = line[kt+r];
	word[k-kt] = 0;
	if( (atof(word) == 0) && (strcmp(word,"0") != 0) ){
		printf("\n> Error. \"%s\" is not a number\n",word);
		printf("> Try again\n\n");
		return 1;
	}
	ival = atoi(word);
	if((ival < 0)){
		printf("\n> Error. YEAR must be nonnegative\n",tmpTok);
		printf("> Try again\n\n");
		return 1;
	}
	curData.ftime = ival;

	putElement(src,curData);

	return 0;
}

//
// ����� ������� �� �����
//
int printQueue(TQueue *src)
{
	printf("> Printing items...\n");
	if(!src->head){
		printf("> No items\n");
		printf("> Done\n");
		return 0;
	}

	TQueueItem *tmp = src->head;
	printItem(tmp->data);

	while(tmp->next){
		tmp = tmp->next;
		printItem(tmp->data);
	}

	printf("> Done\n");
	return 0;
}

//
// ������ ������ ��������
//
int printItem(TData data)
{
	printf("%d %s, %d, %d\n",data.snum,data.model,data.year,data.ftime);
	return 0;
}

//
// ��������� �������� � �������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        �� ������� �������� ������
//
int putElement(TQueue *src, TData data)
{
	TQueueItem *tmp = (TQueueItem*)malloc(sizeof(TQueueItem));
	if(!tmp) return 1;
	tmp->next = NULL;
	tmp->data = data;
	if(src->tail) src->tail->next = tmp;
	src->tail = tmp;
	if(!src->head) src->head = src->tail;
	return 0;
}

//
// ������� �������� �� �������
//
//    ������������ ��������
//       0                        ��� ��
//       1                        �� ������� �������� ������
//
int getElement(TQueue *src, TData *data)
{
	if(!src->head) return 1;
	TQueueItem *tmp = src->head;
	*data = tmp->data;
	src->head = src->head->next;
	free(tmp);
	if(!src->head) src->tail = NULL;
	return 0;
}
