#include <stdio.h>
#include <string.h>


int main(int argc, char *argv[])
{
	//Init
	int count = 0;
	unsigned char text[20][80];
	unsigned char line[80];
	unsigned char chars[1600];
	puts("> Enter Text:");
	for(int i=0;i<20;i++){
		gets(line);
		if(line[0]==0){break;}
		count++;
		strcpy(text[i],line);
	}

	int j, r = 0;
	for(int i=0;i<count;i++){
		j = 0;
		while(text[i][j]!=0){
			if((text[i][j]!=' ')&&(text[i][j]!=',')&&(text[i][j]!='.')&&(text[i][j]!=':')&&
					(text[i][j]!=';')&&(text[i][j]!='-')&&(text[i][j]!='?')&&(text[i][j]!='!')){
				chars[r] = text[i][j];
				if((chars[r] >= 61)&&(chars[r] <= 90)){chars[r] = chars[r] + 32;}
				if((chars[r] >= 128)&&(chars[r] <= 143)){chars[r] = chars[r] + 32;}
				if((chars[r] >= 144)&&(chars[r] <= 159)){chars[r] = chars[r] + 80;}
				if(chars[r] == 240){chars[r] = 241;}
				r++;
			}
			j++;
		}
	}
	chars[r] = 0;

	//Search Palindromes
	int palindrome, w, maxlen = 0;
	unsigned char word[1600] = "";
	for(int i=0; i<strlen(chars);i++){
		for(int j=strlen(chars)-1;j>=i;j--){
			palindrome = 1;
			w = 1;
			for(int r=i;r<j;r++){
				if(chars[j+i-r] != chars[r]){
					palindrome = 0;
				}
				w++;
			}
			if(palindrome){
				if(w>=maxlen){
					maxlen = w;
					for(int wi=0;wi<w;wi++){
						word[wi]=chars[i+wi];
					}
					word[w] = 0;
				}
			}
		}
	}

	
	printf("> Polindrome == %s\n",word);
	return 0;
}

