#include <stdio.h>
#include <string.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{
	//Init
	double list[20][16];
	char names[20][64];
	int n, count = 0;
	char *c = NULL;
	char line[256], word[256];
	//Init {list} {names}
	for(int i=0;i<20;i++){
		gets(line);
//		if((strpbrk(line,"=")==NULL)||(strpbrk(line,"[")==NULL)||(strpbrk(line,"]")==NULL)){
//			puts("");
//			puts("> Error");
//			return 0;
//		}
		if(line[0]==0){break;}
		count++;
		n = 0;
		c = strtok(line,"=[] ,");
		strcpy(names[i],c);
		c = strtok(NULL,"=[] ,");
		while(c!=NULL){
			strcpy(word,c);
			list[i][n+1] = atof(word);
			n++;
			c = strtok(NULL,"=[] ,");
		}
		list[i][0] = n;
	}

	//Sort Elements
	double tmp;
	for(int line=0;line<count;line++)
		for(int i=1;i<list[line][0];i++)
			for(int j=i;j<list[line][0]+1;j++)
				if(list[line][i] > list[line][j]){
					tmp = list[line][i];
					list[line][i] = list[line][j];
					list[line][j] = tmp;
				}

	//Sort Lines
	char tmpStr[64];
	double tmpVal;
	for(int line1=0;line1<count-1;line1++)
		for(int line2=line1;line2<count;line2++)
			if(strcmp(names[line1],names[line2])>0){
				strcpy(tmpStr,names[line1]);
				strcpy(names[line1],names[line2]);
				strcpy(names[line2],tmpStr);
				for(int i=0;i<16;i++){
					tmpVal = list[line1][i];
					list[line1][i] = list[line2][i];
					list[line2][i] = tmpVal;
				}
			}
		
	

	//Print Results
	for(int line=0;line<count;line++){
		printf("%s=[",names[line]);
		for(int j=0;j<list[line][0];j++){
			printf("%.2lf",list[line][j+1]);
			if(j!=list[line][0]-1)printf(", ");
		}
		printf("]\n");
	}
	printf("\n");
	return 0;
}

