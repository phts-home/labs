/*
******************************************************************************
*       ������������ ������ #10. ������� #1. ������� #1
*
*       ��������: ������� ��. 06��-1 ��� ����� ������
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/*
******************************************************************************
*                                 DATA TYPES
******************************************************************************
*/
typedef struct {
	char lname[16];						// �������
	char fname[16];						// ���
	char pname[16];						// ��������
	long numb;								// ����� �������
	char date[11];							// ����
} tStudent;


/*
******************************************************************************
*                                 VARIBLES
******************************************************************************
*/
int amount[2] = {0,0};					// ���-�� �������� ����� � ������ ������
int searchesAmount[2] = {0,0};		// ���-�� �������� �������
double averageAmount[2] = {0,0};		// ������� �������� ��������


/*
******************************************************************************
*                                 PROTOTYPES
******************************************************************************
*/
tStudent *initArray(unsigned);
int initSize(void);

int inputArray(tStudent *, unsigned);
int inputElement(tStudent *, unsigned, unsigned);

int printArray(tStudent *, unsigned);
int printElement(tStudent *, unsigned, unsigned);

int initLinearSearch(tStudent *, unsigned);
int initBinarySearch(tStudent *, unsigned);
int searchLinear(tStudent *, unsigned, long);
int searchBinary(tStudent *, unsigned, long);
int binSearch(tStudent *, int, int, long);
int sortArray(tStudent *, unsigned);

int printStatistics(tStudent *, unsigned);

int checkComandStr(tStudent *, unsigned, int, char *[]);


/*
******************************************************************************
*                                 FUNCTIONS
******************************************************************************
*/

int main(int argc, char *argv[])
{
	int size;
	do{
		size = initSize();
	}while(size == 0);

	tStudent *ar;
	ar = initArray(size);
	inputArray(ar, size);
/*
	strcpy(ar[0].lname,"aaa");
	strcpy(ar[0].fname,"ddd");
	strcpy(ar[0].pname,"xxx");
	ar[0].numb = 123456;
	strcpy(ar[0].date,"11.11.1111");
	strcpy(ar[1].lname,"sss");
	strcpy(ar[1].fname,"bbb");
	strcpy(ar[1].pname,"ggg");
	ar[1].numb = 112222;
	strcpy(ar[1].date,"10.11.1111");
	strcpy(ar[2].lname,"eee");
	strcpy(ar[2].fname,"rrr");
	strcpy(ar[2].pname,"ttt");
	ar[2].numb = 111111;
	strcpy(ar[2].date,"11.22.2222");
	strcpy(ar[3].lname,"ggg");
	strcpy(ar[3].fname,"bbb");
	strcpy(ar[3].pname,"uuu");
	ar[3].numb = 999999;
	strcpy(ar[3].date,"09.11.1111");
	strcpy(ar[4].lname,"aaa");
	strcpy(ar[4].fname,"sss");
	strcpy(ar[4].pname,"ddd");
	ar[4].numb = 111111;
	strcpy(ar[4].date,"02.10.2222");
//*/
	sortArray(ar,size);

	if(checkComandStr(ar, size, argc, argv) == 1){
		printf("> Error. Wrong parameter\n");
	}

	int ch, canExit = 0;
	while(canExit == 0){
		printf("\n");
		printf("------ Menu ------\n");
		printf("1 - Search Records (Linear)\n");
		printf("2 - Search Records (Binary)\n");
		printf("3 - Print\n");
		printf("4 - Statistics\n");
		printf("0 - Exit\n");
		printf("------------------\n");
		printf("> Your Choice   ");
		scanf("%d",&ch);
		printf("\n");
		switch(ch){
			case 1:{
				initLinearSearch(ar, size);
				break;
			}
			case 2:{
				initBinarySearch(ar, size);
				break;
			}
			case 3:{
				printArray(ar, size);
				break;
			}
			case 4:{
				printStatistics(ar, size);
				break;
			}
			case 0:{
				canExit = 1;
				break;
			}
		}
	}
	return 0;
}

//
// �������� � ������������� �������
//
tStudent *initArray(unsigned size)
{
	tStudent *source;
	source = (tStudent *)calloc(size, sizeof(tStudent));
	return source;
}

//
// ������������� ������� �������
// 	������������ ��������:
// 		0 - ������
// 	 !=0 - ��� ��
//
int initSize(void)
{
	int size;
	char strsize[257];
	printf("> Set database size (unsigned)\n> ");
	scanf("%s",&strsize);
	printf("\n");
	if((strcmp(strsize,"0")!=0)&&(atoi(strsize) == 0)){
		printf("> Error. Size must be of integer type\n\n");
		return 0;
	}
	size = atoi(strsize);
	if(size < 1){
		printf("> Error. Minimum database size is 1\n\n");
		return 0;
	}
	if(size > 256){
		printf("> Error. Maximum database size is 256\n\n");
		return 0;
	}
	return size;
}

//
// ���������� �������
//
int inputArray(tStudent *ar, unsigned size)
{
	//
	// er					������ ������
	// 	1 - ������
	// 	0 - ��� ��
	//
	int er = 0;

	printf("> Enter elements of array\n");
	printf("> Format: last_name first_name patronymic [record_book_no], DD.MM.YYYY\n");
	for(int i=0; i<size; i++){
		do{
			er = inputElement(ar, size, i);
		}while(er);
	}
	printf("> Done\n");
	return 0;
}

//
// ���������� ������ ��������
// 	������������ ��������:
// 		1 - ������
// 		0 - ��� ��
//
int inputElement(tStudent *ar, unsigned size, unsigned index)
{
	//
	// line[257]	��������������� ������ � ���������� ������
	// word[257]	������� � line
	// st[51]		����� ����� ��������
	// tmpStr[5]	������������� ����������, ��� �������� ���� �� ������������
	// tmpWord[11]	������������� ����������, ����������� ���� date
	// �				������������� ���������� ��� ����������� �����
	// w				����� ������ ����� ����������� ��������
	// k				������� �������
	// val			������������� ����������, ��� �������� ���� �� ������������
	// kt				~
	//
	char line[257], word[257], st[51], tmpStr[5], tmpWord[11];
	int k, kt, w, val;
	char *c = NULL;

	fflush(stdin);
	gets(line);
	fflush(stdin);

	/* ����� ������ ������� */
	k = 0;
	while((line[k] != '[')&&(line[k] != 0)) k++;
	if(k == strlen(line)){
		printf("\n> Error. Record book no. is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	w = 0;
	k++;
	kt = k;
	while((line[kt] != ']')&&(line[kt] != 0)){
		kt++;
		w++;
	}
	if(kt == strlen(line)){
		printf("\n> Error. Record book no. is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	if(w != 6){
		printf("\n> Error. Length of record book no. is 6 figures\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(st,"");
	for(int r=0; r<w; r++) st[r] = line[k+r];
	st[w] = 0;
	if(atol(st) == 0){
		printf("\n> Error. Record book no. must be of integer type\n");
		printf("> Try again\n\n");
		return 1;
	}
	ar[index].numb = atol(st);

	/* ����� ���� */
	k = kt+1;
	while((line[k] == ' ')||(line[k] == ',')) k++;
	strcpy(st,"");
	for(int r=0; r<10; r++) st[r] = line[k+r];
	st[10] = 0;
	if(strcmp(st,"") == 0){
		printf("\n> Error. Date is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	if(!((st[0] != '.')&&(st[1] != '.')&&(st[2] == '.')&&
			(st[3] != '.')&&(st[4] != '.')&&(st[5] == '.')&&
			(st[6] != '.')&&(st[7] != '.')&&(st[8] != '.')&&
			(st[9] != '.'))){
		printf("\n> Error. Date format must be DD.MM.YYYY\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(tmpWord,st);
	c = strtok(tmpWord,".");
	strcpy(tmpStr,c);
	val = atoi(tmpStr);
	if((val <= 0)||(val > 31)){
		printf("\n> Error. \"%s\" is wrong value of a day\n",tmpStr);
		printf("> Try again\n\n");
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpStr,c);
	val = atoi(tmpStr);
	if((val <= 0)||(val > 13)){
		printf("\n> Error. \"%s\" is wrong value of a month\n",tmpStr);
		printf("> Try again\n\n");
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpStr,c);
	val = atoi(tmpStr);
	if((val <= 0)||(val > 9999)){
		printf("\n> Error. \"%s\" is wrong value of a year\n",tmpStr);
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(ar[index].date,st);

	/* ����� ������� */
	c = strtok(line," ");
	strcpy(word,c);
	if(strlen(word) > 15){
		printf("\n> Error. Maximum length of last name is 15 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(ar[index].lname,word);

	/* ����� ����� */
	c = strtok(NULL," ");
	strcpy(word,c);
	if(strlen(word) > 15){
		printf("\n> Error. Maximum length of first name is 15 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(ar[index].fname,word);

	/* ����� �������� */
	c = strtok(NULL," ");
	strcpy(word,c);
	if(strlen(word) > 15){
		printf("\n> Error. Maximum length of patronymic is 15 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(ar[index].pname,word);

	return 0;
}

//
// ����� ������� �� �����
//
int printArray(tStudent *ar, unsigned size)
{
	printf("> Printing records...\n");
	printf("> Total: %d\n",size);
	for(int i=0; i<size; i++)
		printElement(ar,size,i);
	printf("> Done\n");
	return 0;
}

//
// ������ ������ ��������
//
int printElement(tStudent *ar, unsigned size, unsigned index)
{
	printf("%s %s %s [%d], %s\n",ar[index].lname,ar[index].fname,
		ar[index].pname,ar[index].numb,ar[index].date);
	return 0;
}

//
// ������������� ��������� ������
//
int initLinearSearch(tStudent *ar, unsigned size)
{
	long val;
	char strval[257];

	printf("> Linear search\n");
	while(1){
		printf("> Enter record book no. (or nothing to exit)\n");
		fflush(stdin);
		gets(strval);
		if(strcmp(strval,"") == 0) return 0;
		while(atol(strval) == 0){
			printf("\n> Error. Record book no. must be of integer type\n");
			printf("> Try again\n\n");
			fflush(stdin);
			gets(strval);
			if(strcmp(strval,"") == 0) return 0;
		}
		val = atol(strval);
		printf("> Searching...\n");
		if(searchLinear(ar,size,val) == 0) printf("> Done\n\n");
	}
	return 0;
}

//
// ������������� ��������� ������
//
int initBinarySearch(tStudent *ar, unsigned size)
{
	long val;
	char strval[257];

	printf("> Binary search\n");
	while(1){
		printf("> Enter record book no. (or nothing to exit)\n");
		fflush(stdin);
		gets(strval);
		if(strcmp(strval,"") == 0) return 0;
		while(atol(strval) == 0){
			printf("\n> Error. Record book no. must be of integer type\n");
			printf("> Try again\n\n");
			fflush(stdin);
			gets(strval);
			if(strcmp(strval,"") == 0) return 0;
		}
		val = atol(strval);
		printf("> Searching...\n");
		if(searchBinary(ar,size,val) == 0) printf("> Done\n\n");
	}
	return 0;
}

//
// �������� ����� ��������
//		������������ ��������:
//			1 - ������
//			0 - ��� ��
//
int searchLinear(tStudent *ar, unsigned size, long val)
{
	//int count = 0;
	int res = -1;
	searchesAmount[0]++;
	for(int i=0; i<size; i++){
		amount[0]++;
		if(ar[i].numb == val){
			printElement(ar,size,i);
			res = i;
			//count++;
			break;
		}
	}
	averageAmount[0] = (double)amount[0] / (double)searchesAmount[0];
	if(res == -1)
		printf("> Total found: 0\n");
	else{
		printf("> Total found: 1\n");
		if(res != size-1){
			if(ar[res+1].numb != ar[res].numb){
				printf("> This item is unique\n");
			}else{
				printf("> This item is not unique\n");
			}
		}else printf("> This item is unique\n");
	}
	return 0;
}

//
// �������� ����� ��������
//
int searchBinary(tStudent *ar, unsigned size, long val)
{
	unsigned res;
	searchesAmount[1]++;
	if(ar[0].numb == val)
		res = 0;
	else
		if(ar[size-1].numb == val)
			res = size-1;
		else
			res = binSearch(ar,0,size-1,val);
	if(res == -1)
		printf("> Total found: 0\n");
	else{
		printElement(ar,size,res);
		printf("> Total found: 1\n");
		if(res != size-1){
			if(ar[res+1].numb != ar[res].numb){
				printf("> This item is unique\n");
			}else{
				printf("> This item is not unique\n");
			}
		}else printf("> This item is unique\n");
	}
	averageAmount[1] = (double)amount[1] / (double)searchesAmount[1];
	return 0;
}

//
// �������� ����� �������� (���������� ���������� �������)
//
int binSearch(tStudent *ar, int low, int high, long val)
{
	int mid;
	amount[1]++;
	if((high-low) <= 1) return -1;
	mid = (low+high)/2;
	if(ar[mid].numb == val) return mid;
		else if(ar[mid].numb > val) return binSearch(ar,low,mid,val);
			else return binSearch(ar,mid,high,val);
}

//
// ���������� �������
//
int sortArray(tStudent *ar, unsigned size)
{
	tStudent tmpEl;
	for(int i=0; i<size-1; i++)
		for(int j=i+1; j<size; j++)
			if(ar[i].numb > ar[j].numb){
				tmpEl = ar[i];
				ar[i] = ar[j];
				ar[j] = tmpEl;
			}
	return 0;
}

//
// ������ ����������
//
int printStatistics(tStudent *ar, unsigned size)
{
	printf("> Statistics\n");
	printf("> Average amount of iterations in linear search %lf\n", averageAmount[0]);
	printf("> Average amount of iterations in binary search %lf\n", averageAmount[1]);
	return 0;
}

//
// ������ ��������� ������
//		������������ ��������:
//			1 - ������
//			0 - ��� ��
//
int checkComandStr(tStudent *ar, unsigned size, int argc1, char *argv1[])
{
	if(argc1 > 1){
		for(int i=1; i<argc1; i++)
			if(argv1[i][0] == 'a'){
				if(strcmp(argv1[i],"a:linear") == 0)
					initLinearSearch(ar,size);
				else
					if(strcmp(argv1[i],"a:binary") == 0)
						initBinarySearch(ar,size);
					else
						return 1;
				break;
			}
	}
	return 0;
}
