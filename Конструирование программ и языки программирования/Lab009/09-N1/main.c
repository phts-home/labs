/*
******************************************************************************
*       ������������ ������ #9. ������� #1. ������� #16
*
*       ��������: ������� ��. 06��-1 ��� ����� ������
******************************************************************************
*/


/*
******************************************************************************
*                                 INCLUDES
******************************************************************************
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/*
******************************************************************************
*                                 DATA TYPES
******************************************************************************
*/
typedef struct {unsigned sender; unsigned receiver; char text[51]; char date[11];} tMessage;


/*
******************************************************************************
*                                 PROTOTYPES
******************************************************************************
*/
tMessage *initArray(unsigned);
int initSize(void);
void inputArray(tMessage *, unsigned);
int inputElement(tMessage *, unsigned, unsigned);
void printArray(tMessage *, unsigned);
void sortArray1(tMessage *, unsigned);
void sortArray2(tMessage *, unsigned);
int sortEnum(tMessage *, unsigned, int, int);
int sortShell(tMessage *, unsigned, int, int);
int checkComandStr(tMessage *, unsigned, int, char *[]);


/*
******************************************************************************
*                                 FUNCTIONS
******************************************************************************
*/
int main(int argc, char *argv[])
{
	int size;
	do{
		size = initSize();
	}while(size == 0);

	tMessage *ar;
	ar = initArray(size);
	inputArray(ar, size);
/*
	ar[0].sender = 234;
	ar[0].receiver = 345;
	strcpy(ar[0].text,"qwer");
	strcpy(ar[0].date,"22.11.4444");
	ar[1].sender = 123;
	ar[1].receiver = 234;
	strcpy(ar[1].text,"asdf");
	strcpy(ar[1].date,"11.12.3333");
	ar[2].sender = 111;
	ar[2].receiver = 345;
	strcpy(ar[2].text,"fghj");
	strcpy(ar[2].date,"22.12.4444");
	ar[3].sender = 200;
	ar[3].receiver = 234;
	strcpy(ar[3].text,"fghjkl");
	strcpy(ar[3].date,"11.10.4444");
//*/

	if(checkComandStr(ar, size, argc, argv) == 1){
		printf("> Error. Wrong parameter\n");
	}

	int ch, canExit = 0;
	while(canExit == 0){
		printf("\n");
		printf("------ Menu ------\n");
		printf("1 - Sort Records (Enum)\n");
		printf("2 - Sort Records (Shell)\n");
		printf("3 - Print\n");
		printf("0 - Exit\n");
		printf("------------------\n");
		printf("> Your Choice   ");
		scanf("%d",&ch);
		printf("\n");
		switch(ch){
			case 1:{
				sortArray1(ar, size);
				break;
			}
			case 2:{
				sortArray2(ar, size);
				break;
			}
			case 3:{
				printArray(ar, size);
				break;
			}
			case 0:{
				canExit = 1;
				break;
			}
		}
	}
	return 0;
}

//
// ������������� ������� �������
//		0 - ������
//	 !=0 - ��� ��
//
int initSize(void)
{
	int size;
	char strsize[257];
	printf("> Set database size (unsigned)\n> ");
	scanf("%s",&strsize);
	printf("\n");
	if((strcmp(strsize,"0")!=0)&&(atoi(strsize) == 0)){
		printf("> Error. Size must be of integer type\n\n");
		return 0;
	}
	size = atoi(strsize);
	if(size < 1){
		printf("> Error. Minimum database size is 1\n\n");
		return 0;
	}
	if(size > 256){
		printf("> Error. Maximum database size is 256\n\n");
		return 0;
	}
	return size;
}

//
// �������� � ������������� �������
//
tMessage *initArray(unsigned size)
{
	tMessage *source;
	source = (tMessage *)calloc(size, sizeof(tMessage));
	return source;
}

//
// ���������� �������
//
void inputArray(tMessage *ar, unsigned size)
{
	//
	// er					������ ������
	//		1 - ������
	//		0 - ��� ��
	//
	int er = 0;

	printf("> Enter elements of array\n");
	printf("> Format: AAA BBB \"Message\" DD.MM.YYYY\n");
	for(int i=0; i<size; i++){
		do{
			er = inputElement(ar, size, i);
		}while(er);
	}
	printf("> Done\n");
}

//
// ���������� ������ ��������
//		1 - ������
//		0 - ��� ��
//
int inputElement(tMessage *ar, unsigned size, unsigned index)
{
	//
	// line[257]	��������������� ������ � ���������� ������
	// word[257]	������� � line
	// st[51]		����� ����� ���������
	//	tmpStr[5]	������������� ����������, ��� �������� ���� �� ������������
	//	tmpWord[11]	������������� ����������, ����������� ���� date
	// �				������������� ���������� ��� ����������� �����
	// w				����� ������ ����� ���������
	// k				������� �������
	// val			������������� ����������, ��� �������� ���� �� ������������
	// kt				~
	//
	char line[257], word[257], st[51], tmpStr[5], tmpWord[11];
	int k, kt, w, val;
	char *c = NULL;

	fflush(stdin);
	gets(line);
	fflush(stdin);

	/* ����� ������ ��������� */
	k = 0;
	while((line[k] != '"')&&(line[k] != 0)) k++;
	if(k == strlen(line)){
		printf("\n> Error. Text of message is not found\n",word);
		printf("> Try again\n\n");
		return 1;
	}
	w = 0;
	k++;
	kt = k;
	while((line[kt] != '"')&&(line[kt] != 0)){
		kt++;
		w++;
	}
	if(kt == strlen(line)){
		printf("\n> Error. Text of message is not found\n",word);
		printf("> Try again\n\n");
		return 1;
	}
	if(w > 50){
		printf("\n> Error. Maximum size of message is 50 symbols\n");
		printf("> Try again\n\n");
		return 1;
	}
	if(w == 0){
		printf("\n> Error. Minimum size of message is 1 symbol\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(st,"");
	for(int r=0; r<w; r++) st[r] = line[k+r];
	st[w] = 0;
	strcpy(ar[index].text,st);

	/* ����� ���� */
	k = kt+1;
	while(line[k] == ' ') k++;

	strcpy(st,"");
	for(int r=0; r<10; r++) st[r] = line[k+r];
	st[10] = 0;
	if(strcmp(st,"") == 0){
		printf("\n> Error. Date is not found\n");
		printf("> Try again\n\n");
		return 1;
	}
	if(!((st[0] != '.')&&(st[1] != '.')&&(st[2] == '.')&&
			(st[3] != '.')&&(st[4] != '.')&&(st[5] == '.')&&
			(st[6] != '.')&&(st[7] != '.')&&(st[8] != '.')&&
			(st[9] != '.'))){
		printf("\n> Error. Date format must be DD.MM.YYYY\n");
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(tmpWord,st);
	c = strtok(tmpWord,".");
	strcpy(tmpStr,c);
	val = atoi(tmpStr);
	if((val <= 0)||(val > 31)){
		printf("\n> Error. \"%s\" is wrong value of a day\n",tmpStr);
		printf("> Try again\n\n");
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpStr,c);
	val = atoi(tmpStr);
	if((val <= 0)||(val > 13)){
		printf("\n> Error. \"%s\" is wrong value of a month\n",tmpStr);
		printf("> Try again\n\n");
		return 1;
	}
	c = strtok(NULL,".");
	strcpy(tmpStr,c);
	val = atoi(tmpStr);
	if((val <= 0)||(val > 9999)){
		printf("\n> Error. \"%s\" is wrong value of a year\n",tmpStr);
		printf("> Try again\n\n");
		return 1;
	}
	strcpy(ar[index].date,st);

	/* ����� ������ ����������� */
	c = strtok(line," ");
	strcpy(word,c);
	if(strlen(word) != 3){
		printf("\n> Error. Length of address of sender must be 3 symbols\n",word);
		printf("> Try again\n\n");
		return 1;
	}
	if(atoi(word) == 0){
		printf("\n> Error. \"%s\" is not a number\n",word);
		printf("> Try again\n\n");
		return 1;
	}
	ar[index].sender = atoi(word);

	/* ����� ������ ���������� */
	c = strtok(NULL," ");
	strcpy(word,c);
	if(strlen(word) != 3){
		printf("\n> Error. Length of address of receiver must be 3 symbols\n",word);
		printf("> Try again\n\n");
		return 1;
	}
	if(atoi(word) == 0){
		printf("\n> Error. \"%s\" is not a number\n",word);
		printf("> Try again\n\n");
		return 1;
	}
	ar[index].receiver = atoi(word);

	return 0;
}

//
// ����� ������� �� �����
//
void printArray(tMessage *ar, unsigned size)
{
	printf("> Printing records...\n");
	printf("> Total: %d\n",size);
	for(int i=0; i<size; i++)
		printf("%d %d \"%s\" %s\n",ar[i].sender,ar[i].receiver,ar[i].text,ar[i].date);
	printf("> Done\n");
}

//
// ���������� � ���������� ��������������
//
void sortArray1(tMessage *ar, unsigned size)
{
	//
	//	dir							����������� ����������
	//		1 - �� �����������
	//		0 - �� ��������
	//	row							����� ����, �� ���. ����������� ������
	//		1 - Sender
	//		2 - Receiver
	//		3 - Text
	//		4 - Date
	//
	int dir, row;

	printf("> Sort records\n");
	printf("> Select Row [1-Sender; 2-Receiver; 3-Text; 4-Date]\n> ");
	scanf("%d",&row);
	if((row > 4)||(row < 1)){printf("> Error\n"); return;}
	printf("> Set Direction [1-Direct | 0-Indirect]\n> ");
	scanf("%d",&dir);
	printf("> Sorting...\n");

	if(sortEnum(ar,size,row,dir) == 0) printf("> Done\n");
}

//
// ���������� � ���������� ������� �����
//
void sortArray2(tMessage *ar, unsigned size)
{
	//
	//	dir							����������� ����������
	//		1 - �� �����������
	//		0 - �� ��������
	//	row							����� ����, �� ���. ����������� ������
	//		1 - Sender
	//		2 - Receiver
	//		3 - Text
	//		4 - Date
	//
	int dir, row;

	printf("> Sort records\n");
	printf("> Select Row [1-Sender; 2-Receiver; 3-Text; 4-Date]\n> ");
	scanf("%d",&row);
	if((row > 4)||(row < 1)){printf("> Error\n"); return;}
	printf("> Set Direction [1-Direct | 0-Indirect]\n> ");
	scanf("%d",&dir);
	printf("> Sorting...\n");

	if(sortShell(ar,size,row,dir) == 0) printf("> Done\n");
}

//
// ���������� ��������������
//		1 - ������
//		0 - ��� ��
//
int sortEnum(tMessage *ar, unsigned size, int row, int dir)
{
	//
	//	d1, d2, m1, m2, y1, y2	�������� ���, ������, ���� � ����� date[11] 
	//										���� ��������� ������� �����.
	//	tmpStr[5]					������������� ����������, �������� d1, d2, m1, 
	//										 m2, y1, y2 ��� ����������� � �����������
	//	tmpWord[11]					������������� ����������, ����������� ���� date
	//	�								������������� ���������� ��� ����������� �����
	//	indAr							������, � ���. ��������� ������������� ���������
	//										��������� ��������� ������� � �������������
	//										������������������
	//	tmpAr							����� ������� ar
	//
	int d1, d2, m1, m2, y1, y2;
	char tmpStr[5], tmpWord[11];
	char *c = NULL;

	int *indAr;
	indAr = (int *)calloc(size, sizeof(int));
	memset(indAr, 0, size*sizeof(int));

	tMessage *tmpAr;
	tmpAr = initArray(size);
	for(int i=0; i<size; i++) tmpAr[i] = ar[i];

	if(row == 1){
		for(int i=size-1; i>0; i--)
			for(int j=i-1; j>=0; j--)
				if(((dir)&&(ar[i].sender > ar[j].sender))||((!dir)&&(ar[i].sender < ar[j].sender)))
					indAr[i]++; else indAr[j]++;
		for(int i=0; i<size; i++) ar[indAr[i]] = tmpAr[i];
		free(tmpAr);
		return 0;
	}
	if(row == 2){
		for(int i=size-1; i>0; i--)
			for(int j=i-1; j>=0; j--)
				if(((dir)&&(ar[i].receiver > ar[j].receiver))||((!dir)&&(ar[i].receiver < ar[j].receiver)))
					indAr[i]++; else indAr[j]++;
		for(int i=0; i<size; i++) ar[indAr[i]] = tmpAr[i];
		free(tmpAr);
		return 0;
	}
	if(row == 3){
		for(int i=size-1; i>0; i--)
			for(int j=i-1; j>=0; j--)
				if(((dir)&&(strcmp(ar[i].text,ar[j].text) > 0))||((!dir)&&(strcmp(ar[i].text,ar[j].text) < 0)))
					indAr[i]++; else indAr[j]++;
		for(int i=0; i<size; i++) ar[indAr[i]] = tmpAr[i];
		free(tmpAr);
		return 0;
	}
	if(row == 4){
		for(int i=size-1; i>0; i--)
			for(int j=i-1; j>=0; j--){
				strcpy(tmpWord,ar[i].date);
				c = strtok(tmpWord,".");
				strcpy(tmpStr,c);
				d1 = atoi(tmpStr);
				c = strtok(NULL,".");
				strcpy(tmpStr,c);
				m1 = atoi(tmpStr);
				c = strtok(NULL,".");
				strcpy(tmpStr,c);
				y1 = atoi(tmpStr);
				strcpy(tmpWord,ar[j].date);
				c = strtok(tmpWord,".");
				strcpy(tmpStr,c);
				d2 = atoi(tmpStr);
				c = strtok(NULL,".");
				strcpy(tmpStr,c);
				m2 = atoi(tmpStr);
				c = strtok(NULL,".");
				strcpy(tmpStr,c);
				y2 = atoi(tmpStr);
				if(dir){
					if((y1>y2)||((y1==y2)&&(m1>m2))||((y1==y2)&&(m1==m2)&&(d1>d2))) indAr[i]++; else indAr[j]++;
				}else{
					if((y1<y2)||((y1==y2)&&(m1<m2))||((y1==y2)&&(m1==m2)&&(d1<d2))) indAr[i]++; else indAr[j]++;
				}
			}
		for(int i=0; i<size; i++) ar[indAr[i]] = tmpAr[i];
		free(tmpAr);
		return 0;
	}

	return 1;
}

//
// ���������� ������� �����
//		1 - ������
//		0 - ��� ��
//
int sortShell(tMessage *ar, unsigned size, int row, int dir)
{
	//
	//	d1, d2, m1, m2, y1, y2	�������� ���, ������, ���� � ����� date[11] 
	//										���� ��������� ������� �����.
	//	tmpStr[5]					������������� ����������, �������� d1, d2, m1, 
	//										 m2, y1, y2 ��� ����������� � �����������
	//	tmpWord[11]					������������� ����������, ����������� ���� date
	//	�								������������� ���������� ��� ����������� �����
	//	tmpItem						��������������� �������
	//	stp[4]						~
	//	fs, j, i						~
	//
	int d1, d2, m1, m2, y1, y2;
	char tmpStr[5], tmpWord[11];
	char *c = NULL;

	int stp[4] = {9,5,3,1};
	int fs, j, i;
	tMessage tmpItem;

	if(row == 1){
		for(int n=0; n<4; n++){
			fs = 0;
			while(fs < size){
				i = fs;
				j = fs;
				while(i < size){
					if(((dir)&&(ar[i].sender < ar[j].sender))||((!dir)&&(ar[i].sender > ar[j].sender)))
						j = i;
					i += stp[n];
				}
				if(j > fs){
					tmpItem = ar[j];
					ar[j] = ar[fs];
					ar[fs] = tmpItem;
				}
				fs += stp[n];
			}
		}
		return 0;
	}
	if(row == 2){
		for(int n=0; n<4; n++){
			fs = 0;
			while(fs < size){
				i = fs;
				j = fs;
				while(i < size){
					if(((dir)&&(ar[i].receiver < ar[j].receiver))||((!dir)&&(ar[i].receiver > ar[j].receiver)))
						j = i;
					i += stp[n];
				}
				if(j > fs){
					tmpItem = ar[j];
					ar[j] = ar[fs];
					ar[fs] = tmpItem;
				}
				fs += stp[n];
			}
		}
		return 0;
	}
	if(row == 3){
		for(int n=0; n<4; n++){
			fs = 0;
			while(fs < size){
				i = fs;
				j = fs;
				while(i < size){
					if(((dir)&&(strcmp(ar[i].text,ar[j].text) < 0))||((!dir)&&(strcmp(ar[i].text,ar[j].text) > 0)))
						j = i;
					i += stp[n];
				}
				if(j > fs){
					tmpItem = ar[j];
					ar[j] = ar[fs];
					ar[fs] = tmpItem;
				}
				fs += stp[n];
			}
		}
		return 0;
	}
	if(row == 4){
		for(int n=0; n<4; n++){
			fs = 0;
			while(fs < size){
				i = fs;
				j = fs;
				while(i < size){
					strcpy(tmpWord,ar[i].date);
					c = strtok(tmpWord,".");
					strcpy(tmpStr,c);
					d1 = atoi(tmpStr);
					c = strtok(NULL,".");
					strcpy(tmpStr,c);
					m1 = atoi(tmpStr);
					c = strtok(NULL,".");
					strcpy(tmpStr,c);
					y1 = atoi(tmpStr);
					strcpy(tmpWord,ar[j].date);
					c = strtok(tmpWord,".");
					strcpy(tmpStr,c);
					d2 = atoi(tmpStr);
					c = strtok(NULL,".");
					strcpy(tmpStr,c);
					m2 = atoi(tmpStr);
					c = strtok(NULL,".");
					strcpy(tmpStr,c);
					y2 = atoi(tmpStr);
					if(!dir){
						if((y1>y2)||((y1==y2)&&(m1>m2))||((y1==y2)&&(m1==m2)&&(d1>d2)))
							j = i;
					}else{
						if((y1<y2)||((y1==y2)&&(m1<m2))||((y1==y2)&&(m1==m2)&&(d1<d2)))
							j = i;
					}
					i += stp[n];
				}
				if(j > fs){
					tmpItem = ar[j];
					ar[j] = ar[fs];
					ar[fs] = tmpItem;
				}
				fs += stp[n];
			}
		}
		return 0;
	}

	return 1;
}

//
// ������ ��������� ������
//		1 - ������
//		0 - ��� ��
//
int checkComandStr(tMessage *ar, unsigned size, int argc1, char *argv1[])
{
	int valDir, valField;
	if(argc1 > 1){
		for(int i=1; i<argc1; i++)
			if(argv1[i][0] == 'd'){
				if(strcmp(argv1[i],"d:inc") == 0)
					valDir = 1;
				else
					if(strcmp(argv1[i],"d:dec") == 0)
						valDir = 0;
					else
						return 1;
				break;
			}
		for(int i=1; i<argc1; i++)
			if(argv1[i][0] == 'f'){
				if(strcmp(argv1[i],"f:1") == 0)
					valField = 1;
				else
					if(strcmp(argv1[i],"f:2") == 0)
						valField = 2;
					else
						if(strcmp(argv1[i],"f:3") == 0)
							valField = 3;
						else
							if(strcmp(argv1[i],"f:4") == 0)
								valField = 4;
							else
								return 1;
				break;
			}
		for(int i=1; i<argc1; i++)
			if(argv1[i][0] == 'a'){
				if(strcmp(argv1[i],"a:fast") == 0)
					sortShell(ar,size,valField,valDir);
				else
					if(strcmp(argv1[i],"a:slow") == 0)
						sortEnum(ar,size,valField,valDir);
					else
						return 1;
				break;
			}
		printArray(ar, size);
	}
	return 0;
}
