package com.tsarik.labs.security;

import com.tsarik.cryptography.BigInteger;
import com.tsarik.cryptography.DivisionResult;

public class SecurityLab004 {
	
	public static void main(String[] args) {
		BigInteger bi = BigInteger.newRandom(2);
		System.out.println("random: " + bi);
		System.out.println("random is prime: " + bi.isPrime());
		
		System.out.println("pow: " + BigInteger.TWO.pow(new BigInteger("30", false)));
		
		new SecurityLab004();
	}
	
	public SecurityLab004() {
		printRangeRandomsMethod1(new BigInteger("1", false), new BigInteger("50", false));
		//printRangeRandomsMethod2(new BigInteger("1", false), new BigInteger("50", false));
	}
	
	public void printRangeRandomsMethod1(BigInteger left, BigInteger right) {
		System.out.println("printRangeRandomsMethod1:");
		long startMS = System.currentTimeMillis();
		BigInteger bi = new BigInteger(left);
		while (bi.compareTo(right) <= 0) {
			if (bi.isPrime()) {
				System.out.println(bi);
			}
			bi = bi.sum(new BigInteger("1", false));
		}
		long endMS = System.currentTimeMillis();
		System.out.println("Time (milliseconds): " + (endMS-startMS));
	}
	
	public void printRangeRandomsMethod2(BigInteger left, BigInteger right) {
		System.out.println("printRangeRandomsMethod2:");
		long startMS = System.currentTimeMillis();
		
		BigInteger bi = new BigInteger(left);
		while (bi.compareTo(right) <= 0) {
			if ( (bi.compareTo(BigInteger.TWO) == 0)
					||
					(bi.compareTo(new BigInteger("3", false)) == 0)
					||
					(bi.compareTo(new BigInteger("5", false)) == 0)
					||
					(bi.compareTo(new BigInteger("7", false)) == 0)
					||
					(bi.compareTo(new BigInteger("11", false)) == 0) ) {
				System.out.println(bi);
				bi = bi.sum(new BigInteger("1", false));
				continue;
			}
			if ( (bi.divide(BigInteger.TWO).getRemainder().compareTo(BigInteger.ZERO) == 0)
					||
					(bi.divide(new BigInteger("3", false)).getRemainder().compareTo(BigInteger.ZERO) == 0)
					||
					(bi.divide(new BigInteger("5", false)).getRemainder().compareTo(BigInteger.ZERO) == 0)
					||
					(bi.divide(new BigInteger("7", false)).getRemainder().compareTo(BigInteger.ZERO) == 0)
					||
					(bi.divide(new BigInteger("11", false)).getRemainder().compareTo(BigInteger.ZERO) == 0) ) {
				bi = bi.sum(new BigInteger("1", false));
				continue;
			}
			for (int i = 0; i < 5; i++) {
				if (bi.rabinMillerPrimeTest()) {
					System.out.println(bi);
					break;
				}
			}
			bi = bi.sum(new BigInteger("1", false));
		}
		
		long endMS = System.currentTimeMillis();
		System.out.println("Time (milliseconds): " + (endMS-startMS));
	}
	
	
	
	
	
}
