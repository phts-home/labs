package com.tsarik.labs.security;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.tsarik.cryptography.GammaCoder;
import com.tsarik.cryptography.LinearRandomGeneratorKey;

public class SecurityLab002 {
	
	public static void main(String[] args) {
		String s = "";
		LinearRandomGeneratorKey key = null;
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("input.txt")));
			String s2 = reader.readLine();
			while (s2 != null) {
				s = s.concat(s2 + '\n');
				s2 = reader.readLine();
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("key.txt")));
			String s2 = reader.readLine();
			int a = Integer.valueOf(s2);
			s2 = reader.readLine();
			int b = Integer.valueOf(s2);
			s2 = reader.readLine();
			int y0 = Integer.valueOf(s2);
			s2 = reader.readLine();
			int m = Integer.valueOf(s2);
			key = new LinearRandomGeneratorKey(a, b, y0, m);
			reader.close();
			System.out.println(key);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String incryptedString = GammaCoder.encrypt(s, key);
		System.out.println("encrypt result: " + incryptedString);
		
		String decryptedString = GammaCoder.decrypt(incryptedString, key);
		System.out.println("decrypt result: " + decryptedString);
	}
	
}
