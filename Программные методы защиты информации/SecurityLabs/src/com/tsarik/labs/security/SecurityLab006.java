package com.tsarik.labs.security;

import com.tsarik.cryptography.*;

public class SecurityLab006 {
	
	public static void main(String[] args) {
		RSACoder rsa = new RSACoder(new BigInteger("10", false), new BigInteger("20", false));
		
		RSAKeys keys = rsa.calculateKeys();
		
		long[] s1 = rsa.encrypt("abc123", keys.getOpenKey());
		
		System.out.println(s1);
		
		String s2 = rsa.decrypt(s1, keys.getSecretKey());
		
		System.out.println(s2);
		
	}
	
}
