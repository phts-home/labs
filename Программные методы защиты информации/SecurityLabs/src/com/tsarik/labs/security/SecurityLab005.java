package com.tsarik.labs.security;

import com.tsarik.cryptography.*;

public class SecurityLab005 {
	
	public static void main(String[] args) {
		DiffyHellmanScheme scm = new DiffyHellmanScheme();
		scm.setQ(new BigInteger("27", false));
		//scm.setA(new BigInteger("5", false));
		System.out.println("a: " + scm.generateA());
		System.out.println("xa: " + scm.generateXa());
		System.out.println("ya: " + scm.generateYa());
		System.out.println("xb: " + scm.generateXb());
		System.out.println("yb: " + scm.generateYb());
		System.out.println("K1: " + scm.getK1());
		System.out.println("K2: " + scm.getK2());
		
		/*BigInteger b = new BigInteger(BigInteger.ONE);
		for (int i = 0; i < 20; i++) {
			System.out.println( (i+1) + " " + BigInteger.euler(b));
			b = b.sum(BigInteger.ONE);
		}*/
	}
	
}
