package com.tsarik.labs.security;

import com.tsarik.cryptography.BigInteger;

public class SecurityLab003 {
	
	public static void main(String[] args) {
		BigInteger bi1 = new BigInteger("11006549879654321321654687987954621", false);
		
		BigInteger bi2 = new BigInteger("101", false);
		
		int num = 8;
		
		System.out.println(bi1);
		System.out.println(bi2);
		System.out.println("sum: " + bi1 + " + " + bi2 + "   ==   " + bi1.sum(bi2));
		
		System.out.println("substract: " + bi1 + " - " + bi2 + "   ==   " + bi1.substract(bi2));
		
		System.out.println("mul: " + bi1 + " * " + bi2 + "   ==   " + bi1.multiply(bi2));
		
		System.out.println("mul: " + bi1 + " * " + num + "   ==   " + bi1.multiply(num));
		
		System.out.println("div: " + bi1 + " / " + bi2 + "   ==   " + bi1.divide(bi2));
		
		System.out.println("random: " + BigInteger.newRandom(30));
		
	}
	
}
