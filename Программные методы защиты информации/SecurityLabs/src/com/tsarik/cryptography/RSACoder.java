package com.tsarik.cryptography;

public class RSACoder {
	
	public BigInteger highLimit;
	public BigInteger lowLimit;
	
	public RSACoder(BigInteger lowLimit, BigInteger highLimit) {
		this.lowLimit = new BigInteger(lowLimit);
		this.highLimit = new BigInteger(highLimit);
	}
	
	public RSAKeys calculateKeys() {
		BigInteger p = BigInteger.newRandom(lowLimit, highLimit);
		while ( (p.compareTo(BigInteger.ZERO) == 0) || (! p.isPrime()) ) {
			p = BigInteger.newRandom(lowLimit, highLimit);
		}
		//BigInteger p = new BigInteger("17", false);
		System.out.println("p: "+p);
		
		BigInteger q = BigInteger.newRandom(lowLimit, highLimit);
		while ( (q.compareTo(BigInteger.ZERO) == 0) || (q.compareTo(p) == 0) || (! q.isPrime()) ) {
			q = BigInteger.newRandom(lowLimit, highLimit);
		}
		
		//BigInteger q = new BigInteger("23", false);
		System.out.println("q: "+q);
		
		BigInteger n = p.multiply(q);
		
		System.out.println("n: "+n);
		
		BigInteger f = p.substract(BigInteger.ONE).multiply(q.substract(BigInteger.ONE));
		
		System.out.println("f: "+f);
		
		BigInteger e = BigInteger.newRandom(f);
		while ( (e.compareTo(BigInteger.FOUR) <= 0) || (! e.isCoprime(f)) ) {
			e = BigInteger.newRandom(f);
		}
		//BigInteger e = new BigInteger("5", false);
		
		System.out.println("e: "+e);
		
		BigInteger d = new BigInteger(BigInteger.TWO);
		while (d.multiply(e).divide(f).getRemainder().compareTo(BigInteger.ONE) != 0) {
		
			d = d.sum(BigInteger.ONE);
		}
		
		System.out.println("d: "+d);
		
		return new RSAKeys(new RSAOpenKey(e, n), new RSASecretKey(d, n));
	}
	
	public long [] encrypt(String s, RSAOpenKey key) {
		
		byte[] bytes = s.getBytes();
		
		long[] sequence = new long[bytes.length];
		
		for (int i = 0; i < sequence.length; i++) {
			long mi = Binary.toInt(Binary.toBitArray(bytes[i]));
			
			BigInteger bmi = new BigInteger();
			bmi.setValue(mi);
			
			long ci = bmi.pow(key.getE(), key.getN()).longValue();
			
			sequence[i] = ci;
			System.out.println("bmi " + bmi + " mi " + mi+ " ci " + ci + " nb " + sequence[i]);
		}
		
		return sequence;
	}
	
	public String decrypt(long[] sequence, RSASecretKey key) {
		
		byte[] newBytes = new byte[sequence.length];
		
		for (int i = 0; i < newBytes.length; i++) {
			
			BigInteger bci = new BigInteger();
			bci.setValue(sequence[i]);
			
			long mi = bci.pow(key.getD(), key.getN()).longValue();
			
			newBytes[i] = Long.valueOf(mi).byteValue();
			System.out.println("mi " + mi+ " s[i] " + sequence[i] + " nb " + newBytes[i]);
		}
		
		return new String(newBytes);
	}
	
	
	
}
