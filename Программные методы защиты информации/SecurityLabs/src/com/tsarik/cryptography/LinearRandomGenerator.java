package com.tsarik.cryptography;

public class LinearRandomGenerator {
	
	public LinearRandomGenerator(LinearRandomGeneratorKey key) {
		if (key == null) {
			return;
		}
		this.a = key.getA();
		this.b = key.getB();
		this.y0 = key.getY0();
		this.m = key.getM();
		this.value = y0;
	}
	
	public int generateNext() {
		value = (a*value + b) % m;
		return value;
	}
	
	public int getValue() {
		return value;
	}
	
	public int getA() {
		return a;
	}
	
	public int getB() {
		return b;
	}
	
	public int getY0() {
		return y0;
	}
	
	private int a;
	private int b;
	private int y0;
	private int m;
	private int value;
	
}
