package com.tsarik.cryptography;

public class LinearRandomGeneratorKey {
	
	public LinearRandomGeneratorKey(int a, int b, int y0, int m) {
		this.a = a;
		this.b = b;
		this.y0 = y0;
		this.m = m;
	}
	
	public int getA() {
		return a;
	}
	
	public int getB() {
		return b;
	}
	
	public int getY0() {
		return y0;
	}
	
	public int getM() {
		return m;
	}
	
	@Override
	public String toString() {
		return "Key:    a: " + a + "   b: " + b + "   y0: " + y0 + "   m: " + m;
	}
	
	private int a;
	private int b;
	private int y0;
	private int m;
	
}
