package com.tsarik.cryptography;

/**
 * 
 * @author Phil Tsarik
 *
 */
public class BigInteger implements Comparable<BigInteger> {
	
	public static final BigInteger ZERO = new BigInteger("0", false);
	public static final BigInteger ONE = new BigInteger("1", false);
	public static final BigInteger TWO = new BigInteger("2", false);
	public static final BigInteger THREE = new BigInteger("3", false);
	public static final BigInteger FOUR = new BigInteger("4", false);
	
	protected static int BASE = 10;
	
	public BigInteger() {
		this(0);
	}
	
	public BigInteger(int maxSize) {
		digits = new int[maxSize];
		zero();
		size = maxSize;
		
	}
	
	public BigInteger(String stringValue, boolean negative) {
		digits = new int[stringValue.length()];
		int k = 0;
		for (int i = stringValue.length()-1; i >= 0 ; i--) {
			if ( Character.isDigit(stringValue.charAt(i)) ) {
				digits[k] = Integer.valueOf(Character.toString(stringValue.charAt(i)));
				k++;
			}
		}
		size = k;
		this.negative = negative;
	}
	
	public BigInteger(int digits[], boolean negative) {
		this.digits = new int[digits.length];
		int k = 0;
		for (int i = 0; i < digits.length; i++) {
			this.digits[k] = digits[i];
			k++;
		}
		size = k;
		this.negative = negative;
	}
	
	public BigInteger(BigInteger anotherBigInteger) throws NullPointerException {
		if (anotherBigInteger == null) {
			throw new NullPointerException("anotherBigInteger is null");
		}
		this.size = anotherBigInteger.getSize();
		
		int maxSize = anotherBigInteger.getMaxSize();
		digits = new int[maxSize];
		for (int i = 0; i < maxSize; i++) {
			digits[i] = anotherBigInteger.getDigit(i);
		}
	}
	
	@Override
	public int compareTo(BigInteger another) {
		if (this.isNegative() && !another.isNegative()) {
			return -1;
		}
		if (!this.isNegative() && another.isNegative()) {
			return 1;
		}
		int reverse = 1;
		if (this.isNegative() && another.isNegative()) {
			reverse = -1;
		}
		calcSize();
		if (size < another.getSize()) {
			return -1*reverse;
		}
		if (size > another.getSize()) {
			return 1*reverse;
		}
		for (int i = size-1; i >= 0; i--) {
			if (digits[i] > another.getDigit(i)) {
				return 1*reverse;
			}
			if (digits[i] < another.getDigit(i)) {
				return -1*reverse;
			}
		}
		return 0;
	}
	
	public void setValue(long l) {
		digits = new int[Long.SIZE];
		size = 0;
		
		for (int i = 0; i < getMaxSize(); i++) {
		
			digits[i] = (int)(l % BASE);
			
			if (l == 0) {
				break;
			}
			size++;
			l = l / 10;
		}
		
		
	}
	
	public void setValue(String s) {
		digits = new int[s.length()];
		int k = 0;
		for (int i = s.length()-1; i >= 0 ; i--) {
			if ( Character.isDigit(s.charAt(i)) ) {
				digits[k] = Integer.valueOf(Character.toString(s.charAt(i)));
				k++;
			}
		}
	}
	
	public long longValue() {
		long l = 0;
		calcSize();
		int limit = size;
		if (size > Long.SIZE) {
			limit = Long.SIZE;
		}
		for (int i = 0; i < limit; i++) {
			l += digits[i]*Double.valueOf(Math.pow(10, i)).intValue();
		}
		return l;
	}
	
	
	public void zero() {
		for (int i = 0; i < getMaxSize(); i++) {
			digits[i] = 0;
		}
		size = 1;
	}
	
	public long toLong() {
		long res = 0;
		for (int i = 0; i < size; i++) {
			res += digits[i] * (long)Math.pow(BASE, i);
		}
		return res;
	}
	
	@Override
	public String toString() {
		String s = new String("");
		calcSize();
		for (int i = size-1; i >= 0; i--) {
			s = s.concat(String.valueOf(digits[i]));
			if ( ((i % 3) == 0) && (i != 0) ) {
				s = s.concat(" ");
			}
		}
		
		if (s.equals("")) {
			return "0";
		}
		
		if (isNegative()) {
			s = "-".concat(s);
		}
		
		return s;
	}
	
	
	public int getMaxSize() {
		return digits.length;
	}
	
	public int getSize() {
		calcSize();
		return size;
	}
	
	public int getDigit(int n) {
		if ( (n >= digits.length) || (n < 0) ) {
			throw new ArrayIndexOutOfBoundsException();
		}
		return digits[n];
	}
	
	public void setDigit(int n, int val) {
		if ( (n >= digits.length) || (n < 0) ) {
			return;
		}
		digits[n] = val;
	}
	
	public boolean isNegative() {
		return negative;
	}
	
	public void setNegative(boolean negative) {
		this.negative = negative;
	}
	
	
	/*
	 * this+val = new BigInteger
	 */
	public BigInteger sum(BigInteger val) {
		
		if (this.isNegative()) {
			this.setNegative(false);
			BigInteger res = val.substract(this);
			this.setNegative(true);
			return res;
		}
		if (val.isNegative()) {
			val.setNegative(false);
			BigInteger res = this.substract(val);
			val.setNegative(true);
			return res;
		}
		
		BigInteger first;
		BigInteger second;
		BigInteger res;
		
		if (this.getMaxSize() >= val.getMaxSize()) {	
			res = new BigInteger(this.getMaxSize()+1);
			first = this;
			second = val;
		} else {
			res = new BigInteger(val.getMaxSize()+1);
			first = val;
			second = this;
		}
		
		int carry = 0;
		
		int s = second.getSize();
		
		for (int i = 0; i < s; i++) {
			
			res.setDigit(i, this.getDigit(i) + val.getDigit(i) + carry);
			carry = res.getDigit(i) / BASE;
			res.setDigit(i, res.getDigit(i) % BASE);
		}
		
		int s2 = first.getSize();
		
		for (int i = s; i < s2; i++) {
			res.setDigit(i, first.getDigit(i) + carry);
			carry = res.getDigit(i) / BASE;
			res.setDigit(i, res.getDigit(i) % BASE);
		}
		
		res.setDigit(s2, carry);		
		return res;
	}
	
	/*
	 * this-val = new BigInteger
	 */
	public BigInteger substract(BigInteger val) {
		
		// if val < 0 then (this + val)
		if (val.isNegative()) {
			val.setNegative(false);
			BigInteger res = this.sum(val);
			val.setNegative(true);
			return res;
		}
		
		// if this == val then 0
		if (this.compareTo(val) == 0) {
			return new BigInteger("0", false);
		}
		
		BigInteger first = this;
		BigInteger second = val;
		boolean neg = false;
		
		// if this < val then switch a and b and res will be negative
		if (this.compareTo(val) < 0) {
			neg = true;
			first = val;
			second = this;
		}
		
		int maxSizeA = first.getMaxSize();
		int maxSizeB = second.getMaxSize();
		
		
		BigInteger res;
		if (maxSizeA >= maxSizeB) {	
			res = new BigInteger(maxSizeA+1);
		} else {
			res = new BigInteger(maxSizeB+1);
		}
		res.setNegative(neg);
		
		int carry = 0;
		
		int s = second.getSize();
		
		for (int i = 0; i < s; i++) {
			
			res.setDigit(i, first.getDigit(i) - second.getDigit(i) - carry);
			if (res.getDigit(i) < 0) {
				res.setDigit(i, BASE + res.getDigit(i));
				carry = 1;
			} else {
				carry = 0;
			}
		}
		
		int s2 = first.getSize();
		
		for (int i = s; i < s2; i++) {
			res.setDigit(i, first.getDigit(i) - carry);
			if (res.getDigit(i) < 0) {
				res.setDigit(i, BASE + res.getDigit(i));
				carry = 1;
			} else {
				carry = 0;
			}
		}
		
		return res;
	}
	
	/*
	 * this*val = new BigInteger
	 */
	public BigInteger multiply(BigInteger val) {
		
		// if this or val negative then res will be negative
		boolean neg = false;
		if ( (this.isNegative() && !val.isNegative()) || (!this.isNegative() && val.isNegative()) ) {
			neg = true;
		}
		
		BigInteger first;
		BigInteger second;
		BigInteger res;
		
		if (this.getMaxSize() >= val.getMaxSize()) {	
			first = this;
			second = val;
		} else {
			first = val;
			second = this;
		}
		
		res = new BigInteger(this.getMaxSize() + val.getMaxSize());
		res.setNegative(neg);
		
		int carry = 0;
		
		for (int i = 0; i < second.getMaxSize(); i++) {
			carry = 0;
			for (int j = 0; j < first.getMaxSize(); j++) {
				res.setDigit(i+j, res.getDigit(i+j) + second.getDigit(i)*first.getDigit(j) + carry);
				carry = res.getDigit(i+j) / BASE;
				res.setDigit(i+j, res.getDigit(i+j) % BASE);
			}
			int k = i + first.getMaxSize();
			while (carry > 0) {
				res.setDigit(k, res.getDigit(k) + carry);
				carry = res.getDigit(k) / BASE;
				res.setDigit(k, res.getDigit(k) % BASE);
				k++;
			}
		}
		
		return res;
	}
	
	/*
	 * this*val = new BigInteger
	 */
	public BigInteger multiply(int val) {
		
		// if this or val negative then res will be negative
		boolean neg = false;
		if ( (this.isNegative() && val >= 0) || (!this.isNegative() && val < 0) ) {
			neg = true;
		}
		
		BigInteger res = new BigInteger(this.getMaxSize() + 10);
		res.setNegative(neg);
		
		int carry = 0;
		
		int s = this.getSize();
		
		for (int i = 0; i < s; i++) {
			carry = 0;
			
			res.setDigit(i, res.getDigit(i) + this.getDigit(i)*val + carry);
			carry = res.getDigit(i) / BASE;
			res.setDigit(i, res.getDigit(i) % BASE);
			
			int k = i+1;
			while (carry > 0) {
				res.setDigit(k, res.getDigit(k) + carry);
				carry = res.getDigit(k) / BASE;
				res.setDigit(k, res.getDigit(k) % BASE);
				k++;
			}
		}
		
		return res;
	}
	
	/*
	 * this/val = new DivisionResult
	 */
	public DivisionResult divide(BigInteger val) {
		if (val.compareTo(BigInteger.ZERO) == 0) {
			return null;
		}
		
		// if this == val
		if (this.compareTo(val) == 0) {
			return new DivisionResult(new BigInteger("1", false), new BigInteger("0", false));
		}
		
		// if this < val
		if (this.compareTo(val) == 0) {
			return new DivisionResult(new BigInteger("0", false), new BigInteger(this));
		}
		
		// if this or val negative then res will be negative
		boolean neg = false;
		if ( (this.isNegative() && !val.isNegative()) || (!this.isNegative() && val.isNegative()) ) {
			neg = true;
		}
		
		BigInteger rres = new BigInteger(this);
		BigInteger qres = new BigInteger(this.getMaxSize());
		
		rres.setNegative(neg);
		
		int n = val.getSize();
		int m = this.getSize() - n;
		
		if (m == 0) {
			m = 1;
		}
		
		BigInteger f = new BigInteger();
		while (rres.compareTo(val) >= 0) {
			int d = 0;
			f.setValue(Double.valueOf(Math.pow(10, m)).longValue());
			BigInteger bi = val.multiply(f.multiply(d));
			
			BigInteger bi2 = val.multiply(f.multiply(d+1));
			while (bi2.compareTo(rres) <= 0) {
				d++;
				bi = bi2;
				bi2 = val.multiply(f.multiply(d+1));
			}
			rres = rres.substract(bi);
			
			qres.setDigit(m, d);
			m--;
		}
		
		return new DivisionResult(qres, rres);
	}
	
	/*
	 * this^val = new BigInteger
	 */
	public BigInteger pow(BigInteger val) {
		
		BigInteger res = new BigInteger(this);
		
		BigInteger p = new BigInteger(val);
		
		while (p.compareTo(BigInteger.ONE) > 0) {
			p = p.substract(BigInteger.ONE);
			res = res.multiply(this);
		}
		
		return res;
	}
	
	/*
	 * (this*val)%mod = new BigInteger
	 */
	public BigInteger multiply(BigInteger val, BigInteger mod) {
		return multiply(val).divide(mod).getRemainder();
	}
	
	/*
	 * (this^val)%mod = new BigInteger
	 */
	public BigInteger pow(BigInteger val, BigInteger mod) {
		BigInteger res = new BigInteger(BigInteger.ONE);
		
		BigInteger x = new BigInteger(this);
		BigInteger p = new BigInteger(val);
		
		while (p.compareTo(BigInteger.ZERO) != 0) {
			
			if ( (p.getDigit(0) & 1) != 0 ) {
				res = res.multiply(x, mod);
			}
			p = p.divide(BigInteger.TWO).getQuotient();
			x = x.multiply(x, mod);
		}
		
		return res;
	}
	
	public boolean isPrime() {
		BigInteger bi = new BigInteger(BigInteger.TWO);
		BigInteger half = this.divide(bi).getQuotient();
		while (bi.compareTo(half) <= 0) {
			DivisionResult divres = this.divide(bi);
			if (divres.getRemainder().compareTo(BigInteger.ZERO) == 0) {
				return false;
			}
			bi = bi.sum(BigInteger.ONE);
		}
		return true;
	}
	
	public boolean isCoprime(BigInteger another) {
		if (this.compareTo(another) == 0) {
			return false;
		}
		
		BigInteger bi = new BigInteger(BigInteger.TWO);
		
		BigInteger limit;
		if (this.compareTo(another) < 0) {
			limit = this;
		} else {
			limit = another;
		}
		while (bi.compareTo(limit) <= 0) {
			DivisionResult divres1 = this.divide(bi);
			DivisionResult divres2 = another.divide(bi);
			if ( (divres1.getRemainder().compareTo(BigInteger.ZERO) == 0) && (divres2.getRemainder().compareTo(BigInteger.ZERO) == 0) ) {
				return false;
			}
			bi = bi.sum(BigInteger.ONE);
		}
		return true;
	}
	
	public boolean rabinMillerPrimeTest() {
		BigInteger p = this.substract(BigInteger.ONE);
		
		BigInteger b = new BigInteger();
		BigInteger drq = new BigInteger(p);
		BigInteger drr = new BigInteger(p);
		while (drr.compareTo(BigInteger.ZERO) != 0) {
			DivisionResult d = drq.divide(BigInteger.TWO);
			drq = d.getQuotient();
			drr = d.getRemainder();
			b.sum(BigInteger.ONE);
		}
		
		BigInteger pw = new BigInteger(BigInteger.TWO.pow(b));
		BigInteger m = p.divide(pw).getQuotient();
		
		BigInteger a = BigInteger.newRandom(p.getSize()-1);
		
		BigInteger j = BigInteger.ZERO;
		BigInteger z = a.pow(m).divide(this).getRemainder();
		
		if ( (z.compareTo(BigInteger.ONE) == 0) || (z.compareTo(p) == 0) ) {
			return true;
		}
		
		do {
			if (j.compareTo(BigInteger.ZERO) > 0) {
				if (z.compareTo(BigInteger.ONE) == 0) {
					return false;
				}
				z = z.multiply(z);
				z = z.divide(this).getRemainder();
			}
			j = j.sum(BigInteger.ONE);
		} while ( (j.compareTo(b) < 0) && (z.compareTo(p) < 0) );
		
		if ( (j.compareTo(b) == 0) && (z.compareTo(p) != 0) ) {
			return false;
		}
		return true;
	}
	
	
	
	public static BigInteger newRandom(int digits) {
		if (digits < 1) {
			digits = 1;
		}
		BigInteger bi = new BigInteger(digits);
		for (int i = 0; i < bi.getMaxSize(); i++) {
			bi.setDigit(i, Double.valueOf(Math.random() * 10).intValue());
		}
		return bi;
	}
	
	public static BigInteger newRandom(BigInteger max) {
		BigInteger bi = newRandom(max.getSize());
		while (bi.compareTo(max) > 0) {
			bi = newRandom(max.getSize());
		}
		return bi;
	}
	
	public static BigInteger newRandom(BigInteger min, BigInteger max) {
		BigInteger bi = newRandom(max.getSize());
		while ( (bi.compareTo(min) < 0) || (bi.compareTo(max) > 0) ) {
			bi = newRandom(max.getSize());
		}
		return bi;
	}
	
	public static BigInteger euler(BigInteger n) {
		BigInteger res = new BigInteger(BigInteger.ONE);
		
		BigInteger p = new BigInteger(BigInteger.TWO);
		BigInteger n2 = new BigInteger(n);
		
		while (p.compareTo(n2.divide(p).getQuotient()) <= 0) {
			if (n2.divide(p).getRemainder().compareTo(BigInteger.ZERO) == 0) {
				n2 = n2.divide(p).getQuotient();
				while (n2.divide(p).getRemainder().compareTo(BigInteger.ZERO) == 0) {
					n2 = n2.divide(p).getQuotient();
					res = res.multiply(p);
				}
				res = res.multiply(p.substract(BigInteger.ONE));
			}
			p = p.sum(BigInteger.ONE);
		}
		
		if (n2.compareTo(BigInteger.ONE) > 0) {
			return res.multiply(n2.substract(BigInteger.ONE));
		}
		return res;
	}
	
	
	
	private int size;
	private int digits[];
	private boolean negative;
	
	
	private void calcSize() {
		size = digits.length;
		while ( (size >= 1) && (digits[size-1] == 0) ) {
			size--;
		}
	}
	
}
