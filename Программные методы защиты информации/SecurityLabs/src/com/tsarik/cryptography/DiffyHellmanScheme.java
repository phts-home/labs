package com.tsarik.cryptography;

public class DiffyHellmanScheme {
	
	public DiffyHellmanScheme() {
	}
	
	public void setQ(BigInteger q) {
		this.q = new BigInteger(q);
	}
	
	public void setA(BigInteger a) {
		this.a = new BigInteger(a);
	}
	
	public BigInteger generateQ() {
		//q = BigInteger.newRandom(2);
		return null;
	}
	
	public BigInteger generateA() {
		BigInteger fi = BigInteger.euler(q);
		BigInteger fi1 = fi.substract(BigInteger.ONE);
		this.a = new BigInteger(BigInteger.ONE);
		boolean found = false;
		boolean found2 = false;
		while (!found) {
			a = a.sum(BigInteger.ONE);
			if (a.pow(fi, q).compareTo(BigInteger.ONE) != 0) {
				continue;
			}
			BigInteger l = new BigInteger(BigInteger.ONE);
			found2 = true;
			while (l.compareTo(fi1) <= 0) {
				if (a.pow(l, q).compareTo(BigInteger.ONE) == 0) {
					found2 = false;
					break;
				}
				l = l.sum(BigInteger.ONE);
			}
			if (!found2) {
				continue;
			}
			found = true;
		}
		return a;
	}
	
	
	public BigInteger getXa() {
		if (xa == null) {
			return generateXa();
		}
		return xa;
	}
	
	public BigInteger generateXa() {
		xa = BigInteger.newRandom(q.substract(BigInteger.ONE));
		return xa;
	}
	
	public BigInteger getYa() {
		if (ya == null) {
			return generateYa();
		}
		return ya;
	}
	
	public BigInteger generateYa() {
		//BigInteger aa = a.pow(xa);
		ya = a.pow(xa, q);
		return ya;
	}
	
	
	public BigInteger getXb() {
		if (xb == null) {
			return generateXb();
		}
		return xb;
	}
	
	public BigInteger generateXb() {
		xb = BigInteger.newRandom(q.substract(BigInteger.ONE));
		return xb;
	}
	
	public BigInteger getYb() {
		if (yb == null) {
			return generateYb();
		}
		return yb;
	}
	
	public BigInteger generateYb() {
		yb = a.pow(xb, q);
		return yb;
	}
	
	
	public BigInteger getK1() {
		if (k1 == null) {
			return calculateKForUserA();
		}
		return k1;
	}
	
	public BigInteger calculateKForUserA() {
		k1 = yb.pow(xa, q);
		return k1;
	}
	
	public BigInteger getK2() {
		if (k2 == null) {
			return calculateKForUserB();
		}
		return k2;
	}
	
	public BigInteger calculateKForUserB() {
		k2 = ya.pow(xb, q);
		return k2;
	}
	
	
	
	
	BigInteger q;
	BigInteger a;
	BigInteger xa;
	BigInteger ya;
	BigInteger xb;
	BigInteger yb;
	
	BigInteger k1;
	BigInteger k2;
	
}
