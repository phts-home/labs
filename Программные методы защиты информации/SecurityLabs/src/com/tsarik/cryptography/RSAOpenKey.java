package com.tsarik.cryptography;

public class RSAOpenKey {
	
	public RSAOpenKey(BigInteger e, BigInteger n) {
		this.e = e;
		this.n = n;
	}
	
	public BigInteger getE() {
		return e;
	}
	
	public BigInteger getN() {
		return n;
	}
	
	private BigInteger e;
	private BigInteger n;
	
}
