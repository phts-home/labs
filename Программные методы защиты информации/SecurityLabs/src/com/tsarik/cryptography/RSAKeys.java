package com.tsarik.cryptography;

public class RSAKeys {
	
	public RSAKeys(RSAOpenKey ok, RSASecretKey sk) {
		this.ok = ok;
		this.sk = sk;
	}
	
	public RSAOpenKey getOpenKey() {
		return ok;
	}
	
	public RSASecretKey getSecretKey() {
		return sk;
	}
	
	private RSAOpenKey ok;
	private RSASecretKey sk;
	
}
