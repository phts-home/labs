package com.tsarik.cryptography;

public class DivisionResult {
	
	public DivisionResult(BigInteger quotient, BigInteger remainder) {
		this.quotient = new BigInteger(quotient);
		this.remainder = new BigInteger(remainder);
	}
	
	public BigInteger getQuotient() {
		return quotient;
	}
	
	public BigInteger getRemainder() {
		return remainder;
	}
	
	@Override
	public String toString() {
		return "quotient: " + quotient + "; remainder: " + remainder;
	}
	
	private BigInteger quotient;
	private BigInteger remainder;
	
}
