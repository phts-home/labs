package com.tsarik.cryptography;

public class RSASecretKey {
	
	public RSASecretKey(BigInteger d, BigInteger n) {
		this.d = d;
		this.n = n;
	}
	
	public BigInteger getD() {
		return d;
	}
	
	public BigInteger getN() {
		return n;
	}
	
	private BigInteger d;
	private BigInteger n;
	
}
