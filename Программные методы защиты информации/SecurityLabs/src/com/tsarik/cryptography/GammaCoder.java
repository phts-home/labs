package com.tsarik.cryptography;

public class GammaCoder {

	static public String encrypt(String s, LinearRandomGeneratorKey key) {
		LinearRandomGenerator gen = new LinearRandomGenerator(key);
		int r;
		int m;
		int nc;
		char chars[] = new char[s.length()];
		for (int i = 0; i < s.length(); i++) {
			r = gen.generateNext();
			m = s.charAt(i);
			nc = r ^ m;
			chars[i] = (char)nc;
		}
		
		return String.valueOf(chars);
	}
	
	static public String decrypt(String s, LinearRandomGeneratorKey key) {
		
		LinearRandomGenerator gen = new LinearRandomGenerator(key);
		int r;
		int c;
		int nc;
		char chars[] = new char[s.length()];
		for (int i = 0; i < s.length(); i++) {
			r = gen.generateNext();
			c = s.charAt(i);
			nc = r ^ c;
			chars[i] = (char)nc;
		}
		
		return String.valueOf(chars);
	}

}
