model small
stack 64H
.486
.data
message DB "Hello, World$"

.code
Start:
MOV AX, @data
MOV DS, AX

MOV AH, 9H
LEA DX, message
INT 21H

Exit:
MOV AX, 4C00H
INT 21H
END Start