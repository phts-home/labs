.model small
.386
.data
width1 Dw 5
height1 DW 5
x0 DW 20
.stack
.code
main:
mov ax,@data
mov ds,ax

; ������������� ������������
mov ah,0
mov al,3
int 10h
mov ax,0b800h
mov es,ax
;/������������� ������������

; ��������� ������ �������
;; ������ "�"
mov si,x0
shl si,2
mov ax,07dah
mov es:[si],ax

;; ������ "-"
mov cx,width1
sub cx,2
mov ax,07c4h
mov bx,x0
shl bx,2
dr:
  inc bx
  inc bx
  mov si,bx
  mov es:[si],ax
loop dr

;; ������ "7"
inc bx
inc bx
mov si,bx
mov ax,07bfh
mov es:[si],ax
;/��������� ������ �������

; ����� ������
;; ������ "|"
mov cx,height1
sub cx,2
mov bx,x0
shl bx,2
ddr1:
  add bx,160
  mov si,bx
  mov ax,07b3h
  mov es:[si],ax
loop ddr1
;/����� ������

; ��������� ��������� �������
;; ������ "L"
add bx,160
mov si,bx
mov ax,07c0h
mov es:[si],ax

;; ������ "-"
mov cx,width1
sub cx,2
mov ax,07c4h
dr5:
  inc bx
  inc bx
  mov si,bx
  mov es:[si],ax
loop dr5

;; ������ "J"
inc bx
inc bx
mov si,bx
mov ax,07d9h
mov es:[si],ax
;/��������� ��������� �������

; ������ ������
;; ������ "|"
mov cx,height1
sub cx,2
ddr2:
  sub bx,160
  mov si,bx
  mov ax,07b3h
  mov es:[si],ax
loop ddr2
;/������ ������

; ��������
mov ah,0
int 16h
;/��������

Exit:
  mov ax,4c00h
  int 21h

end main