.model small
.stack 256
.data
  InputFile db 'd:\Input.txt',0
  OutputFile db 'd:\Output.txt',0
  InpHandle dw ?
  OutpHandle dw ?
  CurrentCh dw ?
  PrevCh dw ?
.code
main:
mov ax,@data
mov ds,ax

; ������� ���� InputFile
mov ah,3dh
mov al,0
lea dx,InputFile
int 21h
mov InpHandle,ax
;/������� ���� InputFile

; ������� ���� OutputFile
mov ah,3ch
lea dx,OutputFile
mov cx,0
int 21h
mov OutpHandle,ax
;/������� ���� OutputFile

; ������/������
loo1:
  mov bx,CurrentCh
  mov PrevCh,bx
  mov bx,InpHandle
  lea dx,CurrentCh
  mov ah,3fh
  mov cx,1
  int 21h
  cmp ax,0
  je Exit
  
  mov bx,CurrentCh
  cmp bx,PrevCh
  je loo1
  
  mov bx,OutpHandle
  lea dx,CurrentCh
  mov ah,40h
  mov cx,1
  int 21h
jmp loo1
;/������/������

Exit:
  mov ax,4c00h
  int 21h

end main