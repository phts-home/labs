.model small
.stack 256
.data
  x dw ?
  y dw ?
  xl dw ?
  xh dw ?
  yl dw ?
  yh dw ?
.code
main:
mov ax,@data
mov ds,ax

; ������������� ������������
mov ah,0
mov al,3
int 10h
mov ax,0b800h
mov es,ax
;/������������� ������������

; ����
;; ������������� ����
mov ax,0
int 33h
;; ���������� ������ ����
mov ax,0001h
int 33h
;/����

loo1:
  ; ����������� ��������� ����
  ;; bx <- ��������� ������
  ;; cx <- �-����������
  ;; dx <- Y-����������
  mov ax,0003h
  int 33h
  
  cmp bx,00000010b    ;- ������ �� ������ ������ ����??
  jne loo1            ;- ���� ���, �� ����������
  
  ; ������� ������
  push cx
  mov cx,2000
  mov si,0
  loo2:
    mov ax,0
    mov es:[si],ax
    add si,2
  loop loo2
  pop cx
  ;/������� ������
   
  shr cx,3    ; cx = cx*8
  shr dx,3    ; dx = dx*8
  
  mov x,cx
  mov y,dx
  
  ; ���������� X
  mov ax,x
  mov bl,10
  div bl    ; x / 10
  ;; ah <- �������
  ;; al <- �������
  
  mov dl,ah
  mov dh,0
  mov xh,dx
  
  mov dl,al
  mov dh,0
  mov xl,dx
  
  add xl,0d730h
  add xh,0d830h
  ;/���������� X

  ; ���������� �������� �� ����������� X,Y
  mov ax,160
  mul y
  mov bx,x
  shl bx,1
  add ax,bx
  mov si,ax
  ;/���������� �������� �� ����������� X,Y
  
  ; ���������� Y
  mov ax,y
  mov bl,10
  div bl    ; y / 10
  ;; ah <- �������
  ;; al <- �������
  
  mov dl,ah
  mov dh,0
  mov yh,dx
  
  mov dl,al
  mov dh,0
  mov yl,dx
  
  add yl,0a730h
  add yh,0a830h
  ;/���������� Y

  ; ����� �� �����
  mov ax,xl
  mov es:[si],ax
  mov ax,xh
  mov es:[si+2],ax
  mov ax,yl
  mov es:[si+4],ax
  mov ax,yh
  mov es:[si+6],ax
  ;/����� �� �����
jmp loo1

Exit:
  mov ax,4c00h
  int 21h

end main