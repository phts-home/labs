predicates
	avia(integer,string,integer)
	printItemsByNo(integer)
	printItemsNyMaxPrice(integer)
	
	menu(integer)
	
	repeat
	run
goal
	run.
	
clauses
	avia(111,"moskow",1000).
	avia(222,"minsk",1500).
	avia(333,"paris",2000).
	
	printItemsByNo(N) :- avia(N,D,P),write(N,": ",D," - ",P),nl,fail.
	
	printItemsNyMaxPrice(MP) :- avia(N,D,P),MP>P,write(N,": ",D," - ",P),nl,fail.
	
	menu(1) :- avia(N,D,P),write(N,": ",D," - ",P),nl,fail.
	
	menu(2) :- write("enter route no: "),readint(N),printItemsByNo(N).
	
	menu(3) :- write("enter max price: "),readint(MP),printItemsNyMaxPrice(MP).
	
	menu(4) :- write("exit"),nl.
	
	
	repeat.
	repeat :- repeat.
	
	run :- repeat,write("1 - get all"),nl,write("2 - get destination"),nl,write("3 - get route by max price",nl,"4 - exit",nl,nl),nl,readint(M),menu(M),M=4,!,write(exit),nl.
	