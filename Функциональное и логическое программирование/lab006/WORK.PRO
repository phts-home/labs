/*
7. �� ������������������ ����, ����������� ���������, ������� �����, � ������� ������ � ��������� ������� ����������
15. ���������� � ������ ���������� ����, ����� ������� ������
*/


predicates
	runReverse
	reverse(string,string)
	
	
	getWordLen(string,integer)
	delSpace(string,string)
	printWords(string)
	wordCount(string,integer,integer)
	t7
	t15
	
clauses
	
	reverse("","").
	reverse(S,R):-
		frontchar(S,A,S1),
		reverse(S1,R1),
		str_char(ST,A),
		concat(R1,ST,R). 
	
	
	
	getWordLen("",0).
	getWordLen(S,L) :- 
		frontchar(S,C,S1),
		C <> ' ',
		getWordLen(S1,L1),
		L = L1 + 1,
		!;
		getWordLen("",L1),
		L = L1 + 1.
	
	delSpace(W,R) :-
		reverse(W,W1),
		frontchar(W1,' ',W2),
		reverse(W2,WR),
		R = WR;
		R = W.
		
	
	printWords("") :- write("---\n"),!.
	printWords(S) :-
		getWordLen(S,WL),
			frontstr(WL,S,OldW,S1),
			delSpace(OldW,W),
			frontchar(W,C,_),
			reverse(W,W1),
			frontchar(W1,C1,_),
			C = C1,!,
			write("W = >",W,"<\n"),
			printWords(S1);
		getWordLen(S,WL),
			frontstr(WL,S,OldW,S1),
			printWords(S1).
		
	
	wordCount("",_,0).
	wordCount(S,N,R) :- getWordLen(S,WL),
			frontstr(WL,S,OldW,S1),
			delSpace(OldW,W),
			str_len(W,WLen),
			WLen < N,!,
			wordCount(S1,N,R1),
			R = R1 + 1;
		getWordLen(S,WL),
			frontstr(WL,S,OldW,S1),
			wordCount(S1,N,R).
	
	
	t7 :-
		readln(S),
		printWords(S).
		/*getWords("123 1231 54 44 56765 456 678 865 212").*/
	
	
	t15 :-
		readln(S),
		readint(N),
		wordCount(S,N,R),
		/*wordCount("123 1231 54 44 56765 456 67 678 865 212",N,R),*/
		write("Res ",R,"\n").
	
	
	runReverse:-
		write("Enter String: "),
		readln(Str),
		reverse(Str,Res),
		nl,write("Result string: ",Res),nl.
	
	
	