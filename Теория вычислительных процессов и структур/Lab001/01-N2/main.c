/*
******************************************************************************
*       ������������ ������ #1. ������� #2.
*       ������ �������������� ��������� � ��������
*
*       ��������: ������� ��. 06��-1 ��� ����� ������
******************************************************************************
*/


#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include <conio.h>
#include <dirent.h>

char filename[257];

void print(const char *, int);
void copy(const char*, const char*);
void printdir(const char *);

int main(int argc, char *argv[])
{
	if (argc > 1) {
		strcpy(filename,argv[1]);
	} else {
		printf("Error. Cannot open file");
		return 1;
	}
	
	//int ferr = _open(filename, _O_RDWR | _O_CREAT);
	FILE *f = fopen(filename, "wt");
	
	int canexit = 0;
	char c;
	while (!canexit) {
		// read symbol
		c = _getch();
		// if symbol is "ctrl+F"
		if (c == 6) {
			canexit = 1;
		} else {
			putc(c, f);
			printf("%c\n", c);
		}
	}
	printf("\n");

	fclose(f);

	print(filename, 10);

	copy("test2.txt", filename);

	printdir(".");

	return 0;
}

void print(const char *path, int n)
{
	FILE *f = fopen(path, "rt");
	char c;
	int counter = 0;

	printf("> Printing file...\n");
	while (feof(f) == 0) {
		c = getc(f);
		if (feof(f) == 0) {
			printf("%c\n", c);
		}
		counter++;
		if (counter == n) {
			counter = 0;
			printf("> Press any key to continue printing  ");
			_getch();
		}
	}
	printf("> Done\n");
	fclose(f);
}

void copy(const char* dst, const char* src)
{
	FILE *fdst = fopen(dst, "wt");
	FILE *fsrc = fopen(src, "rt");

	if ( (fsrc == NULL) || (fdst == NULL) ) {
		printf("Error. Cannot open file");
	}

	char c;

	while (feof(fsrc) == 0) {
		c = getc(fsrc);
		if (feof(fsrc) == 0) {
			putc(c, fdst);
		}
	}

	fclose(fdst);
	fclose(fsrc);
}

void printdir(const char *src)
{
	_DIR *dsrc = _opendir(src);
	struct _dirent *d = _readdir(dsrc);

	printf("> Printing dir...\n");

	while (d != NULL) {
		d = _readdir(dsrc);
		printf("%s\n", d->d_name);
	}

	printf("> Done\n");

	_closedir(dsrc);
}
