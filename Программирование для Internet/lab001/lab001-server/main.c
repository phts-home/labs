/* 
 * ������������ ������ �1
 * 
 * �������:  ���������������� ��� INTERNET
 * ����:     ���������� TCP-������ � TCP-������
 * 
 * �������:  �������� ������-��������� ����, 
 *           ��������� �� ���������� ����� �����������
 *           (�������� ���������� ����)
 * 
 * ��������: ������� ��. 06��-1 
 *           ����� ������
 * 
 */

#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>

#include <conio.h>

int main(void) {
	WSADATA wsadata;
	WSAStartup(MAKEWORD(2,2), &wsadata);

	// ��������� ������
	struct sockaddr_in peer;
	// �����
	int s;
	int s1;
	// ������������ ������
	char buf[1];
	// �������� �����
	peer.sin_family = AF_INET;
	// ����� �����
	peer.sin_port = htons(7500);
	// �����
	peer.sin_addr.s_addr = htonl(INADDR_ANY);
	
	// ��������� ������ ��� ����������� ����������
	s = socket(AF_INET, SOCK_STREAM, 0);
	if (s < 0) {
		perror("call error: socket\n");
		exit(1);
	}
	
	// �������� ������ ���������� � ������ �����
	if (bind(s, (struct sockaddr *)&peer, sizeof(peer)) < 0) {
		perror("call error: bind\n");
		exit(1);
	}
	
	// ������������� ��������� ����������
	if (listen(s, 5)) {
		perror("call error: listen\n");
		exit(1);
	}
	
	// ����� ����������
	s1 = accept(s, NULL, NULL);
	if (s1 < 0) {
		perror("call error: accept\n");
		exit(1);
	}

	// ���������� ���������� �� ������
	if (recv(s1, buf, 1, 0) <= 0) {
		perror("call error: recv\n");
		exit(1);
	}
	printf("%c\n", buf[0]);

	// �������� ���������� �������
	if (send(s1, "2", 1, 0) <= 0)
		perror("call error: send\n");

	_getch();
	exit(0);
}
