/* 
 * ������������ ������ �1
 * 
 * �������:  ���������������� ��� INTERNET
 * ����:     ���������� TCP-������ � TCP-������
 * 
 * �������:  �������� ������-��������� ����, 
 *           ��������� �� ���������� ����� �����������
 *           (�������� ���������� ����)
 * 
 * ��������: ������� ��. 06��-1 
 *           ����� ������
 * 
 */

#include <winsock2.h>
#include <stdio.h>
#include <stdlib.h>

#include <conio.h>

int main(void) {
	WSADATA wsadata;
	WSAStartup(MAKEWORD(2,2), &wsadata);

	// ��������� ������
	struct sockaddr_in peer;
	// �����
	int s;
	// ������������ ������
	char buf[1];
	// �������� �����
	peer.sin_family = AF_INET;
	// ����� �����
	peer.sin_port = htons(7500);
	// �����
	peer.sin_addr.s_addr = inet_addr("127.0.0.1");
	
	// ��������� ������ ��� ����������� ����������
	s = socket(AF_INET, SOCK_STREAM, 0);
	if (s < 0) {
		perror("call error: socket\n");
		exit(1);
	}

	// ������������ ����������
	if (connect(s, (struct sockaddr *)&peer, sizeof(peer))) {
		perror("call error: connect\n");
		exit(1);
	}

	// �������� ���������� �������
	if (send(s, "1", 1, 0) <= 0) {
		perror("call error: send");
		exit(1); 
	}

	// ���������� ���������� �� �������
	if (recv(s, buf, 1, 0) <= 0)
		perror("call error: recv");
	else
		printf("%c\n", buf[0]);

	_getch();
	exit(0);
}
