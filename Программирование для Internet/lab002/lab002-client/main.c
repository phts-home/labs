/* 
 * ������������ ������ �2
 * 
 * �������:  ���������������� ��� INTERNET
 * ����:     ���������� TCP-������ � TCP-������
 * 
 * �������:  �������� ���������� ���-������� � ���-�������
 * 
 * ��������: ������� ��. 06��-1 
 *           ����� ������
 * 
 */

#include <stdio.h> 
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "skel.h"


/*
 * ������ �������
 * 
 * s �����
 * peerp �����
 */
void client(SOCKET s, struct sockaddr_in *peerp) {
	int rc;
	char buf[129];
	while (1) {
		printf("receiving data...");
		rc = recv(s, buf, sizeof(buf), 0);
		if (rc <= 0) {
			printf("no data\n\n");
			break;
		}
		printf("done\n");
		printf("received char from server is \"%c\"\n", buf[0]);
	}
}

int main(int argc, char **argv) {
	struct sockaddr_in peer;
	SOCKET s;
	
	// �������������
	INIT();
	
	// �������� ���������� ���������� ������
	if (argc != 3) {
		error(1, 0, "parameters error: wrong parameters");
	}
	printf("connecting with server...");
	
	// ��������� ������ � peer
	set_address(argv[1], argv[2], &peer, "tcp");
	
	// ��������� ������ ��� ����������� ����������
	s = socket(AF_INET, SOCK_STREAM, 0);
	if (!isvalidsock(s)) {
		error(1, errno, "call error: socket");
	}
	
	// ������������ ����������
	if (connect(s, (struct sockaddr*)&peer, sizeof(peer))) {
		error(1, errno, "call error: connect");
	}
	printf("done\n");
	
	client(s, &peer);
	
	// �����
	EXIT(0);
	return 0;
}
