/* 
 * ������������ ������ �2
 * 
 * �������:  ���������������� ��� INTERNET
 * ����:     ���������� TCP-������ � TCP-������
 * 
 * �������:  �������� ���������� ���-������� � ���-�������
 * 
 * ��������: ������� ��. 06��-1 
 *           ����� ������
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "skel.h"

char c = 'a';

/*
 * ������ �������
 * 
 * s �����
 * peerp �����
 */
void server(SOCKET s, struct sockaddr_in *peerp) {
	printf("sending data...");
	send(s, &c, 1, 0);
	printf("done\n");
	printf("sended char to client is \"%c\"\n\n", c);
	c++;
}

int main(int argc, char **argv) {
	struct sockaddr_in local;
	struct sockaddr_in peer;
	char *hname;
	char *sname;
	int peerlen;
	SOCKET s1;
	SOCKET s;
	const char on = 1;
	
	// �������������
	INIT();
	
	// �������� ���������� ���������� ������
	if (argc == 2) {
		hname = NULL;
		sname = argv[1];
	} else {
		if (argc == 3) {
			hname = argv[1];
			sname = argv[2];
		} else {
			error(1, 0, "parameters error: wrong parameters");
		}
	}
	
	// ��������� ������ � peer
	set_address(hname, sname, &local, "tcp");
	
	// ��������� ������ ��� ����������� ����������
	s = socket(AF_INET, SOCK_STREAM, 0);
	if (!isvalidsock(s)) {
		error(1, errno, "call error: socket");
	}
	
	// ��������� ���������� ������
	if (setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on))) {
		error(1, errno, "call error: setsockopt");
	}
	
	// �������� ������ ���������� � ������ �����
	if (bind(s,(struct sockaddr *) &local, sizeof(local))) {
		error(1, errno, "call error: bind");
	}
	
	// ������������� ��������� ����������
	if (listen(s, NLISTEN)) {
		error(1, errno, "call error: listen");
	}
	
	do {
		printf("waiting for connection with client...");
		peerlen = sizeof(peer);
		// ����� ����������
		s1 = accept(s, (struct sockaddr *)&peer, &peerlen);
		if (!isvalidsock(s1)) {
			error(1, errno, "call error: accept");
		}
		printf("connected\n");
		
		server(s1, &peer);
		CLOSE(s1);
	} while(1);
	
	// �����
	EXIT(0);
	return 0;
}
