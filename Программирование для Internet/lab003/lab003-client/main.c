/* 
 * ������������ ������ �3
 * 
 * �������:  ���������������� ��� INTERNET
 * ����:     �������� ��������� � ��������� �� ���� TCP
 * 
 * �������:  ����������� ����������� ����������, �������������� 
 *           �������� ������ �� ���� TCP
 * 
 * ��������: ������� ��. 06��-1 
 *           ����� ������
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>

#include "skel.h"


int main(int argc, char **argv) {
	struct sockaddr_in peer;
	char *buf;
	SOCKET s;
	int c;
	// ����� �������� ������
	int blks = 5000;
	// ������ ������ �������� ������
	int sndbufsz = 32*1024;
	// ���������� ������
	int sndsz = 1440;
	
	// �������������
	INIT();
	opterr = 0;
	
	// �������� ���������� ���������� ������
	while ((c = _getopt(argc, argv, "s:b:c:")) != EOF) {
		switch(c){
			case 's':
				sndsz = atoi(optarg);
				break;
			case 'b':
				sndbufsz = atoi(optarg);
				break;
			case 'c':
				blks = atoi(optarg);
				break;
			case '?':
				error(1, 0, "parameters error: wrong parameter %c\n", c);
		}
	}
	if (argc <= optind) {
		error(1, 0, "unknown hostname\n");
	}
	
	if ((buf = malloc(sndsz)) == NULL) {
		error(1, 0, "call error: malloc\n");
	}
	
	// ��������� ������ � peer
	set_address(argv[optind], "9000", &peer, "tcp");
	
	// ��������� ������ ��� ����������� ����������
	s = socket(AF_INET, SOCK_STREAM, 0); 
	if(!isvalidsock(s)) {
		error(1, errno, "call error: socket");
	}
	
	// ��������� ��������� SO_RCVBUF
	if(setsockopt(s, SOL_SOCKET, SO_RCVBUF, (char*)&sndbufsz, sizeof(sndbufsz))) {
		error(1, errno, "call error: setsockopt SO_RCVBUF");
	}
	
	// ������������ ����������
	if(connect(s, (struct sockaddr*)&peer, sizeof(peer))) {
		error(1, errno, "call error: connect");
	}
	
	//�������� ������ ��������� ���������� ���
	while (blks-- > 0) {
		// �������� ������
		if(send(s, buf, sndsz, 0) <= 0){
			error(1, errno, "call error: send");
		}
	}
	
	EXIT(0); 
	return 0;
} 

