/* 
 * ������������ ������ �3
 * 
 * �������:  ���������������� ��� INTERNET
 * ����:     �������� ��������� � ��������� �� ���� TCP
 * 
 * �������:  ����������� ����������� ����������, �������������� 
 *           �������� ������ �� ���� TCP
 * 
 * ��������: ������� ��. 06��-1 
 *           ����� ������
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <getopt.h>

#include "skel.h"


/*
 * ������ �������
 * 
 * s �����
 * rcvbufsz ������ ������
 */
void server(SOCKET s, int rcvbufsz) {
	char *buf;
	int rc;
	int bytes = 0;
	printf("receiving data...");
	if ((buf = malloc(rcvbufsz)) == NULL) {
		error(1, 0, "call error: malloc");
	}
	while (1) {
		if ( (rc = recv(s, buf, rcvbufsz, 0)) <= 0) {
			break;
		}
		bytes += rc;
	}
	printf("done\n");
	printf("received bytes: %d\n\n", bytes);
}

int main(int argc, char **argv) {
	struct sockaddr_in local;
	struct sockaddr_in peer;
	int peerlen;
	SOCKET s1;
	SOCKET s;
	int c;
	// ������ ������ �������� ������
	int rcvbufsz = 32*1024;
	const char on = 1;
	
	// �������������
	INIT();
	opterr = 0;
	
	// �������� ���������� ���������� ������
	while((c = _getopt(argc, argv, "b:")) != EOF){
		switch(c){
			case 'b' :
				rcvbufsz = atoi(optarg);
				break;
			case '?' :
				error(1, 0, "parameters error: wrong parameter %c", c);
		}
	}
	
	// ��������� ������ � peer
	set_address(NULL, "9000", &local, "tcp");
	
	// ��������� ������ ��� ����������� ����������
	s = socket(AF_INET, SOCK_STREAM, 0);
	if(!isvalidsock(s)) {
		error(1, errno, "call error: socket"); 
	}
	
	// ��������� ��������� SO_REUSEADDR
	if(setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on))) {
		error(1, errno, "call error: setsockopt SO_REUSEADDR"); 
	}
	
	// ��������� ��������� SO_RCVBUF
	if(setsockopt(s, SOL_SOCKET, SO_RCVBUF, (char*)&rcvbufsz, sizeof(rcvbufsz))) {
		error(1, errno, "call error: setsockopt SO_RCVBUF");
	}
	
	// �������� ������ ���������� � ������ �����
	if(bind(s,(struct sockaddr *) &local, sizeof(local))) {
		error(1, errno, "call error: bind"); 
	}
	
	// ������������� ��������� ����������
	listen(s, 5);
	do {
		printf("waiting for connection with client...");
		peerlen = sizeof(peer);
		// ����� ����������
		s1 = accept(s, (struct sockaddr*)&peer, &peerlen);
		if(!isvalidsock(s1)) {
			error(1, errno, "call error: accept");
		}
		printf("connected\n");
		
		server(s1, rcvbufsz);
		CLOSE(s1);
	} while(0);
	
	EXIT(0);
	return 0;
}

