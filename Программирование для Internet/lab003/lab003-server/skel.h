#ifndef SKEL_H
#define SKEL_H

#include <winsock2.h>

struct timezone {
	long tz_minuteswest;
	long tz_dsttime;
};

char *program_name;

typedef unsigned int u_int32_t;

void init(char **argv) {
	WSADATA wsadata;
	WSAStartup(MAKEWORD(2,2),&wsadata);
}

#define EMSGSIZE            WSAEMSGSIZE
#define INIT()              init(argv)
#define EXIT(s)             do {\
                              WSACleanup();\
                              exit(s);\
                            } while(0)
#define CLOSE(s)            if(closesocket(s))\
                              error(1, errno, "error call close")
#define errno               (GetLastError())
#define set_errno(e)        SetLastError(e)
#define isvalidsock(s)      ((s) != SOCKET_ERROR)
#define bzero(b,n)          memset((b), 0, (n))
#define sleep(t)            Sleep((t)*1000)
#define WINDOWS 
#define NLISTEN             5

void error(int status, int err, char *fmt, ... ) {
	va_list ap;
	va_start(ap, fmt);
	fprintf(stderr, "%s: ", program_name);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	if(err)
		fprintf(stderr, ": %s (%d)\n", strerror(err), err);
	if(status)
		EXIT(status);
}

void set_address(char *hname,char *sname,struct sockaddr_in *sap, char *protocol) {
	struct servent *sp;
	struct hostent *hp;
	char *endptr;
	short port;
	bzero(sap, sizeof(*sap));
	sap->sin_family = AF_INET;
	if(hname != NULL) {
		hp = gethostbyname(hname);
		if(hp == NULL)
			error(1,0,"unknown host: %s\n", hname);
		sap->sin_addr = *(struct in_addr *)hp->h_addr;
	} else {
		sap->sin_addr.s_addr = htonl(INADDR_ANY);
	}
	port = strtol(sname, &endptr, 0);
	if (*endptr == '\0') {
		sap->sin_port = htons(port);
	} else {
		sp = getservbyname(sname, protocol);
		if(sp == NULL)
			error( 1, 0, "unknown service: %s\n", sname );
		sap->sin_port = sp->s_port;
	}
}

#endif
