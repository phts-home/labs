data Figure = Rectangle Double Double Double Double
		|Circle Double Double Double
		|Union Figure Figure
		|Intersection Figure Figure deriving (Eq, Show) 

contains :: Figure -> Double -> Double -> Bool
contains (Rectangle x1 y1 x2 y2) x y = if x<=x2&&x>=x1&&y<=y2&&y>=y1 then True else False
contains (Circle x1 y1 r) x y = if (x1-x)*(x1-x)+(y1-y)*(y1-y)<=r*r then True else False
contains (Union q1 q2) x y = (contains q1 x y)||(contains q2 x y)
contains (Intersection q1 q2) x y = (contains q1 x y) && (contains q2 x y)

isRectangular :: Figure-> Bool
isRectangular (Rectangle x1 y1 x2 y2) = True
isRectangular (Circle x1 y1 r) = False
isRectangular (Union q1 q2) = (isRectangular q1)&&(isRectangular q2)
isRectangular (Intersection q1 q2) = (isRectangular q1)&&(isRectangular q2)

isNotIntersected :: Figure-> Figure->Bool
isNotIntersected (Rectangle x1 y1 x2 y2)(Rectangle x11 y11 x22 y22) = isEmpty (Rectangle x1 y1 x2 y2)x11 y11 x22 y22
isNotIntersected (Rectangle x11 y11 x22 y22)(Circle x1 y1 r) = isEmpty (Circle x1 y1 r) x11 y11 x22 y22
isNotIntersected (Circle x1 y1 r)(Rectangle x11 y11 x22 y22) = isEmpty (Circle x1 y1 r) x11 y11 x22 y22
isNotIntersected (Circle x1 y1 r1)(Circle x2 y2 r2) = if (x1-x2)*(x1-x2)+(y1-y2)*(y1-y2)<(r1+r2)*(r1+r2) then True else False


isEmpty :: Figure->Double ->Double ->Double ->Double ->Bool
isEmpty (Rectangle x1 y1 x2 y2) x11 y11 x22 y22 = (contains (Rectangle x1 y1 x2 y2) x11 y11)||(contains (Rectangle x1 y1 x2 y2) x11 y22)||(contains (Rectangle x1 y1 x2 y2) x22 y11)||(contains (Rectangle x1 y1 x2 y2) x22 y22)||(contains (Rectangle x11 y11 x22 y22) x1 y1)||(contains (Rectangle x11 y11 x22 y22) x1 y2)||(contains (Rectangle x11 y11 x22 y22) x2 y1)||(contains (Rectangle x11 y11 x22 y22) x2 y2)||(x1<x11&&x2>x22&&y1>y11&&y2<y22)||(x11<x1&&x22>x2&&y11>y1&&y22<y2)                                                                                
isEmpty (Circle x1 y1 r) x11 y11 x22 y22 = (contains(Circle x1 y1 r ) x11 y11)|| (contains(Circle x1 y1 r ) x11 y22)|| (contains(Circle x1 y1 r ) x22 y11)|| (contains(Circle x1 y1 r ) x22 y22)||(contains (Rectangle x11 y11 x22 y22) x1 y1)||(contains (Rectangle x11 y11 x22 y22) (x1+r) y1)||(contains (Rectangle x11 y11 x22 y22) x1 (y1+r))||(contains (Rectangle x11 y11 x22 y22) (x1-r) y1)||(contains (Rectangle x11 y11 x22 y22) x1 (y1-r))||(x11>(x1-r)&&x11<x1&&x22>(x1-r)&&x22<x1&&y11<y1&&y22>y1)||(x11<(x1+r)&&x11>x1&&x22<(x1+r)&&x22>x1&&y11<y1&&y22>y1)||(x11<x1&&x22>x1&&y11<(y1+r)&&y11>y1&&y22<(y1+r)&&y22>y1)||(x11<x1&&x22>x1&&y11>(y1-r)&&y11<y1&&y22>(y1-r)&&y22<y1)										
isEmpty (Union q1 q2) x11 y11 x22 y22 = (isEmpty q1 x11 y11 x22 y22)||(isEmpty q2 x11 y11 x22 y22)
isEmpty (Intersection q1 q2) x11 y11 x22 y22 = (isEmpty q1 x11 y11 x22 y22)&&(isEmpty q2 x11 y11 x22 y22)&&(isNotIntersected q1 q2)
