power2 :: Integer->Integer
power2 0 = 1
power2 1 = 2
power2 n = 2*power2(n-1)


list6 :: Integer->[Integer]
list6 1 = [2]
list6 n = list6(n-1) ++ [power2(n)]