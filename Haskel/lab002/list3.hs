list3 :: Integer->[Integer]
list3 n = if n < 2 then [] else if n == 2 then [2]
	else if even(n) then list3(n-1) ++ [n] else list3(n-1)