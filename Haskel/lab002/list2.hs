list2 :: Integer->[Integer]
list2 n = if n < 1 then [] else if n == 1 then [1]
	else if odd(n) then list2(n-1) ++ [n] else list2(n-1)