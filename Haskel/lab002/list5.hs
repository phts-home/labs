fact :: Integer->Integer
fact 0 = 1
fact 1 = 1
fact n = n*fact(n-1)


list5 :: Integer->[Integer]
list5 1 = [1]
list5 n = list5(n-1) ++ [fact(n)]