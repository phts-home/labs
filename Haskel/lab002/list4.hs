list4 :: Integer->[Integer]
list4 n = if n < 1 then [] else if n == 1 then [1]
	else  list4 (n-1) ++ [n*n]