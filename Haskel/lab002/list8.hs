triangle :: Integer->Integer
triangle 1 = 1
triangle n = n + triangle(n-1)

piramide :: Integer->Integer
piramide 1 = 1
piramide n = triangle(n) + piramide(n-1)

list8 :: Integer->[Integer]
list8 1 = [1]
list8 n = list8(n-1) ++ [piramide(n)]