{- task 1(01) -}

list_nat :: Integer -> [Integer]
list_nat 0 = []
list_nat n = list_nat (n - 1) ++ [n]

{- task 1(02) -}

list_Nnat :: Integer -> [Integer]
list_Nnat 0 = []
list_Nnat n = list_Nnat (n - 1) ++ [2*n - 1]

{- task 1(03) -}

list_Cnat :: Integer -> [Integer]
list_Cnat 0 = []
list_Cnat n = list_Cnat (n - 1) ++ [2*n]

{- task 1(04) -}

list_sq :: Integer -> [Integer]
list_sq 0 = []
list_sq n = list_sq (n - 1) ++ [n*n]

{- task 1(05) -}

fact :: Integer -> Integer
fact 1 = 1
fact n = n*fact (n -1)

list_fact :: Integer -> [Integer]
list_fact 0 = []
list_fact n = list_fact (n - 1) ++ [fact n]

{- task 1(06) -}

pow2 :: Integer -> Integer
pow2 n = if n == 0 then 1 else 2*pow2 (n -1)

list_two :: Integer -> [Integer]
list_two 0 = []
list_two n = list_two (n - 1) ++ [pow2 n]

{- task 1(07) -}

tr_number :: Integer -> Integer
tr_number n = if n == 1 then 1 else n + tr_number (n -1)

list_tr :: Integer -> [Integer]
list_tr 0 = []
list_tr n = if n == 0 then [] else list_pr (n - 1) ++ [tr_number n]

{- task 1(08) -}

pr_number :: Integer -> Integer
pr_number n = if n == 1 then 1 else tr_number n + pr_number (n - 1)

list_pr :: Integer -> [Integer]
list_pr 0 = []
list_pr n = list_pr (n - 1) ++ [pr_number n]


{- task 2(05) -}
rec :: Integer -> Integer
rec n = if n == 0 then 1 else 2*rec (n-1)

twopow :: Integer -> Integer
twopow n = if even n == True then rec n else 2*rec (n-1) 

{- task 2(07) -}
removeEmpty :: [[Char]] -> [[Char]]
removeEmpty [] = []
removeEmpty (x:xs) = if x == "" then removeEmpty xs else x : removeEmpty xs

{- task 2(09) -}
makePositive :: [Integer] -> [Integer] 
makePositive [] = []
makePositive (x:xs) = if (x < 0) then -x : makePositive xs else x : makePositive xs