list1 :: Integer->[Integer]
list1 n = if n < 1 then [] else if n == 1 then [1]
	else  list1 (n-1) ++ [n]