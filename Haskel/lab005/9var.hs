import Char




quicksort :: Ord a => [�] -> [�]
quicksort [] = []
quicksort (x:xs) = quicksort[y|y<-xs,y<=x] ++ [x] ++ quicksort[y|y<-xs,y>x]




data Key =  Tex Char | Upr [Char] | Soch [Char] Char deriving (Show,Eq)

getAlNum :: [Key] -> [Char]
getAlNum [] = []
getAlNum ((Tex x):xs) = [x] ++ getAlNum xs
getAlNum ((Upr _):xs) = getAlNum xs
getAlNum ((Soch _ _):xs) = getAlNum xs

getRaw :: [Key] -> [Char]
getRaw [] = []
getRaw ((Tex x):xs) = [x] ++ getRaw xs
getRaw ((Upr _):xs) = getRaw xs
getRaw ((Soch _ x):xs) = [x] ++ getRaw xs

isCapsLocked :: [Key] -> Bool
isCapsLocked [] = False
isCapsLocked ((Tex _):xs) = isCapsLocked xs
isCapsLocked ((Upr _):xs) = not (isCapsLocked xs)
isCapsLocked ((Soch _ _):xs) = isCapsLocked xs

getString3 :: [Key] -> [Char]
getString3	x = getString2 (reverse x)

getString2 :: [Key] -> [Char]
getString2	x = reverse (getString x)

getString :: [Key] -> [Char]
getString [] = []
getString ((Tex x):xs) =  if (isCapsLocked xs) then [toUpper x] ++ getString xs else [toLower x] ++ getString xs
getString ((Soch x y):xs) = if (isCapsLocked xs) then [toLower y] ++ getString xs else [toUpper y] ++ getString xs
getString ((Upr _):xs) = getString xs

