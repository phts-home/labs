isIncluded :: Double -> Double -> Double -> Double -> Double -> Double -> Bool
isIncluded x1 y1 r1 x2 y2 r2 = if (sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))+r2 < r1) then True else False