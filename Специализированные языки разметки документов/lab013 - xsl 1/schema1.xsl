<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">

<xsl:template match="/">
<h1>Компоненты программы <xsl:value-of select="app/appname"/></h1>
(<xsl:value-of select="app/copyright/company"/>, <xsl:value-of select="app/copyright/year"/>)

<table border="1"><tr><th></th><th></th><th></th></tr>
<xsl:for-each select="app/panel">
	<tr>
	<td>
		<xsl:value-of select="name"/>
	</td><td>
		<xsl:value-of select="description" />
	</td><td><table border="1">
		<xsl:for-each select="tab">
			<tr><td>
				<xsl:value-of select="name" />
			</td></tr>
		</xsl:for-each>
	</table></td>
	</tr>
</xsl:for-each>
</table>
	
</xsl:template>

</xsl:stylesheet>