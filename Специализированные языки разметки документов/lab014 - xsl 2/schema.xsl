<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">

<xsl:template match="/">
<h1>Компоненты программы <xsl:value-of select="app/appname"/></h1>
(<xsl:value-of select="app/copyright/company"/>, <xsl:value-of select="app/copyright/year"/>)
<xsl:apply-templates select="app/panel" order-by="name"/>
<hr/>
</xsl:template>

<xsl:template match="panel[name='Front Panel']">
	<hr/>
	<div style="font-size: large">Панель: <xsl:value-of select="name"/></div>
	<div><xsl:value-of select="description" /></div>
	<div>Закладки: <xsl:apply-templates select="tab"/></div>
</xsl:template>

<xsl:template match="tab">
	<xsl:value-of select="name" />,
</xsl:template>

</xsl:stylesheet>