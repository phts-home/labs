
public class Bird extends Animal {
	
	public Bird() {
		super("Bird");
	}
	
	public Bird(String name) {
		super(name);
	}
	
}
