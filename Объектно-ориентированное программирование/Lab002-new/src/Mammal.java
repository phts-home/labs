
public class Mammal extends Animal {
	
	public Mammal() {
		super("Mammal");
	}
	
	public Mammal(String name) {
		super(name);
	}
	
}
