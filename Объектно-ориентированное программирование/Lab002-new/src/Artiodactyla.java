
public class Artiodactyla extends Animal {
	
	public Artiodactyla() {
		super("Artiodactyla");
	}
	
	public Artiodactyla(String name) {
		super(name);
	}
	
}
