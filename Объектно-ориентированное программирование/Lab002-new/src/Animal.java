import java.util.*;

public class Animal {
	
	public static ArrayList <Animal> list;
	
	public Animal(String name) {
		this.name = name;
		if (list == null) {
			list = new ArrayList<Animal>();
		}
		list.add(this);
	}
	
	public static void show() {
		for(int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).getName());
		}
	}
	
	public String getName() {
		return name;
	}
	
	private String name;
	
}
