
public class Lab002 {
	
	public static void main(String[] args) {
		Artiodactyla t = new Artiodactyla("My Artiodactyla!!!");
		Artiodactyla t2 = new Artiodactyla();
		Artiodactyla t3 = new Artiodactyla();
		Artiodactyla t4 = new Artiodactyla();
		Bird b1 = new Bird("My Bird!!!");
		Bird b2 = new Bird();
		Bird b3 = new Bird();
		Mammal c1 = new Mammal("My Mammal!!!");
		
		Animal.show();
	}

}
