#include <stdio.h>
#include <string.h>
#include <stdlib.h>


class DataBase
{
public:
	DataBase();
	DataBase(int ms);
	DataBase(char *n);
	DataBase(char *n, int ms);
	DataBase(const DataBase &src);
	~DataBase();

public:
	int print();
	int getSize();
	char * getName();
	void setName(char *n);
	void addElem(double e);
	void addElem(double e, int pos);
	void delElem();
	void delElem(int pos);
	double getElem(int pos);

private:
	int size;
	int maxsize;
	char name[257];
	double *ar;
};