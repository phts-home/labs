#include "database.h";



DataBase::DataBase()
{
	printf(">>> constructor\n");
	size = 0;
	maxsize = 10;
	strcpy(name, "db1");
	ar = (double *)calloc(maxsize, sizeof(double));
}

DataBase::DataBase(int ms)
{
	printf(">>> constructor with parameters (int ms)\n");
	size = 0;
	maxsize = ms;
	strcpy(name, "db1");
	ar = (double *)calloc(maxsize, sizeof(double));
}

DataBase::DataBase(char *n)
{
	printf(">>> constructor with parameters (char *n)\n");
	size = 0;
	maxsize = 10;
	strcpy(name, n);
	ar = (double *)calloc(maxsize, sizeof(double));
}

DataBase::DataBase(char *n, int ms)
{
	printf(">>> constructor with parameters (char *n, int ms)\n");
	size = 0;
	maxsize = ms;
	strcpy(name, n);
	ar = (double *)calloc(maxsize, sizeof(double));
}

DataBase::DataBase(const DataBase &src)
{
	printf(">>> copying constructor\n");
	size = src.size;
	maxsize = src.maxsize;
	strcpy(name, src.name);
	ar = (double *)calloc(maxsize, sizeof(double));
	for (int i = 0; i < size; i++) {
		ar[i] = src.ar[i];
	}
}

DataBase::~DataBase()
{
	printf(">>> destructor\n");
	free(ar);
}

int DataBase::print()
{
	for (int i = 0; i < size; i++) {
		printf("%d: %lf\n", i, ar[i]);
	}
	return 0;
}

int DataBase::getSize()
{
	return size;
}

void DataBase::addElem(double e)
{
	if (size != maxsize) {
		ar[size] = e;
		size++;
	}
}

void DataBase::addElem(double e, int pos)
{
	if (pos > size) {
		pos = size;
	}
	if (pos < 0) {
		pos = 0;
	}
	if (size != maxsize) {
		ar[pos] = e;
		size++;
	}
}

void DataBase::delElem()
{
	if (size != 0) {
		size--;
	}
}

void DataBase::delElem(int pos)
{
	if (pos > size) {
		pos = size;
	}
	if (pos < 0) {
		pos = 0;
	}
	if (size != 0) {
		for (int i = pos; i < size-1; i++) {
			ar[i] = ar[i+1];
		}
		size--;
	}
}

double DataBase::getElem(int pos)
{
	return ar[pos];
}