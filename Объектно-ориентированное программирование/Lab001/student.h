class Student
{
public:
	Student();
	Student(char *);
	Student(char *, char *);
	Student(const Student &);
	~Student();

public:
	char *getName();
	char *getGroup();
	char *getFaculty();
	int getYear

private:
	char name[257];
	char group[257];
	char faculty[257];
	int year;
}