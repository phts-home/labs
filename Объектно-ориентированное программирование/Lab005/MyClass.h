#ifndef MYCLASS_H
#define MYCLASS_H

#include <stdio.h>
#include <iostream.h>



class MyClass {

public:
	MyClass();

public:
	friend ostream& operator << (ostream& stream, MyClass& myClass) {
		return stream << myClass.getValue();
	}
    MyClass operator * (MyClass&);
	bool operator > (MyClass&);
	int getValue();
	void setValue(int);

private:
	int value;

};


#endif