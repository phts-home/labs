#include <stdio.h>
#include <iostream.h>


#include "array.cpp"
#include "MyClass.h"



void main()
{
	Array<int> arr1(5);
	Array<int> arr2(5);

	arr1.set(0, 123);
	arr1.set(1, 0);
	arr1.set(2, 0);
	arr1.set(3, 1);
	arr1.set(4, 2);

	arr2.set(0, 123);
	arr2.set(1, 0);
	arr2.set(2, 0);
	arr2.set(3, 1);
	arr2.set(4, 2);

	arr1 * arr2;
	arr1.print();
	cout << *arr1.max() << "\n";

	Array<MyClass> arr3(5);
	Array<MyClass> arr4(5);

	arr3.get(0)->setValue(10);
	arr3.get(1)->setValue(0);
	arr3.get(2)->setValue(0);
	arr3.get(3)->setValue(0);
	arr3.get(4)->setValue(0);

	arr4.get(0)->setValue(10);
	arr4.get(1)->setValue(0);
	arr4.get(2)->setValue(0);
	arr4.get(3)->setValue(0);
	arr4.get(4)->setValue(0);

	arr3 * arr4;
	arr3.print();
	cout << *arr3.max() << "\n";

};