#include "array.h"



template <class T> Array<T>::Array<T>(int size) {
	Array<T>::ar = new T[size];
	Array<T>::size = size;
}

template <class T> Array<T>::~Array<T>() {
	delete[] ar;
}

template <class T> void Array<T>::operator * (Array<T>& obj) {
	if (size != obj.getSize()) {
		return;
	}
	for(int i = 0; i < size; i++) {
		ar[i] = ar[i] * obj.ar[i];
	}
}

template <class T> T *Array<T>::max() {
	if (size == 0) {
		return NULL;
	}
	int w;
	T *max = &ar[0];
	for(int i = 1; i < size; i++) {
		if (ar[i] > *max) {
			*max = ar[i];
		}
	}
	return max;
}

template <class T> T *Array<T>::get(int index) {
	return &ar[index];
}

template <class T> void Array<T>::set(int index, T value) {
	if ( (index < 0) || (index >= size) ) {
		return;
	}
	ar[index] = value;
}

template <class T> int Array<T>::getSize() {
	return size;
}

template <class T> void Array<T>::print(int index) {
	if ( (index < 0) || (index >= size) ) {
		return;
	}
	cout << ar[index];
}

template <class T> void Array<T>::print() {
	cout << "[";
	for (int i = 0; i < size; i++) {
		print(i);
		if (i != size-1) {
			cout << ", ";
		}
	}
	cout << "]\n";
}
