#ifndef ARRAY_H
#define ARRAY_H


#include <stdio.h>
#include <iostream.h>




template <class T> class Array {

	public:
		Array(int);
		~Array();

	public:
		void operator * (Array& obj);
		T *max();
		T *get(int);
		void set(int, T);
		int getSize();
		void print(int);
		void print();

	private:
		T *ar;
		int size;
		
};

#endif