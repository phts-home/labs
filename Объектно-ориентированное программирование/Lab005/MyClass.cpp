#include "MyClass.h"


MyClass::MyClass() {
	MyClass::value = 0;
}

MyClass MyClass::operator * (MyClass& myClass) {
	value *= myClass.getValue();
	return *this;
}

bool MyClass::operator > (MyClass& myClass) {
	return value > myClass.getValue();
}

int MyClass::getValue() {
	return value;
}

void MyClass::setValue(int value) {
	MyClass::value = value;
}