
#include "DVector.h"



double DVector::operator [](int index)
{
	if (index < size) {
		return arr[index];
	}
	return 0.0;
}

DVector &DVector::operator -(double item)
{
	for (int i = 0; i < size; i++) {
		arr[i] -= item;
	}
	return *this;
}

DVector::operator int(void)
{
	return size;
}

DVector::operator char*(void)
{
	char* str = new char[257];
	str[0] = 0;

	char* buf = new char[257];
	buf[0] = 0;

	for (int i = 0; i < size; i++) {
		sprintf(buf, "%lf", arr[i]);
		strcat(str, buf);
		if (i != size-1) {
			strcat(str, ", ");
		}
	}
	return str;
}

DVector::DVector(void)
{
	DVector(0);
}

DVector::DVector(int size)
{
	DVector::size = size;
	arr = new double[size];
	for(int i = 0; i < size; i++){
		arr[i] = 0;
	}
}

DVector::~DVector(void)
{
	delete[] arr;
}

void DVector::add(double item)
{
	insert(size, item);
}

void DVector::insert(int index, double item)
{
	if ( (index < 0) || (index > size) ) {
		return;
	}
	double* tmp = new double[size+1];
	for (int i = 0; i < index; i++) {
		tmp[i] = arr[i];
	}
	tmp[index] = item;
	for (int ii = index; ii < size; ii++) {
		tmp[ii+1] = arr[ii];
	}
	
	delete[] arr;
	size++;
	arr = tmp;
}

void DVector::remove(int index)
{
	if ( (index < 0) || (index >= size) ) {
		return;
	}
	size--;
	double* tmp = new double[size];
	for (int i = 0; i < index; i++) {
		tmp[i] = arr[i];
	}
	for (int ii = index; ii < size; ii++) {
		tmp[ii] = arr[ii+1];
	}
	delete[] arr;
	arr = tmp;
}

void DVector::set(int index, double item) {
	if ( (index < 0) || (index >= size) ) {
		return;
	}
	arr[index] = item;
}

void DVector::clear(void)
{
	size = 0;
	delete[] arr;
	double* arr = new double[size];
}
