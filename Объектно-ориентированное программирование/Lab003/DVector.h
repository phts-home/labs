#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#pragma once



class DVector
{
public:
	double operator [](int);
	DVector &operator -(double);
	operator int(void);
	operator char*(void);

public:
	DVector(void);
	DVector(int);
	~DVector(void);

public:
	void add(double);
	void insert(int, double);
	void remove(int);
	void set(int, double);
	void clear(void);

public:
	double *arr;
	int size;
};
