#include <stdio.h>

#include "DVector.h"



void main()
{
	DVector *vect = new DVector(1);

	vect->add(123.0);
	vect->insert(0, 456.0);

	vect->set(1, 456.0);

	*vect = *vect - 100.0;

	int s = *vect;
	printf("size == %d\n", s);

	char *toString = *vect;
	printf("toString == %s\n", toString);
}