
public class Table extends Furniture {
	
	public Table() {
		super("Table");
	}
	
	public Table(String name) {
		super(name);
	}
	
}
