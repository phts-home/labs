
public class Chair extends Furniture {
	
	public Chair() {
		super("Chair");
	}
	
	public Chair(String name) {
		super(name);
	}
	
}
