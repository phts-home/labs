
public class Bookcase extends Furniture {
	
	public Bookcase() {
		super("Bookcase");
	}
	
	public Bookcase(String name) {
		super(name);
	}
	
}
