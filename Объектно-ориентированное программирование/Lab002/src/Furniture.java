import java.util.*;

public class Furniture {
	
	public static ArrayList <Furniture> list;
	
	public Furniture(String name) {
		this.name = name;
		if (list == null) {
			list = new ArrayList<Furniture>();
		}
		list.add(this);
	}
	
	public static void show() {
		for(int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).getName());
		}
	}
	
	public String getName() {
		return name;
	}
	
	private String name;
	
}
