
public class Lab002 {
	
	public static void main(String[] args) {
		Table t = new Table("My Table!!!");
		Table t2 = new Table();
		Table t3 = new Table();
		Table t4 = new Table();
		Bookcase b1 = new Bookcase("My Bookcase!!!");
		Bookcase b2 = new Bookcase();
		Bookcase b3 = new Bookcase();
		Chair c1 = new Chair("My Chair!!!");
		ComputerTable ct1 = new ComputerTable();
		ComputerTable ct2 = new ComputerTable("My ComputerTable!!!");
		
		Furniture.show();
	}

}
