/* 
 * ���������������� ������������ �������
 * 
 * ������ �����
 * ��. 06��-1
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

int indexOfMinFromArray(int[], int);
int checkEnd(FILE *[], int);


int main(int argc, char *argv[])
{
	const int k = 6;
	// init some files
	FILE *fileA = NULL;
	FILE *filesB[k];
	FILE *filesC[k];
	filesB[0] = fopen("B1.txt", "w+");
	filesB[1] = fopen("B2.txt", "w+");
	filesB[2] = fopen("B3.txt", "w+");
	filesB[3] = fopen("B4.txt", "w+");
	filesB[4] = fopen("B5.txt", "w+");
	filesB[5] = fopen("B6.txt", "w+");
	
	// write B-files from A
	fileA = fopen("A.txt", "r");
	int old = 0, curBFile = 0;
	while (!feof(fileA)) {
		char line[51];
		int num = 0;
		fgets(line, 50, fileA);
		num = atoi(line);
		if (old > num) {
			curBFile++;
			curBFile %= k;
		}
		fputs(line, filesB[curBFile]);
		old = num;
	}
	
	// create C-files from B or B-files from C (depending on c)
	int c = 0;
	while (1) {
		// rewrite B* files or C* files by turn
		if (c % 2) {
			filesC[0] = fopen("B1.txt", "w+");
			filesC[1] = fopen("B2.txt", "w+");
			filesC[2] = fopen("B3.txt", "w+");
			filesC[3] = fopen("B4.txt", "w+");
			filesC[4] = fopen("B5.txt", "w+");
			filesC[5] = fopen("B6.txt", "w+");
		} else {
			filesC[0] = fopen("C1.txt", "w+");
			filesC[1] = fopen("C2.txt", "w+");
			filesC[2] = fopen("C3.txt", "w+");
			filesC[3] = fopen("C4.txt", "w+");
			filesC[4] = fopen("C5.txt", "w+");
			filesC[5] = fopen("C6.txt", "w+");
		}
		
		for (int i = 0; i < k; i++) {
			fsetpos(filesB[i], 0);
		}
		
		// first time make array with first elements in sequences from B-files
		int t[k];
		char line[51];
		for (int i = 0; i < k; i++) {
			if (!feof(filesB[i])) {
				if (fgets(line, 50, filesB[i]) != NULL) {
					t[i] = atoi(line);
				} else {
					t[i] = -1;
				}
			} else {
				t[i] = -1;
			}
		}
		
		// writing to C-files
		int curCFile = 0;
		while (1) {
			int num[k];
			int old[k];
			for (int i = 0; i < k; i++) {
				num[i] = t[i];
				old[i] = num[i];
			}
			
			while (1) {
				int ind = indexOfMinFromArray(num, k);
				if (ind == -1) {
					break;
				}
				char numAsStr[20];
				_itoa(num[ind], numAsStr, 10);
				fputs(strcat(numAsStr, "\n"), filesC[curCFile]);
				
				if ( !feof(filesB[ind]) ) {
					if (fgets(line, 50, filesB[ind]) != NULL) {
						t[ind] = atoi(line);
					} else {
						t[ind] = -1;
					}
				} else {
					t[ind] = -1;
				}
				if ( old[ind] <= t[ind] ) {
					num[ind] = t[ind];
					old[ind] = num[ind];
				} else {
					num[ind] = -1;
				}
			}
			
			if (indexOfMinFromArray(t, k) == -1) {
				break;
			}
			
			curCFile++;
			curCFile %= k;
		}
		
		// swicth files
		for (int i = 0; i < k; i++) {
			FILE *t = filesB[i];
			filesB[i] = filesC[i];
			filesC[i] = t;
		}
		
		c++;
		
		// check end
		if (checkEnd(filesB, k)) {
			printf("\nDone. Sorted Array Was Stored In ");
			if (c % 2) {
				printf("C1.txt\n\n");
			} else {
				printf("B1.txt\n\n");
			}
			break;
		}
	}
	
	for (int i = 0; i < k; i++) {
		fclose(filesB[i]);
		fclose(filesC[i]);
	}
	fclose(fileA);
	_getch();
	return 0;
}

int indexOfMinFromArray(int arr[], int size) {
	int ii = 0;
	while ( (arr[ii] == -1) && (ii < size) ) {
		ii++;
	}
	if (ii >= size) {
		return -1;
	}
	int min = arr[ii], index = ii;
	for (int i = ii; i < size; i++) {
		if (arr[i] == -1) {
			continue;
		}
		if (arr[i] < min) {
			min = arr[i];
			index = i;
		}
	}
	return index;
}

int checkEnd(FILE *files[], int size) {
	for (int i = 1; i < size; i++) {
		int old = ftell(files[i]);
		fseek(files[i], 0, SEEK_SET);
		int beg = ftell(files[i]);
		
		fseek(files[i], 0, SEEK_END);
		int end = ftell(files[i]);
		if (beg != end) {
			return 0;
		}
		fseek(files[i], old, SEEK_SET);
	}
	return 1;
}
