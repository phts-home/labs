/* 
 * ������������ �������
 * 
 * ������� ������
 * ��. 06��-1
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <conio.h>

int indexOfMinFromArray(int[], int);
int checkEnd(FILE *[], int);


int main(int argc, char *argv[])
{
	const int k = 2;
	// init some files
	FILE *fileA = fopen("A.txt", "r");
	FILE *fileRes = fopen("Res.txt", "w+");
	FILE *filesB[k];
	
	int c = 0;
	while (1) {
		filesB[0] = fopen("B1.txt", "w+");
		filesB[1] = fopen("B2.txt", "w+");
		
		int old = 0, curBFile = 0;
		while (!feof(fileA)) {
			char line[51];
			int num = 0;
			if (fgets(line, 50, fileA) != NULL) {
				num = atoi(line);
			} else {
				break;
			}
			if (old > num) {
				curBFile++;
				curBFile %= k;
			}
			fputs(line, filesB[curBFile]);
			old = num;
		}
		fclose(fileA);
		fileRes = fopen("Res.txt", "w+");
		
		for (int i = 0; i < k; i++) {
			fsetpos(filesB[i], 0);
		}
		
		int t[k];
		char line[51];
		for (int i = 0; i < k; i++) {
			if (!feof(filesB[i])) {
				if (fgets(line, 50, filesB[i]) != NULL) {
					t[i] = atoi(line);
				} else {
					t[i] = -1;
				}
			} else {
				t[i] = -1;
			}
		}
		
		while (1) {
			int num[k];
			int old2[k];
			for (int i = 0; i < k; i++) {
				num[i] = t[i];
				old2[i] = num[i];
			}
			
			while (1) {
				int ind = indexOfMinFromArray(num, k);
				if (ind == -1) {
					break;
				}
				char numAsStr[20];
				_itoa(num[ind], numAsStr, 10);
				fputs(strcat(numAsStr, "\n"), fileRes);
				
				if ( !feof(filesB[ind]) ) {
					if (fgets(line, 50, filesB[ind]) != NULL) {
						t[ind] = atoi(line);
					} else {
						t[ind] = -1;
					}
				} else {
					t[ind] = -1;
				}
				if ( old2[ind] <= t[ind] ) {
					num[ind] = t[ind];
					old2[ind] = num[ind];
				} else {
					num[ind] = -1;
				}
			}
			
			if (indexOfMinFromArray(t, k) == -1) {
				break;
			}
		}
		
		c++;
		fclose(fileRes);
		fileA = fopen("Res.txt", "r");
		
		// check end
		if (checkEnd(filesB, k)) {
			printf("Done\n");
			break;
		}
	}
	
	for (int i = 0; i < k; i++) {
		fclose(filesB[i]);
	}
	fclose(fileA);
	
	_getch();
	return 0;
}

int indexOfMinFromArray(int arr[], int size) {
	int ii = 0;
	while ( (arr[ii] == -1) && (ii < size) ) {
		ii++;
	}
	if (ii >= size) {
		return -1;
	}
	int min = arr[ii], index = ii;
	for (int i = ii; i < size; i++) {
		if (arr[i] == -1) {
			continue;
		}
		if (arr[i] < min) {
			min = arr[i];
			index = i;
		}
	}
	return index;
}

int checkEnd(FILE *files[], int size) {
	for (int i = 1; i < size; i++) {
		int old = ftell(files[i]);
		fseek(files[i], 0, SEEK_SET);
		int beg = ftell(files[i]);
		
		fseek(files[i], 0, SEEK_END);
		int end = ftell(files[i]);
		if (beg != end) {
			return 0;
		}
		fseek(files[i], old, SEEK_SET);
	}
	return 1;
}
