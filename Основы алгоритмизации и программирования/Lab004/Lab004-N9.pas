{ ...TODO... }
program Lab004_N9;
type
  TElement = ^TRec;
  TRec = record
           Data : String[16];
           Next : TElement;
         end;

var
  LineBegin, LineEnd: TElement;
  File1, File2: Text;
  Res: Boolean;

procedure WriteEl(var LineBegin, LineEnd: TElement; c: String);
var
  u: TElement;
begin
  New(u);
  u^.Data:=c;
  u^.Next:=nil;
  if LineBegin=nil then
    LineBegin:=u
  else
    LineEnd^.Next:=u;
  LineEnd:=u;
end;

procedure ReadEl(var LineBegin, LineEnd: TElement; var c: String);
var
  u: TElement;
function Free(El: TElement): Boolean;
begin
  Free:=(El = Nil);
end;
begin
  if not Free(LineBegin)then
  begin
    c:=LineBegin^.Data;
    u:=LineBegin;
    LineBegin:=LineBegin^.Next;
    Dispose(u);
  end;
end;

procedure CreateLine;
var
  i: Integer;
  Line: String;
begin
  LineBegin:=nil;
  LineEnd:=nil;
  Assign(File1,'File001.txt');
  Reset(File1);
  while not Eof(File1) do
  begin
    ReadLn(File1,Line);
    WriteEl(LineBegin, LineEnd, Line);
  end;
  Close(File1);
end;

function Check: Boolean;
var
  i: Integer;
  FileRes: Text;
  Line: String;
  Val: String;
  Res: Boolean;
begin
  Assign(File2,'File002.txt');
  Reset(File2);
  Assign(FileRes,'FileRes.txt');
  Rewrite(FileRes);
  Res:=True;
  while not Eof(File2) do
  begin
    ReadLn(File2,Line);
    ReadEl(LineBegin, LineEnd, Val);
    WriteLn(FileRes,Line,'  ||  ',Val);
    if Line <> Val then
    begin
      Res:=False;
      break;
    end;
  end;
  Close(File2);
  Close(FileRes);
  Result:=Res;
end;

begin
  CreateLine;
  Res:=Check;
  if Res then
    WriteLn('OK')
  else
    WriteLn('Different');
end.
