uses Crt;
type
  TBookInfo = Record
              rAuthor: String[20];
              rTitle: String[20];
              rYear: Integer;
              end;
  TBooks = ^TBookList;
  TBookList = Record
              BookInfo: TBookInfo;
              NextBook: TBooks;
              end;

var
  BeginList, CurPos, PrevPos: TBooks;
  c: Char;
  
procedure GenerateList;
var
  InputFile: Text;
  FileName: String;
  i: Byte;
begin
Assign(InputFile,'Input.txt');
Reset(InputFile);
New(CurPos);
BeginList:=CurPos;
while not Eof(InputFile)do
  begin
  with CurPos^.BookInfo do
    begin
    ReadLn(InputFile, rAuthor);
    ReadLn(InputFile, rTitle);
    ReadLn(InputFile, rYear);
    end;
  PrevPos:=CurPos;
  New(CurPos);
  PrevPos^.NextBook:=CurPos;
  end;
PrevPos^.NextBook:=nil;
Close(InputFile);
end;

procedure PrintList;
var
  i: Byte;
begin
WriteLn('> Printing List...');
CurPos:=BeginList;
i:=1;
while CurPos<>nil do
  begin
  with CurPos^.BookInfo do
    begin
    Write(i:3,':   Author: ',rAuthor,'; Title: ', rTitle,'; Year: ', rYear);
    end;
  Inc(i);
  CurPos:=CurPos^.NextBook;
  WriteLn;
  end;
WriteLn;
end;

procedure SearchItems;
var
  i: Byte;
begin
CurPos:=BeginList;
i:=1;
while CurPos <> nil do
  begin
  with CurPos^.BookInfo do
    if rYear < 1985 then
      begin
      Write(i:3,':   Author: ',rAuthor,'; Title: ', rTitle,'; Year: ', rYear);
      WriteLn;
      Inc(i);
      end;
  PrevPos:=CurPos;
  CurPos:=CurPos^.NextBook;
  end;
end;

procedure DelItems;
begin
CurPos:=BeginList;
while CurPos <> nil do
  begin
  with CurPos^.BookInfo do
    if rAuthor = '������' then
      begin
      if CurPos = BeginList then
        BeginList:=CurPos^.NextBook
      else 	
        begin
        PrevPos^.NextBook:=CurPos^.NextBook;
        CurPos:=PrevPos;
        Break;
        end;
      end;
  PrevPos:=CurPos;
  CurPos:=CurPos^.NextBook;
  end;
end;

procedure SortItems;
var
  Ss, Sz : TBooks;
  Key : Boolean;
begin
repeat
  Key:=False;
  CurPos:=BeginList;
  Ss:=CurPos^.NextBook;
  while Ss <> nil do
    begin
    if CurPos^.BookInfo.rYear < Ss^.BookInfo.rYear then
      begin
      if CurPos = BeginList then
        begin
        BeginList := Ss;
        CurPos^.NextBook:=Ss^.NextBook;
        Ss^.NextBook:=CurPos;
        end
      else
        begin
    		Sz := PrevPos^.NextBook;
        PrevPos^.NextBook:=CurPos^.NextBook;
        CurPos^.NextBook:=Ss^.NextBook;
        Ss^.NextBook:=Sz;
        end;
      Key := True;
      end;
    PrevPos:=CurPos;
    CurPos:=CurPos^.NextBook;
    if CurPos = nil then Ss:=nil else Ss:=CurPos^.NextBook;
    end;
until Key = False;
end;

procedure AddItem;
var
  Added: Boolean;
  i, j, Count: Byte;
  InsItem: TBookInfo;
begin
WriteLn('> Enter Count Of New Items');
Write('> ');
ReadLn(Count);
for i:=1 to Count do
  with InsItem do
    begin
    WriteLn('> Enter Author');
    Write('> ');
    ReadLn(rAuthor);
    WriteLn('> Enter Title');
    Write('> ');
    ReadLn(rTitle);
    WriteLn('> Enter Year');
    Write('> ');
    ReadLn(rYear);
    CurPos:=BeginList;
    Added:=False;
    while(CurPos <> nil)and(Added = False) do
      begin
      if CurPos^.BookInfo.rYear < InsItem.rYear then
        begin
        Added:=True;
        if CurPos = BeginList then
          begin
          New(CurPos);
          CurPos^.NextBook:=BeginList;
          BeginList:=CurPos;
          CurPos^.BookInfo:=InsItem;
  				end
        else
          begin
          New(CurPos);
          CurPos^.NextBook:=PrevPos^.NextBook;
          PrevPos^.NextBook:=CurPos;
          CurPos^.BookInfo:=InsItem;
          end;
        end;
      PrevPos:=CurPos;
      CurPos:=CurPos^.NextBook;
      end;
    If Added = False then
      begin
  		New(CurPos);
      PrevPos^.NextBook:=CurPos;
      CurPos^.NextBook:=nil;
      CurPos^.BookInfo:=InsItem;
      end;
    end;
end;

begin
ClrScr;

WriteLn('> Reading From File');
GenerateList;
PrintList;
c:=ReadKey;

WriteLn('> Print Items With [rYear < 1985]');
SearchItems;
WriteLn;
c:=ReadKey;

WriteLn('> Delete Items With [rAuthor = ''������'']');
DelItems;
PrintList;
c:=ReadKey;

WriteLn('> Sort Items');
SortItems;
PrintList;
c:=ReadKey;

New(CurPos);
WriteLn('> Add Item(s)');
AddItem;
PrintList;
c:=ReadKey;

WriteLn('> Press Any Key To Exit');
WriteLn;
c:=ReadKey;
end.
