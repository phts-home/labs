{ #9 }
uses Crt;
type TStack = ^TElement;
     TElement = Record
                  Val: Integer;
                  Next: TStack;
                  end;

var Stack1, Stack2, ResultStack, ResultStack2: TStack;
    i, CurrentVal, SearchVal: Integer;
    FuncRes: Boolean;

procedure AddNew(Stack: TStack; Value: Integer; var ResStack: TStack);
var Temp: TStack;
begin
New(Temp);
Temp^.Val:=Value;
Temp^.Next:=Stack;
Stack:=Temp;

ResStack:=Stack;
end;

procedure ReadAndDel(Stack: TStack; var ResStack: TStack; Value: Integer);
var Temp: TStack;
begin
if Stack=nil then Exit;
Temp:=Stack;
Stack:=Stack^.Next;
Value:=Stack^.Val;
Dispose(Temp);

ResStack:=Stack;
end;

procedure GetTopItem(Stack: TStack; var ResStack: TStack; Value: Integer);
var Res: Boolean;
begin
if Stack <> nil then Value:=Stack^.Val;
end;

procedure InputStack(var ResStack: TStack);
var Stack: TStack;
    Value, Code: Integer;
    StrVal: String;
    EscPressed: Boolean;
    c: Char;
begin
Stack:=nil;
EscPressed:=False;
while not EscPressed do
  begin
  Write('> ');
  StrVal:='';
  repeat
    c:=ReadKey;
    case c of
      #27:
        begin
        EscPressed:=True;
        Write('>> Esc');
        Break;
        end;
      #48..#57:
        StrVal:=StrVal+c;
      #8:
        StrVal:=Copy(StrVal,1,Length(StrVal)-1);
      end;
    DelLine;
    GotoXY(1,WhereY);
    Write('> ',StrVal);
  until c = #13;
  if not EscPressed then
    begin
    Val(StrVal,Value,Code);
    AddNew(Stack,Value,Stack);
    end;
  WriteLn;
  end;
ResStack:=Stack;
end;

procedure PrintStack(Stack: TStack);
var Temp: TStack;
begin
Temp:=Stack;
while Stack <> nil do
  begin
  Write(Stack^.Val,'  ');
  Stack:=Stack^.Next;
  end;
WriteLn;
end;

procedure ReverseStack(Stack: TStack; var ResStack: TStack);
var Temp: TStack;
begin
Temp:=nil;
while Stack <> nil do
  begin
  AddNew(Temp,Stack^.Val,Temp);
  Stack:=Stack^.Next;
  end;
ResStack:=Temp;
end;

function SearchItem(Val: Integer; var ResStack, ResStack2: TStack): Boolean;
var Found: Boolean;
    TmpSt1, TmpSt2, PartSt: TStack;
begin
Found:=False;
ResStack:=nil;
TmpSt1:=nil;
TmpSt1:=Stack1;
TmpSt2:=nil;
TmpSt2:=Stack2;
PartSt:=nil;
while(TmpSt1 <> nil)and(not Found)do
  begin
  if TmpSt1^.Val = Val then
    begin
    Found:=True;
    PartSt:=TmpSt1;
    end
  else
    begin
    AddNew(ResStack,TmpSt1^.Val,ResStack);
    TmpSt1:=TmpSt1^.Next;
    end;
  end;

SearchItem:=Found;
if not Found then
  Exit;

while(TmpSt2 <> nil)do
  begin
  AddNew(ResStack,TmpSt2^.Val,ResStack);
  TmpSt2:=TmpSt2^.Next;
  end;

while(PartSt <> nil)do
  begin
  AddNew(ResStack,PartSt^.Val,ResStack);
  PartSt:=PartSt^.Next;
  end;
end;

begin
ClrScr;
Stack1:=nil;
Stack2:=nil;

WriteLn('> Input Stack#1 (Press Esc to end)');
InputStack(Stack1);
WriteLn;
WriteLn('> Input Stack#2 (Press Esc to end)');
InputStack(Stack2);
WriteLn;

WriteLn('> Enter number:');
ReadLn(SearchVal);
WriteLn;

if SearchItem(SearchVal,ResultStack,ResultStack2) then
  begin
  WriteLn('> Print ResultStack');
  PrintStack(ResultStack);
  end
else
  WriteLn('> Error. Nothing Was Found');

WriteLn;

WriteLn('> Press Any Key To Exit');
ReadKey;
end.
