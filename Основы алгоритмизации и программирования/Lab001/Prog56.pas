//56
type TPolynome = Record
                Free, Fir, Sec, Thr, Four: Byte;
                end;
                
var ArPol: Array [1..10] of TPolynome;
    Pol, ResPol: TPolynome;
    i, Count: Byte;

begin
Write('���-�� ��-� = ');
ReadLn(Count);
for i:= 1 to Count do
  begin
  WriteLn(i,':');
  Write('x^4 * ');
  ReadLn(Pol.Fir);
  Write('x^3 * ');
  ReadLn(Pol.Sec);
  Write('x^2 * ');
  ReadLn(Pol.Thr);
  Write('x * ');
  ReadLn(Pol.Four);
  Write('����:');
  ReadLn(Pol.Free);
  ArPol[i]:=Pol;
  end;

ResPol.Fir:=0;
ResPol.Sec:=0;
ResPol.Thr:=0;
ResPol.Four:=0;
ResPol.Free:=0;
for i:= 1 to Count do
  begin
  ResPol.Fir:=ResPol.Fir + ArPol[i].Fir;
  ResPol.Sec:=ResPol.Sec + ArPol[i].Sec;
  ResPol.Thr:=ResPol.Thr + ArPol[i].Thr;
  ResPol.Four:=ResPol.Four + ArPol[i].Four;
  ResPol.Free:=ResPol.Free + ArPol[i].Free;
  end;
  
WriteLn(ResPol.Fir,'*x^4 + ',ResPol.Sec,'*x^3 + ',ResPol.Thr,'*x^2 + ',ResPol.Four,'*x + ',ResPol.Free);
end.
