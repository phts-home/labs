type
  TDay = 1..7;
  TItem = Record
          FlightNo: Integer;
          Dep: String[20];
          Dest: String[10];
          Day: TDay;
          end;
const
  Count = 5;
var
  List: Array [1..Count] of TItem;
  Item: TItem;
  i, Method: Integer;
  
procedure LinearSearch;
var Found: Boolean;
begin
WriteLn('> LinearSearch Method');
WriteLn('> Printing...');
Found:=False;
for i:=1 to Count do
  begin
  Item:=List[i];
  if Item.FlightNo > 50 then
    begin
    Found:=True;
    WriteLn('FlightNo = ',Item.FlightNo,'; Departure = ',Item.Dep,
      '; Destination = ',Item.Dest,'; Day = ',Item.Day);
    end;
  end;
if not Found then
  WriteLn('> Nothing Was Found');
WriteLn;
end;

procedure BinarySearch;
var
  First, Last, Cur: Integer;
begin
WriteLn('> BinarySearch Method');
WriteLn('> Printing...');
First:=1;
Last:=Count;
while First <= Last do
  begin
  Cur:=(First+Last)div 2;
  Item:=List[Cur];
  if Item.FlightNo = 50 then
    Break
  else
    if Item.FlightNo > 50 then
      Last:=Cur-1
    else
      First:=Cur+1;
  end;
  
if List[Cur].FlightNo < 50 then
  Cur:=Cur+2
else
  if List[Cur].FlightNo = 50 then
    Inc(Cur);

for i:=Cur to Count do
  begin
  Item:=List[i];
  WriteLn('FlightNo = ',Item.FlightNo,'; Departure = ',Item.Dep,
      '; Destination = ',Item.Dest,'; Day = ',Item.Day);
  end;
end;

begin
{ Initialization }
WriteLn('> Enter The List');
for i:=1 to Count do
  begin
  WriteLn('> #',i);
  Write('> FlightNo = ');
  ReadLn(Item.FlightNo);
  Write('> Departure = ');
  ReadLn(Item.Dep);
  Write('> Destination = ');
  ReadLn(Item.Dest);
  Write('> Day = ');
  ReadLn(Item.Day);
  List[i]:=Item;
  end;
WriteLn;
WriteLn('> Set Method Of Searhing (1 - Liniar, 2 - Binary)');
Write('> ');
ReadLn(Method);
WriteLn;

case Method of
  1: LinearSearch;
  2: BinarySearch;
  end;
end.
