var Ar: Array [1..32,1..33] of Integer;
    RowCount, ColCount, i, j, Sum, TempVal: Integer;
    Sorted: Boolean;

begin
{ Initialization }
WriteLn('> Enter RowCount [1..32]');
Write('> ');
ReadLn(RowCount);
WriteLn;
WriteLn('> Enter ColCount [1..32]');
Write('> ');
ReadLn(ColCount);
WriteLn;
WriteLn('> Enter Elements Of Array');
for i:=1 to RowCount do
  begin
  WriteLn('> Row#',i);
  for j:=1 to ColCount do
    begin
    Write('> ');
    ReadLn(Ar[i,j]);
    end;
  end;
WriteLn;

{ Calculate }
for i:=1 to RowCount do
  begin
  Sum:=0;
  for j:=1 to ColCount do
    begin
    Sum:=Sum + Ar[i,j];
    end;
  Ar[i,ColCount+1]:=Sum;
  end;
  
{ Sort }
repeat
Sorted:=True;
for i:=1 to RowCount-1 do
  if Ar[i,ColCount+1] > Ar[i+1,ColCount+1] then
    begin
    for j:=1 to ColCount+1 do
      begin
      TempVal:=Ar[i,j];
      Ar[i,j]:=Ar[i+1,j];
      Ar[i+1,j]:=TempVal;
      end;
    Sorted:=False;
    end;
until Sorted;

{ Print }
WriteLn('> Sorted Array');
for i:=1 to RowCount do
  begin
  Write('> Row#',i,' (sum = ',Ar[i,ColCount+1],')   ');
  for j:=1 to ColCount do
    begin
    Write(Ar[i,j],' ');
    end;
  WriteLn;
  end;
WriteLn;
end.
