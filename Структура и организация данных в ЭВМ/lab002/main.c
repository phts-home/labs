/* 
 * ������������ ������ �2
 * 
 * �������:  ��������� � ����������� ������ � ���
 * ����:     ������������ ��������� ������. ����������� ������ � ��������� 
 *           ���������
 * 
 * �������:  25
 * �������:  �������� ��������� ���������� ������ �� �����������
 * 
 * ��������: ������� ��. 06��-1 
 *           ����� ������
 * 
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>



typedef struct _TListItem {
   int data;
   struct _TListItem *next;
} TListItem;
typedef struct {
   int dir;
   TListItem *head;
   TListItem *cur;
} TList;



int initList(TList *);
int printList(TList *);
int sortList(TList *);

int addItem(TList *, int);



int main(int argc, char *argv[])
{
	TList *list;
	initList(list);

	addItem(list, 1);
	addItem(list, 4);
	addItem(list, 100);
	addItem(list, 10);
	addItem(list, 6);
	addItem(list, 8);

	printList(list);
	sortList(list);
	printList(list);

	return 0;
}

/**
 * Initialises the specefied list
 */
int initList(TList *src)
{
	src->cur = NULL;
	src->head = NULL;
	return 0;
}

/**
 * Print the specefied list
 */
int printList(TList *src)
{
	printf("Printing items...\n");
	if (!src->head) {
		printf("<No items>\n");
		printf("Done\n\n");
		return 0;
	}
	src->cur = src->head;
	while (src->cur) {
		printf("%d\n",src->cur->data);
		src->cur = src->cur->next;
	}
	src->cur = src->head;
	while (src->cur->next) {
		src->cur = src->cur->next;
	}
	printf("Done\n\n");
	return 0;
}

/**
 * Sorts the specefied list
 */
int sortList(TList *src)
{
	printf("Sorting list...");
	if (!src->head) return 1;
	int canExit = 0;
	while (!canExit) {
		canExit = 1;
		src->cur = src->head;
		while (src->cur->next) {
			if (src->cur->data > src->cur->next->data) {
				int d = src->cur->data;
				src->cur->data = src->cur->next->data;
				src->cur->next->data = d;
				canExit = 0;
			} else src->cur = src->cur->next;
		}
	}
	printf("Done\n\n");
	return 0;
}

/**
 * Adds item with the specefied data to the specefied list
 */
int addItem(TList *src, int data)
{
	TListItem *tmp = (TListItem *)malloc(sizeof(TListItem));
	if (!tmp) return 1;
	if (!src->head) {
		src->head = tmp;
		tmp->next = NULL;
	} else {
		if (!src->cur)
			src->cur = src->head;
		while (src->cur->next) 
			src->cur = src->cur->next;
		src->cur->next = tmp;
		tmp->next = NULL;
	}
	tmp->data = data;
	src->cur = tmp;
	return 0;
}
