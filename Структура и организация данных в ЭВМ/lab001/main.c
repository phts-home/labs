/* 
 * ������������ ������ �1
 * 
 * �������:  ��������� � ����������� ������ � ���
 * ����:     ������. ������� �������
 * 
 * �������:  15
 * �������:  � ��������������� ��������� ����� �������� �������� � ��������� 
 *           (�������, ����� ������, ���� ��������), �������� � �������� 
 *           (�������, ����� �������� ������, ������ �� �������). ���������� 
 *           ������� ������� �� ����� �����������, ������ �� ������: �������, 
 *           ����� ������. ���������� ������� ����� ������� �������� 11.06.90 
 *           � �����, ��������� ������ ���������, ������� �����������: 
 *           �������, ����� ������, ������
 * 
 * ��������: ������� ��. 06��-1 
 *           ����� ������
 * 
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>



typedef struct {
	char lastName[20];
	int num;
	int mark;
} Student;

typedef struct  {
	char subject[20];
	int group;
	char date[20];
	Student student[3];
} Document;


int main() {
	// initialization
	int docNum = 2;
	Document document[docNum];

	strcpy(document[0].date, "11.06.90");
	strcpy(document[0].subject, "Inf");
	document[0].group = 1;

	strcpy(document[0].student[0].lastName, "Ivanov");
	strcpy(document[0].student[1].lastName, "Petrov");
	strcpy(document[0].student[2].lastName, "Sidorov");

	document[0].student[0].mark = 3;
	document[0].student[1].mark = 2;
	document[0].student[2].mark = 4;

	document[0].student[0].num = 111111;
	document[0].student[1].num = 222222;
	document[0].student[2].num = 333333;


	strcpy(document[1].date, "12.06.90");
	strcpy(document[1].subject, "jkhgutdr");
	document[1].group = 2;
	
	strcpy(document[1].student[0].lastName, "qwerty");
	strcpy(document[1].student[1].lastName, "asdfghj");
	strcpy(document[1].student[2].lastName, "zxxvvnm");

	document[1].student[0].mark = 5;
	document[1].student[1].mark = 4;
	document[1].student[2].mark = 3;

	document[1].student[0].num = 444444;
	document[1].student[1].num = 555555;
	document[1].student[2].num = 666666;
	
	// �� ����� �����������
	printf("\nTask #1\n");
	for (int i = 0; i < docNum; i++) {
		if (strcmp(document[i].subject, "Inf") == 0) {
			for (int st = 0; st < 3; st++) {
				if (document[i].student[st].mark < 3) {
					printf("%s  %d  %d\n",document[i].student[st].lastName, document[i].student[st].num, document[i].student[st].mark);
				}
			}
		}
	}

	// ������� 11.06.90
	printf("\nTask #2\n");
	for (int i = 0; i < docNum; i++) {
		if (strcmp(document[i].date, "11.06.90") == 0) {
			printf("%d\n", document[i].group);
		}
	}

	// ������� �����������
	printf("\nTask #3\n");
	for (int i = 0; i < docNum; i++) {
		if (strcmp(document[i].subject, "Inf") == 0) {
			for (int st = 0; st < 3; st++) {
				printf("%s  %d  %d\n",document[i].student[st].lastName, document[i].student[st].num, document[i].student[st].mark);
			}
		}
	}

	return 0;
}
