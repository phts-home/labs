#include "tree.h"

TREENODE *createTreeNode(int value) {
	TREENODE *src = malloc(sizeof(TREENODE));
	if (!src) {
		printf("Error: Unable to create tree");
		return NULL;
	}
	src->value = value;
	src->right = src->left = NULL;
	return src;
}

void printTree(TREENODE *src) {
	if (src == NULL) return;
	printTree(src->left);
	printf("%d ",src->value);
	printTree(src->right);
}

int findMax(TREENODE *src) {
	sortTree(src);
	while (src->right){
		src = src->right;
	}
	return src->value;
}

void sortTree(TREENODE *src) {
	wl = 1;
	st = 0;
	k = 0;
	TREENODE *root = src;
	while (wl) {
		wl = 0;
		st = 1;
		while (st) {
			st = 0;
			sortItems(src);
		}
		sortLeftSubTree(src->left, root);
		sortRightSubTree(src->right, root);
	}
}

void sortItems(TREENODE *src) {
	if(src == NULL) return;
	if (src->left) {
		if (src->left->value > src->value) {
			k = src->left->value;
			src->left->value = src->value;
			src->value = k;
			st = 1;
		}
	}
	if (src->right) {
		if (src->right->value < src->value) {
			k = src->right->value;
			src->right->value = src->value;
			src->value = k;
			st = 1;
		}
	}
}

void sortLeftSubTree(TREENODE *src, TREENODE *root) {
	if(src == NULL) return;
	if(src->left){
		if((src->left->value > src->value)||(src->left->value > root->value)){
			k = src->left->value;
			src->left->value = src->value;
			src->value = k;
			wl = 1;
		}
		sortLeftSubTree(src->left, root);
	}
	if(src->right){
		if((src->right->value < src->value)||(src->right->value > root->value)){
			k = src->right->value;
			src->right->value = src->value;
			src->value = k;
			wl = 1;
		}
		sortLeftSubTree(src->right, root);
	}
}

void sortRightSubTree(TREENODE *src, TREENODE *root) {
	if(src == NULL) return;
	if(src->left){
		if((src->left->value > src->value)||(src->left->value < root->value)){
			k = src->left->value;
			src->left->value = src->value;
			src->value = k;
			wl = 1;
		}
		sortRightSubTree(src->left, root);
	}
	if(src->right){
		if((src->right->value < src->value)||(src->right->value < root->value)){
			k = src->right->value;
			src->right->value = src->value;
			src->value = k;
			wl = 1;
		}
		sortRightSubTree(src->right, root);
	}
}
