#ifndef TREE_H
#define TREE_H


#include <stdio.h>
#include <stdlib.h>


typedef struct _TREENODE{
	int value;
	struct _TREENODE *left;
	struct _TREENODE *right;
} TREENODE;

int k;
int st, wl;

TREENODE *createTreeNode(int);
void printTree(TREENODE *);
int findMax(TREENODE *);
void sortTree(TREENODE *);
void sortItems(TREENODE *);
void sortLeftSubTree(TREENODE *, TREENODE *);
void sortRightSubTree(TREENODE *, TREENODE *);



#endif
