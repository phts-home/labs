/* 
 * ������������ ������ �5
 * 
 * �������:  ��������� � ����������� ������ � ���
 * ����:     ���������� � ����� � ��������
 * 
 * �������:  3
 * �������:  ����������� ����� ������������� �������� ������
 * 
 * ��������: ������� ��. 06��-1 
 *           ����� ������
 * 
 */

#include <stdio.h>

#include "tree.h"

TREENODE *root;

int main(int argc, char *argv[]) {
	TREENODE *tmp, *treeNode;
	tmp = createTreeNode(2000);
	root = tmp;
	tmp = createTreeNode(3000);
	root->left = tmp;
	tmp = createTreeNode(678);
	root->right = tmp;
	treeNode = root->left;
	tmp = createTreeNode(78);
	treeNode->left = tmp;
	treeNode = root->right;
	tmp = createTreeNode(900);
	treeNode->right = tmp;
	treeNode = treeNode->right;
	tmp = createTreeNode(5);
	treeNode->left = tmp;
	tmp = createTreeNode(76);
	treeNode->right = tmp;
	treeNode = treeNode->left;
	tmp = createTreeNode(1000);
	treeNode->left = tmp;

	printTree(root);

	printf("\n\n%d\n", findMax(root));
	return 0;
}

