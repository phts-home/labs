#ifndef DEQ_H
#define DEQ_H

#include <stdlib.h>

typedef struct _TDeqItem {
	int data;
	struct _TDeqItem *left;
	struct _TDeqItem *right;
} TDeqItem;
typedef struct {
	TDeqItem *leftHead;
	TDeqItem *rightHead;
	TDeqItem *cur;
	int size;
} TDeq;


TDeq *createDeq(int);
int writeRight(TDeq *, int);
int readRight(TDeq *);
int writeLeft(TDeq *, int);
int readLeft(TDeq *);
int deqSize(TDeq *);
void printDeq(TDeq *);


#endif
