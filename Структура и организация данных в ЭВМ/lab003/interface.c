#include "interface.h"

int drwMenu(TDeq *src)
{
	int ch, canExit = 0;
	while (!canExit) {
		printf("--- Menu ---\n");
		printf("1 - writeRight\n");
		printf("2 - readRight\n");
		printf("3 - writeLeft\n");
		printf("4 - readLeft\n");
		printf("5 - printDeq\n");
		printf("6 - deqSize\n");
		printf("0 - Exit\n");
		printf("---------------------\n");
		printf("> Your Choice   ");
		scanf("%d",&ch);
		printf("\n");
		switch(ch){
			case 1:{
				execWriteRight(src);
				break;
			}
			case 2:{
				execReadRight(src);
				break;
			}
			case 3:{
				execWriteLeft(src);
				break;
			}
			case 4:{
				execReadLeft(src);
				break;
			}
			case 5:{
				printDeq(src);
				break;
			}
			case 6:{
				execDeqSize(src);
				break;
			}
			case 0:{
				canExit = 1;
				break;
			}
		}
	}
	return 0;
}

void execWriteRight(TDeq *src) {
	printf("Enter data to write to right:");
	int data;
	scanf("%d", &data);
	writeRight(src, data);
	printf("\n");
}

void execWriteLeft(TDeq *src) {
	printf("Enter data to write to left:");
	int data;
	scanf("%d", &data);
	writeLeft(src, data);
	printf("\n");
}

void execReadRight(TDeq *src) {
	printf("Read from right: %d\n\n", readRight(src));
}

void execReadLeft(TDeq *src) {
	printf("Read from left: %d\n\n", readLeft(src));
}

void execDeqSize(TDeq *src) {
	printf("Deq size: %d\n\n", deqSize(src));
}
