#include "deq.h"

TDeq *createDeq(int size) {
	TDeq *src = (TDeq *)malloc(sizeof(TDeq));

	src->leftHead = NULL;
	src->rightHead = NULL;
	src->cur = NULL;

	for (int i = 0; i < size; i++) {
		writeRight(src, 0);
	}
	
	return src;
}

int writeRight(TDeq * src, int data) {
	TDeqItem *tmp = (TDeqItem *)malloc(sizeof(TDeqItem));
	if (!tmp) return 1;
	if (src->size == 0) {
		src->leftHead = tmp;
		src->rightHead = tmp;
		tmp->left = NULL;
		tmp->right = NULL;
	} else {
		TDeqItem *item = src->rightHead;
		item->right = tmp;
		tmp->right = NULL;
		tmp->left = item;
		src->rightHead = tmp;
	}
	src->size++;
	tmp->data = data;
	src->cur = tmp;
	return 0;
}

int readRight(TDeq *src) {
	if (!src) {
		return -1;
	}
	if (!src->rightHead) {
		return -2;
	}
	int data = src->rightHead->data;
	TDeqItem *item = src->rightHead->left;
	item->right = NULL;

	free(src->rightHead);
	src->size--;

	src->rightHead = item;
	return data;
}

int writeLeft(TDeq *src, int data) {
	TDeqItem *tmp = (TDeqItem *)malloc(sizeof(TDeqItem));
	if (!tmp) return 1;
	if (src->size == 0) {
		src->leftHead = tmp;
		src->rightHead = tmp;
		tmp->left = NULL;
		tmp->right = NULL;
	} else {
		TDeqItem *item = src->leftHead;
		item->left = tmp;
		tmp->left = NULL;
		tmp->right = item;
		src->leftHead = tmp;
	}
	src->size++;
	tmp->data = data;
	src->cur = tmp;
	return 0;
}

int readLeft(TDeq *src) {
	if (!src) {
		return -1;
	}
	if (!src->leftHead) {
		return -2;
	}
	int data = src->leftHead->data;
	TDeqItem *item = src->leftHead->right;
	item->left = NULL;

	free(src->leftHead);
	src->size--;

	src->leftHead = item;
	return data;
}

int deqSize(TDeq *src) {
	if (!src) {
		return -1;
	}
	return src->size;
}

void printDeq(TDeq *src) {
	TDeqItem *item = src->leftHead;
	while (item) {
		printf("%d  ", item->data);
		item = item->right;
	}
	printf("\n");
}
