#include "tree.h"

/**
 * Creates a tree
 */
TTree *createTree(void) {
	TTree *tree;
	tree->root = NULL;
	return tree;
}

/**
 * Deletes the specefied tree
 */
int destroyTree(TTree *src) {
	deleteElement(src->root);
	src->root = NULL;
	return 0;
}

/**
 * Deletes the specefied element and all its children
 */
int deleteElement(TTreeElem *elem) {
	if(!elem) return 0;
	if(elem->left) delElement(elem->left);
	if(elem->right) delElement(elem-> right);
	if(elem->parent){
		if(elem->parent->left == elem)
			elem->parent->left = NULL;
		else 
			elem->parent->right = NULL;
	}
	free(elem);
	return 0;
}

/**
 * Gets a parent element of the specefied one
 */
TTreeElem *getParent(const TTreeElem *elem) {
	if(!elem) return NULL;
	return elem->parent;
}

/**
 * Gets the left element of the specefied one
 */
TTreeElem *getLeftChild(const TTreeElem *elem) {
	if(!elem) return NULL;
	return elem->left;
}

/**
 * Gets the right element of the specefied one
 */
TTreeElem *moveRight(const TTreeElem *elem) {
	if(!elem) return NULL;
	return elem->right;
}

/**
 * Adds the left child
 */
int addLeft(TTreeElem *elem, int data) {
	if(!elem || elem->left) return 1;
	tTreeElem *tmp = (tTreeElem *)malloc(sizeof(tTreeElem));
	if(!tmp) return 1;
	tmp->left = NULL;
	tmp->right = NULL;
	tmp->data = data;
	tmp->parent = elem;
	elem->left = tmp;
	return 0;
}

/**
 * Adds the right child
 */
int addRight(TTreeElem *elem, int data) {
	if(!elem || elem->right) return 1;
	tTreeElem *tmp = (tTreeElem *)malloc(sizeof(tTreeElem));
	if(!tmp) return 1;
	tmp->left = NULL;
	tmp->right = NULL;
	tmp->data = data;
	tmp->parent = elem;
	elem->right = tmp;
	return 0;
}
