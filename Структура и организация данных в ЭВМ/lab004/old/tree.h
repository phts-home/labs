#ifndef TREE_H
#define TREE_H

#include <stdlib.h>

typedef struct _TTreeItem {
	int data;
	struct _TTreeItem *leftChild;
	struct _TTreeItem *rightChild;
	struct _TTreeItem *parent;
} TTreeItem;
typedef struct {
	TTreeItem *root;
	TTreeItem *cur;
} TTree;


TTree *createTree(void);
int destroyTree(TTree *);
int deleteElement(TTreeItem *);

TTreeItem *getParent(const TTreeItem *);
TTreeItem *getLeftChild(const TTreeItem *);
TTreeItem *getRightChild(const TTreeItem *);

int addLeft(TTreeItem *, int);
int addRight(TTreeItem *, int);


#endif
