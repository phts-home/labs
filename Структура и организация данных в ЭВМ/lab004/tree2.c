#include "tree2.h"


TREE *createTreeItem_2(int data) {
	TREE *src = malloc(sizeof(TREE));
	if (!src) {
		printf("Error: Unable to create list\n\n");
		return NULL;
	}
	src->data = data;
	src->right = src->left = NULL;
	return src;
}

void initTree_2(void) {
	/*
                 _5_
	            |   |
               _7  _1_
	          |   |   |
             _3_  4  _8
	        |   |   |
            2   6  10_
	                  |
                      9
	*/
	TREE *tmp, *ttree;
	tmp = createTreeItem_2(5);
	tree_2 = tmp;
	tmp = createTreeItem_2(7);
	tree_2->left = tmp;
	tmp = createTreeItem_2(1);
	tree_2->right = tmp;
	ttree = tree_2->left;
	tmp = createTreeItem_2(3);
	ttree->left = tmp;
	ttree = ttree->left;
	tmp = createTreeItem_2(2);
	ttree->left = tmp;
	tmp = createTreeItem_2(6);
	ttree->right = tmp;
	ttree = tree_2->right;
	tmp = createTreeItem_2(4);
	ttree->left = tmp;
	tmp = createTreeItem_2(8);
	ttree->right = tmp;
	ttree = ttree->right;
	tmp = createTreeItem_2(10);
	ttree->left = tmp;
	ttree = ttree->left;
	tmp = createTreeItem_2(9);
	ttree->right = tmp;
}

int subTreeDepth_2(TREE *src) {
	if (src == NULL) {
		return depth_2=0;
	}
	if (subTreeDepth_2(src->left) > subTreeDepth_2(src->right))
		subTreeDepth_2(src->left);
	else
		subTreeDepth_2(src->right);
	return depth_2++;

}
