#include "tree1.h"


void initTree_1(void) {
	for(int i=0; i<100; i++)
		tree_1[i] = 0;
	/*
                 __5_
                |    |
               _7_  _1_
              |   ||   |
             _3_ 134  _8
            |   |    |
           _2_  6   10_
          |   |        |
         20  20        9
	*/
	tree_1[0] = 5;
	tree_1[1] = 7;
	tree_1[2] = 1;
	tree_1[3] = 3;
	tree_1[4] = 13;
	tree_1[5] = 4;
	tree_1[6] = 8;
	tree_1[7] = 2;
	tree_1[8] = 6;
	tree_1[13] = 10;
	tree_1[15] = 20;
	tree_1[16] = 20;
	tree_1[28] = 9;
}

int subTreeDepth_1(int number) {
	if(tree_1[number] == 0){
		return depth_1=1;
	}
	if (subTreeDepth_1(number*2+1) > subTreeDepth_1(number*2+2) )
		subTreeDepth_1(number*2+1);
	else
		subTreeDepth_1(number*2+2);
	return depth_1++;

}
