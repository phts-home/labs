#ifndef TREE2_H
#define TREE2_H


#include <stdio.h>
#include <stdlib.h>


typedef struct _TREE {
	int data;
	struct _TREE *left;
	struct _TREE *right;
} TREE;

TREE *tree_2;
int depth_2;


TREE *createTreeItem_2(int);
void initTree_2(void);
int subTreeDepth_2(TREE *);
void printTree_2(TREE *);



#endif
