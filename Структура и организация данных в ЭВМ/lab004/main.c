/* 
 * ������������ ������ �4
 * 
 * �������:  ��������� � ����������� ������ � ���
 * ����:     �������� �������
 * 
 * �������:  6
 * �������:  ���������� ������� ������ ���������
 * 
 * ��������: ������� ��. 06��-1 
 *           ����� ������
 * 
 */

#include <stdlib.h>

#include "interface.h"
#include "tree1.h"
#include "tree2.h"


int main(int argc, char *argv[]) {
	// ������ 1
	initTree_1();
	printf("\n\n");
	printf("depth: %d\n", subTreeDepth_1(1)+1);

	// ������ 2
	initTree_2();
	printf("\n");
	printf("depth: %d\n", subTreeDepth_2(tree_2->left)+2);
	return 0;
}

