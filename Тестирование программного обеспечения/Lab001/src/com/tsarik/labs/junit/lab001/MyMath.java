package com.tsarik.labs.junit.lab001;

public class MyMath {
	
	public static double sum(double... args) {
		if (args == null || args.length == 0) {
			return 0;
		}
		
		double r = 0;
		for (int i = 0; i < args.length; i++) {
			r += args[i];
		}
		return r;
	}
	
	public static double substract(double a, double b) {
		return a - b;
	}
	
	public static double multiply(double... args) {
		if (args == null || args.length == 0) {
			return 0;
		}
		
		double r = 1;
		for (int i = 0; i < args.length; i++) {
			r *= args[i];
		}
		return r;
	}
	
	public static double divide(double a, double b) {
		if (b == 0) {
			throw new NullPointerException("Division by zero (b == 0)");
		}
		return a / b;
	}
	
	public static double sin(double a) {
		return Math.sin(a);
	}
	
	public static double cos(double a) {
		return Math.cos(a);
	}
	
	public static double atan(double a) {
		return Math.atan(a);
	}
	
	/**
	 * 
	 * @param a
	 * @param b >= 0
	 * @return a ^ b
	 */
	public static double pow(double a, int b) throws IllegalArgumentException {
		if (b < 0) {
			throw new IllegalArgumentException("b < 0");
		}
		double r = 1;
		for (int i = 0; i < b; i++) {
			r *= a;
		}
		return r;
	}
	
	public static double abs(double a) {
		return Math.abs(a);
	}
	
}
