package com.tsarik.labs.junit.lab001;

public class Expressions {
	
	public double getA(double x, double y, double z) {
		double a1 = MyMath.sum(1, MyMath.pow(MyMath.cos(MyMath.sum(x,y)), 2));
		double a2 = MyMath.sum(2, MyMath.abs(MyMath.substract(x, MyMath.divide(MyMath.multiply(2,x),(MyMath.sum(1, MyMath.multiply(x,x,z,z)))))));
		return MyMath.sum(MyMath.divide(a1, a2), x);
	}
	
	public double getB(double x, double y, double z) {
		return MyMath.pow(MyMath.sin(MyMath.atan(1/x)), 2); 
	}
	
}
