package com.tsarik.labs.junit.lab001;


public class Lab001 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Expressions exp = new Expressions();
		
		System.out.println("a(1,1,1) = " + exp.getA(1, 1, 1));
		System.out.println("b(1,1,1) = " + exp.getB(1, 1, 1));
		
		//runTests();
	}
	/*
	public static void runTests() {
		TestRunner runner = new TestRunner();
		TestSuite suite = new TestSuite();
		suite.addTest(new MyMathTest("testSum"));
		runner.doRun(suite);
	}*/

}
