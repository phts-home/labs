package com.tsarik.labs.junit.lab001;

import static org.junit.Assert.*;

import org.junit.Test;

public class MyMathTest {
	
	
	@Test
	public void testSum() {
		assertTrue(MyMath.sum(1,2,3) == 6);
		assertTrue(MyMath.sum() == 0);
	}
	
	@Test
	public void testSubstract() {
		assertTrue(MyMath.substract(2, 1) == 1);
	}

	@Test
	public void testMultiply() {
		assertTrue(MyMath.multiply(1,2,3) == 6);
		assertTrue(MyMath.multiply() == 0);
	}

	@Test
	public void testDivide() {
		assertTrue(MyMath.divide(4, 2) == 2);
	}
	
	@Test (expected = NullPointerException.class)
	public void testDivideException() {
		MyMath.divide(4, 0);
	}

	@Test
	public void testSin() {
		assertTrue(MyMath.sin(0) == 0);
	}

	@Test
	public void testCos() {
		assertTrue(MyMath.cos(0) == 1);
	}

	@Test
	public void testAtan() {
		assertTrue(MyMath.atan(0) == 0);
	}

	@Test
	public void testPow() {
		assertTrue(MyMath.pow(2, 2) == 4);
	}
	
	@Test (expected = IllegalArgumentException.class)
	public void testPowException() {
		MyMath.pow(2, -1);
	}

	@Test
	public void testAbs() {
		assertTrue(MyMath.abs(-2) == 2);
	}
	
}
