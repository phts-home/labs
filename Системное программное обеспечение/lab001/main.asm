.386
.model Flat, STDCALL
option casemap:none


includelib D:\masm32\lib\kernel32.lib



.DATA
ExitProcess PROTO uExitCode:DWORD
WriteConsoleA PROTO :DWORD, :DWORD, :DWORD, :DWORD, :DWORD
SetConsoleTitleA PROTO :DWORD
GetStdHandle PROTO :DWORD
Sleep PROTO :DWORD

.DATA?
handle DD ?

.CONST
message DB "Hello World", 0
head DB "lab001", 0

.CODE
Start:
	push offset head
	call SetConsoleTitleA

	push -11
	call GetStdHandle

	mov handle, eax

	push 0
	push 0
	push 11
	push offset message
	push handle
	call WriteConsoleA
	
	push 500
	call Sleep

	push 0
	call ExitProcess

	END Start