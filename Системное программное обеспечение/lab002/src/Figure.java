import java.util.*;

public class Figure {
	
	public Figure(String type, List <Integer> coords) {
		this.type = type;
		this.coords = coords;
	}
	
	public String getType() {
		return type;
	}
	
	public List <Integer> getCoordinates() {
		return coords;
	}
	
	private String type;
	private List <Integer> coords;
	
}
