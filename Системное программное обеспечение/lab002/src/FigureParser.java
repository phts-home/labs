import java.io.*;
import java.util.*;


public class FigureParser {
	
	public FigureParser() {
		String s = new String ("123");
		System.out.println(s.substring(1, 2));
		
		List <String> list = readFromFile("lab002-in.txt");
		List <Figure> figures = parse(list);
		for (int i = 0; i < figures.size(); i++) {
			//System.out.println(figures.get(i).getType() + "(" + figures.get(i).getCoordinates().get(0) + ", ...)");
			System.out.println(figures.get(i).getType());
			printList(figures.get(i).getCoordinates());
		}
	}
	
	private List <String> readFromFile(String path) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(path));
			List <String> list = new ArrayList <String> ();
			String line = new String();
			while ((line = reader.readLine()) != null) {
				list.add(line);
			}
			return list;
		}
		catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	private List <Figure> parse(List <String> list) {
		List <Figure> figures = new ArrayList <Figure> ();
		for (currentLineNo = 0; currentLineNo < list.size(); currentLineNo++) {
			currentLine = list.get(currentLineNo);
			currentPos = 0;
			while (currentPos < currentLine.length()) {
				if ( (currentLine.charAt(currentPos)!=' ')&&(currentLine.charAt(currentPos)!='\t')&&(currentLine.charAt(currentPos)!='\n') ) {
					figures.add(parseFigure(list));
				}
				currentPos++;
			}
		}
		return figures;
	}
	
	private Figure parseFigure(List <String> list) {
		int p = currentPos;
		System.out.println("currentLine == " + currentLine);
		System.out.println("p == " + String.valueOf(p) + " ch == " + currentLine.charAt(p));
		// search for the figure's type
		while ( !isSpaceChar(currentLine.charAt(currentPos)) ) {
			if (currentPos+1 >= currentLine.length()) {
				break;
			}
			if (currentLine.charAt(currentPos)=='(') {
				break;
			}
			currentPos++;
		}
		System.out.println("currentPos == " + String.valueOf(currentPos) + " ch == " + currentLine.charAt(currentPos));
		String figureType = null;
		if ( (currentPos+1 == currentLine.length()) && (!isSpaceChar(currentLine.charAt(currentPos))) && (currentLine.charAt(currentPos)!=',') && (currentLine.charAt(currentPos)!=')') ) {
			figureType = currentLine.substring(p);
		} else {
			figureType = currentLine.substring(p, currentPos);
		}
		System.out.println("figureType == " + figureType);
		
		// search for the figure's coordinates
		List <Integer> figureCoords = new ArrayList <Integer> ();
		while ( (isSpaceChar(currentLine.charAt(currentPos))) && (currentLine.charAt(currentPos) != '(') ) {
			currentPos++;
			if (currentPos >= currentLine.length()) {
				currentPos = 0;
				currentLineNo++;
				currentLine = list.get(currentLineNo);
			}
		}
		currentPos++;
		if (currentPos >= currentLine.length()) {
			currentPos = 0;
			currentLineNo++;
			currentLine = list.get(currentLineNo);
		}
		while (true) {
			while ( isSpaceChar(currentLine.charAt(currentPos)) || (currentLine.charAt(currentPos)==',') || (currentLine.charAt(currentPos)=='(') ) {
				currentPos++;
				if (currentPos >= currentLine.length()) {
					currentPos = 0;
					currentLineNo++;
					currentLine = list.get(currentLineNo);
				}
			}
			System.out.println("*currentPos == " + String.valueOf(currentPos) + " ch == " + currentLine.charAt(currentPos) + " currentLine == " + currentLine);
			p = currentPos;
			while ( (!isSpaceChar(currentLine.charAt(currentPos))) && (currentLine.charAt(currentPos)!=',') && (currentLine.charAt(currentPos)!=')') ) {
				if (currentPos+1 >= currentLine.length()) {
					break;
				}
				currentPos++;
			}
			System.out.println("**currentPos == " + String.valueOf(currentPos) + " ch == " + currentLine.charAt(currentPos));
			if ( (currentPos+1 == currentLine.length()) && (!isSpaceChar(currentLine.charAt(currentPos))) && (currentLine.charAt(currentPos)!=',') && (currentLine.charAt(currentPos)!=')') ) {
				figureCoords.add(Integer.valueOf(currentLine.substring(p)));
			} else {
				figureCoords.add(Integer.valueOf(currentLine.substring(p, currentPos)));
			}
			
			//currentPos++;
			while ( isSpaceChar(currentLine.charAt(currentPos)) ) {
				currentPos++;
				if (currentPos >= currentLine.length()) {
					currentPos = 0;
					currentLineNo++;
					currentLine = list.get(currentLineNo);
				}
			}
			System.out.println("***currentPos == " + String.valueOf(currentPos) + " ch == " + currentLine.charAt(currentPos));
			if (currentLine.charAt(currentPos) == ')') {
				break;
			}
			currentPos++;
			if (currentPos >= currentLine.length()) {
				currentPos = 0;
				currentLineNo++;
				currentLine = list.get(currentLineNo);
			}
		}
		
		return new Figure(figureType, figureCoords);
	}
	
	private boolean isSpaceChar(char c) {
		return (c==' ')||(c=='\t')||(c=='\n');
	}
	
	private void printList(List <Integer> list) {
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i));
		}
	}
	
	private int currentPos;
	private String currentLine;
	private int currentLineNo;
	
}
