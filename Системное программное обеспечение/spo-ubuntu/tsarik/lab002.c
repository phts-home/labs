#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

int main()
{
	int fin, fout;
	char buf[1];
	
	if ((fin = open("lab002.in", O_RDONLY)) == -1) {
		perror("open error");
		_exit(-1);
	}

	if ((fout = open("lab002.out", O_WRONLY|O_CREAT, 644)) == -1) {
		perror("open error");
		_exit(-1);
	}

	int nw = 0;
	int count = read(fin,buf,1);
	int written = 0;
	while (1) {
		printf("buf[0] == %c\n",buf[0]);
		while ( (count > 0) && ((buf[0] == ' ')||(buf[0] == '\t')||(buf[0] == '\n')||(buf[0] == 0)) ) {
			written = write(fout,buf,1);
			if (written != 1) {
				perror("write error");
				close(fin);
				close(fout);
				_exit(-3);
			}
			printf("space - %d\n",buf[0]);
			count = read(fin,buf,1);
		}
		if (count == 0) {
			break;
		}
		nw++;
		printf("nw == %d\n",nw);
		while ( (count > 0) && (buf[0] != ' ')&& (buf[0] != '\t') && (buf[0] != '\n') && (buf[0] != 0) ) {
			if (nw % 2 == 1) {
				written = write(fout,buf,1);
				if (written != 1) {
					perror("write error");
					close(fin);
					close(fout);
					_exit(-3);
				}
				printf("***");
			}
			printf("%c\n",buf[0]);
			count = read(fin,buf,1);
		}
		if (count == 0) {
			break;
		}
	}

	close(fin);
	close(fout);

	printf("done\n");
	_exit(0);
}
