#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <dirent.h>

int main()
{
	char *dirpath;
	
	dirpath = (char*)malloc(sizeof(char)*1000);
	printf("-> ");
	gets(dirpath);
	//strcpy(dirpath,"/home/user/tsarik");
	
	struct dirent *dir;
	DIR *dir_ds;
	
	if ( (dir_ds = opendir(dirpath)) == NULL ) {
		perror("unable to open directory");
		return 1;
	}
	
	while ( (dir = readdir(dir_ds)) != NULL ) {
		if ( !access(dir->d_name, F_OK | X_OK | R_OK) ) {
			if ( (strcmp(dir->d_name,".") != 0) && (strcmp(dir->d_name,"..") != 0) ) {
				printf("%s\n", dir->d_name);
			}
		}
	}
	closedir(dir_ds);
	
	free(dirpath);
	
	printf("\n-> done\n");
	_exit(0);
}
