(clear)


;----------------------------




(deftemplate db-band
	(slot name)
	(slot style)
)

(deftemplate db-drumkit
	(slot model)
	(slot drumkit-level)
	(slot style)
	(slot price)
)

(deftemplate found-model
	(slot model)
	(slot price)
)






;------------------


(deffacts bands
	(db-band (name Metallica) (style metal))
	(db-band (name Ayreon) (style metal))
	(db-band (name Therion) (style metal))
	(db-band (name Nightwish) (style metal))
	(db-band (name Slayer) (style metal))
	(db-band (name Deep_Purple) (style rock))
	(db-band (name Pink_Floyd) (style rock))
	(db-band (name Uriah_Heep) (style rock))
	(db-band (name Rainbow) (style rock))
	(db-band (name Led_Zeppelin) (style rock))
	(db-band (name Apple_Tea) (style jazz))
	(db-band (name Simply_Red) (style jazz))
	(db-band (name Fourplay) (style jazz))
	(db-band (name Material) (style jazz))
)







;------------------


(deffacts drumkits 
	(db-drumkit (model Sonor-SQ1) (drumkit-level sub-entry) (style rock) (price 900))
	(db-drumkit (model Sonor-SQ1) (drumkit-level sub-entry) (style metal) (price 900))
	(db-drumkit (model Sonor-SQ1) (drumkit-level sub-entry) (style jazz) (price 900))
	
	(db-drumkit (model Sonor-SQ2) (drumkit-level entry) (style rock) (price 1000))
	(db-drumkit (model Sonor-SQ2) (drumkit-level entry) (style metal) (price 1000))
	(db-drumkit (model Sonor-SQ2) (drumkit-level entry) (style jazz) (price 1000))
	
	(db-drumkit (model Sonor-Force_3007_Studio_1) (drumkit-level semi-pro) (style rock) (price 1500))
	(db-drumkit (model Sonor-Force_2007_Studio_1) (drumkit-level semi-pro) (style rock) (price 1300))
	(db-drumkit (model Sonor-Force_1007_Studio_1) (drumkit-level semi-pro) (style rock) (price 900))
	(db-drumkit (model Sonor-Force_1007_Studio_2) (drumkit-level semi-pro) (style rock) (price 950))
	
	(db-drumkit (model Sonor-Force_507_Studio_1) (drumkit-level student) (style rock) (price 800))
	(db-drumkit (model Sonor-Force_507_Studio_2) (drumkit-level student) (style rock) (price 850))
	(db-drumkit (model Mapex-BM6225A) (drumkit-level student) (style rock) (price 1500))
	(db-drumkit (model Mapex-BM6225A) (drumkit-level student) (style metal) (price 1500))
	(db-drumkit (model Mapex-BM5255A) (drumkit-level student) (style jazz) (price 1700))
	(db-drumkit (model Mapex-SW4815A) (drumkit-level student) (style rock) (price 2000))
	
	(db-drumkit (model Sonor-SC_Stage_1_Shell) (drumkit-level semi-pro) (style rock) (price 2000))
	(db-drumkit (model Sonor-SC_Stage_2_Shell) (drumkit-level semi-pro) (style rock) (price 2100))
	(db-drumkit (model Sonor-SC_Stage_3_Shell) (drumkit-level semi-pro) (style rock) (price 2200))
	
	(db-drumkit (model Mapex-SW6225A) (drumkit-level semi-pro) (style metal) (price 1600))
	(db-drumkit (model Mapex-PM6225A) (drumkit-level semi-pro) (style metal) (price 2100))
	(db-drumkit (model Premier-Spirit_of_Lily) (drumkit-level semi-pro) (style rock) (price 2500))
	(db-drumkit (model Premier-Maple_Traditional) (drumkit-level semi-pro) (style jazz) (price 2600))
	(db-drumkit (model Premier-Shell_Packs) (drumkit-level semi-pro) (style jazz) (price 2400))
	
	(db-drumkit (model Sonor-Force_3007_Studio_2) (drumkit-level pro) (style rock) (price 1600))
	(db-drumkit (model Sonor-Force_2007_Studio_2) (drumkit-level pro) (style rock) (price 1200))
	(db-drumkit (model Mapex-VX5255RA) (drumkit-level pro) (style rock) (price 2100))
	(db-drumkit (model Premier-Birch_Traditional) (drumkit-level pro) (style rock) (price 2700))
)

(reset)
