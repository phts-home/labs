

; ������ ������ ?question � ���������� ������� $?allowed-values
(deffunction ask-question (?question $?allowed-values)
	(printout t ?question)
	(bind ?answer (read))
	(if (lexemep ?answer)
		then
		(bind ?answer (lowcase ?answer))
	)
	(while (not (member ?answer ?allowed-values)) do
		(printout t ?question)
		(bind ?answer (read))
		(if (lexemep ?answer)
			then
			(bind ?answer (lowcase ?answer)))
	)
	(printout t crlf)
	?answer
)

; ���������� ������������, ����� � ���� ���� ����
(deffunction ask-exp ()
	(bind ?response (ask-question "Enter your experience [more-3 from-1-to-3 less-1]: " more-3 from-1-to-3 less-1))
	?response
)

; ���������� ������������, ����� � ���� �����������
(deffunction ask-education ()
	(bind ?response (ask-question "Enter your education [none medium high]: " none medium high))
	?response
)

; ���������� ������������, ��� ���� ����� ��������������� ���������
(deffunction ask-usage ()
	(bind ?response (ask-question "Enter usage [training repetitions concerts records]: " training repetitions concerts records))
	?response
)

; ���������� ������������, � ����� ����� ����� ������
(deffunction ask-style ()
	(bind ?response (ask-question "Enter style [rock metal jazz unknown]: " rock metal jazz unknown))
	?response
)

; ���������� ������������, ������� � ���� ���� ��������
(deffunction ask-money ()
	(printout t "Enter money: ")
	(bind ?answer (read))
	?answer
)

; ������ ������������ ������� ������, �������� � ������� �����
(deffunction ask-band ()
	(printout t "Enter band playing in similar style (use _ as space separator): ")
	(bind ?answer (read))
	?answer
)

; ���������� ������������ ������ �������, ���� ����� �������
(deffunction ask-enough-operation ()
	(bind ?response (ask-question "Choose operation [buy term cancel]: " buy term cancel))
	?response
)


; ���������� ������������ ������ �������, ���� ����� �� �������
(deffunction ask-notenough-operation ()
	(bind ?response (ask-question "Choose operation [term cancel]: " term cancel))
	?response
)


; ���������� ������������ ��� ���������
(deffunction ask-term-operation ()
	(bind ?response (ask-question "Choose term type [6-month 3-month]: " 6-month 3-month))
	?response
)

