(defrule Start
	(initial-fact)
	=>
	(assert (exp (ask-exp)))
	(assert (education (ask-education)))
)





(defrule Set-Skill-Pro
	(or
		(and
			(exp more-3)
			(education none)
		)
		(and
			(exp more-3)
			(education medium)
		)
		(and
			(exp from1-to-3)
			(education high)
		)
	)
	=>
	(assert (skill pro))
	
	(assert (usage (ask-usage)))

)

(defrule Set-Skill-SemiPro
	(or
		(and
			(exp from1-to-3)
			(education none)
		)
		(and
			(exp from1-to-3)
			(education medium)
		)
		(and
			(exp from1-to-3)
			(education high)
		)
		(and
			(exp less-1)
			(education high)
		)
	)
	=>
	(assert (skill semi-pro))
	
	(assert (usage (ask-usage)))
)

(defrule Set-Skill-Beginner
	(or
		(and
			(exp less-1)
			(education none)
		) 
		(and
			(exp less-1)
			(education medium)
		)
	)
	=>
	(assert (skill beginner))
	
	(assert (usage (ask-usage)))
)









(defrule Set-Drumkit-Level-SubEntry
	(or
		(and
			(usage training)
			(skill beginner)
		) 
		(and
			(usage training)
			(skill semi-pro)
		)
	)
	=>
	(assert (drumkit-level sub-entry))
	
	(assert (style (ask-style)))
)

(defrule Set-Drumkit-Level-Entry
	(or
		(and
			(usage repetitions)
			(skill beginner)
		) 
		(and
			(usage repetitions)
			(skill semi-pro)
		)
		(and
			(usage training)
			(skill pro)
		)
	)
	=>
	(assert (drumkit-level entry))
	
	(assert (style (ask-style)))
)

(defrule Set-Drumkit-Level-Student
	(or
		(and
			(usage concerts)
			(skill beginner)
		) 
		(and
			(usage repetitions)
			(skill pro)
		)
	)
	=>
	(assert (drumkit-level student))
	
	(assert (style (ask-style)))
)

(defrule Set-Drumkit-Level-SemiPro
	(or
		(and
			(usage concerts)
			(skill semi-pro)
		)
		(and
			(usage concerts)
			(skill pro)
		)
	)
	=>
	(assert (drumkit-level semi-pro))
	
	(assert (style (ask-style)))
)

(defrule Set-Drumkit-Level-Pro
	(or
		(and
			(usage records)
			(skill beginner)
		) 
		(and
			(usage records)
			(skill semi-pro)
		)
		(and
			(usage records)
			(skill pro)
		)
	)
	=>
	(assert (drumkit-level pro))
	
	(assert (style (ask-style)))
)

(defrule If-Style-Unknown
	(style unknown)
	(not (band ?))
	=>
	(assert (band (ask-band)) )
)

(defrule Set-Style-By-Band
	(style unknown)
	(band ?b)
	?style-facts <- (style ?)
	(db-band (name ?b) (style ?st))
	=>
	(retract ?style-facts)
	(assert (style ?st))
)

(defrule Style-Not-Found
	(style unknown)
	(band ?)
	?style-facts <- (style ?)
	=>
	(printout t crlf "Band not found. Style will be set default value (rock)" crlf crlf)
	(retract ?style-facts)
	(assert (style rock))
)




(defrule Find-Model
	(not (style unknown))
	(drumkit-level ?l)
	(style ?s)
	(db-drumkit (model ?m) (drumkit-level ?l) (style ?s) (price ?p))
	=>
	(printout t crlf "Searching model..." crlf)
	(assert (found-model (model ?m) (price ?p)))
	(printout t crlf "Model found: " ?m ", Price: " ?p crlf crlf)
	
	(assert(cash (ask-money)))
)

(defrule Model-Not-Found
	(not (style unknown))
	(exp ?)
	(education ?)
	(usage ?)
	(skill ?)
	(drumkit-level ?)
	(not (found-model))
	=>
	(printout t crlf "Nothing found. Sorry :)" crlf crlf)
)




(defrule If-Money-Enough
	(cash ?c)
	(found-model (model ?m) (price ?p))
	(test (>= ?c ?p))
	=>
	(assert (enough true))
	(assert (operation (ask-enough-operation)))
)

(defrule If-Money-Not-Enough
	(cash ?c)
	(found-model (model ?m) (price ?p))
	(test (< ?c ?p))
	=>
	(assert (enough false))
	(assert (operation (ask-notenough-operation)))
)




(defrule Do-Operation-Buy
	(enough true)
	(operation buy)
	(found-model (model ?m) (price ?p))
	=>
	(printout t crlf "Buy model" ?m ", Price: " ?p crlf crlf)
)

(defrule Operation-Term
	(enough ?)
	(operation term)
	(not (term ?))
	(found-model (model ?m) (price ?p))
	=>
	(assert (term (ask-term-operation)))
)

(defrule Do-Operation-Term
	(enough ?)
	(operation term)
	(term ?t)
	(found-model (model ?m) (price ?p))
	=>
	(printout t crlf "Buy model" ?m ", Price: " ?p ", Term: " ?t crlf crlf)
)


(defrule Do-Operation-Cancel
	(enough ?)
	(operation cancel)
	=>
	(printout t crlf "Cancel. Goodbye" crlf crlf)
)









