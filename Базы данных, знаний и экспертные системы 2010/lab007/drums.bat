(clear)



;;;;;;;;;;;;;;;;;;;;;;;;;
;;; TEMPLATES
;;;;;;;;;;;;;;;;;;;;;;;;;

(deftemplate db-band
	(slot name)
	(slot style)
)

(deftemplate db-drumkit
	(slot model)
	(slot drumkit-level)
	(slot style)
	(slot price)
)

(deftemplate found-model
	(slot model)
	(slot price)
)






;;;;;;;;;;;;;;;;;;;;;;;;;
;;; DATABASE
;;;;;;;;;;;;;;;;;;;;;;;;;


(deffacts bands
	(db-band (name Metallica) (style metal))
	(db-band (name Ayreon) (style metal))
	(db-band (name Therion) (style metal))
	(db-band (name Nightwish) (style metal))
	(db-band (name Slayer) (style metal))
	(db-band (name Deep_Purple) (style rock))
	(db-band (name Pink_Floyd) (style rock))
	(db-band (name Uriah_Heep) (style rock))
	(db-band (name Rainbow) (style rock))
	(db-band (name Led_Zeppelin) (style rock))
	(db-band (name Apple_Tea) (style jazz))
	(db-band (name Simply_Red) (style jazz))
	(db-band (name Fourplay) (style jazz))
	(db-band (name Material) (style jazz))
)

(deffacts drumkits 
	(db-drumkit (model Sonor-SQ1) (drumkit-level sub-entry) (style rock) (price 900))
	(db-drumkit (model Sonor-SQ1) (drumkit-level sub-entry) (style metal) (price 900))
	(db-drumkit (model Sonor-SQ1) (drumkit-level sub-entry) (style jazz) (price 900))
	
	(db-drumkit (model Sonor-SQ2) (drumkit-level entry) (style rock) (price 1000))
	(db-drumkit (model Sonor-SQ2) (drumkit-level entry) (style metal) (price 1000))
	(db-drumkit (model Sonor-SQ2) (drumkit-level entry) (style jazz) (price 1000))
	
	(db-drumkit (model Sonor-Force_3007_Studio_1) (drumkit-level semi-pro) (style rock) (price 1500))
	(db-drumkit (model Sonor-Force_2007_Studio_1) (drumkit-level semi-pro) (style rock) (price 1300))
	(db-drumkit (model Sonor-Force_1007_Studio_1) (drumkit-level semi-pro) (style rock) (price 900))
	(db-drumkit (model Sonor-Force_1007_Studio_2) (drumkit-level semi-pro) (style rock) (price 950))
	
	(db-drumkit (model Sonor-Force_507_Studio_1) (drumkit-level student) (style rock) (price 800))
	(db-drumkit (model Sonor-Force_507_Studio_2) (drumkit-level student) (style rock) (price 850))
	(db-drumkit (model Mapex-BM6225A) (drumkit-level student) (style rock) (price 1500))
	(db-drumkit (model Mapex-BM6225A) (drumkit-level student) (style metal) (price 1500))
	(db-drumkit (model Mapex-BM5255A) (drumkit-level student) (style jazz) (price 1700))
	(db-drumkit (model Mapex-SW4815A) (drumkit-level student) (style rock) (price 2000))
	
	(db-drumkit (model Sonor-SC_Stage_1_Shell) (drumkit-level semi-pro) (style rock) (price 2000))
	(db-drumkit (model Sonor-SC_Stage_2_Shell) (drumkit-level semi-pro) (style rock) (price 2100))
	(db-drumkit (model Sonor-SC_Stage_3_Shell) (drumkit-level semi-pro) (style rock) (price 2200))
	
	(db-drumkit (model Mapex-SW6225A) (drumkit-level semi-pro) (style metal) (price 1600))
	(db-drumkit (model Mapex-PM6225A) (drumkit-level semi-pro) (style metal) (price 2100))
	(db-drumkit (model Premier-Spirit_of_Lily) (drumkit-level semi-pro) (style rock) (price 2500))
	(db-drumkit (model Premier-Maple_Traditional) (drumkit-level semi-pro) (style jazz) (price 2600))
	(db-drumkit (model Premier-Shell_Packs) (drumkit-level semi-pro) (style jazz) (price 2400))
	
	(db-drumkit (model Sonor-Force_3007_Studio_2) (drumkit-level pro) (style rock) (price 1600))
	(db-drumkit (model Sonor-Force_2007_Studio_2) (drumkit-level pro) (style rock) (price 1200))
	(db-drumkit (model Mapex-VX5255RA) (drumkit-level pro) (style rock) (price 2100))
	(db-drumkit (model Premier-Birch_Traditional) (drumkit-level pro) (style rock) (price 2700))
)

(reset)



;;;;;;;;;;;;;;;;;;;;;;;;;
;;; FUNCTONS
;;;;;;;;;;;;;;;;;;;;;;;;;



; ������ ������ ?question � ���������� ������� $?allowed-values
(deffunction ask-question (?question $?allowed-values)
	(printout t ?question)
	(bind ?answer (read))
	(if (lexemep ?answer)
		then
		(bind ?answer (lowcase ?answer))
	)
	(while (not (member ?answer ?allowed-values)) do
		(printout t ?question)
		(bind ?answer (read))
		(if (lexemep ?answer)
			then
			(bind ?answer (lowcase ?answer)))
	)
	(printout t crlf)
	?answer
)

; ���������� ������������, ����� � ���� ���� ����
(deffunction ask-exp ()
	(bind ?response (ask-question "Enter your experience [more-3 from-1-to-3 less-1]: " more-3 from-1-to-3 less-1))
	?response
)

; ���������� ������������, ����� � ���� �����������
(deffunction ask-education ()
	(bind ?response (ask-question "Enter your education [none medium high]: " none medium high))
	?response
)

; ���������� ������������, ��� ���� ����� ��������������� ���������
(deffunction ask-usage ()
	(bind ?response (ask-question "Enter usage [training repetitions concerts records]: " training repetitions concerts records))
	?response
)

; ���������� ������������, � ����� ����� ����� ������
(deffunction ask-style ()
	(bind ?response (ask-question "Enter style [rock metal jazz unknown]: " rock metal jazz unknown))
	?response
)

; ���������� ������������, ������� � ���� ���� ��������
(deffunction ask-money ()
	(printout t "Enter money: ")
	(bind ?answer (read))
	?answer
)

; ������ ������������ ������� ������, �������� � ������� �����
(deffunction ask-band ()
	(printout t "Enter band playing in similar style (use _ as space separator): ")
	(bind ?answer (read))
	?answer
)

; ���������� ������������ ������ �������, ���� ����� �������
(deffunction ask-enough-operation ()
	(bind ?response (ask-question "Choose operation [buy term cancel]: " buy term cancel))
	?response
)


; ���������� ������������ ������ �������, ���� ����� �� �������
(deffunction ask-notenough-operation ()
	(bind ?response (ask-question "Choose operation [term cancel]: " term cancel))
	?response
)


; ���������� ������������ ��� ���������
(deffunction ask-term-operation ()
	(bind ?response (ask-question "Choose term type [6-month 3-month]: " 6-month 3-month))
	?response
)




;;;;;;;;;;;;;;;;;;;;;;;;;
;;; RULES
;;;;;;;;;;;;;;;;;;;;;;;;;

(defrule Start
	(initial-fact)
	=>
	(assert (exp (ask-exp)))
	(assert (education (ask-education)))
)





(defrule Set-Skill-Pro
	(or
		(and
			(exp more-3)
			(education none)
		)
		(and
			(exp more-3)
			(education medium)
		)
		(and
			(exp from1-to-3)
			(education high)
		)
	)
	=>
	(assert (skill pro))
	
	(assert (usage (ask-usage)))

)

(defrule Set-Skill-SemiPro
	(or
		(and
			(exp from1-to-3)
			(education none)
		)
		(and
			(exp from1-to-3)
			(education medium)
		)
		(and
			(exp from1-to-3)
			(education high)
		)
		(and
			(exp less-1)
			(education high)
		)
	)
	=>
	(assert (skill semi-pro))
	
	(assert (usage (ask-usage)))
)

(defrule Set-Skill-Beginner
	(or
		(and
			(exp less-1)
			(education none)
		) 
		(and
			(exp less-1)
			(education medium)
		)
	)
	=>
	(assert (skill beginner))
	
	(assert (usage (ask-usage)))
)









(defrule Set-Drumkit-Level-SubEntry
	(or
		(and
			(usage training)
			(skill beginner)
		) 
		(and
			(usage training)
			(skill semi-pro)
		)
	)
	=>
	(assert (drumkit-level sub-entry))
	
	(assert (style (ask-style)))
)

(defrule Set-Drumkit-Level-Entry
	(or
		(and
			(usage repetitions)
			(skill beginner)
		) 
		(and
			(usage repetitions)
			(skill semi-pro)
		)
		(and
			(usage training)
			(skill pro)
		)
	)
	=>
	(assert (drumkit-level entry))
	
	(assert (style (ask-style)))
)

(defrule Set-Drumkit-Level-Student
	(or
		(and
			(usage concerts)
			(skill beginner)
		) 
		(and
			(usage repetitions)
			(skill pro)
		)
	)
	=>
	(assert (drumkit-level student))
	
	(assert (style (ask-style)))
)

(defrule Set-Drumkit-Level-SemiPro
	(or
		(and
			(usage concerts)
			(skill semi-pro)
		)
		(and
			(usage concerts)
			(skill pro)
		)
	)
	=>
	(assert (drumkit-level semi-pro))
	
	(assert (style (ask-style)))
)

(defrule Set-Drumkit-Level-Pro
	(or
		(and
			(usage records)
			(skill beginner)
		) 
		(and
			(usage records)
			(skill semi-pro)
		)
		(and
			(usage records)
			(skill pro)
		)
	)
	=>
	(assert (drumkit-level pro))
	
	(assert (style (ask-style)))
)

(defrule If-Style-Unknown
	(style unknown)
	(not (band ?))
	=>
	(assert (band (ask-band)) )
)

(defrule Set-Style-By-Band
	(style unknown)
	(band ?b)
	?style-facts <- (style ?)
	(db-band (name ?b) (style ?st))
	=>
	(retract ?style-facts)
	(assert (style ?st))
)

(defrule Style-Not-Found
	(style unknown)
	(band ?)
	?style-facts <- (style ?)
	=>
	(printout t crlf "Band not found. Style will be set default value (rock)" crlf crlf)
	(retract ?style-facts)
	(assert (style rock))
)




(defrule Find-Model
	(not (style unknown))
	(drumkit-level ?l)
	(style ?s)
	(db-drumkit (model ?m) (drumkit-level ?l) (style ?s) (price ?p))
	=>
	(printout t crlf "Searching model..." crlf)
	(assert (found-model (model ?m) (price ?p)))
	(printout t crlf "Model found: " ?m ", Price: " ?p crlf crlf)
	
	(assert(cash (ask-money)))
)

(defrule Model-Not-Found
	(not (style unknown))
	(exp ?)
	(education ?)
	(usage ?)
	(skill ?)
	(drumkit-level ?)
	(not (found-model))
	=>
	(printout t crlf "Nothing found. Sorry :)" crlf crlf)
)




(defrule If-Money-Enough
	(cash ?c)
	(found-model (model ?m) (price ?p))
	(test (>= ?c ?p))
	=>
	(assert (enough true))
	(assert (operation (ask-enough-operation)))
)

(defrule If-Money-Not-Enough
	(cash ?c)
	(found-model (model ?m) (price ?p))
	(test (< ?c ?p))
	=>
	(assert (enough false))
	(assert (operation (ask-notenough-operation)))
)




(defrule Do-Operation-Buy
	(enough true)
	(operation buy)
	(found-model (model ?m) (price ?p))
	=>
	(printout t crlf "Buy model" ?m ", Price: " ?p crlf crlf)
)

(defrule Operation-Term
	(enough ?)
	(operation term)
	(not (term ?))
	(found-model (model ?m) (price ?p))
	=>
	(assert (term (ask-term-operation)))
)

(defrule Do-Operation-Term
	(enough ?)
	(operation term)
	(term ?t)
	(found-model (model ?m) (price ?p))
	=>
	(printout t crlf "Buy model" ?m ", Price: " ?p ", Term: " ?t crlf crlf)
)


(defrule Do-Operation-Cancel
	(enough ?)
	(operation cancel)
	=>
	(printout t crlf "Cancel. Goodbye" crlf crlf)
)







(run)






