// ordered

(assert
	(exp more-3)
	(exp from-1-to-3)
	(exp less-1)

	(education none)
	(education medium)
	(education high)

	(usage trenning)
	(usage repetitions)
	(usage concerts)
	(usage records)

	(style rock)
	(style metal)
	(style jazz)
	(style dont-know)
	(style not-found)

	(skill pro)
	(skill semi-pro)
	(skill beginner)

	(drumkit-level sub-entry)
	(drumkit-level entry)
	(drumkit-level student)
	(drumkit-level semi-pro)
	(drumkit-level pro)

	(enough true)
	(enough false)

	(operation buy)
	(operation term)
	(operation cancel)

	(term month-3)
	(term month-6)
	
)





(assert
	(drumkit Sonor-SQ2 sub-entry rock 1000)
	(drumkit Sonor-Force_3007_Studio_1 1500)
	(drumkit Sonor-Force_3007_Studio_2 1600)
	(drumkit Sonor-Force_2007_Studio_1 1300)
	(drumkit Sonor-Force_2007_Studio_2 1200)
	(drumkit Sonor-Force_1007_Studio_1 900)
	(drumkit Sonor-Force_1007_Studio_2 950)
	(drumkit Sonor-Force_507_Studio_1 800)
	(drumkit Sonor-Force_507_Studio_2 850)
	(drumkit Sonor-SC_Stage_1_Shell 2000)
	(drumkit Sonor-SC_Stage_2_Shell 2100)
	(drumkit Sonor-SC_Stage_3_Shell 2200)
	(drumkit Mapex-BM6225A 1500)
	(drumkit Mapex-BM5255A 1700)
	(drumkit Mapex-SW6225A 1600)
	(drumkit Mapex-SW4815A 2000)
	(drumkit Mapex-PM6225A 2100)
	(drumkit Mapex-PM6296A 2200)
	(drumkit Mapex-MB6225A 2300)
	(drumkit Mapex-VX5255RA 2100)
	(drumkit Premier-Spirit_of_Lily 2500)
	(drumkit Premier-Maple_Traditional 2600)
	(drumkit Premier-Birch_Traditional 2700)
	(drumkit Premier-Shell_Packs 2400)
)


