


(assert
	(exp more-3)
	(exp from-1-to-3)
	(exp less-1)

	(education none)
	(education medium)
	(education high)

	(usage trenning)
	(usage repetitions)
	(usage concerts)
	(usage records)

	(style rock)
	(style metal)
	(style jazz)
	(style dont-know)
	(style not-found)

	(skill pro)
	(skill semi-pro)
	(skill beginner)

	(drumkit-level sub-entry)
	(drumkit-level entry)
	(drumkit-level student)
	(drumkit-level semi-pro)
	(drumkit-level pro)

	(enough true)
	(enough false)

	(operation buy)
	(operation term)
	(operation cancel)

	(term month-3)
	(term month-6)
	
)





(deftemplate drumkit
	(slot model)
	(slot drumkit-level)
	(slot style)
	(slot price)
)

(deftemplate found-model
	(slot model)
	(slot price)
)


(deffacts drumkits 
	(drumkit (model Sonor-SQ2) (drumkit-level entry) (style rock) (price 1000))
	(drumkit (model Sonor-Force_3007_Studio_1) (drumkit-level semi-pro) (style rock) (price 1500))
	(drumkit (model Sonor-Force_3007_Studio_2) (drumkit-level pro) (style rock) (price 1600))
	(drumkit (model Sonor-Force_2007_Studio_1) (drumkit-level semi-pro) (style rock) (price 1300))
	(drumkit (model Sonor-Force_2007_Studio_2) (drumkit-level pro) (style rock) (price 1200))
	(drumkit (model Sonor-Force_1007_Studio_1) (drumkit-level semi-pro) (style rock) (price 900))
	(drumkit (model Sonor-Force_1007_Studio_2) (drumkit-level semi-pro) (style rock) (price 950))
	(drumkit (model Sonor-Force_507_Studio_1) (drumkit-level student) (style rock) (price 800))
	(drumkit (model Sonor-Force_507_Studio_2) (drumkit-level student) (style rock) (price 850))
	(drumkit (model Sonor-SC_Stage_1_Shell) (drumkit-level semi-pro) (style rock) (price 2000))
	(drumkit (model Sonor-SC_Stage_2_Shell) (drumkit-level semi-pro) (style rock) (price 2100))
	(drumkit (model Sonor-SC_Stage_3_Shell) (drumkit-level semi-pro) (style rock) (price 2200))
	(drumkit (model Mapex-BM6225A) (drumkit-level student) (style rock) (price 1500))
	(drumkit (model Mapex-BM5255A) (drumkit-level student) (style jazz) (price 1700))
	(drumkit (model Mapex-SW6225A) (drumkit-level semi-pro) (style metal) (price 1600))
	(drumkit (model Mapex-SW4815A) (drumkit-level student) (style rock) (price 2000))
	(drumkit (model Mapex-PM6225A) (drumkit-level semi-pro) (style metal) (price 2100))
	(drumkit (model Mapex-PM6296A) (drumkit-level student) (style rock) (price 2200))
	(drumkit (model Mapex-MB6225A) (drumkit-level student) (style rock) (price 2300))
	(drumkit (model Mapex-VX5255RA) (drumkit-level pro) (style rock) (price 2100))
	(drumkit (model Premier-Spirit_of_Lily) (drumkit-level semi-pro) (style rock) (price 2500))
	(drumkit (model Premier-Maple_Traditional) (drumkit-level semi-pro) (style jazz) (price 2600))
	(drumkit (model Premier-Birch_Traditional) (drumkit-level pro) (style rock) (price 2700))
	(drumkit (model Premier-Shell_Packs) (drumkit-level semi-pro) (style jazz) (price 2400))
)













