(defgeneric connect)


;;; Connects a one output component to a one input component.
(defmethod connect ((?out ONE-OUTPUT) (?in ONE-INPUT)) 
   (send ?out put-output-1-link ?in) 
   (send ?out put-output-1-link-pin 1)
   (send ?in  put-input-1-link ?out)
   (send ?in  put-input-1-link-pin 1))



;;; Connects a one output component to one pin of a two input component.
(defmethod connect ((?out ONE-OUTPUT) (?in TWO-INPUT) (?in-pin INTEGER)) 
   (send ?out put-output-1-link ?in)
   (send ?out put-output-1-link-pin ?in-pin)
   (send ?in  (sym-cat put-input- ?in-pin -link) ?out)
   (send ?in  (sym-cat put-input- ?in-pin -link-pin) 1))







(defmethod connect ((?out ONE-OUTPUT) (?in THREE-INPUT) (?in-pin INTEGER))
	(send ?out put-output-1-link ?in)
	(send ?out put-output-1-link-pin ?in-pin)
	(send ?in (sym-cat put-input- ?in-pin -link) ?out)
	(send ?in (sym-cat put-input- ?in-pin -link-pin) 1)
)

(defmethod connect ((?out ONE-OUTPUT) (?in FOUR-INPUT) (?in-pin INTEGER))
	(send ?out put-output-1-link ?in)
	(send ?out put-output-1-link-pin ?in-pin)
	(send ?in (sym-cat put-input- ?in-pin -link) ?out)
	(send ?in (sym-cat put-input- ?in-pin -link-pin) 1)
)








;;; Connects one pin of a two output component to a one input component.
(defmethod connect ((?out TWO-OUTPUT) (?out-pin INTEGER) (?in ONE-INPUT)) 
   (send ?out (sym-cat put-output- ?out-pin -link) ?in)
   (send ?out (sym-cat put-output- ?out-pin -link-pin) 1)
   (send ?in put-input-1-link ?out)
   (send ?in put-input-1-link-pin ?out-pin))



;;; Connects one pin of a two output component 
;;; to one pin of a two input component.
(defmethod connect ((?out TWO-OUTPUT) (?out-pin INTEGER)
                    (?in TWO-INPUT) (?in-pin INTEGER)) 
   (send ?out (sym-cat put-output- ?out-pin -link) ?in)
   (send ?out (sym-cat put-output- ?out-pin -link-pin) ?in-pin)
   (send ?in  (sym-cat put-input- ?in-pin -link) ?out)
   (send ?in  (sym-cat put-input- ?in-pin -link-pin) ?out-pin))


(defmethod connect ((?out TWO-OUTPUT) (?out-pin INTEGER) (?in THREE-INPUT) (?in-pin INTEGER))
	(send ?out (sym-cat put-output- ?out-pin -link) ?in)
	(send ?out (sym-cat put-output- ?out-pin -link-pin) ?in-pin)
	(send ?in (sym-cat put-input- ?in-pin -link) ?out)
	(send ?in (sym-cat put-input- ?in-pin -link-pin) ?out-pin)
)

(defmethod connect ((?out TWO-OUTPUT) (?out-pin INTEGER) (?in FOUR-INPUT) (?in-pin INTEGER))
	(send ?out (sym-cat put-output- ?out-pin -link) ?in)
	(send ?out (sym-cat put-output- ?out-pin -link-pin) ?in-pin)
	(send ?in (sym-cat put-input- ?in-pin -link) ?out)
	(send ?in (sym-cat put-input- ?in-pin -link-pin) ?out-pin)
)




(defglobal 
	?*gray-code*     = (create$)
	?*sources*       = (create$)
	?*max-iterations* = 0
)

(deffunction change-which-bit (?x)
	(bind ?i 1)
	(while (and (evenp ?x) (!= ?x 0)) do
		(bind ?x (div ?x 2))
		(bind ?i (+ ?i 1))
	)
	?i
)



(deffunction connect-circuit ())




