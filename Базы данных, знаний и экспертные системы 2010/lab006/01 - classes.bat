

(defclass COMPONENT
  (is-a USER)
  (slot ID# (create-accessor write)))

(defclass NO-OUTPUT
  (is-a USER)
  (slot number-of-outputs (access read-only) 
                          (default 0)
                          (create-accessor read)))

(defmessage-handler NO-OUTPUT compute-output ())

(defclass ONE-OUTPUT
  (is-a NO-OUTPUT)
  (slot number-of-outputs (access read-only) 
                          (default 1)
                          (create-accessor read))
  (slot output-1 (default UNDEFINED) (create-accessor write))
  (slot output-1-link (default GROUND) (create-accessor write))
  (slot output-1-link-pin (default 1) (create-accessor write)))

(defmessage-handler ONE-OUTPUT put-output-1 after (?value)
   (send ?self:output-1-link 
         (sym-cat put-input- ?self:output-1-link-pin)
         ?value))

(defclass TWO-OUTPUT
  (is-a ONE-OUTPUT)
  (slot number-of-outputs (access read-only) 
                          (default 2)
                          (create-accessor read))
  (slot output-2 (default UNDEFINED) (create-accessor write))
  (slot output-2-link (default GROUND) (create-accessor write))
  (slot output-2-link-pin (default 1) (create-accessor write)))

(defmessage-handler TWO-OUTPUT put-output-1 after (?value)
   (send ?self:output-2-link 
         (sym-cat put-input- ?self:output-2-link-pin)
         ?value))

(defclass NO-INPUT
  (is-a USER)
  (slot number-of-inputs (access read-only) 
                         (default 0)
                         (create-accessor read)))

(defclass ONE-INPUT
  (is-a NO-INPUT)
  (slot number-of-inputs (access read-only) 
                         (default 1)
                         (create-accessor read))
  (slot input-1 (default UNDEFINED) 
                (visibility public)
                (create-accessor read-write))
  (slot input-1-link (default GROUND) (create-accessor write))
  (slot input-1-link-pin (default 1) (create-accessor write)))





(defmessage-handler ONE-INPUT put-input-1 after (?value)
   (send ?self compute-output))

(defclass TWO-INPUT
  (is-a ONE-INPUT)
  (slot number-of-inputs (access read-only) 
                         (default 2)
                         (create-accessor read))
  (slot input-2 (default UNDEFINED) 
                (visibility public)
                (create-accessor write))
  (slot input-2-link (default GROUND) (create-accessor write))
  (slot input-2-link-pin (default 1) (create-accessor write)))

(defmessage-handler TWO-INPUT put-input-2 after (?value)
   (send ?self compute-output))
 
(defclass SOURCE
  (is-a NO-INPUT ONE-OUTPUT COMPONENT)
  (role concrete)
  (slot output-1 (default UNDEFINED) (create-accessor write)))

(defclass SINK
  (is-a ONE-INPUT NO-OUTPUT COMPONENT)
  (role concrete)
  (slot input-1 (default UNDEFINED) (create-accessor read-write)))





(defclass THREE-INPUT
	(is-a TWO-INPUT)
	(slot number-of-inputs (access read-only) (default 3) (create-accessor read))
	(slot input-3 (default UNDEFINED) (visibility public) (create-accessor write))
	(slot input-3-link (default GROUND) (create-accessor write))
	(slot input-3-link-pin (default 1) (create-accessor write))
	(slot ID# (create-accessor read))
)

(defmessage-handler THREE-INPUT put-input-3 after (?value) 
	(send ?self compute-output)
)

(defclass FOUR-INPUT
	(is-a THREE-INPUT)
	(slot number-of-inputs (access read-only) (default 4) (create-accessor read))
	(slot input-4 (default UNDEFINED) (visibility public) (create-accessor write))
	(slot input-4-link (default GROUND) (create-accessor write))
	(slot input-4-link-pin (default 1) (create-accessor write))
	(slot ID# (create-accessor read))
)

(defmessage-handler FOUR-INPUT put-input-4 after (?value) 
	(send ?self compute-output)
)









;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; NOT GATE COMPONENT

(defclass NOT-GATE
  (is-a ONE-INPUT ONE-OUTPUT COMPONENT)
  (role concrete))

(deffunction not# (?x) (- 1 ?x))

(defmessage-handler NOT-GATE compute-output ()
   (if (integerp ?self:input-1) then
       (send ?self put-output-1 (not# ?self:input-1))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; AND GATE COMPONENT

(defclass AND-GATE
  (is-a TWO-INPUT ONE-OUTPUT COMPONENT)
  (role concrete))

(deffunction and# (?x ?y) 
  (if (and (!= ?x 0) (!= ?y 0)) then 1 else 0))

(defmessage-handler AND-GATE compute-output ()
   (if (and (integerp ?self:input-1) 
            (integerp ?self:input-2)) then
       (send ?self put-output-1 (and# ?self:input-1 ?self:input-2))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; OR GATE COMPONENT

(defclass OR-GATE
  (is-a TWO-INPUT ONE-OUTPUT COMPONENT)
  (role concrete))

(deffunction or# (?x ?y) 
  (if (or (!= ?x 0) (!= ?y 0)) then 1 else 0))

(defmessage-handler OR-GATE compute-output ()
   (if (and (integerp ?self:input-1) 
            (integerp ?self:input-2)) then
       (send ?self put-output-1 (or# ?self:input-1 ?self:input-2))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; NAND GATE COMPONENT

(defclass NAND-GATE
  (is-a TWO-INPUT ONE-OUTPUT COMPONENT)
  (role concrete))

(deffunction nand# (?x ?y) 
  (if (not (and (!= ?x 0) (!= ?y 0))) then 1 else 0))

(defmessage-handler NAND-GATE compute-output ()
   (if (and (integerp ?self:input-1) 
            (integerp ?self:input-2)) then
       (send ?self put-output-1 (nand# ?self:input-1 ?self:input-2))))

;;;*******************
;;; XOR GATE COMPONENT

(defclass XOR-GATE
  (is-a TWO-INPUT ONE-OUTPUT COMPONENT)
  (role concrete))

(deffunction xor# (?x ?y) 
  (if (or (and (= ?x 1) (= ?y 0))
          (and (= ?x 0) (= ?y 1))) then 1 else 0))

(defmessage-handler XOR-GATE compute-output ()
   (if (and (integerp ?self:input-1) 
            (integerp ?self:input-2)) then
       (send ?self put-output-1 (xor# ?self:input-1 ?self:input-2))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; SPLITTER COMPONENT

(defclass SPLITTER
  (is-a ONE-INPUT TWO-OUTPUT COMPONENT)
  (role concrete))

(defmessage-handler SPLITTER compute-output ()
   (if (integerp ?self:input-1) then
       (send ?self put-output-1 ?self:input-1)
       (send ?self put-output-2 ?self:input-1)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; LED COMPONENT

(defclass LED
  (is-a ONE-INPUT NO-OUTPUT COMPONENT)
  (role concrete))

;;; Returns the current value of each LED 
;;; instance in a multifield value.
(deffunction LED-response ()
   (bind ?response (create$))
   (do-for-all-instances ((?led LED)) TRUE
      (bind ?response (create$ ?response (send ?led get-input-1))))
   ?response)






;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; NOT-AND-GATE-3

(defclass NOT-AND-GATE-3
	(is-a THREE-INPUT ONE-OUTPUT COMPONENT)
	(role concrete)
)
(deffunction and-not-3# (?a ?b ?c) 
	(if (and (!= ?a 0) (!= ?b 0) (!= ?c 0)) then 0 else 1
	)
)
(defmessage-handler NOT-AND-GATE-3 compute-output ()
	(if (and (integerp ?self:input-1) (integerp ?self:input-2) (integerp ?self:input-3)) then
		(send ?self put-output-1 (and-not-3# ?self:input-1 ?self:input-2 ?self:input-3))
	)
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; NOT-AND-GATE-4

(defclass NOT-AND-GATE-4
	(is-a FOUR-INPUT ONE-OUTPUT COMPONENT)
	(role concrete)
)

(deffunction and-not-4# (?a ?b ?c ?d) 
	(if (and (!= ?a 0) (!= ?b 0) (!= ?c 0) (!= ?d 0)) then 0 else 1
	)
)
(defmessage-handler NOT-AND-GATE-4 compute-output ()
	(if (and (integerp ?self:input-1) (integerp ?self:input-2) (integerp ?self:input-3) (integerp ?self:input-4)) then
		(send ?self put-output-1 (and-not-4# ?self:input-1 ?self:input-2 ?self:input-3 ?self:input-4))
	)
)






