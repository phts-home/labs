
(defrule startup
	=>
	;; Initialize the circuit by connecting the components
	(connect-circuit) 
	;; Setup the globals. 
	(bind ?*sources* (find-all-instances ((?x SOURCE)) TRUE))
	(do-for-all-instances ((?x SOURCE)) TRUE
		 (bind ?*gray-code* (create$ ?*gray-code* 0)))
	(bind ?*max-iterations* (round (** 2 (length ?*sources*))))
	;; Do the first response.
	(assert (current-iteration 0))
 )




(defrule compute-response-1st-time
	?f <- (current-iteration 0)
	=>
	;; Set all of the sources to zero.
	(do-for-all-instances ((?source SOURCE)) TRUE (send ?source put-output-1 0))
	;; Determine the initial LED response.
	(assert (result ?*gray-code* =(str-implode (LED-response))))
	;; Begin the iteration process of looping through the gray code combinations.
	(retract ?f)
	(assert (current-iteration 1))
)



(defrule compute-response-other-times
	?f <- (current-iteration ?n&~0&:(< ?n ?*max-iterations*))
	=>
	;; Change the gray code, saving the changed bit value.
	(bind ?pos (change-which-bit ?n))
	(bind ?nv (- 1 (nth ?pos ?*gray-code*)))
	(bind ?*gray-code* (replace$ ?*gray-code* ?pos ?pos ?nv))
	;; Change the single changed source
	(send (nth ?pos ?*sources*) put-output-1 ?nv)   
	;; Determine the LED response to the input.
	(assert (result ?*gray-code* =(str-implode (LED-response))))
	;; Assert the new iteration fact
	(retract ?f)
	(assert (current-iteration =(+ ?n 1)))
)



(defrule merge-responses
	(declare (salience 10))
	?f1 <- (result $?b  ?x $?e ?response)
	?f2 <- (result $?b ~?x $?e ?response)
	=>
	(retract ?f1 ?f2)
	(assert (result $?b * $?e ?response))
)

(defrule print-header
	(declare (salience -10))
	=>
	(assert (print-results))
	(do-for-all-instances ((?x SOURCE)) TRUE (format t " %3s " (sym-cat ?x)))
	(printout t " | ")
	(do-for-all-instances ((?x LED)) TRUE (format t " %3s " (sym-cat ?x)))
	(format t "%n")
	(do-for-all-instances ((?x SOURCE)) TRUE (printout t "-----"))
	(printout t "-+-")
	(do-for-all-instances ((?x LED)) TRUE (printout t "-----"))
	(format t "%n")
)


(defrule print-result
	(print-results)
	?f <- (result $?input ?response)
	(not (result $?input-2 ?response-2&:(< (str-compare ?response-2 ?response) 0)))
	=>
	(retract ?f)
	;; Print the input from the sources.
	(while (neq ?input (create$)) do
		(printout t "  " (nth 1 ?input) "  ")
		(bind ?input (rest$ ?input)))
	;; Print the output from the LEDs.
	(printout t " | ")
	(bind ?response (str-explode ?response))
	(while (neq ?response (create$)) do
		(printout t "  " (nth 1 ?response) "  ")
		(bind ?response (rest$ ?response)))
	(printout t crlf)
)
