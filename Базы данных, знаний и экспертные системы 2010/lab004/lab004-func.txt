(defglobal ?*len* = 0)
(defglobal ?*ires* = 0)
(defglobal ?*sres* = 0)

(defgeneric mul )
  
(defmethod mul ( (?a STRING) ($?b STRING) ) 
	(bind ?*len* 1)
	(bind ?*sres* ?a)
	(while (<= ?*len* (length $?b)) do
		(bind ?*sres* (str-cat ?*sres* (nth ?*len* $?b)) )
		(bind ?*len* (+ ?*len* 1))
	)
	(printout t ?*sres* crlf)

)

(defmethod mul ((?a NUMBER) ($?b NUMBER)) 
	
	(bind ?*len* (length $?b))
	(bind ?*ires* ?a)
	(while (> ?*len* 0) do
		(bind ?*ires* (* ?*ires* (nth ?*len* $?b)) )
		(bind ?*len* (- ?*len* 1))
	)
	(printout t ?*ires* crlf)
	 
)

(defmethod mul ( (?a STRING) (?b NUMBER) ) 
	
	(bind ?*sres* ?a)
	(bind ?*len* ?b)
	(while (> ?*len* 1) do
		(bind ?*sres* (str-cat ?a ?*sres*))
		(bind ?*len* (- ?*len* 1))
	)
	(printout t ?*sres* crlf)
	
)
