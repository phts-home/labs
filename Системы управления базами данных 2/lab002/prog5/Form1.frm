VERSION 5.00
Begin VB.Form Form1 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Form1"
   ClientHeight    =   4695
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5730
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4695
   ScaleWidth      =   5730
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command20 
      Caption         =   "+/-"
      Height          =   375
      Left            =   2760
      TabIndex        =   20
      Top             =   3480
      Width           =   495
   End
   Begin VB.CommandButton Command19 
      Caption         =   "1/x"
      Height          =   375
      Left            =   4560
      TabIndex        =   19
      Top             =   3000
      Width           =   495
   End
   Begin VB.CommandButton Command18 
      Caption         =   ","
      Height          =   375
      Left            =   3360
      TabIndex        =   18
      Top             =   3480
      Width           =   495
   End
   Begin VB.CommandButton Command17 
      Caption         =   "sqrt"
      Height          =   375
      Left            =   4560
      TabIndex        =   17
      Top             =   2040
      Width           =   495
   End
   Begin VB.CommandButton Command16 
      Caption         =   "/"
      Height          =   375
      Left            =   3960
      TabIndex        =   16
      Top             =   2040
      Width           =   495
   End
   Begin VB.CommandButton Command15 
      Caption         =   "*"
      Height          =   375
      Left            =   3960
      TabIndex        =   15
      Top             =   2520
      Width           =   495
   End
   Begin VB.CommandButton Command14 
      Caption         =   "-"
      Height          =   375
      Left            =   3960
      TabIndex        =   14
      Top             =   3000
      Width           =   495
   End
   Begin VB.CommandButton Command13 
      Caption         =   "="
      Height          =   375
      Left            =   4560
      TabIndex        =   13
      Top             =   3480
      Width           =   495
   End
   Begin VB.CommandButton Command12 
      Caption         =   "+"
      Height          =   375
      Left            =   3960
      TabIndex        =   12
      Top             =   3480
      Width           =   495
   End
   Begin VB.CommandButton Command11 
      Caption         =   "C"
      Height          =   375
      Left            =   3960
      TabIndex        =   11
      Top             =   1560
      Width           =   1095
   End
   Begin VB.CommandButton Command10 
      Caption         =   "0"
      Height          =   375
      Left            =   2160
      TabIndex        =   10
      Top             =   3480
      Width           =   495
   End
   Begin VB.CommandButton Command9 
      Caption         =   "9"
      Height          =   375
      Left            =   3360
      TabIndex        =   9
      Top             =   2040
      Width           =   495
   End
   Begin VB.CommandButton Command8 
      Caption         =   "8"
      Height          =   375
      Left            =   2760
      TabIndex        =   8
      Top             =   2040
      Width           =   495
   End
   Begin VB.CommandButton Command7 
      Caption         =   "7"
      Height          =   375
      Left            =   2160
      TabIndex        =   7
      Top             =   2040
      Width           =   495
   End
   Begin VB.CommandButton Command6 
      Caption         =   "6"
      Height          =   375
      Left            =   3360
      TabIndex        =   6
      Top             =   2520
      Width           =   495
   End
   Begin VB.CommandButton Command5 
      Caption         =   "5"
      Height          =   375
      Left            =   2760
      TabIndex        =   5
      Top             =   2520
      Width           =   495
   End
   Begin VB.CommandButton Command4 
      Caption         =   "4"
      Height          =   375
      Left            =   2160
      TabIndex        =   4
      Top             =   2520
      Width           =   495
   End
   Begin VB.CommandButton Command3 
      Caption         =   "3"
      Height          =   375
      Left            =   3360
      TabIndex        =   3
      Top             =   3000
      Width           =   495
   End
   Begin VB.CommandButton Command2 
      Caption         =   "2"
      Height          =   375
      Left            =   2760
      TabIndex        =   2
      Top             =   3000
      Width           =   495
   End
   Begin VB.CommandButton Command1 
      Caption         =   "1"
      Height          =   375
      Left            =   2160
      TabIndex        =   1
      Top             =   3000
      Width           =   495
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   1080
      TabIndex        =   0
      Top             =   480
      Width           =   3975
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim operand As Integer
Dim numPressed, commaPressed As Boolean
Dim num1 As Double

Private Sub Command1_Click()
    Text1.Text = Text1.Text & "1"
End Sub

Private Sub Command10_Click()
    If (numPressed = False) Then
        Text1.Text = ""
    End If
    numPressed = True
    Text1.Text = Text1.Text & "0"
End Sub

Private Sub Command11_Click()
    Text1.Text = ""
    numPressed = False
    commaPressed = False
End Sub

Private Sub Command12_Click()
    Text1.Text = Replace(Text1.Text, ",", ".")
    Select Case operand
        Case 0: num1 = Val(Text1.Text)
        Case 1: num1 = num1 + Val(Text1.Text)
        Case 2: num1 = num1 - Val(Text1.Text)
        Case 3: num1 = num1 * Val(Text1.Text)
        Case 4: num1 = num1 / Val(Text1.Text)
    End Select
    operand = 1
    Text1.Text = num1
    numPressed = False
    commaPressed = False
End Sub

Private Sub Command13_Click()
    Text1.Text = Replace(Text1.Text, ",", ".")
    Select Case operand
        Case 0: num1 = Val(Text1.Text)
        Case 1: num1 = num1 + Val(Text1.Text)
        Case 2: num1 = num1 - Val(Text1.Text)
        Case 3: num1 = num1 * Val(Text1.Text)
        Case 4: num1 = num1 / Val(Text1.Text)
    End Select
    operand = 0
    Text1.Text = num1
    numPressed = False
    commaPressed = False
End Sub

Private Sub Command14_Click()
    Text1.Text = Replace(Text1.Text, ",", ".")
    Select Case operand
        Case 0: num1 = Val(Text1.Text)
        Case 1: num1 = num1 + Val(Text1.Text)
        Case 2: num1 = num1 - Val(Text1.Text)
        Case 3: num1 = num1 * Val(Text1.Text)
        Case 4: num1 = num1 / Val(Text1.Text)
    End Select
    operand = 2
    Text1.Text = num1
    numPressed = False
    commaPressed = False
End Sub

Private Sub Command15_Click()
    Text1.Text = Replace(Text1.Text, ",", ".")
    Select Case operand
        Case 0: num1 = Val(Text1.Text)
        Case 1: num1 = num1 + Val(Text1.Text)
        Case 2: num1 = num1 - Val(Text1.Text)
        Case 3: num1 = num1 * Val(Text1.Text)
        Case 4: num1 = num1 / Val(Text1.Text)
    End Select
    operand = 3
    Text1.Text = num1
    numPressed = False
    commaPressed = False
End Sub

Private Sub Command16_Click()
    Text1.Text = Replace(Text1.Text, ",", ".")
    Select Case operand
        Case 0: num1 = Val(Text1.Text)
        Case 1: num1 = num1 + Val(Text1.Text)
        Case 2: num1 = num1 - Val(Text1.Text)
        Case 3: num1 = num1 * Val(Text1.Text)
        Case 4: num1 = num1 / Val(Text1.Text)
    End Select
    operand = 4
    Text1.Text = num1
    numPressed = False
    commaPressed = False
End Sub

Private Sub Command17_Click()
    Text1.Text = Replace(Text1.Text, ",", ".")
    num = Val(Text1.Text)
    If (num < 0) Then
        Text1.Text = "Error"
        numPressed = False
    End If
    num = Sqr(num)
    Text1.Text = num
    numPressed = False
    commaPressed = False
End Sub

Private Sub Command18_Click()
    If (commaPressed = False) Then
        Text1.Text = Text1.Text & "."
        commaPressed = True
    End If
End Sub

Private Sub Command19_Click()
    Text1.Text = Replace(Text1.Text, ",", ".")
    num = Val(Text1.Text)
    num = 1 / num
    Text1.Text = num
    numPressed = False
    commaPressed = False
End Sub

Private Sub Command2_Click()
    If (numPressed = False) Then
        Text1.Text = ""
    End If
    numPressed = True
    Text1.Text = Text1.Text & "2"
End Sub

Private Sub Command20_Click()
    Text1.Text = Replace(Text1.Text, ",", ".")
    num = Val(Text1.Text)
    num = -1 * num
    Text1.Text = num
    numPressed = False
    commaPressed = False
End Sub

Private Sub Command3_Click()
    If (numPressed = False) Then
        Text1.Text = ""
    End If
    numPressed = True
    Text1.Text = Text1.Text & "3"
End Sub

Private Sub Command4_Click()
    If (numPressed = False) Then
        Text1.Text = ""
    End If
    numPressed = True
    Text1.Text = Text1.Text & "4"
End Sub

Private Sub Command5_Click()
    If (numPressed = False) Then
        Text1.Text = ""
    End If
    numPressed = True
    Text1.Text = Text1.Text & "5"
End Sub

Private Sub Command6_Click()
    If (numPressed = False) Then
        Text1.Text = ""
    End If
    numPressed = True
    Text1.Text = Text1.Text & "6"
End Sub

Private Sub Command7_Click()
    If (numPressed = False) Then
        Text1.Text = ""
    End If
    numPressed = True
    Text1.Text = Text1.Text & "7"
End Sub

Private Sub Command8_Click()
    If (numPressed = False) Then
        Text1.Text = ""
    End If
    numPressed = True
    Text1.Text = Text1.Text & "8"
End Sub

Private Sub Command9_Click()
    If (numPressed = False) Then
        Text1.Text = ""
    End If
    numPressed = True
    Text1.Text = Text1.Text & "9"
End Sub

Private Sub Form_Load()
    op1 = 0
    op2 = 0
End Sub
